FROM openjdk:8-jre-alpine AS NEWRELIC
RUN apk add wget && apk add unzip && wget --no-check-certificate https://download.newrelic.com/newrelic/java-agent/newrelic-agent/current/newrelic-java.zip
RUN unzip newrelic-java.zip
RUN rm newrelic-java.zip
COPY newrelic.yml newrelic/

FROM openjdk:8-jdk
WORKDIR /opt

RUN mkdir -p /var/log/restaurant-one-portal
RUN mkdir -p /opt/restaurant-one-portal
COPY --from=newrelic /newrelic /opt/newrelic
ENV JAVA_ARGS ""
ENV TZ Asia/Kolkata
ENV APP_PORT 8088
EXPOSE $APP_PORT

COPY build/libs/restaurant-one-view-1.0-SNAPSHOT.jar /opt/restaurant-one-portal/rop.jar
CMD java $JAVA_ARGS -jar /opt/restaurant-one-portal/rop.jar