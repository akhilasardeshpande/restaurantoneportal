package com.swiggy.restaurantone.validationTest;

import com.swiggy.restaurantone.data.businessEntity.Role;
import com.swiggy.restaurantone.data.coreEntity.PermissionEntity;
import com.swiggy.restaurantone.data.coreEntity.RoleEntity;
import com.swiggy.restaurantone.mapper.PermissionMapper;
import com.swiggy.restaurantone.service.RoleService;
import com.swiggy.restaurantone.validation.RoleValidation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Created by ganesh on 10/2/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ValidationHelperTest {
    @InjectMocks
    private RoleValidation validationHelper;
    @Mock
    private RoleService roleService;
    private List<RoleEntity> entities;
    private List<PermissionEntity> permissions;

    @Mock
    private PermissionMapper permissionMapper;
    private Role role;
    @Before
    public void setUp(){
        entities = new ArrayList<>();
        permissions = new ArrayList<>();
        permissions.add(new PermissionEntity());
        role = Role.builder().level(100000L).permissions(permissionMapper.entityToBusinessObject(permissions)).name("SM").build();
        role.setId(1l);
        List<Role> roles = new ArrayList<>();
        roles.add(role);
        when(roleService.findAll()).thenReturn(roles);
    }

    @Test
    public void validRoleTest(){
        when(roleService.findRoleName(role.getName())).thenReturn(role);
        assertTrue("test failed giving valid role", validationHelper.isValidRole(role).isValid());
    }

    @Test
    public void invalidRoleTest(){
        when(roleService.findRoleName(role.getName())).thenReturn(null);
        assertTrue("test failed on giving invalid role", !validationHelper.isValidRole(role).isValid());
    }

}
