package com.swiggy.restaurantone.validationTest;

import com.swiggy.restaurantone.data.businessEntity.*;
import com.swiggy.restaurantone.data.coreEntity.PermissionEntity;
import com.swiggy.restaurantone.data.coreEntity.UserEntity;
import com.swiggy.restaurantone.service.CityService;
import com.swiggy.restaurantone.service.TeamService;
import com.swiggy.restaurantone.service.UserService;
import com.swiggy.restaurantone.validation.RoleValidation;
import com.swiggy.restaurantone.validation.UserValidation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

/**
 * Created by ganesh on 10/2/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserValidationTest {
    @InjectMocks
    private UserValidation userValidation;
    @Mock
    private RoleValidation validationHelper;
    @Mock
    private UserService userService;

    @Mock
    private CityService cityService;

    @Mock
    private TeamService teamService;

    private User user;

    private List<Permission> permissions;
    private List<PermissionEntity> permissionEntities;
    private UserEntity userEntity;
    private Role role;

    @Before
    public void setUp(){
        userValidation.setContactEditEnabled(true);
        userValidation.setRolesEditEnabled(false);
        permissions = new ArrayList<>();
        permissions.add(new Permission());
        permissionEntities = new ArrayList<>();
        PermissionEntity permission = new PermissionEntity();
        permission.setPermission("Test");
        permissionEntities.add(permission);
        role = Role.builder().level(100000L).permissions(null).name("SM").build();
        userEntity = new UserEntity("test","9686676251","test@swiggy.in",1l,"{}", permissionEntities, 1L, new Date(), new Date(), 1l);
        user= new User("ganesh",1l,"8798989898","ganesh.d@swiggy.in",role,"", permissions, null, null, new Manager(1l, "test"), new City(1l, "test"), new Team(1l, "test"), "");
        mockedMethods();
    }

    private void mockedMethods() {
        when(validationHelper.isValidRole(role)).thenReturn(new ValidationResult("", true));
        when(userService.find(anyLong())).thenReturn(user);
        when(cityService.findCity(anyLong())).thenReturn(new City(1l, "test"));
        when(teamService.find(anyLong())).thenReturn(new Team(1l, "test"));
    }

    @Test
    public void validUserEmailTest(){
        assertTrue("user validation failed when user email ends with swiggy.in", userValidation.validate(user, false).isEmpty());
    }

    @Test
    public void inValidUserEmailTest(){
        user= new User("ganesh",1l,"8798989898","ganesh.d@gmail.com", role,"", permissions, null, null, new Manager(1l, "test"), new City(1l, "test city"), new Team(1l, "test team"),"");
        assertTrue("user validation failed when user email ends does not have swiggy.in", !userValidation.validate(user, false).isEmpty());
    }
}
