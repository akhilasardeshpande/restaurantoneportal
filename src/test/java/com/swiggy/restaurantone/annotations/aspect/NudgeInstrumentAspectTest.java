package com.swiggy.restaurantone.annotations.aspect;

import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.restaurantEvent.instrumentation.InstrumentService;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.NudgeDetailsEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NudgeInstrumentAspectTest {

    @Mock
    private InstrumentService instrumentService;

    @Mock
    private AuthorizationHelper authorizationHelper;

    @InjectMocks
    private NudgeInstrumentAspect aspect;

    @Test
    public void instrumentTest() {
        String userEmail = "test@gmail.com";
        doNothing().when(instrumentService).doInstrumentNudgeEvent(any(NudgeDetailsEvent.class));
        when(authorizationHelper.fetchUser()).thenReturn(User.builder().email(userEmail).build());
        aspect.instrument(null, getTestResult());
        verify(authorizationHelper, times(1)).fetchUser();
        verify(instrumentService, times(1)).doInstrumentNudgeEvent(any(NudgeDetailsEvent.class));
    }

    @Test
    public void instrumentTestFail() {
        when(authorizationHelper.fetchUser()).thenThrow(new RuntimeException("User Not Found"));
        aspect.instrument(null, getTestResult());
        verify(authorizationHelper, times(1)).fetchUser();
    }


    private Object getTestResult() {
        return NudgeDetailsEvent.builder().id("ddedbf5f-2a89-49b9-b641-c5327e548da6")
                .name("test").content(new HashMap<>()).build();
    }
}
