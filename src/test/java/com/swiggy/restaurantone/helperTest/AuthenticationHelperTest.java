package com.swiggy.restaurantone.helperTest;
import com.swiggy.restaurantone.data.ApplicationEntity.LoginUserInfo;
import com.swiggy.restaurantone.data.ApplicationEntity.UserAuthInfo;
import com.swiggy.restaurantone.data.businessEntity.Role;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.AuthenticationHelper;
import com.swiggy.restaurantone.service.GoogleAuthenticationService;
import com.swiggy.restaurantone.service.SessionService;
import com.swiggy.restaurantone.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
/**
 * Created by ganesh on 21/2/17.
 */
@RunWith(MockitoJUnitRunner.class)

public class AuthenticationHelperTest {
    @InjectMocks
    private AuthenticationHelper authenticationHelper;
    @Mock
    private GoogleAuthenticationService googleAuthenticationService;

    @Mock
    private SessionService sessionService;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Mock
    private UserService userService;
    private LoginUserInfo loginUserInfo;
    private List<User> users;
    private UserAuthInfo userAuthInfo;

    @Before
    public void setUp() throws Exception {
        loginUserInfo = new LoginUserInfo("Google","{\\\"code\\\" : \\\"4/JQPbjwMiqzN6muLzL-f40IH0_HB79PP0MLEjZvAkrL8\\\"}");
        userAuthInfo = new UserAuthInfo("ganesh.dareshwar@swiggy.in","{\\\\\\\\\\\\\\\"refreshToken\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"1/CuBvB522HTaGBACXoTX5hDD4le0S-xqTJfD-6Y6ZG_DFCyOR6SIURRuHuRm4mi25\\\\\\\\\\\\\\\",\\\\\\\\\\\\\\\"accessToken\\\\\\\\\\\\\\\":\\\\\\\\\\\\\\\"ya29.Glv1A82VZR8gGYUtohepHd5SJ0vR9rCAfqCLmbkEGFZtPTJMHuzxKOe-crj95qsi_B8XLet-xgbWpwR14Xl7Mmt6ZgLkRfOAX_5i_ZNvnRBtrr9zZJHE6vyppreq\\\\\\\\\\\\\\\"}");
        users= new ArrayList<>();
        Role role = Role.builder().level(100000L).permissions(null).name("SM").build();
        users.add(User.builder().name("auth test").id(1l).email("ganesh.dareshwar@swiggy.in").contactNumber("9876543212").role(role).build());
        mock_methods();
        authenticationHelper.setExpiryTime(150000l);
    }

    private void mock_methods() throws IOException {
        when(googleAuthenticationService.authenticate(anyObject())).thenReturn(userAuthInfo);
        when(userService.getUsersByCriteria(anyObject())).thenReturn(users);

    }

    @Test
    public void loginTest() throws IOException {
        User user = authenticationHelper.login(loginUserInfo);
        assertTrue("Login failed",user == users.get(0));
        verify(sessionService).createAndSaveSession(anyLong(),anyObject(),anyString(),anyLong(),anyString());
        verify(httpServletResponse,times(2)).addHeader(anyString(),anyString());
    }
}
