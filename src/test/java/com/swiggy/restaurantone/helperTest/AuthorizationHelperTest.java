package com.swiggy.restaurantone.helperTest;

import com.swiggy.restaurantone.data.businessEntity.RestaurantUserMap;
import com.swiggy.restaurantone.data.businessEntity.UserRelationship;
import com.swiggy.restaurantone.data.coreEntity.Session;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.RestaurantUserMapService;
import com.swiggy.restaurantone.service.SessionService;
import com.swiggy.restaurantone.service.UserRelationshipService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;
/**
 * Created by ganesh on 21/2/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthorizationHelperTest {
    @InjectMocks
    private AuthorizationHelper authorizationHelper;
    @Mock
    private HttpServletRequest httpServletRequest;
    @Mock
    private RestaurantUserMapService restMap;
    @Mock
    private UserRelationshipService userMap;
    @Mock
    private SessionService sessionService;
    private List<UserRelationship> userRelationships;
    private List<RestaurantUserMap> restaurantUserMaps;
    private Session session;

    @Before
    public void setUp(){
        userRelationships = new ArrayList<>();
        userRelationships.add(new UserRelationship(1l,2l, ""));
        restaurantUserMaps = new ArrayList<>();
        restaurantUserMaps.add(new RestaurantUserMap(111l,211l,1l, ""));
        session = Session.builder().userId(1l).sessionId(UUID.fromString("64c4243b-7334-4509-a84a-c659e28953f9")).tokenExpirationTime(System.currentTimeMillis()+150000).build();
        method_mock();
    }

    private void method_mock() {
        when(httpServletRequest.getHeader("userId")).thenReturn(String.valueOf(1l));
        when(userMap.GetAllChildUsersByUserId(anyLong())).thenReturn(userRelationships);
        when(restMap.getRestaurantsByParentUserId(anyLong())).thenReturn(restaurantUserMaps.stream().map(restaurantUserMap -> restaurantUserMap.getRestaurantId()).collect(Collectors.toSet()));
        when(httpServletRequest.getHeader("sessionId")).thenReturn("64c4243b-7334-4509-a84a-c659e28953f9");
        when(sessionService.getSession(anyLong(),anyObject())).thenReturn(session);
    }

    @Test
    public void checkAuthorizationByRestaurantNullTest(){
        Boolean result = authorizationHelper.checkAuthorization(Arrays.asList(2l),null,false);
        assertTrue("checkAuthorizationTest with restaurant id null is failed",result);

    }

    @Test
    public void checkAuthorizationTest(){
        Boolean res = authorizationHelper.checkAuthorization(Arrays.asList(2l),Arrays.asList(211l),true);
        assertTrue("checkAuthorizationTest is failed",res);
    }

    @Test
    public void checkAuthorizationByChildIdsBySelf(){
        assertTrue("checkAuthorizationByChildIds by self  failed",authorizationHelper.checkAuthorization(2l,null,true));
    }

    @Test
    public void checkAuthorizationByChildIdsWithoutSelf(){
        assertTrue("checkAuthorizationByChildIds by self = false is failed",authorizationHelper.checkAuthorization(2l,null,false));
    }

    @Test
    public void authenticateUserSessionTest(){
        when(httpServletRequest.getMethod()).thenReturn("GET");
        assertTrue("Failed to authenticate User session",authorizationHelper.authenticateUserSession());
    }
}
