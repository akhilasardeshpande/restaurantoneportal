package com.swiggy.restaurantone.headlesscms.controller;

import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.headlesscms.HeadlessCMSController;
import com.swiggy.restaurantone.headlesscms.HeadlessCMSResponse;
import com.swiggy.restaurantone.headlesscms.HeadlessCMSServiceImpl;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HeadlessCMSControllerTest {

    private JSONObject request;

    @Mock
    private AuthorizationHelper authorizationHelper;

    @Mock
    private HeadlessCMSServiceImpl headlessCMSService;

    @InjectMocks
    private HeadlessCMSController headlessCMSController;

    private JSONObject getDummyRequestBody() {
        JSONObject request = new JSONObject();
        request.put("It", "All");
        request.put("Started", "With");
        request.put("Big", "Bang");
        return request;
    }

    @Before
    public void setUp() {
        request = getDummyRequestBody();
    }

    @Test
    public void createSchemaWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.createSchema(request);
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void createSchemaWhenSuccessTest(){
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.createSchema(request)).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.createSchema(request);
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void updateSchemaWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.updateSchema(request);
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void updateSchemaWhenSuccessTest(){
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.updateSchema(request)).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.updateSchema(request);
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getAllSchemasWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.getAllSchemas();
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void getAllSchemasWhenSuccessTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.getAllSchemas()).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.getAllSchemas();
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getSchemaByIdWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.getSchemaById("123");
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void getSchemaByIdWhenSuccessTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.getSchemaById("123")).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.getSchemaById("123");
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void createContentWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.createContent(request);
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void createContentWhenSuccessTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.createContent(request)).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.createContent(request);
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getContentWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.getContentById("123");
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void getContentWhenSuccessTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.getContentById("123")).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.getContentById("123");
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void publishContentWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.publishContent(request);
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void publishContentWhenSuccessTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.publishContent(request)).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.publishContent(request);
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void archiveContentWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.archiveContent(request);
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void archiveContentWhenSuccessTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.archiveContent(request)).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.archiveContent(request);
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void updateContentWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.updateContent(request);
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void updateContentWhenSuccessTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.updateContent(request)).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.updateContent(request);
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void createCollectionWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.createCollection(request);
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void createCollectionWhenSuccessTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.createCollection(request)).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.createCollection(request);
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void updateCollectionWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.updateCollection(request);
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void updateCollectionWhenSuccessTest()  {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.updateCollection(request)).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.updateCollection(request);
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void reorderCollectionWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.reorderCollection(request);
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void reorderCollectionWhenSuccessTest()  {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.reorderCollection(request)).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.reorderCollection(request);
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getAllCollectionsWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.getAllCollections();
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void getAllCollectionsWhenSuccessTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.getAllCollections()).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.getAllCollections();
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getCollectionByIdWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.getCollectionById("123");
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void getCollectionByIdWhenSuccessTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.getCollectionById("123")).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.getCollectionById("123");
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void createSpaceWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.createSpace(request);
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void createSpaceWhenSuccessTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.createSpace(request)).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.createSpace(request);
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getAllSpacesWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.getAllSpaces();
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void getAllSpacesWhenSuccessTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.getAllSpaces()).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.getAllSpaces();
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getAllSpacesBySpaceIdWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.getAllSpacesBySpaceId("123");
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void getAllSpacesBySpaceIdWhenSuccessTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.getAllSpacesBySpaceId("123")).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.getAllSpacesBySpaceId("123");
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getSpaceDetailsWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.getSpaceDetails("123");
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void getSpaceDetailsWhenSuccessTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.getSpaceDetails("123")).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.getSpaceDetails("123");
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void updateSpaceWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.updateSpace(request);
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void updateSpaceWhenSuccessTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.updateSpace(request)).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.updateSpace(request);
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void reorderSpaceWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.reorderSpace(request);
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void reorderSpaceWhenSuccessTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.reorderSpace(request)).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.reorderSpace(request);
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void deleteSpaceWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res =  headlessCMSController.deleteSpace(request);
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void deleteSpaceWhenSuccessTest()  {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);

        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", null, null);
        when(headlessCMSService.deleteSpace(request)).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse =  headlessCMSController.deleteSpace(request);
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getAllSegmentWhenUserIsNotAuthorizedTest() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);

        HeadlessCMSResponse res = headlessCMSController.getAllSegments();
        assertEquals(res.getMessage(), "Unauthorized");
    }

    @Test
    public void getAllSegmentWhenSuccessTest() {
        User user = User.builder().email("test@swiggy.in").build();
        JSONObject request = new JSONObject();
        request.put("user_email", "test@swiggy.in");
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);
        when(authorizationHelper.fetchUser()).thenReturn(user);

        String[] segments = new String[]{"default", "seg-001"};
        HeadlessCMSResponse expectedResponse = new HeadlessCMSResponse(1, "success", segments, null);
        when(headlessCMSService.getAllSegments(request)).thenReturn(expectedResponse);
        HeadlessCMSResponse actualResponse = headlessCMSController.getAllSegments();
        assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
        assertEquals(expectedResponse.getData(), actualResponse.getData());
    }
}