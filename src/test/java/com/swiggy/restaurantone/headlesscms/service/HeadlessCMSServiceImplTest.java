package com.swiggy.restaurantone.headlesscms.service;

import com.swiggy.restaurantone.data.businessEntity.City;
import com.swiggy.restaurantone.data.businessEntity.Manager;
import com.swiggy.restaurantone.data.businessEntity.Team;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.headlesscms.HeadlessCMSResponse;
import com.swiggy.restaurantone.headlesscms.HeadlessCMSServiceClient;
import com.swiggy.restaurantone.headlesscms.HeadlessCMSServiceException;
import com.swiggy.restaurantone.headlesscms.HeadlessCMSServiceImpl;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.RoleService;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.function.Supplier;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class HeadlessCMSServiceImplTest {

    private User user;

    private JSONObject request;

    @Mock
    private HeadlessCMSServiceClient headlessCMSServiceClient;

    @Mock
    private AuthorizationHelper authorizationHelper;

    @Mock
    private Call<HeadlessCMSResponse> headlessCMSResponseCall;

    @Mock
    private RoleService roleService;

    @Mock
    Supplier<Call<HeadlessCMSResponse>> supplier;

    @InjectMocks
    private HeadlessCMSServiceImpl headlessCMSService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private JSONObject getDummyRequestBody() {
        JSONObject request = new JSONObject();
        request.put("It", "All");
        request.put("Started", "With");
        request.put("Big", "Bang");
        return request;
    }

    private HeadlessCMSResponse getHeadlessCMSResponse() {
        HeadlessCMSResponse hcmsr = new HeadlessCMSResponse();
        hcmsr.setCode(1);
        hcmsr.setMessage("success");
        hcmsr.setData(new JSONObject());
        hcmsr.setError(null);
        return hcmsr;
    }

    @Before
    public void setUp() {
        request = getDummyRequestBody();
        user = User.builder()
                .name("abc")
                .email("pqr@xyz.in")
                .contactNumber("123456")
                .role(roleService.findRoleById(7l))
                .manager(Manager.builder().id(868l).name("ASM Manager").build())
                .city(City.builder().id(1l).name("Bangalore").build())
                .team(Team.builder().id(1l).name("city team").build())
                .updatedBy("updatedby")
                .build();
        when(authorizationHelper.fetchUser()).thenReturn(user);
    }

    @Test
    public void createSchemaExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.createSchema(request)).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.createSchema(request);
        verify(headlessCMSServiceClient).createSchema(request);
    }

    @Test
    public void createSchemaValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.createSchema(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.createSchema(request);
        verify(headlessCMSServiceClient).createSchema(request);
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void createSchemaFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.createSchema(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.createSchema(request);
        verify(headlessCMSServiceClient).createSchema(request);
    }

    @Test
    public void updateSchemaExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.updateSchema(request)).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.updateSchema(request);
        verify(headlessCMSServiceClient).updateSchema(request);
    }

    @Test
    public void updateSchemaValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.updateSchema(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.updateSchema(request);
        verify(headlessCMSServiceClient).updateSchema(request);
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void updateSchemaFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.updateSchema(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.updateSchema(request);
        verify(headlessCMSServiceClient).updateSchema(request);
    }

    @Test
    public void getAllSchemasExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getAllSchemas()).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.getAllSchemas();
        verify(headlessCMSServiceClient).getAllSchemas();
    }

    @Test
    public void getAllSchemasValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.getAllSchemas()).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.getAllSchemas();
        verify(headlessCMSServiceClient).getAllSchemas();
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getAllSchemasFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getAllSchemas()).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.getAllSchemas();
        verify(headlessCMSServiceClient).getAllSchemas();
    }

    @Test
    public void getSchemaByIdExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getSchemaById("!23")).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.getSchemaById("123");
        verify(headlessCMSServiceClient).getSchemaById("123");

    }

    @Test
    public void getSchemaByIdValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.getSchemaById("123")).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.getSchemaById("123");
        verify(headlessCMSServiceClient).getSchemaById("123");
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getSchemaByIdFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getSchemaById("123")).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.getSchemaById("123");
        verify(headlessCMSServiceClient).getSchemaById("123");
    }

    @Test
    public void createContentExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.createContent(request)).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.createContent(request);
        verify(headlessCMSServiceClient).createContent(request);

    }

    @Test
    public void createContentValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.createContent(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.createContent(request);
        verify(headlessCMSServiceClient).createContent(request);
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void createContentFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.createContent(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.createContent(request);
        verify(headlessCMSServiceClient).createContent(request);
    }

    @Test
    public void getContentByIdExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getContentById("123")).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.getContentById("123");
        verify(headlessCMSServiceClient).getContentById("123");
    }

    @Test
    public void getContentByIdValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.getContentById("123")).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.getContentById("123");
        verify(headlessCMSServiceClient).getContentById("123");
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getContentByIdFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getContentById("123")).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.getContentById("123");
        verify(headlessCMSServiceClient).getContentById("123");
    }

    @Test
    public void publishContentByIdExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.publishContent(request)).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.publishContent(request);
        verify(headlessCMSServiceClient).publishContent(request);
    }

    @Test
    public void publishContentValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.publishContent(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.publishContent(request);
        verify(headlessCMSServiceClient).publishContent(request);
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void publishContentFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.publishContent(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.publishContent(request);
        verify(headlessCMSServiceClient).publishContent(request);
    }

    @Test
    public void archiveContentByIdExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.archiveContent(request)).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.archiveContent(request);
        verify(headlessCMSServiceClient).archiveContent(request);
    }

    @Test
    public void archiveContentValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.archiveContent(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.archiveContent(request);
        verify(headlessCMSServiceClient).archiveContent(request);
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void archiveContentFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.archiveContent(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.archiveContent(request);
        verify(headlessCMSServiceClient).archiveContent(request);
    }

    @Test
    public void updateContentByIdExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.updateContent(request)).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.updateContent(request);
        verify(headlessCMSServiceClient).updateContent(request);
    }

    @Test
    public void updateContentValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.updateContent(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.updateContent(request);
        verify(headlessCMSServiceClient).updateContent(request);
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void updateContentFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.updateContent(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.updateContent(request);
        verify(headlessCMSServiceClient).updateContent(request);
    }

    @Test
    public void createCollectionByIdExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.createCollection(request)).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.createCollection(request);
        verify(headlessCMSServiceClient).createCollection(request);
    }

    @Test
    public void createCollectionValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.createCollection(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.createCollection(request);
        verify(headlessCMSServiceClient).createCollection(request);
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void createCollectionFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.createCollection(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.createCollection(request);
        verify(headlessCMSServiceClient).createCollection(request);
    }

    @Test
    public void updateCollectionByIdExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.updateCollection(request)).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.updateCollection(request);
        verify(headlessCMSServiceClient).updateCollection(request);
    }

    @Test
    public void updateCollectionValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.updateCollection(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.updateCollection(request);
        verify(headlessCMSServiceClient).updateCollection(request);
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void updateCollectionFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.updateCollection(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.updateCollection(request);
        verify(headlessCMSServiceClient).updateCollection(request);
    }

    @Test
    public void reorderCollectionByIdExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.reorderCollection(request)).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.reorderCollection(request);
        verify(headlessCMSServiceClient).reorderCollection(request);
    }

    @Test
    public void reorderCollectionValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.reorderCollection(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.reorderCollection(request);
        verify(headlessCMSServiceClient).reorderCollection(request);
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void reorderCollectionFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.reorderCollection(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.reorderCollection(request);
        verify(headlessCMSServiceClient).reorderCollection(request);
    }

    @Test
    public void getAllCollectionsByIdExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getAllCollections()).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.getAllCollections();
        verify(headlessCMSServiceClient).getAllCollections();
    }

    @Test
    public void getAllCollectionsValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.getAllCollections()).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.getAllCollections();
        verify(headlessCMSServiceClient).getAllCollections();
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getAllCollectionsFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getAllCollections()).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.getAllCollections();
        verify(headlessCMSServiceClient).getAllCollections();
    }

    @Test
    public void getCollectionByIdExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getCollectionById("123")).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.getCollectionById("123");
        verify(headlessCMSServiceClient).getCollectionById("123");
    }

    @Test
    public void getCollectionByIdValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.getCollectionById("123")).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.getCollectionById("123");
        verify(headlessCMSServiceClient).getCollectionById("123");
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getCollectionByIdFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getCollectionById("123")).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.getCollectionById("123");
        verify(headlessCMSServiceClient).getCollectionById("123");
    }

    @Test
    public void createSpaceExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.createSpace(request)).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.createSpace(request);
        verify(headlessCMSServiceClient).createSpace(request);
    }

    @Test
    public void createSpaceValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.createSpace(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.createSpace(request);
        verify(headlessCMSServiceClient).createSpace(request);
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void createSpaceFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.createSpace(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.createSpace(request);
        verify(headlessCMSServiceClient).createSpace(request);
    }

    @Test
    public void getAllSpacesExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getAllSpaces()).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.getAllSpaces();
        verify(headlessCMSServiceClient).getAllSpaces();
    }

    @Test
    public void getAllSpacesValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.getAllSpaces()).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.getAllSpaces();
        verify(headlessCMSServiceClient).getAllSpaces();
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getAllSpacesFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getAllSpaces()).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.getAllSpaces();
        verify(headlessCMSServiceClient).getAllSpaces();
    }

    @Test
    public void getAllSpacesBySpaceIdExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getAllSpacesBySpaceId("123")).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.getAllSpacesBySpaceId("123");
        verify(headlessCMSServiceClient).getAllSpacesBySpaceId("123");
    }

    @Test
    public void getAllSpacesBySpaceIdValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.getAllSpacesBySpaceId("123")).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.getAllSpacesBySpaceId("123");
        verify(headlessCMSServiceClient).getAllSpacesBySpaceId("123");
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getAllSpacesBySpaceIdFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getAllSpacesBySpaceId("123")).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.getAllSpacesBySpaceId("123");
        verify(headlessCMSServiceClient).getAllSpacesBySpaceId("123");
    }

    @Test
    public void getSpaceDetailsExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getSpaceDetails("123")).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.getSpaceDetails("123");
        verify(headlessCMSServiceClient).getSpaceDetails("123");
    }

    @Test
    public void getSpaceDetailsValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.getSpaceDetails("123")).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.getSpaceDetails("123");
        verify(headlessCMSServiceClient).getSpaceDetails("123");
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void getSpaceDetailsFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getSpaceDetails("123")).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.getSpaceDetails("123");
        verify(headlessCMSServiceClient).getSpaceDetails("123");
    }

    @Test
    public void updateSpaceExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.updateSpace(request)).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.updateSpace(request);
        verify(headlessCMSServiceClient).updateSpace(request);
    }

    @Test
    public void updateSpaceValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.updateSpace(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.updateSpace(request);
        verify(headlessCMSServiceClient).updateSpace(request);
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void updateSpaceFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.updateSpace(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.updateSpace(request);
        verify(headlessCMSServiceClient).updateSpace(request);
    }

    @Test
    public void reorderSpaceExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.reorderSpace(request)).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.reorderSpace(request);
        verify(headlessCMSServiceClient).reorderSpace(request);
    }

    @Test
    public void reorderSpaceValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.reorderSpace(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.reorderSpace(request);
        verify(headlessCMSServiceClient).reorderSpace(request);
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void reorderSpaceFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.reorderSpace(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.reorderSpace(request);
        verify(headlessCMSServiceClient).reorderSpace(request);
    }

    @Test
    public void deleteSpaceExceptionTest() {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.deleteSpace(request)).thenThrow(HeadlessCMSServiceException.class);
        headlessCMSService.deleteSpace(request);
        verify(headlessCMSServiceClient).deleteSpace(request);
    }

    @Test
    public void deleteSpaceValidResponseTest() throws IOException {
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.deleteSpace(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));
        HeadlessCMSResponse actualResponse = headlessCMSService.deleteSpace(request);
        verify(headlessCMSServiceClient).deleteSpace(request);
        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
    }

    @Test
    public void deleteSpaceFailedResponseTest() throws IOException {
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.deleteSpace(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);
        headlessCMSService.deleteSpace(request);
        verify(headlessCMSServiceClient).deleteSpace(request);
    }

    @Test
    public void getAllSegmentsExceptionTest() {
        JSONObject request = new JSONObject();
        request.put("userEmail", "test@swiggy.in");
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getAllSegments(request)).thenThrow(HeadlessCMSServiceException.class);

        headlessCMSService.getAllSegments(request);
        verify(headlessCMSServiceClient).getAllSegments(request);
    }

    @Test
    public void getAllSegmentsValidResponseTest() throws IOException {
        JSONObject request = new JSONObject();
        request.put("userEmail", "test@swiggy.in");
        HeadlessCMSResponse expectedResponse = getHeadlessCMSResponse();
        when(headlessCMSServiceClient.getAllSegments(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(Response.success(expectedResponse));

        HeadlessCMSResponse actualResponse = headlessCMSService.getAllSegments(request);
        verify(headlessCMSServiceClient).getAllSegments(request);

        Assert.assertEquals(expectedResponse.getMessage(), actualResponse.getMessage());
        Assert.assertEquals(expectedResponse.getData(), actualResponse.getData());
    }

    @Test
    public void getAllSegmentsFailedResponseTest() throws IOException {
        JSONObject request = new JSONObject();
        request.put("userEmail", "test@swiggy.in");
        expectedException.expect(HeadlessCMSServiceException.class);
        when(headlessCMSServiceClient.getAllSegments(request)).thenReturn(headlessCMSResponseCall);
        when(headlessCMSResponseCall.execute()).thenReturn(null);

        headlessCMSService.getAllSegments(request);
        verify(headlessCMSServiceClient).getAllSegments(request);
    }

}