package com.swiggy.restaurantone.vendorconfig;

import com.swiggy.commons.Json;
import com.swiggy.commons.response.Response;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.vendorconfig.controllers.VendorConfigController;
import com.swiggy.restaurantone.vendorconfig.pojo.request.fetch.FeatureConfigFetchRequest;
import com.swiggy.restaurantone.vendorconfig.pojo.request.fetch.IGCCConfigFetchRequest;
import com.swiggy.restaurantone.vendorconfig.pojo.request.fetch.RestaurantConfigFetchRequest;
import com.swiggy.restaurantone.vendorconfig.pojo.request.update.VendorConfigUpdateRequest;
import com.swiggy.restaurantone.vendorconfig.pojo.response.fetch.VendorConfigFetchGlobalResponse;
import com.swiggy.restaurantone.vendorconfig.pojo.response.fetch.VendorConfigFetchResponseItem;
import com.swiggy.restaurantone.vendorconfig.pojo.response.update.VendorConfigUpdateResponse;
import com.swiggy.restaurantone.vendorconfig.service.VendorConfigServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.HttpStatusCode;
import org.mockserver.model.JsonBody;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;

@RunWith(MockitoJUnitRunner.class)
public class VendorConfigControllerTest {

    @InjectMocks
    private VendorConfigController vendorConfigController;

    @Mock
    private AuthorizationHelper authorizationHelper;

    private ClientAndServer mockServer;

    @Before
    public void setUp() {
        mockServer = startClientAndServer(6789);
        VendorConfigClientConfiguration vendorConfigClientConfiguration =
                new VendorConfigClientConfiguration();
        ReflectionTestUtils.setField(vendorConfigClientConfiguration, "vendorConfigServiceHostName",
                "http://localhost:6789/");
        ReflectionTestUtils.setField(vendorConfigClientConfiguration, "vendorConfigServiceTimeout",
                "10000");
        VendorConfigServiceImpl vendorConfigService = new VendorConfigServiceImpl();
        ReflectionTestUtils.setField(vendorConfigService, "vendorConfigServiceClient",
                vendorConfigClientConfiguration.vendorConfigServiceClient());
        ReflectionTestUtils.setField(vendorConfigController, "vendorConfigService", vendorConfigService);
    }

    private void setUpMock_testFetchRestaurantConfig() {
        HashMap<String, Integer> valueMap = new HashMap<>();
        valueMap.put("00:00-01:00", 5);
        valueMap.put("01:00-02:00", 15);
        VendorConfigFetchResponseItem vendorConfigServiceGetResponse
                = VendorConfigFetchResponseItem.builder()
                .primaryKey("RID")
                .value(valueMap).build();
        String[] rids = { "9990", "9991", "9992" };
        String[] failureRids = { "1001", "9001" };
        for (String rid: rids) {
            vendorConfigServiceGetResponse.setPrimaryKey(rid);
            mockServer.when(
                    HttpRequest.request("/config/rid").withMethod("GET")
                            .withQueryStringParameter("i", rid)
            ).respond(
                    HttpResponse.response()
                            .withStatusCode(HttpStatusCode.OK_200.code())
                            .withBody(JsonBody.json(Json.serialize(vendorConfigServiceGetResponse)))
            );
        }
        VendorConfigFetchResponseItem failureVendorConfigServiceGetResponse
                = VendorConfigFetchResponseItem.builder().message("Could not get Config").build();
        for (String rid: failureRids) {
            mockServer.when(
                    HttpRequest.request("/config/rid").withMethod("GET")
                            .withQueryStringParameter("i", rid)
            ).respond(
                    HttpResponse.response()
                            .withStatusCode(HttpStatusCode.NOT_FOUND_404.code())
                            .withBody(JsonBody.json(Json.serialize(failureVendorConfigServiceGetResponse)))
            );
        }
    }

    private void setUpMock_metaProjectNames() {
        VendorConfigFetchResponseItem vendorConfigServiceGetResponse
                = VendorConfigFetchResponseItem.builder()
                .typeKey("metaprojectfeatures")
                .primaryKey("metaprojectnames")
                .value(new String[]{"fssaifeature","rxdxbarcode"})
                .build();
        mockServer.when(
                HttpRequest.request("/config/metaprojectfeatures").withMethod("GET")
                        .withQueryStringParameter("i", "metaprojectnames")
        ).respond(
                HttpResponse.response()
                        .withStatusCode(HttpStatusCode.OK_200.code())
                        .withBody(JsonBody.json(Json.serialize(vendorConfigServiceGetResponse)))
        );
    }

    private void setUpMock_rxdxProject() {
        VendorConfigFetchResponseItem vendorConfigServiceGetResponse
                = VendorConfigFetchResponseItem.builder()
                .typeKey("metaprojectfeatures")
                .primaryKey("rxdxbarcode")
                .value(new String[]{"IS_BULK_ORDER_CHECK_ENABLED"})
                .build();
        mockServer.when(
                HttpRequest.request("/config/metaprojectfeatures").withMethod("GET")
                        .withQueryStringParameter("i", "rxdxbarcode")
        ).respond(
                HttpResponse.response()
                        .withStatusCode(HttpStatusCode.OK_200.code())
                        .withBody(JsonBody.json(Json.serialize(vendorConfigServiceGetResponse)))
        );
    }

    private void setUpMock_testFetchFeatureConfig() {
        setUpMock_metaProjectNames();
        setUpMock_rxdxProject();
        VendorConfigFetchResponseItem failureVendorConfigServiceGetResponse
                = VendorConfigFetchResponseItem.builder().message("Could not get Config").build();
        mockServer.when(
                HttpRequest.request("/config/fssaifeature").withMethod("GET")
                        .withQueryStringParameter("i", "9991")
        ).respond(
                HttpResponse.response()
                        .withStatusCode(HttpStatusCode.NOT_FOUND_404.code())
                        .withBody(JsonBody.json(Json.serialize(failureVendorConfigServiceGetResponse)))
        );
    }

    private void setUpMock_testFetchIGCCConfig() {
        HashMap<String, String> valueMap = new HashMap<>();
        valueMap.put("PILOT_VERSION", "0.9");
        valueMap.put("WRONG_ITEMS", "enabled");
        valueMap.put("MISSING_ITEMS", "enabled");
        valueMap.put("QUANTITY_ISSUES", "disabled");
        valueMap.put("QUALITY_ISSUES", "disabled");
        VendorConfigFetchResponseItem vendorConfigServiceGetResponse
                = VendorConfigFetchResponseItem.builder()
                .primaryKey("RID")
                .value(valueMap).build();
        String[] rids = { "9990", "9991", "9992" };
        String[] failureRids = { "1001", "9001" };
        for (String rid: rids) {
            vendorConfigServiceGetResponse.setPrimaryKey(rid);
            mockServer.when(
                    HttpRequest.request("/config/igcc").withMethod("GET")
                            .withQueryStringParameter("i", rid)
            ).respond(
                    HttpResponse.response()
                            .withStatusCode(HttpStatusCode.OK_200.code())
                            .withBody(JsonBody.json(Json.serialize(vendorConfigServiceGetResponse)))
            );
        }
        VendorConfigFetchResponseItem failureVendorConfigServiceGetResponse
                = VendorConfigFetchResponseItem.builder().message("Could not get Config").build();
        for (String rid: failureRids) {
            mockServer.when(
                    HttpRequest.request("/config/igcc").withMethod("GET")
                            .withQueryStringParameter("i", rid)
            ).respond(
                    HttpResponse.response()
                            .withStatusCode(HttpStatusCode.NOT_FOUND_404.code())
                            .withBody(JsonBody.json(Json.serialize(failureVendorConfigServiceGetResponse)))
            );
        }
    }

    @Test
    public void testFetchFeatureConfig() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        setUpMock_testFetchFeatureConfig();
        String[] rids = { "9990", "9991" };
        FeatureConfigFetchRequest featureConfigFetchRequest = new FeatureConfigFetchRequest(rids);
        Response<?> response = vendorConfigController.fetchFeatureConfiguration(featureConfigFetchRequest);
        Assert.assertEquals(1, response.getStatusCode());
    }

    @Test
    public void testFetchRestaurantConfig() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        setUpMock_testFetchRestaurantConfig();
        String[] rids = { "9990", "9991", "9992", "9001" };
        RestaurantConfigFetchRequest restaurantConfigRequest = new RestaurantConfigFetchRequest(rids);
        Response<?> response = vendorConfigController.fetchRestaurantConfig(restaurantConfigRequest);
        Assert.assertEquals(1, response.getStatusCode());
    }

    private void setUpMock_testUpdateGlobalConfiguration() {
        VendorConfigUpdateResponse vendorConfigServiceUpdateResponse =
                VendorConfigUpdateResponse.builder().message("success").build();
        mockServer.when(
                HttpRequest.request("/config/global").withMethod("POST")
        ).respond(
                HttpResponse.response()
                        .withStatusCode(HttpStatusCode.OK_200.code())
                        .withBody(JsonBody.json(Json.serialize(vendorConfigServiceUpdateResponse)))
        );
    }

    @Test
    public void testUpdateGlobalConfiguration() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        setUpMock_testUpdateGlobalConfiguration();
        VendorConfigUpdateRequest vendorConfigUpdateRequest = new VendorConfigUpdateRequest("TEST", "GLOBAL", "VALUE");
        Response<?> response = vendorConfigController.updateGlobalConfiguration(vendorConfigUpdateRequest);
        Assert.assertEquals(1, response.getStatusCode());
    }

    private void setupMock_testGetAllGlobalConfiguration() {
        List<VendorConfigFetchResponseItem> list = new ArrayList<>();
        list.add(
                VendorConfigFetchResponseItem.builder().primaryKey("TEST").typeKey("global").build()
        );
        VendorConfigFetchGlobalResponse vendorConfigServiceGetResponse =
                VendorConfigFetchGlobalResponse.builder().itemList(list).itemCount(1).scannedCount(1).lastEvaluatedKey(null).build();
        mockServer.when(
                HttpRequest.request("/config/global/all").withMethod("GET")
        ).respond(
                HttpResponse.response()
                        .withStatusCode(HttpStatusCode.OK_200.code())
                        .withBody(JsonBody.json(Json.serialize(vendorConfigServiceGetResponse)))
        );
    }

    @Test
    public void testGetAllGlobalConfiguration() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        setupMock_testGetAllGlobalConfiguration();
        Response<?> response = vendorConfigController.getAllGlobalConfiguration();
        Assert.assertEquals(1,response.getStatusCode());
    }

    private void setupMock_testDeleteGlobalConfiguration() {
        VendorConfigUpdateResponse vendorConfigServiceUpdateResponse =
                VendorConfigUpdateResponse.builder().message("success").build();
        mockServer.when(
                HttpRequest.request("/config/global").withMethod("DELETE")
        ).respond(
                HttpResponse.response()
                        .withStatusCode(HttpStatusCode.OK_200.code())
                        .withBody(JsonBody.json(Json.serialize(vendorConfigServiceUpdateResponse)))
        );
    }

    @Test
    public void testDeleteGlobalConfiguration() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        setupMock_testDeleteGlobalConfiguration();
        Response<?> response = vendorConfigController.deleteGlobalConfiguration("test");
        Assert.assertEquals(1,response.getStatusCode());
    }

    @Test
    public void testDeleteGlobalConfigurationUnauthorized() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        Response<?> response = vendorConfigController.deleteGlobalConfiguration("test");
        Assert.assertEquals(0,response.getStatusCode());
    }

    @Test
    public void testFetchRestaurantConfigUnauthorized() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        Response<?> response = vendorConfigController.fetchRestaurantConfig(null);
        Assert.assertEquals(0,response.getStatusCode());
    }

    @Test
    public void testUpdateGlobalConfigurationUnauthorized() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        Response<?> response = vendorConfigController.updateGlobalConfiguration(null);
        Assert.assertEquals(0,response.getStatusCode());
    }

    @Test
    public void testGetAllGlobalConfigurationUnauthorized() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        Response<?> response = vendorConfigController.getAllGlobalConfiguration();
        Assert.assertEquals(0,response.getStatusCode());
    }

    @Test
    public void testFetchIGCCConfigUnauthorized() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        Response<?> response = vendorConfigController.fetchIGCCConfiguration(null);
        Assert.assertEquals(0,response.getStatusCode());
    }

    @Test
    public void testFetchIGCCConfig() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        setUpMock_testFetchIGCCConfig();
        String[] rids = { "9990", "9991", "9992", "9001" };
        IGCCConfigFetchRequest igccConfigRequest = new IGCCConfigFetchRequest(rids);
        Response<?> response = vendorConfigController.fetchIGCCConfiguration(igccConfigRequest);
        Assert.assertEquals(1, response.getStatusCode());
    }

    private void setUpMock_testUpdateIGCCConfiguration() {
        VendorConfigUpdateResponse vendorConfigServiceUpdateResponse =
                VendorConfigUpdateResponse.builder().message("success").build();
        mockServer.when(
                HttpRequest.request("/config/igcc").withMethod("POST")
        ).respond(
                HttpResponse.response()
                        .withStatusCode(HttpStatusCode.OK_200.code())
                        .withBody(JsonBody.json(Json.serialize(vendorConfigServiceUpdateResponse)))
        );
    }

    @Test
    public void testUpdateIGCCConfiguration() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        setUpMock_testUpdateIGCCConfiguration();
        List<VendorConfigUpdateRequest> igccConfigUpdateRequestList = Collections.singletonList(
                VendorConfigUpdateRequest.builder().key("TEST").value("VALUE").build());
        Response<?> response = vendorConfigController.updateIGCCConfiguration(igccConfigUpdateRequestList);
        Assert.assertEquals(1, response.getStatusCode());
    }

    @After
    public void stopServer() {
        mockServer.stop();
    }
}
