package com.swiggy.restaurantone.controller;

import com.swiggy.commons.Json;
import com.swiggy.commons.response.Response;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.pojos.communication_service.UploadUrlRequest;
import com.swiggy.restaurantone.pojos.communication_service.UploadUrlResponse;
import com.swiggy.restaurantone.service.AmazonS3ClientService;
import com.swiggy.restaurantone.service.FileUrlService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(MockitoJUnitRunner.class)
public class FileUrlControllerTest {

    @Mock
    private AuthorizationHelper mockAuthorizationHelper;

    @Mock
    private FileUrlService mockFileUrlService;

    @Mock
    private AmazonS3ClientService amazonS3ClientService;

    @InjectMocks
    private FileUrlController fileUrlControllerUnderTest;

    private final static String CONST_PRE_SIGNED_URL = "https://nudge-uat.s3.ap-southeast-1.amazonaws.com/6c78ba41-59d0-4914-a666-42f059b4be01_TestVideo.mov?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20201206T144432Z&X-Amz-SignedHeaders=content-type%3Bhost&X-Amz-Expires=25200&X-Amz-Credential=AKIATDLCJBP73PXOKRTY%2F20201206%2Fap-southeast-1%2Fs3%2Faws4_request&X-Amz-Signature=32ab525fda4d16f78c6d9cebe8b188629e0a39093c75262785efaa2cec3b8108";

    @Before
    public void setUp() {
        initMocks(this);
        ReflectionTestUtils.setField(mockFileUrlService, "cdnDomain", "cdn-host-name");
        ReflectionTestUtils.setField(mockFileUrlService, "amazonS3ClientService", amazonS3ClientService);
        when(mockFileUrlService.fetchUploadUrl(any(UploadUrlRequest.class))).thenCallRealMethod();
        when(mockFileUrlService.computeCdnUrl(anyString())).thenCallRealMethod();
        when(amazonS3ClientService.getPreSignedUrl(anyString(),anyString(),any())).thenReturn(
                CONST_PRE_SIGNED_URL
        );
    }

    @Test
    public void testFetchCdnUrlsUnauthorizedCase() {
        final UploadUrlRequest cdnRequest = new UploadUrlRequest(
                true, "fileName", "application/json"
        );
        when(mockAuthorizationHelper.authenticateUserSession()).thenReturn(false);
        final Response<?> result = fileUrlControllerUnderTest.fetchUploadUrl(cdnRequest);
        Assert.assertEquals("Unauthorized", result.getStatusMessage());
    }

    @Test
    public void testFetchCdnUrlsCdnCase() {
        final UploadUrlRequest cdnRequest = new UploadUrlRequest(
                true, "TestVideo.mov", "quicktime/movie"
        );
        when(mockAuthorizationHelper.authenticateUserSession()).thenReturn(true);
        System.out.println(mockFileUrlService.cdnDomain);
        final Response<?> cdnResponse = fileUrlControllerUnderTest.fetchUploadUrl(cdnRequest);
        UploadUrlResponse uploadUrlResponse = Json.deserialize(Json.serialize(cdnResponse.getData()), UploadUrlResponse.class);
        Assert.assertEquals("https://cdn-host-name/6c78ba41-59d0-4914-a666-42f059b4be01_TestVideo.mov",
                uploadUrlResponse.getCdnUrl());
        Assert.assertEquals(CONST_PRE_SIGNED_URL, uploadUrlResponse.getPreSignedUrl());
    }

    @Test
    public void testFetchCdnUrlsNoCdnCase() {
        final UploadUrlRequest cdnRequest = new UploadUrlRequest(
                false, "TestVideo.mov", "quicktime/movie"
        );
        when(mockAuthorizationHelper.authenticateUserSession()).thenReturn(true);
        final Response<?> cdnResponse = fileUrlControllerUnderTest.fetchUploadUrl(cdnRequest);
        System.out.println(Json.serialize(cdnResponse));
        UploadUrlResponse uploadUrlResponse = Json.deserialize(Json.serialize(cdnResponse.getData()), UploadUrlResponse.class);
        Assert.assertEquals(CONST_PRE_SIGNED_URL, uploadUrlResponse.getPreSignedUrl());
    }
}
