package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.ItemTrimmingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @author Asif
 */
@RunWith(MockitoJUnitRunner.class)
public class ItemTrimmingControllerTest {

    @Mock
    private AuthorizationHelper authorizationHelper;

    @Mock
    private ItemTrimmingService itemTrimmingService;

    @InjectMocks
    private ItemTrimmingController itemTrimmingController;

    @Mock
    MultipartFile file;


    @Test
    public void itemTrimmingTestWhenUserIsNotAuthorized() {
        when(authorizationHelper.authenticateUserSession()).thenReturn(false);

        Response<?> res =  itemTrimmingController.itemTrimming(file);
        assertEquals(res.getStatusMessage(), "Unauthorized");

    }

    @Test
    public void itemTrimmingTestWhenSuccess() throws IOException {
        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(itemTrimmingService.applyHolidaySlot(file.getInputStream())).thenReturn("success");
        Response<?> res =  itemTrimmingController.itemTrimming(file);
        assertEquals(res.getData(), "success");

    }
}
