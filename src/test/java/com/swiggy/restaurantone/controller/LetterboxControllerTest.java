package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.ErrorResponse;
import com.swiggy.commons.response.Response;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.pojos.requests.letterbox.UploadRequest;
import com.swiggy.restaurantone.service.LetterboxService;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LetterboxControllerTest {
    private String email;
    private String filename;
    private String uploadType;
    private int page;

    @Mock
    private AuthorizationHelper authorizationHelper;

    @Mock
    private LetterboxService letterboxService;

    @InjectMocks
    private LetterboxController letterboxController;

    private Response getMockResponse(){
        Response response = new Response();
        response.setStatusCode(1);
        response.setStatusMessage("success");
        response.setData(new JSONObject());

        return response;
    }

    private Response getUnAuthorizedResponse(){
        return new ErrorResponse("Unauthorized");
    }

    @Before
    public void setUp() {
        email = "rahul.alam@swiggy.in";
        filename = "test.csv";
        uploadType = "Restaurant Persona Details Upload";
        page = 0;
    }


    @Test
    public void getTemplatesTest() throws IOException {
        Response expectedResponse = getMockResponse();

        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);
        when(letterboxService.getTemplates()).thenReturn(expectedResponse);

        Response<?> actualResponse = letterboxController.getTemplates();

        assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    @Test
    public void getTemplatesTest_UnAuthorized() throws IOException {
        Response expectedResponse = getUnAuthorizedResponse();

        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);
        when(letterboxService.getTemplates()).thenReturn(expectedResponse);

        Response<?> actualResponse = letterboxController.getTemplates();

        assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    public void getTemplatesTest_UnAuthenticated() throws IOException {
        Response expectedResponse = getUnAuthorizedResponse();

        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);
        when(letterboxService.getTemplates()).thenReturn(expectedResponse);

        Response<?> actualResponse = letterboxController.getTemplates();

        assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    @Test
    public void uploadTest() throws IOException {
        UploadRequest request = new UploadRequest(filename, uploadType, null);
        Response expectedResponse = getMockResponse();

        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);
        when(letterboxService.upload(request)).thenReturn(expectedResponse);

        Response<?> actualResponse = letterboxController.upload(request);

        assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    @Test
    public void uploadTest_UnAuthorized() throws IOException {
        UploadRequest request = new UploadRequest(filename, uploadType, null);
        Response expectedResponse = getUnAuthorizedResponse();

        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);
        when(letterboxService.upload(request)).thenReturn(expectedResponse);

        Response<?> actualResponse = letterboxController.upload(request);

        assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    @Test
    public void uploadTest_UnAuthenticated() throws IOException {
        UploadRequest request = new UploadRequest(filename, uploadType, null);
        Response expectedResponse = getUnAuthorizedResponse();

        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);
        when(letterboxService.upload(request)).thenReturn(expectedResponse);

        Response<?> actualResponse = letterboxController.upload(request);

        assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    @Test
    public void presignedUrlTest() throws IOException {
        Response expectedResponse = getMockResponse();

        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);
        when(letterboxService.getPresignedUrl(filename, uploadType)).thenReturn(expectedResponse);

        Response<?> actualResponse = letterboxController.presignedUrl(filename, uploadType);

        assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    @Test
    public void presignedUrlTest_UnAuthorized() throws IOException {
        Response expectedResponse = getUnAuthorizedResponse();

        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);
        when(letterboxService.getPresignedUrl(filename, uploadType)).thenReturn(expectedResponse);

        Response<?> actualResponse = letterboxController.presignedUrl(filename, uploadType);

        assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    @Test
    public void presignedUrlTest_UnAuthenticated() throws IOException {
        Response expectedResponse = getUnAuthorizedResponse();

        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);
        when(letterboxService.getPresignedUrl(filename, uploadType)).thenReturn(expectedResponse);

        Response<?> actualResponse = letterboxController.presignedUrl(filename, uploadType);

        assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    @Test
    public void userAuditLogTest() throws IOException {
        Response expectedResponse = getMockResponse();

        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);
        when(letterboxService.getUserAuditLogs()).thenReturn(expectedResponse);

        Response<?> actualResponse = letterboxController.userAuditLogs();

        assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    @Test
    public void userAuditLogTest_UnAuthorized() throws IOException {
        Response expectedResponse = getUnAuthorizedResponse();

        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);
        when(letterboxService.getUserAuditLogs()).thenReturn(expectedResponse);

        Response<?> actualResponse = letterboxController.userAuditLogs();

        assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    @Test
    public void userAuditLogTest_UnAuthenticated() throws IOException {
        Response expectedResponse = getUnAuthorizedResponse();

        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);
        when(letterboxService.getUserAuditLogs()).thenReturn(expectedResponse);

        Response<?> actualResponse = letterboxController.userAuditLogs();

        assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    @Test
    public void userUploadStatusTest() throws IOException {
        Response expectedResponse = getMockResponse();

        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);
        when(letterboxService.getUserUploadStatus(uploadType, page)).thenReturn(expectedResponse);

        Response<?> actualResponse = letterboxController.userUploadStatus(uploadType, page);

        assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    @Test
    public void userUploadStatusTest_UnAuthorized() throws IOException {
        Response expectedResponse = getUnAuthorizedResponse();

        when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(false);
        when(letterboxService.getUserUploadStatus(uploadType, page)).thenReturn(expectedResponse);

        Response<?> actualResponse = letterboxController.userUploadStatus(uploadType, page);

        assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    @Test
    public void userUploadStatusTest_UnAuthenticated() throws IOException {
        Response expectedResponse = getUnAuthorizedResponse();

        when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        when(authorizationHelper.checkPermissions(anyLong())).thenReturn(true);
        when(letterboxService.getUserUploadStatus(uploadType, page)).thenReturn(expectedResponse);

        Response<?> actualResponse = letterboxController.userUploadStatus(uploadType, page);

        assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }
}
