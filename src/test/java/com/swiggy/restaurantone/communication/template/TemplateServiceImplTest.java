package com.swiggy.restaurantone.communication.template;

import com.swiggy.commons.Json;
import com.swiggy.restaurantone.communication.CommEngineResponse;
import okhttp3.ResponseBody;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TemplateServiceImplTest {
    @Mock
    private TemplateServiceClient templateServiceClient;

    @Mock
    private Call<CommEngineResponse> commEngineResponseCall;

    @InjectMocks
    private TemplateServiceImpl templateService;

    private CommEngineResponse commEngineResponse;
    private Map<String, Object> dummyRequestBody;

    private List<String> templateTypes;
    private List<String> templateNames;

    @Before
    public void setUp() {
        commEngineResponse = new CommEngineResponse();
        commEngineResponse.setCode(1);
        commEngineResponse.setMessage("success");
        commEngineResponse.setData(new Object());

        templateTypes = new ArrayList<>();
        templateTypes.add("EMAIL");

        templateNames = new ArrayList<>();
        templateNames.add("testEmail");

        dummyRequestBody = new HashMap<>();
        dummyRequestBody.put("key", "value");
        dummyRequestBody.put("hello", "there");
    }

    private CommEngineResponse getEmptyErrorResponse() {
        CommEngineResponse commEngineResponse = new CommEngineResponse();
        commEngineResponse.setCode(0);
        commEngineResponse.setMessage("error");
        commEngineResponse.setData(null);
        return commEngineResponse;
    }

    @Test
    public void testCreateTemplateErrorJsonResponse() throws IOException {
        when(templateServiceClient.createTemplate(dummyRequestBody)).thenReturn(commEngineResponseCall);
        CommEngineResponse commEngineResponse2 = getEmptyErrorResponse();
        when(commEngineResponseCall.execute()).thenReturn(Response.error(400,
                ResponseBody.create(null, Json.serialize(commEngineResponse2))));
        Assert.assertNotNull(templateService.createTemplate(dummyRequestBody));
    }

    @Test
    public void testCreateTemplateErrorTrashResponse() throws IOException {
        when(templateServiceClient.createTemplate(dummyRequestBody)).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.error(500,
                ResponseBody.create(null, "null")));
        Assert.assertNotNull(templateService.createTemplate(dummyRequestBody));
    }

    @Test
    public void testValidResponse() throws IOException {
        when(templateServiceClient.getTemplateById("1")).thenReturn(commEngineResponseCall);
        when(templateServiceClient.updateTemplate(dummyRequestBody)).thenReturn(commEngineResponseCall);
        when(templateServiceClient.createTemplate(dummyRequestBody)).thenReturn(commEngineResponseCall);
        when(templateServiceClient.searchTemplate("name", "type")).thenReturn(commEngineResponseCall);
        when(templateServiceClient.sendTestMessage(dummyRequestBody)).thenReturn(commEngineResponseCall);
        when(templateServiceClient.templateHistoryById(1L, 1, 1)).thenReturn(commEngineResponseCall);

        when(commEngineResponseCall.execute()).thenReturn(Response.success(commEngineResponse));

        templateService.getTemplateById("1");
        templateService.updateTemplate(dummyRequestBody);
        templateService.createTemplate(dummyRequestBody);
        templateService.getAllTemplates("type", "name");
        templateService.sendTestMessage(dummyRequestBody);
        templateService.getTemplateHistoryById(1L, 1, 1);

        verify(templateServiceClient).getTemplateById("1");
        verify(templateServiceClient).updateTemplate(dummyRequestBody);
        verify(templateServiceClient).createTemplate(dummyRequestBody);
        verify(templateServiceClient).searchTemplate("name", "type");
        verify(templateServiceClient).sendTestMessage(dummyRequestBody);
        verify(templateServiceClient).templateHistoryById(1L, 1, 1);
    }

    @org.junit.Test(expected = TemplateServiceException.class)
    public void testFailedResponse() throws IOException {
        when(templateServiceClient.getTemplateById(Mockito.anyString())).thenReturn(commEngineResponseCall);
        when(templateServiceClient.updateTemplate(dummyRequestBody)).thenReturn(commEngineResponseCall);
        when(templateServiceClient.createTemplate(dummyRequestBody)).thenReturn(commEngineResponseCall);
        when(templateServiceClient.searchTemplate("name", "type")).thenReturn(commEngineResponseCall);
        when(templateServiceClient.sendTestMessage(dummyRequestBody)).thenReturn(commEngineResponseCall);
        when(templateServiceClient.templateHistoryById(1L, 1, 1)).thenReturn(commEngineResponseCall);

        when(commEngineResponseCall.execute()).thenReturn(null);

        templateService.getTemplateById("1");
        templateService.updateTemplate(dummyRequestBody);
        templateService.createTemplate(dummyRequestBody);
        templateService.getAllTemplates("type", "name");
        templateService.sendTestMessage(dummyRequestBody);
        templateService.getTemplateHistoryById(1L, 1, 1);

        verify(templateServiceClient).getTemplateById("1");
        verify(templateServiceClient).updateTemplate(dummyRequestBody);
        verify(templateServiceClient).createTemplate(dummyRequestBody);
        verify(templateServiceClient).searchTemplate("name", "type");
        verify(templateServiceClient).sendTestMessage(dummyRequestBody);
        verify(templateServiceClient).templateHistoryById(1L, 1, 1);
    }

    @org.junit.Test(expected = TemplateServiceException.class)
    public void testException() {
        when(templateServiceClient.getTemplateById(Mockito.anyString())).thenThrow(IOException.class);
        when(templateServiceClient.updateTemplate(dummyRequestBody)).thenThrow(IOException.class);
        when(templateServiceClient.createTemplate(dummyRequestBody)).thenThrow(IOException.class);
        when(templateServiceClient.searchTemplate("name", "type")).thenThrow(IOException.class);
        when(templateServiceClient.sendTestMessage(dummyRequestBody)).thenThrow(IOException.class);
        when(templateServiceClient.templateHistoryById(1L, 1, 1)).thenThrow(IOException.class);

        templateService.getTemplateById("1");
        templateService.updateTemplate(dummyRequestBody);
        templateService.createTemplate(dummyRequestBody);
        templateService.getAllTemplates("type", "name");
        templateService.sendTestMessage(dummyRequestBody);
        templateService.getTemplateHistoryById(1L, 1, 1);

        verify(templateServiceClient).getTemplateById(Mockito.anyString());
        verify(templateServiceClient).updateTemplate(dummyRequestBody);
        verify(templateServiceClient).createTemplate(dummyRequestBody);
        verify(templateServiceClient).searchTemplate("name", "type");
        verify(templateServiceClient).sendTestMessage(dummyRequestBody);
        verify(templateServiceClient).templateHistoryById(1L, 1, 1);
    }
}