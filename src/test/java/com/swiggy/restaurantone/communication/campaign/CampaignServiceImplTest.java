package com.swiggy.restaurantone.communication.campaign;

import com.swiggy.restaurantone.communication.CommEngineResponse;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CampaignServiceImplTest {
    @Mock
    private CampaignServiceClient campaignServiceClient;

    @Mock
    private Call<CommEngineResponse> commEngineResponseCall;

    @InjectMocks
    private CampaignServiceImpl campaignService;

    private CommEngineResponse commEngineResponse;

    @Before
    public void setUp(){
        commEngineResponse = new CommEngineResponse();
        commEngineResponse.setCode(1);
        commEngineResponse.setMessage("success");
        commEngineResponse.setData(new Object());
    }

    @org.junit.Test
    public void getAllCampaignsValidResponse() throws IOException{
        when(campaignServiceClient.getAllCampaigns()).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(commEngineResponse));

        campaignService.getAllCampaigns();

        verify(campaignServiceClient).getAllCampaigns();
    }
    @org.junit.Test(expected = CampaignServiceException.class)
    public void getAllCampaignsFailedResponse() throws IOException{
        when(campaignServiceClient.getAllCampaigns()).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);

        campaignService.getAllCampaigns();

        verify(campaignServiceClient).getAllCampaigns();
    }

    @org.junit.Test(expected = CampaignServiceException.class)
    public void getAllCampaignsException() {
        when(campaignServiceClient.getAllCampaigns()).thenThrow(IOException.class);

        campaignService.getAllCampaigns();

        verify(campaignServiceClient.getAllCampaigns());
    }

    @org.junit.Test
    public void getCampaignByIdValidResponse() throws IOException{
        when(campaignServiceClient.getCampaignById(1903456789L)).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(commEngineResponse));

        campaignService.getCampaignById(1903456789L);

        verify(campaignServiceClient).getCampaignById(1903456789L);
    }
    @org.junit.Test(expected = CampaignServiceException.class)
    public void getCampaignByIdFailedResponse() throws IOException{
        when(campaignServiceClient.getCampaignById(1903456789L)).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);

        campaignService.getCampaignById(1903456789L);

        verify(campaignServiceClient).getCampaignById(1903456789L);
    }

    @org.junit.Test(expected = CampaignServiceException.class)
    public void getCampaignByIdException() {
        when(campaignServiceClient.getCampaignById(1903456789L)).thenThrow(IOException.class);

        campaignService.getCampaignById(1903456789L);

        verify(campaignServiceClient.getCampaignById(1903456789L));
    }

    @org.junit.Test
    public void deleteCampaignByCampaignIdValidResponse() throws IOException{
        when(campaignServiceClient.deleteCampaignByCampaignId(1903456789L)).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(commEngineResponse));

        campaignService.deleteCampaignByCampaignId(1903456789L);

        verify(campaignServiceClient).deleteCampaignByCampaignId(1903456789L);
    }
    @org.junit.Test(expected = CampaignServiceException.class)
    public void deleteCampaignByCampaignIdFailedResponse() throws IOException{
        when(campaignServiceClient.deleteCampaignByCampaignId(1903456789L)).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);

        campaignService.deleteCampaignByCampaignId(1903456789L);

        verify(campaignServiceClient).deleteCampaignByCampaignId(1903456789L);
    }

    @org.junit.Test(expected = CampaignServiceException.class)
    public void deleteCampaignByCampaignIdException() {
        when(campaignServiceClient.deleteCampaignByCampaignId(1903456789L)).thenThrow(IOException.class);

        campaignService.deleteCampaignByCampaignId(1903456789L);

        verify(campaignServiceClient.deleteCampaignByCampaignId(1903456789L));
    }

    @org.junit.Test
    public void updateCampaignValidResponse() throws IOException{
        Object campaignBody = new Object();

        when(campaignServiceClient.updateCampaign(campaignBody)).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(commEngineResponse));

        campaignService.updateCampaign(campaignBody);

        verify(campaignServiceClient).updateCampaign(campaignBody);
    }
    @org.junit.Test(expected = CampaignServiceException.class)
    public void updateCampaignFailedResponse() throws IOException{
        Object campaignBody = new Object();

        when(campaignServiceClient.updateCampaign(campaignBody)).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);

        campaignService.updateCampaign(campaignBody);

        verify(campaignServiceClient).updateCampaign(campaignBody);
    }

    @org.junit.Test(expected = CampaignServiceException.class)
    public void updateCampaignException() {
        Object campaignBody = new Object();

        when(campaignServiceClient.updateCampaign(campaignBody)).thenThrow(IOException.class);

        campaignService.updateCampaign(campaignBody);

        verify(campaignServiceClient.updateCampaign(campaignBody));
    }

    @org.junit.Test
    public void campaignFacadeValidResponse() throws IOException{
        Object campaignBody = new Object();

        when(campaignServiceClient.campaignFacade(campaignBody)).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(commEngineResponse));

        campaignService.campaignFacade(campaignBody);

        verify(campaignServiceClient).campaignFacade(campaignBody);
    }
    @org.junit.Test(expected = CampaignServiceException.class)
    public void campaignFacadeFailedResponse() throws IOException{
        Object campaignBody = new Object();

        when(campaignServiceClient.campaignFacade(campaignBody)).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);

        campaignService.campaignFacade(campaignBody);

        verify(campaignServiceClient).updateCampaign(campaignBody);
    }

    @org.junit.Test(expected = CampaignServiceException.class)
    public void campaignFacadeException() {
        Object campaignBody = new Object();

        when(campaignServiceClient.campaignFacade(campaignBody)).thenThrow(IOException.class);

        campaignService.campaignFacade(campaignBody);

        verify(campaignServiceClient.campaignFacade(campaignBody));
    }
}