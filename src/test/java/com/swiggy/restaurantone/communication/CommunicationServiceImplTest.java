package com.swiggy.restaurantone.communication;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CommunicationServiceImplTest {
    @Mock
    private CommunicationServiceClient communicationServiceClient;

    @Mock
    private Call<CommEngineResponse> commEngineResponseCall;

    @InjectMocks
    private CommunicationServiceImpl communicationService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private Map<String, Object> getDummyRequestBody() {
        Map<String, Object> drb = new HashMap<>();
        drb.put("key", "value");
        drb.put("hello", "there");
        drb.put("random", "check");
        drb.put("Under", "Resist");
        return drb;
    }

    private CommEngineResponse getCommEngineResponse() {
        CommEngineResponse cer = new CommEngineResponse();
        cer.setCode(1);
        cer.setMessage("success");
        cer.setData(new Object());
        return cer;
    }

    @Test
    public void getAllTemplatesExceptionTest() {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.searchTemplate("random", "random")).thenThrow(IOException.class);
        communicationService.getAllTemplates("random", "random");
        verify(communicationServiceClient).searchTemplate("random", "random");
    }

    @Test
    public void getAllTemplatesValidResponseTest() throws IOException {
        when(communicationServiceClient.searchTemplate("random", "random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(getCommEngineResponse()));
        communicationService.getAllTemplates("random", "random");
        verify(communicationServiceClient).searchTemplate("random", "random");

    }

    @Test
    public void getAllTemplatesFailedResponseTest() throws IOException {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.searchTemplate("random", "random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);
        communicationService.getAllTemplates("random", "random");
        verify(communicationServiceClient).searchTemplate("random", "random");
    }

    @Test
    public void getTemplateByIdTestException() {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.getTemplateById("random")).thenThrow(IOException.class);
        communicationService.getTemplateById("random");
        verify(communicationServiceClient).getTemplateById("random");
    }

    @Test
    public void getTemplateByIdValidResponseTest() throws IOException {
        when(communicationServiceClient.getTemplateById("random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(getCommEngineResponse()));
        communicationService.getTemplateById("random");
        verify(communicationServiceClient).getTemplateById("random");
    }

    @Test
    public void getTemplateByIdFailedResponseTest() throws IOException {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.getTemplateById("random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);
        communicationService.getTemplateById("random");
        verify(communicationServiceClient).getTemplateById("random");
    }

    @Test
    public void getUpdateTestException() {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.updateTemplate("random")).thenThrow(IOException.class);
        communicationService.updateTemplate("random");
        verify(communicationServiceClient).updateTemplate("random");
    }

    @Test
    public void updateTemplateValidResponseTest() throws IOException {
        when(communicationServiceClient.updateTemplate("random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(getCommEngineResponse()));
        communicationService.updateTemplate("random");
        verify(communicationServiceClient).updateTemplate("random");
    }

    @Test
    public void updateTemplateFailedResponseTest() throws IOException {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.updateTemplate("random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);
        communicationService.updateTemplate("random");
        verify(communicationServiceClient).updateTemplate("random");
    }

    @Test
    public void createTemplateTestException() {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.createTemplate("random")).thenThrow(IOException.class);
        communicationService.createTemplate("random");
        verify(communicationServiceClient).createTemplate("random");
    }

    @Test
    public void createTemplateValidResponseTest() throws IOException {
        when(communicationServiceClient.createTemplate("random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(getCommEngineResponse()));
        communicationService.createTemplate("random");
        verify(communicationServiceClient).createTemplate("random");
    }

    @Test
    public void createTemplateFailedResponseTest() throws IOException {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.createTemplate("random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);
        communicationService.createTemplate("random");
        verify(communicationServiceClient).createTemplate("random");
    }

    @Test
    public void sendTestMessageTestException() {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.sendTestMessage("random")).thenThrow(IOException.class);
        communicationService.sendTestMessage("random");
        verify(communicationServiceClient).sendTestMessage("random");
    }

    @Test
    public void sendTestMessageValidResponseTest() throws IOException {
        when(communicationServiceClient.sendTestMessage("random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(getCommEngineResponse()));
        communicationService.sendTestMessage("random");
        verify(communicationServiceClient).sendTestMessage("random");
    }

    @Test
    public void sendTestMessageFailedResponseTest() throws IOException {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.sendTestMessage("random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);
        communicationService.sendTestMessage("random");
        verify(communicationServiceClient).sendTestMessage("random");
    }

    @Test
    public void createEventTestException() {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.createEvent("random")).thenThrow(IOException.class);
        communicationService.createEvent("random");
        verify(communicationServiceClient).createEvent("random");
    }

    @Test
    public void createEventValidResponseTest() throws IOException {
        when(communicationServiceClient.createEvent("random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(getCommEngineResponse()));
        communicationService.createEvent("random");
        verify(communicationServiceClient).createEvent("random");
    }

    @Test
    public void createEventFailedResponseTest() throws IOException {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.createEvent("random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);
        communicationService.createEvent("random");
        verify(communicationServiceClient).createEvent("random");
    }

    @Test
    public void deleteEventTestException() {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.deleteEvent("random")).thenThrow(IOException.class);
        communicationService.deleteEvent("random");
        verify(communicationServiceClient).deleteEvent("random");
    }

    @Test
    public void deleteEventValidResponseTest() throws IOException {
        when(communicationServiceClient.deleteEvent("random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(getCommEngineResponse()));
        communicationService.deleteEvent("random");
        verify(communicationServiceClient).deleteEvent("random");
    }

    @Test
    public void deleteEventFailedResponseTest() throws IOException {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.deleteEvent("random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);
        communicationService.deleteEvent("random");
        verify(communicationServiceClient).deleteEvent("random");
    }


    @Test
    public void getAllEventsTestException() {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.getAllEvents()).thenThrow(IOException.class);
        communicationService.getAllEvents();
        verify(communicationServiceClient).getAllEvents();
    }

    @Test
    public void getAllEventsValidResponseTest() throws IOException {
        when(communicationServiceClient.getAllEvents()).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(getCommEngineResponse()));
        communicationService.getAllEvents();
        verify(communicationServiceClient).getAllEvents();
    }

    @Test
    public void getAllEventsFailedResponseTest() throws IOException {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.getAllEvents()).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);
        communicationService.getAllEvents();
        verify(communicationServiceClient).getAllEvents();
    }

    @Test
    public void getAllEventsByComponentTestException() {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.getAllEventsByComponent("random")).thenThrow(IOException.class);
        communicationService.getAllEventsByComponent("random");
        verify(communicationServiceClient).getAllEventsByComponent("random");
    }

    @Test
    public void getAllEventsByComponentValidResponseTest() throws IOException {
        when(communicationServiceClient.getAllEventsByComponent("random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(getCommEngineResponse()));
        communicationService.getAllEventsByComponent("random");
        verify(communicationServiceClient).getAllEventsByComponent("random");
    }

    @Test
    public void getAllEventsByComponentsFailedResponseTest() throws IOException {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.getAllEventsByComponent("random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);
        communicationService.getAllEventsByComponent("random");
        verify(communicationServiceClient).getAllEventsByComponent("random");
    }

    @Test
    public void getAllEventsByComponentAndNameTest() {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.getAllEventsByComponentAndName("random", "random")).thenThrow(IOException.class);
        communicationService.getAllEventsByComponentAndName("random", "random");
        verify(communicationServiceClient).getAllEventsByComponentAndName("random", "random");
    }

    @Test
    public void getAllEventsByComponentAndNameValidResponseTest() throws IOException {
        when(communicationServiceClient.getAllEventsByComponentAndName("random", "random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(getCommEngineResponse()));
        communicationService.getAllEventsByComponentAndName("random", "random");
        verify(communicationServiceClient).getAllEventsByComponentAndName("random", "random");
    }

    @Test
    public void getAllEventsByComponentAndNameFailedResponseTest() throws IOException {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.getAllEventsByComponentAndName("random", "random")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);
        communicationService.getAllEventsByComponentAndName("random", "random");
        verify(communicationServiceClient).getAllEventsByComponentAndName("random", "random");
    }

    @Test
    public void testCallCommunicationServiceClient() {
        expectedException.expect(CommunicationServiceException.class);
        when(communicationServiceClient.searchTemplate(Mockito.anyString(), Mockito.anyString())).thenThrow(IOException.class);
        communicationService.getAllTemplates("random", "random");
    }

}
