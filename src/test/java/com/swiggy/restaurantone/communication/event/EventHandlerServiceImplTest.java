package com.swiggy.restaurantone.communication.event;

import com.swiggy.restaurantone.communication.CommEngineResponse;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EventHandlerServiceImplTest {
    @Mock
    private EventHandlerApiClient eventHandlerApiClient;

    @Mock
    private Call<CommEngineResponse> commEngineResponseCall;

    @InjectMocks
    private EventHandlerServiceImpl eventHandlerService;

    private CommEngineResponse commEngineResponse;

    @Before
    public void setUp(){
        commEngineResponse = new CommEngineResponse();
        commEngineResponse.setCode(1);
        commEngineResponse.setMessage("success");
        commEngineResponse.setData(new Object());
    }

    @org.junit.Test
    public void getAllEventsByPodValidResponse() throws IOException {
        when(eventHandlerApiClient.getAllEventsByPods("vendor")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(commEngineResponse));

        eventHandlerService.getAllEventsByPod("vendor");

        verify(eventHandlerApiClient).getAllEventsByPods("vendor");
    }
    @org.junit.Test(expected = EventHandlerServiceException.class)
    public void getAllEventsByPodFailedResponse() throws IOException{
        when(eventHandlerApiClient.getAllEventsByPods("vendor")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);

        eventHandlerService.getAllEventsByPod("vendor");

        verify(eventHandlerApiClient).getAllEventsByPods("vendor");
    }

    @org.junit.Test(expected = EventHandlerServiceException.class)
    public void getAllEventsByPodException() {
        when(eventHandlerApiClient.getAllEventsByPods("vendor")).thenThrow(IOException.class);

        eventHandlerService.getAllEventsByPod("vendor");

        verify(eventHandlerApiClient).getAllEventsByPods("vendor");
    }

    @org.junit.Test
    public void getAllTemplateVariablesByPodValidResponse() throws IOException {
        when(eventHandlerApiClient.getAllTemplateVariablesByPod("vendor")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(Response.success(commEngineResponse));

        eventHandlerService.getAllTemplateVariablesByPod("vendor");

        verify(eventHandlerApiClient).getAllTemplateVariablesByPod("vendor");
    }
    @org.junit.Test(expected = EventHandlerServiceException.class)
    public void getAllTemplateVariablesByPodFailedResponse() throws IOException{
        when(eventHandlerApiClient.getAllTemplateVariablesByPod("vendor")).thenReturn(commEngineResponseCall);
        when(commEngineResponseCall.execute()).thenReturn(null);

        eventHandlerService.getAllTemplateVariablesByPod("vendor");

        verify(eventHandlerApiClient).getAllTemplateVariablesByPod("vendor");
    }

    @org.junit.Test(expected = EventHandlerServiceException.class)
    public void getAllTemplateVariablesByPodException() {
        when(eventHandlerApiClient.getAllTemplateVariablesByPod("vendor")).thenThrow(IOException.class);

        eventHandlerService.getAllTemplateVariablesByPod("vendor");

        verify(eventHandlerApiClient).getAllTemplateVariablesByPod("vendor");
    }

}
