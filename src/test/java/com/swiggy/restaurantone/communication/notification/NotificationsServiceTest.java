package com.swiggy.restaurantone.communication.notification;

import com.swiggy.kafka.clients.producer.Producer;
import com.swiggy.restaurantone.pojos.commEngine.CommEngineEvent;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Value;

import java.util.Collections;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class NotificationsServiceTest {

    @InjectMocks
    private NotificationsService mockNotificationsService;

    @Mock
    private Producer mockKafkaTxnProducer;
    private CommEngineEvent mockCommEngineEvent;
    private String mockCommEngineEventStr;

    @Value("${kafka.txn.producer.comm-event.topic.name}")
    private String mockCommTopic;

    private Long mockRestaurantId;
    private Map<String, Object> mockParams;
    private Map<String, Object> mockBadParams;

    @Before
    public void setUp() {
        mockRestaurantId = 999L;
        mockParams = Collections.singletonMap("event_name", "mock_event");
        mockCommEngineEvent = CommEngineEvent.builder().build();
        mockCommEngineEventStr = mockCommEngineEvent.toString();
        mockMethods();
    }

    private void mockMethods() {
    }

    @Test(expected=NullPointerException.class)
    public void notifyRestaurantTestException() {
        mockNotificationsService.notifyRestaurant(mockRestaurantId, null);
    }

    @Test(expected=Test.None.class)
    public void notifyRestaurantTest() {
        mockNotificationsService.notifyRestaurant(mockRestaurantId, mockParams);
    }

    @Test(expected=Test.None.class)
    public void sendEventToCommTest() {
        mockNotificationsService.sendEventToComm(mockCommEngineEvent);
    }



}
