package com.swiggy.restaurantone.serviceTest;

import com.swiggy.restaurantone.data.businessEntity.*;
import com.swiggy.restaurantone.data.coreEntity.PermissionEntity;
import com.swiggy.restaurantone.data.coreEntity.UserEntity;
import com.swiggy.restaurantone.dataAccess.UserDao;
import com.swiggy.restaurantone.mapper.UserMapper;
import com.swiggy.restaurantone.service.UserRelationshipService;
import com.swiggy.restaurantone.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by balamuneeswaran.s on 2/8/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;


    @Mock
    private UserMapper userMapper;
    @Mock
    private UserDao userDao;

    @Mock
    private UserRelationshipService userRelationshipService;

    private List<UserEntity> userEntities;
    private List<User> users;
    private List<PermissionEntity> permissionEntities;
    private List<Permission> permissions;
    private UserEntity user;
    User userBo;

    @Before
    public void setup() {
        permissionEntities = new ArrayList<>();
        PermissionEntity permission = new PermissionEntity();
        permission.setPermission("Test");
        permissionEntities.add(permission);
        permissions = new ArrayList<>();
        permissions.add(new Permission());
        user = new UserEntity("test","9686676251","test@swiggy.in",1l,"{}", permissionEntities, 1L, new Date(), new Date(), 1l);
        userEntities = new ArrayList<>();
        userEntities.add(user);
        Role role = Role.builder().level(100000L).permissions(permissions).name("SM").build();
        userBo = new User(user.name,user.getId(),user.contactNumber,user.email,role,user.properties,permissions, new Date(), new Date(), new Manager(1l, "test"), new City(1l, "test city"), new Team(1l, "test team"),"");
        users =  new ArrayList<>();
        users.add(userBo);
        when(userDao.findByIdInOrEmailInOrContactNumberIn(anyList(),anyList(),anyList(),anyObject())).thenReturn(userEntities);
        when(userDao.findOne(anyLong())).thenReturn(user);
        when(userMapper.entityToBusinessObject(anyList())).thenReturn(users);
        when(userMapper.entityToBusinessObject(user)).thenReturn(userBo);
        when(userMapper.businessToEntity(userBo)).thenReturn(user);
        when(userRelationshipService.updateUserManagerRelationship(anyList())).thenReturn(new LinkedList());
    }


    @Test
    public void getUsersByCriteriaSuccessTest() {
        UserCriteria userCriteria = new UserCriteria();
        userCriteria.setIdList(Arrays.asList(1l));
        List<User> actualResponse =  userService.getUsersByCriteria(userCriteria);
        PageRequest pageRequest = new PageRequest(0,100, Sort.Direction.ASC,"id");
        verify(userDao).findByIdInOrEmailInOrContactNumberIn(userCriteria.getIdList(),Arrays.asList(),
                Arrays.asList(), pageRequest);
    }

    @Test
    public void getUsersByCriteriaPaginationTest() {
        UserCriteria userCriteria = new UserCriteria();
        userCriteria.setEmailList(Arrays.asList("test@swiggy.in"));
        userCriteria.setMobileNumberList(Arrays.asList("9686676351"));
        Pagination pagination = new Pagination(2,25,"name",false);
        userCriteria.setPagination(pagination);
        List<User> actualResponse =  userService.getUsersByCriteria(userCriteria);
        assertEquals("user size assert", actualResponse.size(),users.size());
        assertEquals("user name assert", actualResponse.get(0), users.get(0));
        PageRequest pageRequest = new PageRequest(pagination.getPageNumber()-1,pagination.getSize(), Sort.Direction.DESC,pagination.getSortColumn());
        verify(userDao).findByIdInOrEmailInOrContactNumberIn(Arrays.asList(),userCriteria.getEmailList(),
                userCriteria.getMobileNumberList(), pageRequest);
    }

    @Test
    public void updateUserTest() {
        when(userDao.save(userEntities.get(0))).thenReturn(userEntities.get(0));
        when(userDao.save(user)).thenReturn(user);
        when(userMapper.businessRequestToEntity(userBo)).thenReturn(user);
        User actualResponse =  userService.updateUser(userBo);
        assertEquals("user name assert", actualResponse, users.get(0));
        assertEquals("user save assert", userService.updateUser(userBo), userBo);
    }

}
