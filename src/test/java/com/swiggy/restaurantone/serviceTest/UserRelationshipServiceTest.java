package com.swiggy.restaurantone.serviceTest;

import com.swiggy.restaurantone.data.businessEntity.UserRelationship;
import com.swiggy.restaurantone.data.coreEntity.UserRelationshipEntity;
import com.swiggy.restaurantone.dataAccess.UserRelationshipDao;
import com.swiggy.restaurantone.mapper.UserRelationshipMapper;
import com.swiggy.restaurantone.service.UserRelationshipService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

/**
 * Created by balamuneeswaran.s on 2/9/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserRelationshipServiceTest {

    @InjectMocks
    private UserRelationshipService userRelationshipService;

    @Mock
    private UserRelationshipDao userRelationshipDao;

    @Mock
    private UserRelationshipMapper userRelationshipMapper;

    @Mock
    private RedisTemplate redisTemplate;

    private List<UserRelationshipEntity> userRelationshipEntities;
    private List<UserRelationship> userRelationships;

    @Before
    public void setup() {
        userRelationshipEntities = new ArrayList<>();
        UserRelationshipEntity userRelationshipEntity = new UserRelationshipEntity(1L,2L,true,new Date(), new Date(), "1", "2");
        userRelationshipEntities.add(userRelationshipEntity);
        userRelationships = new ArrayList<>();
        UserRelationship userRelationship = new UserRelationship(userRelationshipEntity.getUserId(),userRelationshipEntity.getChildUserId(), userRelationshipEntity.getUpdatedBy());
        userRelationships.add(userRelationship);
        when(userRelationshipDao.findByUserIdAndIsDirectParent(anyLong(), anyBoolean())).thenReturn(userRelationshipEntities);
        when(userRelationshipDao.findByUserId(anyLong())).thenReturn(userRelationshipEntities);
        when(userRelationshipDao.findByChildUserId(anyLong())).thenReturn(userRelationshipEntities);
        when(userRelationshipDao.deleteByChildUserId(anyLong())).thenReturn(null);
        when(userRelationshipDao.deleteByUserId(anyLong())).thenReturn(null);
        when(userRelationshipDao.findByChildUserIdAndIsDirectParent(anyLong(), anyBoolean())).thenReturn(userRelationshipEntities);
        when(userRelationshipMapper.entityToBusinessObject(anyList())).thenReturn(userRelationships);
        when(userRelationshipMapper.entityToBusinessObject(userRelationshipEntity)).thenReturn(userRelationships.get(0));
        when(userRelationshipMapper.businessToEntity(anyList())).thenReturn(userRelationshipEntities);
        when(userRelationshipMapper.businessToEntity(userRelationship)).thenReturn(userRelationshipEntities.get(0));
    }

    @Test
    public void getImmediateChildUsersByUserIdTest() {
        List<UserRelationship> actualReponse = userRelationshipService.GetImmediateChildUsersByUserId(1l);
        assertEquals("User relationship response assert", actualReponse, userRelationships);
        verify(userRelationshipDao).findByUserIdAndIsDirectParent(1l, true);
        verify(userRelationshipMapper).entityToBusinessObject(userRelationshipEntities);
    }

    @Test
    public void getImmediateParentUsersByUserIdTest() {
        List<UserRelationship> actualReponse = userRelationshipService.GetImmediateParentUsersByUserId(1l);
        assertEquals("User relationship response assert", actualReponse.get(0), userRelationships.get(0));
        verify(userRelationshipDao).findByChildUserIdAndIsDirectParent(1l, true);
        verify(userRelationshipMapper).entityToBusinessObject(userRelationshipEntities.get(0));
    }

    @Test
    public void getAllChildUsersByUserIdTest() {
        List<UserRelationship> actualReponse = userRelationshipService.GetAllChildUsersByUserId(1l);
        assertEquals("User relationship response assert", actualReponse, userRelationships);
        verify(userRelationshipDao).findByUserId(1l);
        verify(userRelationshipMapper).entityToBusinessObject(userRelationshipEntities);
    }

    @Test
    public void updateUserRelationshipTest() {
        List<UserRelationship> actualReponse = userRelationshipService.UpdateUserRelationship(userRelationships);
        assertEquals("User relationship response assert", actualReponse, userRelationships);
        verify(userRelationshipMapper, times(2)).entityToBusinessObject(userRelationshipEntities);
        verify(userRelationshipDao, times(1)).deleteByChildUserId(2l);
        verify(userRelationshipDao, times(1)).deleteByUserId(2l);
        verify(userRelationshipDao, times(1)).findByChildUserId(1l);
        verify(userRelationshipDao, times(2)).save(anyList());
        verify(userRelationshipMapper, times(1)).businessToEntity(userRelationships.get(0));

    }
}
