package com.swiggy.restaurantone.serviceTest;

import com.swiggy.restaurantone.data.businessEntity.RestaurantUserMap;
import com.swiggy.restaurantone.data.businessEntity.UserRelationship;
import com.swiggy.restaurantone.data.coreEntity.RestaurantUserMapEntity;
import com.swiggy.restaurantone.dataAccess.RestaurantUserMapDao;
import com.swiggy.restaurantone.mapper.UserRestaurantMapMapper;
import com.swiggy.restaurantone.service.CacheService;
import com.swiggy.restaurantone.service.RestaurantUserMapService;
import com.swiggy.restaurantone.service.UserRelationshipService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by ganesh on 9/2/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class RestaurantUserMapServiceTest {
    @InjectMocks
    private RestaurantUserMapService restaurantUserMapService;
    @Mock
    private UserRestaurantMapMapper mapMapper;
    @Mock
    private RestaurantUserMapDao mapDao;
    @Mock
    private UserRelationshipService relationshipService;
    @Mock
    private RedisTemplate redisTemplate;
    @Mock
    private CacheService cacheService = new CacheService(redisTemplate);

    private List<RestaurantUserMapEntity> entities;
    private List<RestaurantUserMap> userMaps;
    private List<UserRelationship> userRelationships;

    @Before
    public void setUp(){
        entities = new ArrayList<>();
        entities.add(new RestaurantUserMapEntity(211l,1l, 1l, new Date(),new Date(),"",""));
        entities.add(new RestaurantUserMapEntity(211l,2l, 1l, new Date(),new Date(),"",""));
        userMaps = new ArrayList<>();
        userMaps.add(new RestaurantUserMap(1l,211l,1l, ""));
        userMaps.add(new RestaurantUserMap(2l,211l,2l, ""));
        userRelationships = new ArrayList<>();
        userRelationships.add(new UserRelationship(1l,2l, ""));

        mock_methods();
    }

    private void mock_methods() {
        when(mapDao.findByUserId(anyLong())).thenReturn(entities);
        when(mapDao.findByUserIdIn(anyList())).thenReturn(entities);
        when(mapMapper.entityToBusinessObject(anyListOf(RestaurantUserMapEntity.class))).thenReturn(userMaps);
        when(mapMapper.businessToEntity(any(RestaurantUserMap.class))).thenReturn(entities.get(0)).thenReturn(entities.get(1));
        when(relationshipService.GetAllChildUsersByUserId(anyLong())).thenReturn(userRelationships);
        when(mapDao.findOne(anyLong())).thenReturn(entities.get(0)).thenReturn(entities.get(1));
    }

    @Test
    public void getRestaurantsByUserIdTest(){
        List<RestaurantUserMap> restaurantUserMaps= restaurantUserMapService.getRestaurantsByUserId(1l);
        assertEquals("Restaurant user map get by user id failed",userMaps,restaurantUserMaps);
        verify(mapMapper).entityToBusinessObject(anyListOf(RestaurantUserMapEntity.class));
    }

    @Test
    public void getRestaurantsByParentUserIdTest(){
        Set<Long> rum= restaurantUserMapService.getRestaurantsByParentUserId(1l);
        Set<Long> rum2= userMaps.stream().map(userMap -> userMap.restaurantId).collect(Collectors.toSet());
        assertEquals("Restaurant user map get by user id failed",rum2, rum);
        verify(relationshipService).GetAllChildUsersByUserId(anyLong());
        verify(mapMapper).entityToBusinessObject(anyListOf(RestaurantUserMapEntity.class));
    }

    @Test
    public void updateRestaurantUserMapTest(){
        when(mapDao.save(any(RestaurantUserMapEntity.class))).thenReturn(entities.get(0)).thenReturn(entities.get(1));
        when(mapMapper.convertToBO(any(RestaurantUserMapEntity.class))).thenReturn(userMaps.get(0)).thenReturn(userMaps.get(1));
        List<RestaurantUserMap> map = restaurantUserMapService.updateRestaurantUserMap(userMaps);
        assertTrue("updating restaurant user map failed", map.equals(userMaps));
    }

}
