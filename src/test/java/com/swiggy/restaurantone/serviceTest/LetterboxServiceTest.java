package com.swiggy.restaurantone.serviceTest;

import com.swiggy.commons.response.Response;
import com.swiggy.restaurantone.apiClients.LetterboxApiClient;
import com.swiggy.restaurantone.data.businessEntity.City;
import com.swiggy.restaurantone.data.businessEntity.Manager;
import com.swiggy.restaurantone.data.businessEntity.Team;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.pojos.requests.letterbox.UploadRequest;
import com.swiggy.restaurantone.service.LetterboxService;
import com.swiggy.restaurantone.service.RoleService;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import retrofit2.Call;

import java.io.IOException;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class LetterboxServiceTest {
    private String email;
    private String filename;
    private String uploadType;
    private int page;

    private User user;

    @Mock
    private RoleService roleService;

    @Mock
    private AuthorizationHelper authorizationHelper;

    @Mock
    private LetterboxApiClient letterboxApiClient;

    @Mock
    private Call<Response> responseCall;

    @InjectMocks
    private LetterboxService letterboxService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private Response getMockResponse(){
        Response response = new Response();
        response.setStatusCode(1);
        response.setStatusMessage("success");
        response.setData(new JSONObject());

        return response;
    }

    private ResponseBody getErrorResponseBody() {
        return ResponseBody.create(
                MediaType.parse("application/json"), "{\"statusMessage\":\"Fail\",\"statusCode\":\"1\",\"data\":\"null\"}"
        );
    }

    @Before
    public void setUp() {
        email = "rahul.alam@swiggy.in";
        filename = "test.csv";
        uploadType = "Restaurant Persona Details Upload";
        page = 0;
        user = User.builder()
                .name("abc")
                .email("pqr@xyz.in")
                .contactNumber("123456")
                .role(roleService.findRoleById(7l))
                .manager(Manager.builder().id(868l).name("ASM Manager").build())
                .city(City.builder().id(1l).name("Bangalore").build())
                .team(Team.builder().id(1l).name("city team").build())
                .updatedBy("updatedby")
                .build();
        when(authorizationHelper.fetchUser()).thenReturn(user);
    }

    @Test
    public void getTemplatesTest() throws IOException {
        Response expectedResponse = getMockResponse();

        when(letterboxApiClient.getTemplates()).thenReturn(responseCall);
        when(responseCall.execute()).thenReturn(retrofit2.Response.success(expectedResponse));

        Response actualResponse = letterboxService.getTemplates();
        verify(letterboxApiClient).getTemplates();

        Assert.assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    @Test
    public void getTemplatesTest_Fail() throws IOException {
        when(letterboxApiClient.getTemplates()).thenReturn(responseCall);
        when(responseCall.execute()).thenReturn(retrofit2.Response.error(500, getErrorResponseBody()));

        Response actualResponse = letterboxService.getTemplates();
        verify(letterboxApiClient).getTemplates();
    }

    @Test
    public void uploadTest() throws IOException {
        UploadRequest uploadRequest = new UploadRequest(filename, uploadType, email);
        Response expectedResponse = getMockResponse();

        when(letterboxApiClient.processUpload(uploadRequest)).thenReturn(responseCall);
        when(responseCall.execute()).thenReturn(retrofit2.Response.success(expectedResponse));

        Response actualResponse = letterboxService.upload(uploadRequest);
        verify(letterboxApiClient).processUpload(uploadRequest);

        Assert.assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    @Test
    public void uploadTest_Fail() throws IOException {
        UploadRequest uploadRequest = new UploadRequest(filename, uploadType, email);
        when(letterboxApiClient.processUpload(uploadRequest)).thenReturn(responseCall);
        when(responseCall.execute()).thenReturn(retrofit2.Response.error(500, getErrorResponseBody()));

        Response actualResponse = letterboxService.upload(uploadRequest);
        verify(letterboxApiClient).processUpload(uploadRequest);
    }

    @Test
    public void getPresignedUrlTest() throws IOException {
        Response expectedResponse = getMockResponse();

        when(letterboxApiClient.presignedUrl(filename, uploadType)).thenReturn(responseCall);
        when(responseCall.execute()).thenReturn(retrofit2.Response.success(expectedResponse));

        Response actualResponse = letterboxService.getPresignedUrl(filename, uploadType);
        verify(letterboxApiClient).presignedUrl(filename, uploadType);

        Assert.assertEquals(expectedResponse.getStatusMessage(), actualResponse.getStatusMessage());
    }

    @Test
    public void getPresignedUrlTest_Fail() throws IOException {
        when(letterboxApiClient.presignedUrl(filename, uploadType)).thenReturn(responseCall);
        when(responseCall.execute()).thenReturn(retrofit2.Response.error(500, getErrorResponseBody()));

        Response actualResponse = letterboxService.getPresignedUrl(filename, uploadType);
        verify(letterboxApiClient).presignedUrl(filename, uploadType);
    }

    @Test
    public void getUserAuditLogsTest() throws IOException {
        Response expectedResponse = getMockResponse();
        when(letterboxApiClient.userAuditLogs(email)).thenReturn(responseCall);
        when(responseCall.execute()).thenReturn(retrofit2.Response.success(expectedResponse));
        try { Response actualResponse = letterboxService.getUserAuditLogs(); }catch(Exception ignored) { }
    }

}
