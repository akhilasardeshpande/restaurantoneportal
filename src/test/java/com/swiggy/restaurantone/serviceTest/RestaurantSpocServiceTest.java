package com.swiggy.restaurantone.serviceTest;

import com.swiggy.restaurantone.data.businessEntity.*;
import com.swiggy.restaurantone.helper.RestaurantSpocHelper;
import com.swiggy.restaurantone.service.RestaurantSpocServiceImpl;
import com.swiggy.restaurantone.service.RoleService;
import com.swiggy.restaurantone.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RestaurantSpocServiceTest {

    @InjectMocks
    private RestaurantSpocServiceImpl mockRestaurantSpocServiceImpl;

    @Mock
    private RestaurantSpocHelper restaurantSpocHelper;

    @Mock
    private RoleService roleService;

    @Mock
    private UserService userService;

    private User mockAsmUser;
    private User mockSmUser;
    private RestaurantSpoc mockRestaurantSpoc;
    private RestaurantUserMap mockUpdatedRestaurantAsmMap;
    private RestaurantUserMap mockUpdatedRestaurantSmMap;
    private static final Long ASM_ROLE_ID = 7l;
    private static final Long ASM_MANAGER_ID = 868l;
    private static final String ASM_MANAGER_NAME = "ASM Manager";
    private static final Long SM_ROLE_ID = 16l;

    @Before
    public void setUp(){
        mockRestaurantSpoc = RestaurantSpoc.builder()
                                .restaurantId(999L)
                                .smName("Test SM Name")
                                .smEmail("Test SM Email")
                                .smMobile("Test SM Mobile")
                                .asmName("Test ASM Name")
                                .asmEmail("Test ASM Email")
                                .asmMobile("Test ASM Mobile")
                                .supportEmail("Test Support Email")
                                .supportPhone("Test Support Phone")
                                .updatedBy("Test Updated By")
                                .build();
        mockAsmUser = User.builder()
                .name(mockRestaurantSpoc.getAsmName())
                .email(mockRestaurantSpoc.getAsmEmail())
                .contactNumber(mockRestaurantSpoc.getAsmMobile())
                .role(roleService.findRoleById(ASM_ROLE_ID))
                .manager(Manager.builder().id(ASM_MANAGER_ID).name(ASM_MANAGER_NAME).build())
                .city(City.builder().id(1l).name("Bangalore").build())
                .team(Team.builder().id(1l).name("city team").build())
                .updatedBy(mockRestaurantSpoc.getUpdatedBy())
                .build();
        mockSmUser = User.builder()
                .name(mockRestaurantSpoc.getAsmName())
                .email(mockRestaurantSpoc.getAsmEmail())
                .contactNumber(mockRestaurantSpoc.getAsmMobile())
                .role(roleService.findRoleById(ASM_ROLE_ID))
                .manager(Manager.builder().id(ASM_MANAGER_ID).name(ASM_MANAGER_NAME).build())
                .city(City.builder().id(1l).name("Bangalore").build())
                .team(Team.builder().id(1l).name("city team").build())
                .updatedBy(mockRestaurantSpoc.getUpdatedBy())
                .build();
        mockUpdatedRestaurantAsmMap = RestaurantUserMap.builder()
                                        .id(999L)
                                        .restaurantId(999L)
                                        .userId(999L)
                                        .updatedBy("Test User")
                                        .build();
        mockUpdatedRestaurantSmMap = RestaurantUserMap.builder()
                .id(999L)
                .restaurantId(999L)
                .userId(999L)
                .updatedBy("Test User")
                .build();

        mockMethods();
    }

    private void mockMethods() {
        when(restaurantSpocHelper.getSpoc(mockRestaurantSpoc.getRestaurantId(), ASM_ROLE_ID)).thenReturn(java.util.Optional.ofNullable(mockAsmUser));
        when(restaurantSpocHelper.updateSpoc(anyObject())).thenReturn(mockSmUser);
        when(userService.addUser(mockSmUser)).thenReturn(mockAsmUser);
        when(restaurantSpocHelper.getSpoc(mockRestaurantSpoc.getRestaurantId(), SM_ROLE_ID)).thenReturn(java.util.Optional.ofNullable(mockSmUser));
        when(restaurantSpocHelper.updateRestaurantSpocMap(anyLong(), anyObject())).thenReturn(mockUpdatedRestaurantAsmMap);
        when(restaurantSpocHelper.updateRestaurantSpocMap(anyLong(), anyObject())).thenReturn(mockUpdatedRestaurantSmMap);
    }


    @Test
    public void getUpdateSpocTest1() {
        mockRestaurantSpocServiceImpl.updateSpoc(mockRestaurantSpoc);
        verify(restaurantSpocHelper).sendSpocNotification(mockRestaurantSpoc);
    }

    @Test
    public void getRestaurantSpocTest() {
        RestaurantSpoc restaurantSpoc = mockRestaurantSpocServiceImpl.getRestaurantSpoc(mockRestaurantSpoc.getRestaurantId(), "NOT_DAILY");
        assertEquals(restaurantSpoc.getRestaurantId().longValue(), mockRestaurantSpoc.getRestaurantId().longValue());
    }

}
