package com.swiggy.restaurantone.serviceTest;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.swiggy.restaurantone.service.AmazonS3ClientService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;;
import org.springframework.context.annotation.PropertySource;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
@PropertySource(
        "amazon.s3.bucket.appNotification=testBucket"
)
public class AmazonS3ClientServiceTest {

    @InjectMocks
    private AmazonS3ClientService amazonS3ClientService;

    @Mock
    AmazonS3 testClient;

    @Test
    public void getPreSignedUrl() throws MalformedURLException {
        URL url = new URL("https://s3.ap-southeast-1.amazonaws.com");
        Mockito.when(testClient.generatePresignedUrl(Mockito.any(GeneratePresignedUrlRequest.class))).thenReturn(url);
        assertEquals(url.toString(), amazonS3ClientService.getPreSignedUrl("filename", "text/csv", HttpMethod.GET));
    }
}