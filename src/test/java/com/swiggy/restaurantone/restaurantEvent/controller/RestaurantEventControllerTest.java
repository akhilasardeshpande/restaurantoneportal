package com.swiggy.restaurantone.restaurantEvent.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.restaurantone.Constant.ResponseConstants;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventStatusEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantEventRequest;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantUpdateEventRequest;
import com.swiggy.restaurantone.restaurantEvent.pojos.response.RestaurantEventDetails;
import com.swiggy.restaurantone.restaurantEvent.pojos.response.RestaurantEventResponse;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.ExclusiveRestaurants;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.InclusiveRestaurants;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.RestaurantDetails;
import com.swiggy.restaurantone.restaurantEvent.services.RestaurantEventServiceImpl;
import com.swiggy.restaurantone.utils.LocalDateTimeDeserializer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@RunWith(MockitoJUnitRunner.class)
public class RestaurantEventControllerTest {

    @Mock
    private AuthorizationHelper authorizationHelper;

    @Mock
    private RestaurantEventServiceImpl restaurantEventService;

    @InjectMocks
    private RestaurantEventController restaurantEventController;

    private RestaurantEventRequest restaurantEventRequest;

    private RestaurantUpdateEventRequest restaurantUpdateEventRequest;

    private List<RestaurantEventDetails> restaurantEventDetailsList = new ArrayList<>();

    private RestaurantEventResponse restaurantEventResponse;

    private LocalDateTime localFromTime;

    private LocalDateTime localToTime;

    private String fromTime;

    private String toTime;

    private Long eventId;

    @Before
    public void SetUp() {
        eventId = 123L;
        fromTime = "2020-04-25T12:16:45";
        toTime = "2020-04-27T12:16:45";
        List<Integer> cityIds = new ArrayList<>();
        cityIds.add(2);
        List<Integer> partnerTypes = new ArrayList<>();
        partnerTypes.add(2);
        List<Long> inclusiveRestaurantIds = new ArrayList<>();
        inclusiveRestaurantIds.add(9990L);
        List<Long> exclusiveRestaurantIds = new ArrayList<>();
        inclusiveRestaurantIds.add(9991L);
        RestaurantDetails restaurantDetails = RestaurantDetails.builder().
                inclusiveRestaurants(InclusiveRestaurants.builder().isAllCitiesSelected(false).cityIds(cityIds).partnerTypes(partnerTypes).restaurantIds(inclusiveRestaurantIds)
                        .build())
                .exclusiveRestaurants(ExclusiveRestaurants.builder().restaurantIds(exclusiveRestaurantIds).build())
                .build();

        restaurantEventRequest = RestaurantEventRequest.builder()
                .type(EventTypeEnum.RCC)
                .action(ActionEnum.ADD)
                .description("add to rcc")
                .meta(restaurantDetails)
                .build();

        restaurantUpdateEventRequest = RestaurantUpdateEventRequest.builder().action(ActionEnum.UNDO).build();

        RestaurantEventDetails restaurantEventDetails = RestaurantEventDetails.builder()
                .action(ActionEnum.ADD)
                .id(eventId)
                .status(EventStatusEnum.SUCCESS)
                .createdBy("test@swiggy.in")
                .type(EventTypeEnum.RCC)
                .updatedBy("updated@swiggy.in")
                .updatedAt(LocalDateTime.now())
                .createdAt(LocalDateTime.now())
                .description("add to rcc")
                .undoAllowed(true)
                .build();

        restaurantEventDetailsList.add(restaurantEventDetails);

        localFromTime = LocalDateTimeDeserializer.convertStringToLocalDateTime(fromTime);
        localToTime = LocalDateTimeDeserializer.convertStringToLocalDateTime(toTime);

        restaurantEventResponse = RestaurantEventResponse.builder().id(eventId).build();
    }


    @Test
    public void createRestaurantEventSuccessTest() {
        Mockito.when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        Mockito.when(restaurantEventService.createRestaurantEvent(restaurantEventRequest)).thenReturn(eventId);
        Response<RestaurantEventResponse> response = restaurantEventController.createRestaurantEvent(restaurantEventRequest);
        Assert.assertEquals(response.getStatusCode(), ResponseConstants.SUCCESS_STATUS_CODE.intValue());
        Assert.assertEquals(response.getData().getId(), restaurantEventResponse.getId());
    }

    @Test
    public void createRestaurantEventUnAuthorizationFailureTest() {
        Mockito.when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        Response<?> response = restaurantEventController.createRestaurantEvent(restaurantEventRequest);
        Assert.assertEquals(response.getStatusCode(), ResponseConstants.FAILURE_STATUS_CODE.intValue());
    }

    @Test
    public void createRestaurantEventExceptionTest() {
        Mockito.when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        Mockito.when(restaurantEventService.createRestaurantEvent(restaurantEventRequest)).thenThrow(new RuntimeException());
        Response<?> response = restaurantEventController.createRestaurantEvent(restaurantEventRequest);
        Assert.assertEquals(response.getStatusCode(), ResponseConstants.FAILURE_STATUS_CODE.intValue());
    }

    @Test
    public void updateRestaurantEventSuccessTest() {
        Mockito.when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        Mockito.when(restaurantEventService.updateEventWithAction(restaurantUpdateEventRequest, eventId)).thenReturn(true);
        Response<?> response = restaurantEventController.updateRestaurantEvent(eventId, restaurantUpdateEventRequest);
        Assert.assertEquals(response.getStatusCode(), ResponseConstants.SUCCESS_STATUS_CODE.intValue());
    }

    @Test
    public void updateRestaurantEventFailureTest() {
        Mockito.when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        Mockito.when(restaurantEventService.updateEventWithAction(restaurantUpdateEventRequest, eventId)).thenReturn(false);
        Response<?> response = restaurantEventController.updateRestaurantEvent(eventId, restaurantUpdateEventRequest);
        Assert.assertEquals(response.getStatusCode(), ResponseConstants.FAILURE_STATUS_CODE.intValue());
    }

    @Test
    public void updateRestaurantEventAuthorizationFailureTest() {
        Mockito.when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        Response<?> response = restaurantEventController.updateRestaurantEvent(eventId, restaurantUpdateEventRequest);
        Assert.assertEquals(response.getStatusCode(), ResponseConstants.FAILURE_STATUS_CODE.intValue());
    }

    @Test
    public void updateRestaurantEventExceptionTest() {
        Mockito.when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        Mockito.when(restaurantEventService.updateEventWithAction(restaurantUpdateEventRequest, eventId)).thenThrow(new RuntimeException());
        Response<?> response = restaurantEventController.updateRestaurantEvent(eventId, restaurantUpdateEventRequest);
        Assert.assertEquals(response.getStatusCode(), ResponseConstants.FAILURE_STATUS_CODE.intValue());
    }

    @Test
    public void getRestaurantSuccessTest() {
        Mockito.when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        Mockito.when(restaurantEventService.getEventDetailsByType(EventTypeEnum.RCC, localFromTime, localToTime)).thenReturn(restaurantEventDetailsList);
        Response<?> response = restaurantEventController.getRestaurantEvent(EventTypeEnum.RCC, fromTime, toTime);
        Assert.assertEquals(response.getData(), restaurantEventDetailsList);
    }

    @Test
    public void getRestaurantAuthorizationFailureTest() {
        Mockito.when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        Response<?> response = restaurantEventController.getRestaurantEvent(EventTypeEnum.RCC, fromTime, toTime);
        Assert.assertEquals(response.getStatusCode(), ResponseConstants.FAILURE_STATUS_CODE.intValue());
    }

    @Test
    public void getRestaurantExceptionTest() {
        Mockito.when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        Mockito.when(restaurantEventService.getEventDetailsByType(EventTypeEnum.RCC, localFromTime, localToTime)).thenThrow(new RuntimeException());
        Response<?> response = restaurantEventController.getRestaurantEvent(EventTypeEnum.RCC, fromTime, toTime);
        Assert.assertEquals(response.getStatusCode(), ResponseConstants.FAILURE_STATUS_CODE.intValue());
    }

    @Test
    public void getRestaurantByIdSuccessTest() {
        Mockito.when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        Mockito.when(restaurantEventService.getEventDetailsById(Mockito.anyLong())).thenReturn(restaurantEventDetailsList.get(0));
        Response<?> response = restaurantEventController.getRestaurantEvent(3330L);
        Assert.assertEquals(response.getStatusCode(), ResponseConstants.SUCCESS_STATUS_CODE.intValue());
    }

    @Test
    public void getRestaurantByIdAuthorizationFailureTest() {
        Mockito.when(authorizationHelper.authenticateUserSession()).thenReturn(false);
        Response<?> response = restaurantEventController.getRestaurantEvent(3330L);
        Assert.assertEquals(response.getStatusCode(), ResponseConstants.FAILURE_STATUS_CODE.intValue());
    }

    @Test
    public void getRestaurantByIdExceptionTest() {
        Mockito.when(authorizationHelper.authenticateUserSession()).thenReturn(true);
        Mockito.when(restaurantEventService.getEventDetailsById(Mockito.anyLong())).thenThrow(new RuntimeException());
        Response<?> response = restaurantEventController.getRestaurantEvent(3330L);
        Assert.assertEquals(response.getStatusCode(), ResponseConstants.FAILURE_STATUS_CODE.intValue());
    }
}
