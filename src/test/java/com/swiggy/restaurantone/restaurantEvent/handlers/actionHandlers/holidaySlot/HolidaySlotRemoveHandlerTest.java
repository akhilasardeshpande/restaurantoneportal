package com.swiggy.restaurantone.restaurantEvent.handlers.actionHandlers.holidaySlot;

import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.RestaurantHolidaySlotHelper;
import com.swiggy.restaurantone.pojos.requests.rms.RestaurantToggleStatusRMS;
import com.swiggy.restaurantone.response.CommonsResponse;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.RestaurantResolverAction;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.holidaySlot.HolidaySlotRemoveHandler;
import com.swiggy.restaurantone.restaurantEvent.instrumentation.InstrumentServiceImpl;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.ExclusiveRestaurants;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.InclusiveRestaurants;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.RestaurantDetails;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class HolidaySlotRemoveHandlerTest {

    @Mock
    private RestaurantResolverAction restaurantResolverAction;

    @Mock
    private RestaurantHolidaySlotHelper restaurantHolidaySlotHelper;

    @InjectMocks
    private HolidaySlotRemoveHandler holidaySlotRemoveHandler;

    private RestaurantDetails meta;

    private User user;

    List<Long> expectRestaurantIds;

    private Long eventId;

    @Mock
    private InstrumentServiceImpl instrumentService;

    @Before
    public void setUp() {

        eventId = 123L;
        List<Integer> cityIds = new ArrayList<>();
        cityIds.add(2);
        List<Integer> partnerTypes = new ArrayList<>();
        partnerTypes.add(3);

        List<Long> inclusiveRestaurantIds = new ArrayList<>();
        inclusiveRestaurantIds.add(9993L);
        List<Long> exclusiveRestaurantIds = new ArrayList<>();
        exclusiveRestaurantIds.add(9991L);
        meta = RestaurantDetails.builder().
                inclusiveRestaurants(InclusiveRestaurants.builder().isAllCitiesSelected(false).cityIds(cityIds).partnerTypes(partnerTypes).restaurantIds(inclusiveRestaurantIds)
                        .build())
                .exclusiveRestaurants(ExclusiveRestaurants.builder().restaurantIds(exclusiveRestaurantIds).build())
                .build();

        user = User.builder().email("test@swiggy.in").build();

        expectRestaurantIds = new ArrayList<>();
        expectRestaurantIds.add(9993L);
    }

    @Test
    public void removeHolidaySlotSuccessTest() throws IOException {
        CommonsResponse<Boolean> commonsResponse = new CommonsResponse<>();
        commonsResponse.setData(true);
        commonsResponse.setCode("1");
        RestaurantToggleStatusRMS restaurantToggleStatusRMS = RestaurantToggleStatusRMS.builder().restaurantId(9990L).build();
        Mockito.when(restaurantResolverAction.getRestaurants(Mockito.any(RestaurantDetails.class))).thenReturn(expectRestaurantIds);
        Mockito.when(restaurantHolidaySlotHelper.updateRestaurantStatusInRMSCache(restaurantToggleStatusRMS)).thenReturn(true);
        Mockito.doNothing().when(instrumentService).doInstrument(Mockito.anyLong(), Mockito.anyString(), Mockito.anyString());
        Mockito.when(restaurantHolidaySlotHelper.openRestaurantFromSelfServe(9993L, "test@swiggy.in")).thenReturn(commonsResponse);
        boolean response = holidaySlotRemoveHandler.handler(meta, user, eventId);
        Assert.assertTrue(response);
    }

    @Test
    public void removeHolidaySlotWithEmptyRestaurantIdsTest() throws IOException {
        CommonsResponse<Boolean> commonsResponse = new CommonsResponse<>();
        commonsResponse.setData(true);
        commonsResponse.setCode("1");
        RestaurantToggleStatusRMS restaurantToggleStatusRMS = RestaurantToggleStatusRMS.builder().restaurantId(9990L).build();
        Mockito.when(restaurantResolverAction.getRestaurants(Mockito.any(RestaurantDetails.class))).thenReturn(new ArrayList<>());
        Mockito.when(restaurantHolidaySlotHelper.updateRestaurantStatusInRMSCache(restaurantToggleStatusRMS)).thenReturn(true);
        Mockito.doNothing().when(instrumentService).doInstrument(Mockito.anyLong(), Mockito.anyString(), Mockito.anyString());
        Mockito.when(restaurantHolidaySlotHelper.openRestaurantFromSelfServe(9993L, "test@swiggy.in")).thenReturn(commonsResponse);
        boolean response = holidaySlotRemoveHandler.handler(meta, user, eventId);
        Assert.assertTrue(response);
    }

    @Test
    public void removeHolidaySlotExceptionTest() {
        try {
            RestaurantToggleStatusRMS restaurantToggleStatusRMS = RestaurantToggleStatusRMS.builder().restaurantId(9990L).build();
            Mockito.when(restaurantResolverAction.getRestaurants(Mockito.any(RestaurantDetails.class))).thenReturn(expectRestaurantIds);
            Mockito.when(restaurantHolidaySlotHelper.updateRestaurantStatusInRMSCache(restaurantToggleStatusRMS)).thenReturn(true);
            Mockito.doNothing().when(instrumentService).doInstrument(Mockito.anyLong(), Mockito.anyString(), Mockito.anyString());
            Mockito.when(restaurantHolidaySlotHelper.openRestaurantFromSelfServe(9993L, "test@swiggy.in")).thenThrow(new IOException("self serve exception"));
            holidaySlotRemoveHandler.handler(meta, user, eventId);
        } catch (IOException ex) {
            Assert.assertEquals(ex.getMessage(), "self serve exception");
        }
    }
}
