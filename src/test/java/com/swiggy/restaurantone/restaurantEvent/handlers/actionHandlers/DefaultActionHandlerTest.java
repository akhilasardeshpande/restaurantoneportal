package com.swiggy.restaurantone.restaurantEvent.handlers.actionHandlers;

import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.DefaultActionHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

@RunWith(MockitoJUnitRunner.class)
public class DefaultActionHandlerTest {

    @InjectMocks
    private DefaultActionHandler defaultActionHandler;

    @Test
    public void defaultActionTest() throws IOException {
        Object meta = new Object();
        User user = User.builder().build();
        boolean response = defaultActionHandler.handler(meta, user, 123L);
        Assert.assertFalse(response);
    }
}
