package com.swiggy.restaurantone.restaurantEvent.handlers.actionHandlers;

import com.swiggy.commons.Json;
import com.swiggy.dp.client.DPClient;
import com.swiggy.restaurantone.communication.CommEngineResponse;
import com.swiggy.restaurantone.communication.CommunicationServiceClient;
import com.swiggy.restaurantone.communication.CommunicationServiceImpl;
import com.swiggy.restaurantone.communication.config.CommEngineApiClientConfiguration;
import com.swiggy.restaurantone.communication.notification.NotificationsService;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.vendorconfig.FeatureConfigActionHandler;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.VendorConfigDocParser;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.email.VendorConfigUpdateEmailHelper;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.instrumentation.VendorConfigInstrumentHelper;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.validation.FeatureConfigValidationHelper;
import com.swiggy.restaurantone.restaurantEvent.instrumentation.InstrumentService;
import com.swiggy.restaurantone.restaurantEvent.instrumentation.InstrumentServiceImpl;
import com.swiggy.restaurantone.service.AmazonS3ClientService;
import com.swiggy.restaurantone.vendorconfig.VendorConfigClientConfiguration;
import com.swiggy.restaurantone.vendorconfig.pojo.request.upload.VendorConfigBulkUploadRequest;
import com.swiggy.restaurantone.vendorconfig.pojo.response.update.VendorConfigUpdateResponse;
import com.swiggy.restaurantone.vendorconfig.service.VendorConfigServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.HttpStatusCode;
import org.mockserver.model.JsonBody;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;

public class FeatureConfigUpdateActionHandlerTest {

    @Mock
    private AmazonS3ClientService mockAmazonS3ClientService;

    @Mock
    private DPClient dpClient;

    private FeatureConfigActionHandler featureConfigActionHandler;

    private ClientAndServer mockServer;

    private User user;

    public NotificationsService getNotificationService() {
        CommEngineApiClientConfiguration configuration = new
                CommEngineApiClientConfiguration();
        ReflectionTestUtils.setField(configuration, "communicationServiceHostName",
                "http://localhost:6789/");
        ReflectionTestUtils.setField(configuration, "communicationServiceTimeout",
                "10000");
        CommunicationServiceClient client = configuration.communicationServiceClient();
        CommunicationServiceImpl communicationService = new CommunicationServiceImpl();
        ReflectionTestUtils.setField(communicationService, "communicationServiceClient", client);
        NotificationsService notificationsService = new NotificationsService();
        ReflectionTestUtils.setField(notificationsService, "communicationService", communicationService);
        setupCommunicationServiceMock();
        return notificationsService;
    }

    @Before
    public void setUp() {
        initMocks(this);
        mockServer = startClientAndServer(6789);
        VendorConfigClientConfiguration vendorConfigClientConfiguration =
                new VendorConfigClientConfiguration();
        ReflectionTestUtils.setField(vendorConfigClientConfiguration, "vendorConfigServiceHostName",
                "http://localhost:6789/");
        ReflectionTestUtils.setField(vendorConfigClientConfiguration, "vendorConfigServiceTimeout",
                "10000");
        VendorConfigServiceImpl vendorConfigService = new VendorConfigServiceImpl();
        ReflectionTestUtils.setField(vendorConfigService, "vendorConfigServiceClient",
                vendorConfigClientConfiguration.vendorConfigServiceClient());
        InstrumentService instrumentService = new InstrumentServiceImpl();
        ReflectionTestUtils.setField(instrumentService, "dpClient", dpClient);
        NotificationsService notificationsService = getNotificationService();
        featureConfigActionHandler =
                new FeatureConfigActionHandler(
                        new VendorConfigDocParser(mockAmazonS3ClientService),
                        vendorConfigService,
                        new VendorConfigUpdateEmailHelper(notificationsService, mockAmazonS3ClientService),
                        new VendorConfigInstrumentHelper(instrumentService),
                        new FeatureConfigValidationHelper()
                );
        user = User.builder().email("test@gmail.com").build();
        setUpS3Mock();
    }

    private void setUpS3Mock() {
        Answer<BufferedReader> answer = new Answer<BufferedReader>() {
            public BufferedReader answer(InvocationOnMock invocation) throws Throwable {
                String s3Url = invocation.getArgumentAt(0, String.class);
                if (s3Url == null) {
                    return null;
                }
                return new BufferedReader(new InputStreamReader(
                        PrepTimeConfigUpdateActionHandlerTest.class.getResourceAsStream(s3Url)));
            }
        };
        when(mockAmazonS3ClientService.getFileFromS3(anyString())).then(answer);
        when(mockAmazonS3ClientService.getPreSignedUrl(anyString(), anyString(), any())).thenReturn("mockUrl");
    }

    private void setupCommunicationServiceMock() {
        CommEngineResponse commEngineResponse =
                new CommEngineResponse();
        commEngineResponse.setCode(1); commEngineResponse.setMessage("SUCCESS");
        commEngineResponse.setData("data");
        mockServer.when(
                HttpRequest.request("/api/v1/communication").withMethod("POST")
        ).respond(
                HttpResponse.response()
                        .withStatusCode(HttpStatusCode.OK_200.code())
                        .withBody(JsonBody.json(Json.serialize(commEngineResponse)))
        );
    }

    private void setupCSVUploadResponseMock() {
        VendorConfigUpdateResponse vendorConfigServiceUpdateResponse
                = VendorConfigUpdateResponse.builder().message("success").build();
        mockServer.when(
                HttpRequest.request("/config/rxdxbarcode").withMethod("POST")
        ).respond(
                HttpResponse.response()
                        .withStatusCode(HttpStatusCode.OK_200.code())
                        .withBody(JsonBody.json(Json.serialize(vendorConfigServiceUpdateResponse)))
        );
    }

    private void setupCSVUploadFailureResponseMock() {
        VendorConfigUpdateResponse vendorConfigServiceUpdateResponse
                = VendorConfigUpdateResponse.builder().message("fail").build();
        mockServer.when(
                HttpRequest.request("/config/rxdxbarcode").withMethod("POST")
        ).respond(
                HttpResponse.response()
                        .withStatusCode(HttpStatusCode.INTERNAL_SERVER_ERROR_500.code())
                        .withBody(JsonBody.json(Json.serialize(vendorConfigServiceUpdateResponse)))
        );
    }

    @Test
    public void testUploadFeatureConfigFromCSVSuccess() throws IOException {
        setupCSVUploadResponseMock();
        VendorConfigBulkUploadRequest restaurantBulkUploadRequest =
                VendorConfigBulkUploadRequest.builder().url("/vendorconfig/samplecsv/feature/correctUpload.csv").build();
        Assert.assertEquals(true,
                featureConfigActionHandler
                        .handler(restaurantBulkUploadRequest, user, 1289L));
    }

    @Test
    public void testUploadFeatureConfigWrongCSVs() throws IOException {
        for (int index = 0;index < 4;index++) {
            String url = String.format("/vendorconfig/samplecsv/feature/wrongUpload%d.csv", index);
            VendorConfigBulkUploadRequest restaurantBulkUploadRequest =
                    VendorConfigBulkUploadRequest.builder().url(url).build();
            Assert.assertEquals(false,
                    featureConfigActionHandler.handler(restaurantBulkUploadRequest, user, 1289L));
        }

    }

    @Test
    public void testUploadFeatureConfigNullCSV() throws IOException {
        VendorConfigBulkUploadRequest restaurantBulkUploadRequest =
                VendorConfigBulkUploadRequest.builder().url(null).build();
        Assert.assertEquals(false,
                featureConfigActionHandler.handler(restaurantBulkUploadRequest, user, 1289L));
    }

    @Test
    public void testUploadFeatureConfigVendorConfigFailure() throws IOException {
        setupCSVUploadFailureResponseMock();
        VendorConfigBulkUploadRequest restaurantBulkUploadRequest =
                VendorConfigBulkUploadRequest.builder().url("/vendorconfig/samplecsv/feature/correctUpload.csv").build();
        Assert.assertEquals(false,
                featureConfigActionHandler.handler(restaurantBulkUploadRequest, user, 1289L));
    }

    @After
    public void stopServer() {
        mockServer.stop();
    }
}
