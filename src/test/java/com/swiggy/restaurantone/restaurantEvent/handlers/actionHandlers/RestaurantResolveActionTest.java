package com.swiggy.restaurantone.restaurantEvent.handlers.actionHandlers;

import com.swiggy.restaurantone.response.SRSResponse;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.RestaurantResolverAction;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.ExclusiveRestaurants;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.InclusiveRestaurants;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.RestaurantDetails;
import com.swiggy.restaurantone.service.SRSAPIService;
import okhttp3.Request;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class RestaurantResolveActionTest {

    @Mock
    private SRSAPIService srsapiService;

    @InjectMocks
    private RestaurantResolverAction restaurantResolverAction;

    Call<SRSResponse<List<Long>>> dailyResponseCall;

    @Before
    public void setUp(){
         dailyResponseCall = new Call<SRSResponse<List<Long>>>() {
            @Override
            public Response<SRSResponse<List<Long>>> execute() throws IOException {
                List<Long> dailyRestaurants = new ArrayList<>();
                dailyRestaurants.add(9995L);
                dailyRestaurants.add(9996L);

                SRSResponse<List<Long>> srsResponse = new SRSResponse<>();
                srsResponse.setCode("1");
                srsResponse.setData(dailyRestaurants);
                srsResponse.setStatus(1);
                return Response.success(srsResponse);
            }

            @Override
            public void enqueue(Callback<SRSResponse<List<Long>>> callback) {

            }

            @Override
            public boolean isExecuted() {
                return false;
            }

            @Override
            public void cancel() {

            }

            @Override
            public boolean isCanceled() {
                return false;
            }

            @Override
            public Call<SRSResponse<List<Long>>> clone() {
                return null;
            }

            @Override
            public Request request() {
                return null;
            }
        };
    }

    @Test
    public void getRestaurantTest() throws IOException {

        List<Long> restaurantIdFromCityIdsAndPartnerType = new ArrayList<>();
        restaurantIdFromCityIdsAndPartnerType.add(9990L);
        restaurantIdFromCityIdsAndPartnerType.add(9991L);
        restaurantIdFromCityIdsAndPartnerType.add(9992L);
        restaurantIdFromCityIdsAndPartnerType.add(9995L);

        List<Integer> cityIds = new ArrayList<>();
        cityIds.add(2);
        List<Integer> partnerTypes = new ArrayList<>();
        partnerTypes.add(3);

        SRSResponse<List<Long>> srsResponse = new SRSResponse<>();
        srsResponse.setCode("1");
        srsResponse.setData(restaurantIdFromCityIdsAndPartnerType);
        srsResponse.setStatus(1);
        Response<SRSResponse<List<Long>>> response = Response.success(srsResponse);

        Call<SRSResponse<List<Long>>> responseCall = new Call<SRSResponse<List<Long>>>() {
            @Override
            public Response<SRSResponse<List<Long>>> execute() throws IOException {
                return response;
            }

            @Override
            public void enqueue(Callback<SRSResponse<List<Long>>> callback) {

            }

            @Override
            public boolean isExecuted() {
                return false;
            }

            @Override
            public void cancel() {

            }

            @Override
            public boolean isCanceled() {
                return false;
            }

            @Override
            public Call<SRSResponse<List<Long>>> clone() {
                return null;
            }

            @Override
            public Request request() {
                return null;
            }
        };


        List<Long> inclusiveRestaurantIds = new ArrayList<>();
        inclusiveRestaurantIds.add(9993L);
        List<Long> exclusiveRestaurantIds = new ArrayList<>();
        exclusiveRestaurantIds.add(9991L);
        RestaurantDetails restaurantDetails = RestaurantDetails.builder().
                inclusiveRestaurants(InclusiveRestaurants.builder().isAllCitiesSelected(false).cityIds(cityIds).partnerTypes(partnerTypes).restaurantIds(inclusiveRestaurantIds)
                        .build())
                .exclusiveRestaurants(ExclusiveRestaurants.builder().restaurantIds(exclusiveRestaurantIds).build())
                .build();

        List<Long> expectRestaurantIds = new ArrayList<>();
        expectRestaurantIds.add(9990L);
        expectRestaurantIds.add(9992L);
        expectRestaurantIds.add(9993L);


        Mockito.when(srsapiService.findByCityIdsAndPartnerTypes(cityIds, partnerTypes, true)).thenReturn(responseCall);
        Mockito.when(srsapiService.getAllDailyRestaurants()).thenReturn(dailyResponseCall);
        List<Long> responseRestaurantIds = restaurantResolverAction.getRestaurants(restaurantDetails);
        Assert.assertArrayEquals(expectRestaurantIds.toArray(), responseRestaurantIds.toArray());
    }

    @Test
    public void getRestaurantWithEmptySuggestResponseTest() throws IOException {

        List<Long> restaurantIdFromCityIdsAndPartnerType = new ArrayList<>();

        List<Integer> cityIds = new ArrayList<>();
        cityIds.add(2);
        List<Integer> partnerTypes = new ArrayList<>();
        partnerTypes.add(3);

        SRSResponse<List<Long>> srsResponse = new SRSResponse<>();
        srsResponse.setCode("1");
        srsResponse.setData(restaurantIdFromCityIdsAndPartnerType);
        srsResponse.setStatus(1);
        Response<SRSResponse<List<Long>>> response = Response.success(srsResponse);

        Call<SRSResponse<List<Long>>> responseCall = new Call<SRSResponse<List<Long>>>() {
            @Override
            public Response<SRSResponse<List<Long>>> execute() throws IOException {
                return response;
            }

            @Override
            public void enqueue(Callback<SRSResponse<List<Long>>> callback) {

            }

            @Override
            public boolean isExecuted() {
                return false;
            }

            @Override
            public void cancel() {

            }

            @Override
            public boolean isCanceled() {
                return false;
            }

            @Override
            public Call<SRSResponse<List<Long>>> clone() {
                return null;
            }

            @Override
            public Request request() {
                return null;
            }
        };


        List<Long> inclusiveRestaurantIds = new ArrayList<>();
        inclusiveRestaurantIds.add(9993L);
        List<Long> exclusiveRestaurantIds = new ArrayList<>();
        exclusiveRestaurantIds.add(9991L);
        RestaurantDetails restaurantDetails = RestaurantDetails.builder().
                inclusiveRestaurants(InclusiveRestaurants.builder().isAllCitiesSelected(false).cityIds(cityIds).partnerTypes(partnerTypes).restaurantIds(inclusiveRestaurantIds)
                        .build())
                .exclusiveRestaurants(ExclusiveRestaurants.builder().restaurantIds(exclusiveRestaurantIds).build())
                .build();

        List<Long> expectRestaurantIds = new ArrayList<>();
        expectRestaurantIds.add(9993L);


        Mockito.when(srsapiService.findByCityIdsAndPartnerTypes(cityIds, partnerTypes, true)).thenReturn(responseCall);
        Mockito.when(srsapiService.getAllDailyRestaurants()).thenReturn(dailyResponseCall);
        List<Long> responseRestaurantIds = restaurantResolverAction.getRestaurants(restaurantDetails);
        Assert.assertArrayEquals(expectRestaurantIds.toArray(), responseRestaurantIds.toArray());
    }

    @Test
    public void getRestaurantWithEmptySuggestAndOnlyExclusiveResponseTest() throws IOException {

        List<Long> restaurantIdFromCityIdsAndPartnerType = new ArrayList<>();

        List<Integer> cityIds = new ArrayList<>();
        cityIds.add(2);
        List<Integer> partnerTypes = new ArrayList<>();
        partnerTypes.add(3);

        SRSResponse<List<Long>> srsResponse = new SRSResponse<>();
        srsResponse.setCode("1");
        srsResponse.setData(restaurantIdFromCityIdsAndPartnerType);
        srsResponse.setStatus(1);
        Response<SRSResponse<List<Long>>> response = Response.success(srsResponse);

        Call<SRSResponse<List<Long>>> responseCall = new Call<SRSResponse<List<Long>>>() {
            @Override
            public Response<SRSResponse<List<Long>>> execute() throws IOException {
                return response;
            }

            @Override
            public void enqueue(Callback<SRSResponse<List<Long>>> callback) {

            }

            @Override
            public boolean isExecuted() {
                return false;
            }

            @Override
            public void cancel() {

            }

            @Override
            public boolean isCanceled() {
                return false;
            }

            @Override
            public Call<SRSResponse<List<Long>>> clone() {
                return null;
            }

            @Override
            public Request request() {
                return null;
            }
        };


        List<Long> inclusiveRestaurantIds = new ArrayList<>();
        List<Long> exclusiveRestaurantIds = new ArrayList<>();
        exclusiveRestaurantIds.add(9991L);
        RestaurantDetails restaurantDetails = RestaurantDetails.builder().
                inclusiveRestaurants(InclusiveRestaurants.builder().isAllCitiesSelected(false).cityIds(cityIds).partnerTypes(partnerTypes).restaurantIds(inclusiveRestaurantIds)
                        .build())
                .exclusiveRestaurants(ExclusiveRestaurants.builder().restaurantIds(exclusiveRestaurantIds).build())
                .build();

        List<Long> expectRestaurantIds = new ArrayList<>();

        Mockito.when(srsapiService.findByCityIdsAndPartnerTypes(cityIds, partnerTypes, true)).thenReturn(responseCall);
        Mockito.when(srsapiService.getAllDailyRestaurants()).thenReturn(dailyResponseCall);
        List<Long> responseRestaurantIds = restaurantResolverAction.getRestaurants(restaurantDetails);
        Assert.assertArrayEquals(expectRestaurantIds.toArray(), responseRestaurantIds.toArray());
    }

    @Test
    public void getRestaurantWithEmptyCityIdsAndPartnerTypesResponseTest() throws IOException {


        List<Integer> cityIds = new ArrayList<>();
        List<Integer> partnerTypes = new ArrayList<>();


        List<Long> inclusiveRestaurantIds = new ArrayList<>();
        inclusiveRestaurantIds.add(9990L);
        inclusiveRestaurantIds.add(9991L);
        inclusiveRestaurantIds.add(9992L);
        inclusiveRestaurantIds.add(9996L);

        List<Long> exclusiveRestaurantIds = new ArrayList<>();
        exclusiveRestaurantIds.add(9991L);
        RestaurantDetails restaurantDetails = RestaurantDetails.builder().
                inclusiveRestaurants(InclusiveRestaurants.builder().isAllCitiesSelected(false).cityIds(cityIds).partnerTypes(partnerTypes).restaurantIds(inclusiveRestaurantIds)
                        .build())
                .exclusiveRestaurants(ExclusiveRestaurants.builder().restaurantIds(exclusiveRestaurantIds).build())
                .build();

        List<Long> expectRestaurantIds = new ArrayList<>();
        expectRestaurantIds.add(9990L);
        expectRestaurantIds.add(9992L);

        Mockito.when(srsapiService.getAllDailyRestaurants()).thenReturn(dailyResponseCall);
        List<Long> responseRestaurantIds = restaurantResolverAction.getRestaurants(restaurantDetails);
        Assert.assertArrayEquals(expectRestaurantIds.toArray(), responseRestaurantIds.toArray());
    }
}
