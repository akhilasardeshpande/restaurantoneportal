package com.swiggy.restaurantone.restaurantEvent.handlers.eventHandlers;

import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.restaurantEvent.factory.ActionHandlerFactory;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.rcc.RccAddActionHandler;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.rcc.RccRemoveActionHandler;
import com.swiggy.restaurantone.restaurantEvent.handlers.eventHandler.RccEventHandler;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.ExclusiveRestaurants;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.InclusiveRestaurants;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.RestaurantDetails;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class RccEventHandlerTest {


    @Mock
    private ActionHandlerFactory actionHandlerFactory;

    @Mock
    private AuthorizationHelper authorizationHelper;

    @Mock
    private RccAddActionHandler rccAddActionHandler;

    @Mock
    private RccRemoveActionHandler removeActionHandler;

    @InjectMocks
    private RccEventHandler rccEventHandler;

    private RestaurantDetails restaurantDetails;

    private Long eventId;

    @Before
    public void setUp() {
        eventId = 123L;
        List<Integer> cityIds = new ArrayList<>();
        cityIds.add(2);
        List<Integer> partnerTypes = new ArrayList<>();
        partnerTypes.add(2);
        List<Long> inclusiveRestaurantIds = new ArrayList<>();
        inclusiveRestaurantIds.add(9990L);
        List<Long> exclusiveRestaurantIds = new ArrayList<>();
        inclusiveRestaurantIds.add(9991L);
        restaurantDetails = RestaurantDetails.builder().
                inclusiveRestaurants(InclusiveRestaurants.builder().isAllCitiesSelected(false).cityIds(cityIds).partnerTypes(partnerTypes).restaurantIds(inclusiveRestaurantIds)
                        .build())
                .exclusiveRestaurants(ExclusiveRestaurants.builder().restaurantIds(exclusiveRestaurantIds).build())
                .build();
    }

    @Test
    public void successRccHandlerTest() throws IOException {
        User user = User.builder().build();
        Mockito.when(authorizationHelper.isAuthorizedUser(Mockito.any(User.class), Mockito.anyLong())).thenReturn(true);
        Mockito.when(actionHandlerFactory.getActionHandler(EventTypeEnum.RCC, ActionEnum.ADD)).thenReturn(rccAddActionHandler);
        Mockito.when(rccAddActionHandler.handler(restaurantDetails, user, eventId)).thenReturn(true);
        boolean response = rccEventHandler.handler(restaurantDetails, ActionEnum.ADD, user, eventId);
        Assert.assertTrue(response);
    }

    @Test
    public void failureRccHandlerTest() throws IOException {
        User user = User.builder().build();
        Mockito.when(authorizationHelper.isAuthorizedUser(Mockito.any(User.class), Mockito.anyLong())).thenReturn(true);
        Mockito.when(actionHandlerFactory.getActionHandler(EventTypeEnum.RCC, ActionEnum.REMOVE)).thenReturn(removeActionHandler);
        Mockito.when(removeActionHandler.handler(restaurantDetails, user, eventId)).thenReturn(false);
        boolean response = rccEventHandler.handler(restaurantDetails, ActionEnum.REMOVE, user, eventId);
        Assert.assertFalse(response);
    }

    @Test
    public void unAuthorizedUser() {
        User user = User.builder().build();
        Mockito.when(authorizationHelper.isAuthorizedUser(Mockito.any(User.class), Mockito.anyLong())).thenReturn(false);
        boolean response = rccEventHandler.handler(restaurantDetails, ActionEnum.REMOVE, user, eventId);
        Assert.assertFalse(response);
    }

    @Test
    public void rccHandlerExceptionTest() throws IOException {
        User user = User.builder().build();
        Mockito.when(authorizationHelper.isAuthorizedUser(Mockito.any(User.class), Mockito.anyLong())).thenReturn(true);
        Mockito.when(actionHandlerFactory.getActionHandler(EventTypeEnum.RCC, ActionEnum.REMOVE)).thenReturn(removeActionHandler);
        Mockito.when(removeActionHandler.handler(restaurantDetails, user, eventId)).thenThrow(new IOException());
        boolean response = rccEventHandler.handler(restaurantDetails, ActionEnum.REMOVE, user, eventId);
        Assert.assertFalse(response);
    }
}
