package com.swiggy.restaurantone.restaurantEvent.handlers.actionHandlers;

import com.swiggy.commons.Json;
import com.swiggy.dp.client.DPClient;
import com.swiggy.restaurantone.Constant.VendorConfigConstants;
import com.swiggy.restaurantone.communication.notification.NotificationsService;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.vendorconfig.PrepTimeConfigUpdateActionHandler;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.VendorConfigDocParser;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.email.VendorConfigUpdateEmailHelper;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.instrumentation.VendorConfigInstrumentHelper;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.validation.RestaurantConfigValidationHelper;
import com.swiggy.restaurantone.restaurantEvent.instrumentation.InstrumentService;
import com.swiggy.restaurantone.restaurantEvent.instrumentation.InstrumentServiceImpl;
import com.swiggy.restaurantone.service.AmazonS3ClientService;
import com.swiggy.restaurantone.vendorconfig.VendorConfigClientConfiguration;
import com.swiggy.restaurantone.vendorconfig.pojo.request.upload.VendorConfigBulkUploadRequest;
import com.swiggy.restaurantone.vendorconfig.pojo.response.update.VendorConfigUpdateResponse;
import com.swiggy.restaurantone.vendorconfig.service.VendorConfigServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.HttpRequest;
import org.mockserver.model.HttpResponse;
import org.mockserver.model.HttpStatusCode;
import org.mockserver.model.JsonBody;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;

@RunWith(MockitoJUnitRunner.class)
public class PrepTimeConfigUpdateActionHandlerTest {

    @Mock
    private AmazonS3ClientService mockAmazonS3ClientService;

    @Mock
    private NotificationsService notificationsService;

    @Mock
    private DPClient dpClient;

    private PrepTimeConfigUpdateActionHandler prepTimeConfigUpdateActionHandlerUnderTest;

    private ClientAndServer mockServer;

    private User user;

    @Before
    public void setUp() {
        initMocks(this);
        mockServer = startClientAndServer(6789);
        VendorConfigClientConfiguration vendorConfigClientConfiguration =
                new VendorConfigClientConfiguration();
        ReflectionTestUtils.setField(vendorConfigClientConfiguration, "vendorConfigServiceHostName",
                "http://localhost:6789/");
        ReflectionTestUtils.setField(vendorConfigClientConfiguration, "vendorConfigServiceTimeout",
                "10000");
        VendorConfigServiceImpl vendorConfigService = new VendorConfigServiceImpl();
        ReflectionTestUtils.setField(vendorConfigService, "vendorConfigServiceClient",
                vendorConfigClientConfiguration.vendorConfigServiceClient());
        InstrumentService instrumentService = new InstrumentServiceImpl();
        ReflectionTestUtils.setField(instrumentService, "dpClient", dpClient);
        VendorConfigConstants.BATCH_SIZE = 3;
        prepTimeConfigUpdateActionHandlerUnderTest =
                new PrepTimeConfigUpdateActionHandler(
                        new VendorConfigDocParser(mockAmazonS3ClientService),
                        vendorConfigService,
                        new VendorConfigUpdateEmailHelper(notificationsService, mockAmazonS3ClientService),
                        new VendorConfigInstrumentHelper(instrumentService),
                        new RestaurantConfigValidationHelper()
                );
        doNothing().when(notificationsService).sendEmail(anyString(), anyString(), anyList(), anyMap());
        user = User.builder().email("test@gmail.com").build();
        setUpS3Mock();
    }

    private void setUpS3Mock() {
        Answer<BufferedReader> answer = new Answer<BufferedReader>() {
            public BufferedReader answer(InvocationOnMock invocation) throws Throwable {
                String s3Url = invocation.getArgumentAt(0, String.class);
                if (s3Url == null) {
                    return null;
                }
                return new BufferedReader(new InputStreamReader(
                        PrepTimeConfigUpdateActionHandlerTest.class.getResourceAsStream(s3Url)));
            }
        };
        when(mockAmazonS3ClientService.getFileFromS3(anyString())).then(answer);
        when(mockAmazonS3ClientService.getPreSignedUrl(anyString(), anyString(), any())).thenReturn("mockUrl");
    }

    private void setupCSVUploadResponseMock() {
        VendorConfigUpdateResponse vendorConfigServiceUpdateResponse
                = VendorConfigUpdateResponse.builder().message("success").build();
        mockServer.when(
                HttpRequest.request("/config/rid").withMethod("POST")
        ).respond(
                HttpResponse.response()
                        .withStatusCode(HttpStatusCode.OK_200.code())
                        .withBody(JsonBody.json(Json.serialize(vendorConfigServiceUpdateResponse)))
        );
    }

    private void setupCSVUploadFailureResponseMock() {
        VendorConfigUpdateResponse vendorConfigServiceUpdateResponse
                = VendorConfigUpdateResponse.builder().message("fail").build();
        mockServer.when(
                HttpRequest.request("/config/rid").withMethod("POST")
        ).respond(
                HttpResponse.response()
                        .withStatusCode(HttpStatusCode.INTERNAL_SERVER_ERROR_500.code())
                        .withBody(JsonBody.json(Json.serialize(vendorConfigServiceUpdateResponse)))
        );
    }

    @Test
    public void testUploadRestaurantConfigFromCSVSuccess() throws IOException {
        setupCSVUploadResponseMock();
        VendorConfigBulkUploadRequest restaurantBulkUploadRequest =
                VendorConfigBulkUploadRequest.builder().url("/vendorconfig/samplecsv/correctUpload.csv").build();
        Assert.assertEquals(true,
                prepTimeConfigUpdateActionHandlerUnderTest
                        .handler(restaurantBulkUploadRequest, user, 1289L));
    }

    @Test
    public void testUploadRestaurantConfigFromCSVSuccess2() throws IOException {
        setupCSVUploadResponseMock();
        VendorConfigBulkUploadRequest restaurantBulkUploadRequest =
                VendorConfigBulkUploadRequest.builder().url("/vendorconfig/samplecsv/correctUpload2.csv").build();
        Assert.assertEquals(true,
                prepTimeConfigUpdateActionHandlerUnderTest
                        .handler(restaurantBulkUploadRequest, user, 1289L));
    }

    @Test
    public void testUploadRestaurantConfigWrongCSV1() throws IOException {
        VendorConfigBulkUploadRequest restaurantBulkUploadRequest =
                VendorConfigBulkUploadRequest.builder().url("/vendorconfig/samplecsv/wrongUpload1.csv").build();
        Assert.assertEquals(false,
                prepTimeConfigUpdateActionHandlerUnderTest
                        .handler(restaurantBulkUploadRequest, user, 1289L));
    }

    @Test
    public void testUploadRestaurantConfigWrongCSV2() throws IOException {
        VendorConfigBulkUploadRequest restaurantBulkUploadRequest =
                VendorConfigBulkUploadRequest.builder().url("/vendorconfig/samplecsv/wrongUpload2.csv").build();
        Assert.assertEquals(false,
                prepTimeConfigUpdateActionHandlerUnderTest
                        .handler(restaurantBulkUploadRequest, user, 1289L));
    }

    @Test
    public void testUploadRestaurantConfigWrongCSV3() throws IOException {
        VendorConfigBulkUploadRequest restaurantBulkUploadRequest =
                VendorConfigBulkUploadRequest.builder().url("/vendorconfig/samplecsv/wrongUpload3.csv").build();
        Assert.assertEquals(false,
                prepTimeConfigUpdateActionHandlerUnderTest
                        .handler(restaurantBulkUploadRequest, user, 1289L));
    }

    @Test
    public void testUploadRestaurantConfigNullCSV() throws IOException {
        VendorConfigBulkUploadRequest restaurantBulkUploadRequest =
                VendorConfigBulkUploadRequest.builder().url(null).build();
        Assert.assertEquals(false,
                prepTimeConfigUpdateActionHandlerUnderTest
                        .handler(restaurantBulkUploadRequest, user, 1289L));
    }

    @Test
    public void testUploadRestaurantConfigVendorConfigFailure() throws IOException {
        setupCSVUploadFailureResponseMock();
        VendorConfigBulkUploadRequest restaurantBulkUploadRequest =
                VendorConfigBulkUploadRequest.builder().url("/vendorconfig/samplecsv/correctUpload.csv").build();
        Assert.assertEquals(false,
                prepTimeConfigUpdateActionHandlerUnderTest
                        .handler(restaurantBulkUploadRequest, user, 1289L));
    }

    @After
    public void stopServer() {
        mockServer.stop();
    }

}
