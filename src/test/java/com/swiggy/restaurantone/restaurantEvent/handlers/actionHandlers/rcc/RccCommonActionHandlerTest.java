package com.swiggy.restaurantone.restaurantEvent.handlers.actionHandlers.rcc;

import com.swiggy.kafka.clients.producer.Producer;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.rcc.RccCommonActionHandler;
import com.swiggy.restaurantone.restaurantEvent.instrumentation.InstrumentServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class RccCommonActionHandlerTest {

    @Mock
    private Producer kafkaBatchProducer;

    @InjectMocks
    private RccCommonActionHandler rccCommonActionHandler;

    @Mock
    private InstrumentServiceImpl instrumentService;

    private Long eventId;

    @Before
    public void setUp() {
        eventId = 123L;
        ReflectionTestUtils.setField(rccCommonActionHandler, "rccRestaurantBatches", 100);
    }

    @Test
    public void sendRequestToRccTest() {

        List<Long> restaurantIds = new ArrayList<>();
        restaurantIds.add(9990L);
        Mockito.doNothing().when(instrumentService).doInstrument(Mockito.anyLong(), Mockito.anyString(), Mockito.anyString());
        rccCommonActionHandler.sendRequestToRcc(restaurantIds, true, "test@swiggy.in", eventId);
        Mockito.verify(kafkaBatchProducer, Mockito.times(1)).send(Mockito.anyString(), Mockito.anyString());
    }
}
