package com.swiggy.restaurantone.restaurantEvent.handlers.actionHandlers.rcc;

import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.restaurantEvent.exceptions.ActionHandlerException;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.RestaurantResolverAction;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.rcc.RccCommonActionHandler;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.rcc.RccRemoveActionHandler;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.ExclusiveRestaurants;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.InclusiveRestaurants;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.RestaurantDetails;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class RccRemoveActionHandlerTest {

    @Mock
    private RestaurantResolverAction restaurantResolverAction;

    @Mock
    private RccCommonActionHandler rccCommonActionHandler;

    @InjectMocks
    private RccRemoveActionHandler rccRemoveActionHandler;

    private RestaurantDetails meta;

    private User user;

    private List<Long> expectRestaurantIds;

    private Long eventId;

    @Before
    public void setUp() {
        eventId = 123L;
        List<Integer> cityIds = new ArrayList<>();
        cityIds.add(2);
        List<Integer> partnerTypes = new ArrayList<>();
        partnerTypes.add(3);

        List<Long> inclusiveRestaurantIds = new ArrayList<>();
        inclusiveRestaurantIds.add(9993L);
        List<Long> exclusiveRestaurantIds = new ArrayList<>();
        exclusiveRestaurantIds.add(9991L);
        meta = RestaurantDetails.builder().
                inclusiveRestaurants(InclusiveRestaurants.builder().isAllCitiesSelected(false).cityIds(cityIds).partnerTypes(partnerTypes).restaurantIds(inclusiveRestaurantIds)
                        .build())
                .exclusiveRestaurants(ExclusiveRestaurants.builder().restaurantIds(exclusiveRestaurantIds).build())
                .build();

        user = User.builder().email("test@swiggy.in").build();

        expectRestaurantIds = new ArrayList<>();
        expectRestaurantIds.add(9993L);
    }

    @Test
    public void rccRemoveActionSuccessTest() throws IOException {
        Mockito.when(restaurantResolverAction.getRestaurants(Mockito.any(RestaurantDetails.class))).thenReturn(expectRestaurantIds);
        Mockito.doNothing().when(rccCommonActionHandler).sendRequestToRcc(expectRestaurantIds, false, user.email, eventId);
        boolean response = rccRemoveActionHandler.handler(meta, user, eventId);
        Assert.assertTrue(response);

    }

    @Test
    public void rccRemoveActionWithEmptyRestaurantIdsTest() throws IOException {
        Mockito.when(restaurantResolverAction.getRestaurants(Mockito.any(RestaurantDetails.class))).thenReturn(new ArrayList<>());
        Mockito.doNothing().when(rccCommonActionHandler).sendRequestToRcc(expectRestaurantIds, false, user.email, eventId);
        boolean response = rccRemoveActionHandler.handler(meta, user, eventId);
        Assert.assertTrue(response);
    }

    @Test
    public void rccRemoveActionExceptionTest() throws IOException {
        try {
            Mockito.when(restaurantResolverAction.getRestaurants(Mockito.any(RestaurantDetails.class))).thenThrow(new ActionHandlerException("action handler exception"));
            Mockito.doNothing().when(rccCommonActionHandler).sendRequestToRcc(expectRestaurantIds, false, user.email, eventId);
            boolean response = rccRemoveActionHandler.handler(meta, user, eventId);
            Assert.assertTrue(response);
        } catch (ActionHandlerException ex) {
            Assert.assertEquals(ex.getMessage(), "action handler exception");
        }
    }

}
