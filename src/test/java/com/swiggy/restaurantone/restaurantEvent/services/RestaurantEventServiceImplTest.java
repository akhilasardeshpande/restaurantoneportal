package com.swiggy.restaurantone.restaurantEvent.services;

import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.restaurantEvent.dao.EventDao;
import com.swiggy.restaurantone.restaurantEvent.entity.EventEntity;
import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventStatusEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantEventRequest;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantUpdateEventRequest;
import com.swiggy.restaurantone.restaurantEvent.pojos.response.RestaurantEventDetails;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.ExclusiveRestaurants;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.InclusiveRestaurants;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.RestaurantDetails;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.*;

@RunWith(MockitoJUnitRunner.class)
public class RestaurantEventServiceImplTest {

    @Mock
    private Set<EventTypeEnum> undoEventSet;

    @Mock
    private EventDao eventDao;

    @Mock
    private AsyncEventHandler asyncEventHandler;

    @Mock
    private EnumMap<EventTypeEnum, EnumMap<ActionEnum, ActionEnum>> undoActionEnumMap;

    @Mock
    private AuthorizationHelper authorizationHelper;

    private RestaurantEventRequest restaurantEventRequest;

    private User user;

    @InjectMocks
    private RestaurantEventServiceImpl restaurantEventService;

    private RestaurantUpdateEventRequest restaurantUpdateEventRequest;

    private Long eventId;


    @Before
    public void setUp() {

        eventId = 123L;
        List<Integer> cityIds = new ArrayList<>();
        cityIds.add(2);
        List<Integer> partnerTypes = new ArrayList<>();
        partnerTypes.add(2);
        List<Long> inclusiveRestaurantIds = new ArrayList<>();
        inclusiveRestaurantIds.add(9990L);
        List<Long> exclusiveRestaurantIds = new ArrayList<>();
        exclusiveRestaurantIds.add(9991L);
        RestaurantDetails restaurantDetails = RestaurantDetails.builder().
                inclusiveRestaurants(InclusiveRestaurants.builder().isAllCitiesSelected(false).cityIds(cityIds).partnerTypes(partnerTypes).restaurantIds(inclusiveRestaurantIds)
                        .build())
                .exclusiveRestaurants(ExclusiveRestaurants.builder().restaurantIds(exclusiveRestaurantIds).build())
                .build();

        restaurantEventRequest = RestaurantEventRequest.builder()
                .type(EventTypeEnum.RCC)
                .action(ActionEnum.ADD)
                .description("add to rcc")
                .meta(restaurantDetails)
                .build();

        user = User.builder().email("test@swiggy.in").build();

        restaurantUpdateEventRequest = RestaurantUpdateEventRequest.builder().action(ActionEnum.UNDO).build();

    }

    @Test
    public void createRestaurantEventSuccessTest() {

        EventEntity eventEntity = EventEntity.builder()
                .id(eventId).action(ActionEnum.ADD.getValue())
                .status(EventStatusEnum.SUCCESS.name())
                .undoAllowed(true)
                .type(EventTypeEnum.RCC.getValue())
                .build();

        Mockito.when(undoEventSet.contains(restaurantEventRequest.getType())).thenReturn(true);
        Mockito.when(authorizationHelper.fetchUser()).thenReturn(user);
        Mockito.when(eventDao.save(Mockito.any(EventEntity.class))).thenReturn(eventEntity);
        Mockito.doNothing().when(asyncEventHandler).createEvent(Mockito.any(), Mockito.anyBoolean(), Mockito.any(), Mockito.any());
        Long responseEventId = restaurantEventService.createRestaurantEvent(restaurantEventRequest);
        Assert.assertEquals(responseEventId, eventId);
    }

    @Test
    public void createRestaurantEventFailureTest() {

        Mockito.when(undoEventSet.contains(restaurantEventRequest.getType())).thenReturn(true);
        Mockito.when(authorizationHelper.fetchUser()).thenReturn(user);
        Mockito.when(eventDao.save(Mockito.any(EventEntity.class))).thenReturn(null);
        Mockito.doNothing().when(asyncEventHandler).createEvent(Mockito.any(), Mockito.anyBoolean(), Mockito.any(), Mockito.any());
        Long responseEventId = restaurantEventService.createRestaurantEvent(restaurantEventRequest);
        Assert.assertNull(responseEventId);
    }

    @Test
    public void getEventDetailsByIdSuccessTest() {
        EventEntity eventEntity = EventEntity.builder()
                .id(eventId).action(ActionEnum.ADD.getValue())
                .status(EventStatusEnum.SUCCESS.name())
                .undoAllowed(true)
                .type(EventTypeEnum.RCC.getValue())
                .build();
        Mockito.when(eventDao.findById(Mockito.anyLong())).thenReturn(eventEntity);
        Long responseEventId = restaurantEventService.getEventDetailsById(eventId).getId();
        Assert.assertEquals(responseEventId, eventId);
    }

    @Test
    public void updateEventWithActionSuccessTest() {

        EventEntity eventEntity = EventEntity.builder()
                .id(eventId).action(ActionEnum.ADD.getValue())
                .status(EventStatusEnum.SUCCESS.name())
                .undoAllowed(true)
                .type(EventTypeEnum.RCC.getValue())
                .build();

        EnumMap<ActionEnum, ActionEnum> actionEnumMap = new EnumMap<>(ActionEnum.class);
        actionEnumMap.put(ActionEnum.ADD, ActionEnum.REMOVE);

        Mockito.when(eventDao.findById(eventId)).thenReturn(eventEntity);
        Mockito.when(authorizationHelper.fetchUser()).thenReturn(user);
        Mockito.when(undoActionEnumMap.get(EventTypeEnum.RCC)).thenReturn(actionEnumMap);
        Mockito.doNothing().when(asyncEventHandler).updateEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());
        boolean response = restaurantEventService.updateEventWithAction(restaurantUpdateEventRequest, eventId);
        Assert.assertTrue(response);
    }

    @Test
    public void updateEventWithActionFailureTest() {
        EventEntity eventEntity = EventEntity.builder()
                .id(eventId).action(ActionEnum.ADD.getValue())
                .status(EventStatusEnum.SUCCESS.name())
                .undoAllowed(false)
                .type(EventTypeEnum.RCC.getValue())
                .build();

        Mockito.when(eventDao.findById(eventId)).thenReturn(eventEntity);
        Mockito.when(authorizationHelper.fetchUser()).thenReturn(user);
        Mockito.doNothing().when(asyncEventHandler).updateEvent(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any());
        boolean response = restaurantEventService.updateEventWithAction(restaurantUpdateEventRequest, eventId);
        Assert.assertFalse(response);
    }

    @Test
    public void getEventDetailsByTypeSuccessTest() {
        EventEntity eventEntity = EventEntity.builder()
                .id(eventId).action(ActionEnum.ADD.getValue())
                .status(EventStatusEnum.SUCCESS.name())
                .undoAllowed(true)
                .type(EventTypeEnum.RCC.getValue())
                .build();

        List<EventEntity> eventEntityList = new ArrayList<>();
        eventEntityList.add(eventEntity);

        LocalDateTime fromTime = LocalDateTime.now();
        LocalDateTime toTime = LocalDateTime.now().plusDays(2);

        Mockito.when(eventDao.findAllByTypeAndCreatedAtBetween(EventTypeEnum.RCC.getValue(), fromTime, toTime)).thenReturn(eventEntityList);
        List<RestaurantEventDetails> response = restaurantEventService.getEventDetailsByType(EventTypeEnum.RCC, fromTime, toTime);
        Assert.assertEquals(response.get(0).getId(), eventId);
    }
}
