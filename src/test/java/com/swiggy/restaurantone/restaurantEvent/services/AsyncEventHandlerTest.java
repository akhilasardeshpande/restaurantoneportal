package com.swiggy.restaurantone.restaurantEvent.services;

import com.swiggy.commons.Json;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.restaurantEvent.dao.EventDao;
import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.restaurantEvent.factory.EventHandlerFactory;
import com.swiggy.restaurantone.restaurantEvent.handlers.eventHandler.RccEventHandler;
import com.swiggy.restaurantone.restaurantEvent.instrumentation.InstrumentServiceImpl;
import com.swiggy.restaurantone.restaurantEvent.pojos.businessObject.EventBo;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantEventRequest;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantUpdateEventRequest;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.ExclusiveRestaurants;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.InclusiveRestaurants;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.RestaurantDetails;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class AsyncEventHandlerTest {

    @Mock
    private EventHandlerFactory eventHandlerFactory;

    @Mock
    private InstrumentServiceImpl instrumentService;

    @Mock
    private EventDao eventDao;

    @Mock
    private RccEventHandler rccEventHandler;

    @InjectMocks
    private AsyncEventHandler asyncEventHandler;

    private RestaurantEventRequest restaurantEventRequest;

    private EventBo eventBo;

    private Long eventId;

    @Before
    public void setUp() {
        eventId = 123L;
        List<Integer> cityIds = new ArrayList<>();
        cityIds.add(2);
        List<Integer> partnerTypes = new ArrayList<>();
        partnerTypes.add(2);
        List<Long> inclusiveRestaurantIds = new ArrayList<>();
        inclusiveRestaurantIds.add(9990L);
        List<Long> exclusiveRestaurantIds = new ArrayList<>();
        inclusiveRestaurantIds.add(9991L);
        RestaurantDetails restaurantDetails = RestaurantDetails.builder().
                inclusiveRestaurants(InclusiveRestaurants.builder().isAllCitiesSelected(false).cityIds(cityIds).partnerTypes(partnerTypes).restaurantIds(inclusiveRestaurantIds)
                        .build())
                .exclusiveRestaurants(ExclusiveRestaurants.builder().restaurantIds(exclusiveRestaurantIds).build())
                .build();

        restaurantEventRequest = RestaurantEventRequest.builder()
                .type(EventTypeEnum.RCC)
                .action(ActionEnum.ADD)
                .description("add to rcc")
                .meta(restaurantDetails)
                .build();

        eventBo = EventBo.builder().meta(Json.serialize(restaurantDetails)).undoAllowed(true).build();
    }

    @Test
    public void createEventSuccess() {
        Object meta = restaurantEventRequest.getMeta();
        User user = User.builder().build();

        Mockito.when(eventHandlerFactory.getEventHandler(EventTypeEnum.RCC)).thenReturn(rccEventHandler);
        Mockito.when(rccEventHandler.handler(meta, ActionEnum.ADD, user, eventId)).thenReturn(true);
        Mockito.when(eventDao.updateStatus(Mockito.anyLong(), Mockito.anyBoolean(), Mockito.anyString(), Mockito.anyString(), Mockito.any(LocalDateTime.class))).thenReturn(1);
        asyncEventHandler.createEvent(restaurantEventRequest, true, eventId, user);
        Mockito.verify(instrumentService, Mockito.times(1))
                .doInstrument(Mockito.any(RestaurantEventRequest.class), Mockito.anyBoolean(), Mockito.anyLong(), Mockito.anyString(), Mockito.anyString());
    }

    @Test
    public void createEventFailure() {
        Object meta = restaurantEventRequest.getMeta();
        User user = User.builder().build();

        Mockito.when(eventHandlerFactory.getEventHandler(EventTypeEnum.RCC)).thenReturn(rccEventHandler);
        Mockito.when(rccEventHandler.handler(meta, ActionEnum.ADD, user, eventId)).thenReturn(false);
        Mockito.when(eventDao.updateStatus(Mockito.anyLong(), Mockito.anyBoolean(), Mockito.anyString(), Mockito.anyString(), Mockito.any(LocalDateTime.class))).thenReturn(0);
        asyncEventHandler.createEvent(restaurantEventRequest, true, eventId, user);
        Mockito.verify(instrumentService, Mockito.times(1))
                .doInstrument(Mockito.any(RestaurantEventRequest.class), Mockito.anyBoolean(), Mockito.anyLong(), Mockito.anyString(), Mockito.anyString());
    }

    @Test
    public void updateEventSuccess() {
        User user = User.builder().build();
        Object meta = Json.deserialize(eventBo.getMeta(), Object.class);
        RestaurantUpdateEventRequest restaurantUpdateEventRequest = RestaurantUpdateEventRequest.builder().action(ActionEnum.UNDO).build();

        Mockito.when(eventHandlerFactory.getEventHandler(EventTypeEnum.RCC)).thenReturn(rccEventHandler);
        Mockito.when(rccEventHandler.handler(meta, ActionEnum.REMOVE, user, eventId)).thenReturn(true);
        Mockito.when(eventDao.updateStatus(Mockito.anyLong(), Mockito.anyBoolean(), Mockito.anyString(), Mockito.anyString(), Mockito.any(LocalDateTime.class))).thenReturn(1);
        asyncEventHandler.updateEvent(restaurantUpdateEventRequest, eventId, eventBo, EventTypeEnum.RCC, ActionEnum.REMOVE, user);
        Mockito.verify(instrumentService, Mockito.times(1))
                .doInstrument(Mockito.any(EventBo.class), Mockito.any(ActionEnum.class), Mockito.anyString(), Mockito.anyBoolean(), Mockito.anyString());
    }

    @Test
    public void updateEventFailure() {
        User user = User.builder().build();
        Object meta = Json.deserialize(eventBo.getMeta(), Object.class);
        RestaurantUpdateEventRequest restaurantUpdateEventRequest = RestaurantUpdateEventRequest.builder().action(ActionEnum.UNDO).build();

        Mockito.when(eventHandlerFactory.getEventHandler(EventTypeEnum.RCC)).thenReturn(rccEventHandler);
        Mockito.when(rccEventHandler.handler(meta, ActionEnum.REMOVE, user, eventId)).thenReturn(false);
        Mockito.when(eventDao.updateStatus(Mockito.anyLong(), Mockito.anyBoolean(), Mockito.anyString(), Mockito.anyString(), Mockito.any(LocalDateTime.class))).thenReturn(1);
        asyncEventHandler.updateEvent(restaurantUpdateEventRequest, eventId, eventBo, EventTypeEnum.RCC, ActionEnum.REMOVE, user);
        Mockito.verify(instrumentService, Mockito.times(1))
                .doInstrument(Mockito.any(EventBo.class), Mockito.any(ActionEnum.class), Mockito.anyString(), Mockito.anyBoolean(), Mockito.anyString());
    }
}
