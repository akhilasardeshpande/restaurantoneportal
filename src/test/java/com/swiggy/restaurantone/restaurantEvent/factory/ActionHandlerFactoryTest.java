package com.swiggy.restaurantone.restaurantEvent.factory;

import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.rcc.RccAddActionHandler;
import com.swiggy.restaurantone.restaurantEvent.services.ActionHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.EnumMap;

@RunWith(MockitoJUnitRunner.class)
public class ActionHandlerFactoryTest {

    @Mock
    EnumMap<EventTypeEnum, EnumMap<ActionEnum, ActionHandler>> eventActionHandlerMap;

    @InjectMocks
    private ActionHandlerFactory actionHandlerFactory;

    @Mock
    private RccAddActionHandler rccAddActionHandler;

    @Test
    public void getActionHandlerTest() {
        EnumMap<ActionEnum, ActionHandler> actionHandlerEnumMap = new EnumMap<>(ActionEnum.class);
        actionHandlerEnumMap.put(ActionEnum.ADD, rccAddActionHandler);
        Mockito.when(eventActionHandlerMap.get(EventTypeEnum.RCC)).thenReturn(actionHandlerEnumMap);
        ActionHandler actionHandler = actionHandlerFactory.getActionHandler(EventTypeEnum.RCC, ActionEnum.ADD);
        Assert.assertEquals(actionHandler, rccAddActionHandler);
    }
}
