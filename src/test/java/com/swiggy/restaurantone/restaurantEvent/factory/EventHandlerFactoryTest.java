package com.swiggy.restaurantone.restaurantEvent.factory;

import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.restaurantEvent.handlers.eventHandler.RccEventHandler;
import com.swiggy.restaurantone.restaurantEvent.services.EventHandler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.EnumMap;

@RunWith(MockitoJUnitRunner.class)
public class EventHandlerFactoryTest {

    @Mock
    private EnumMap<EventTypeEnum, EventHandler> eventEventHandlerEnumMap;

    @Mock
    private RccEventHandler rccEventHandler;

    @InjectMocks
    private EventHandlerFactory eventHandlerFactory;

    @Test
    public void getEventHandlerTest() {
        Mockito.when(eventEventHandlerEnumMap.get(EventTypeEnum.RCC)).thenReturn(rccEventHandler);
        EventHandler eventHandler = eventHandlerFactory.getEventHandler(EventTypeEnum.RCC);
        Assert.assertEquals(eventHandler, rccEventHandler);
    }
}
