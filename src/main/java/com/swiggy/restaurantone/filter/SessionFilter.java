package com.swiggy.restaurantone.filter;

import com.swiggy.restaurantone.helper.AuthorizationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by balamuneeswaran.s on 1/25/17.
 */
@Component
public class SessionFilter implements Filter {

    @Autowired
    private AuthorizationHelper authorizationHelper;

    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;

        if (!(httpRequest.getRequestURI().contains("rest-one/login")
                || httpRequest.getRequestURI().contains("health-check")
                || httpRequest.getRequestURI().contains("metrics")
                || httpRequest.getMethod().equals("OPTIONS")
                || authorizationHelper.authenticateUserSession())) {
            httpResponse.sendError(401, "unauthorized");
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    public void destroy() {

    }
}
