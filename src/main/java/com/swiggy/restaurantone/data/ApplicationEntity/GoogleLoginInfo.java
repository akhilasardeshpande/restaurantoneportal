package com.swiggy.restaurantone.data.ApplicationEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by rajnishc on 1/27/17.
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GoogleLoginInfo implements Serializable {
    private String code;
}
