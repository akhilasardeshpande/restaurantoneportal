package com.swiggy.restaurantone.data.ApplicationEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by rajnishc on 1/27/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class LoginUserInfo implements Serializable {
    private String authType;
    private String properties;
}
