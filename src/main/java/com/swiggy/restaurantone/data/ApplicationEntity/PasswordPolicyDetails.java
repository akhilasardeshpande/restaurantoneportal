package com.swiggy.restaurantone.data.ApplicationEntity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by rajnishc on 8/17/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PasswordPolicyDetails implements Serializable{
    @JsonProperty(value = "regex")
    private String regex;

    @JsonProperty(value = "policy")
    private PasswordPolicy passwordPolicy;
}
