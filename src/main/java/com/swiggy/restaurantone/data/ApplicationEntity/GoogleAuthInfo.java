package com.swiggy.restaurantone.data.ApplicationEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by balamuneeswaran.s on 1/5/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GoogleAuthInfo implements Serializable {
    private String refreshToken;
    private String accessToken;
}
