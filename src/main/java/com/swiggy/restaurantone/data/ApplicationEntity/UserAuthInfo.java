package com.swiggy.restaurantone.data.ApplicationEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by rajnishc on 1/20/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserAuthInfo implements Serializable {
    private String email;
    private String authProperties;
}
