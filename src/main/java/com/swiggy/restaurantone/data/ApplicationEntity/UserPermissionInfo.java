package com.swiggy.restaurantone.data.ApplicationEntity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by rajnishc on 8/4/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserPermissionInfo implements Serializable {
    @JsonProperty("permission_id")
    @NotNull
    private Long permissionId;
}
