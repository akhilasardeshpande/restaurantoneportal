package com.swiggy.restaurantone.data.ApplicationEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by rajnishc on 9/4/17.
 */
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RmsUserAuthInfo {
    @NotNull
    String email;

    @NotNull
    List<Long> restaurants;
}