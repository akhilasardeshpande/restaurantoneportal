package com.swiggy.restaurantone.data.ApplicationEntity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by rajnishc on 8/9/17.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PasswordPolicy implements Serializable {
    @JsonProperty("min")
    private Long minLength;

    @JsonProperty("max")
    private Long maxLength;

    @JsonProperty("uppercase")
    private Long uppercaseCount;

    @JsonProperty("lowercase")
    private Long lowercaseCount;

    @JsonProperty("digits")
    private Long numbersCount;

    @JsonProperty("special")
    private Long specialCharactersCount;

}
