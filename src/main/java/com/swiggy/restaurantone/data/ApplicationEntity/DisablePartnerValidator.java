package com.swiggy.restaurantone.data.ApplicationEntity;

import com.swiggy.restaurantone.data.businessEntity.PartnerOrRestaurantDisableRequest;
import org.springframework.util.ObjectUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;

/**
 * Created by rajnish.chandra on 2019-05-08
 */
public class DisablePartnerValidator implements
        ConstraintValidator<ValidPartnerDisableRequestAnnotation, PartnerOrRestaurantDisableRequest> {
    
    @Override
    public void initialize(ValidPartnerDisableRequestAnnotation constraintAnnotation) {
    
    }
    
    @Override
    public boolean isValid(PartnerOrRestaurantDisableRequest value, ConstraintValidatorContext context) {
        // validation logic
        String message = "";
        boolean result = true;
        if (ObjectUtils.isEmpty(value.getRests()) && ObjectUtils.isEmpty(value.getPartnerId())) {
            message = "Partner ids and restaurant ids both can't be empty.";
            result = false;
        } else if (!ObjectUtils.isEmpty(value.getRests()) && !ObjectUtils.isEmpty(value.getPartnerId())) {
            message = "Partner ids and restaurant ids both can't be populated.";
            result = false;
        }
        
        if (!ObjectUtils.isEmpty(value.getPartnerId()) && value.getPartnerId() < 4) {
            message = "Partner ids should be greater than 4.";
            result = false;
        } else if (!ObjectUtils.isEmpty(value.getPartnerId()) && value.getPartnerId() > 200) {
            message = "Partner ids should be lesser than 200.";
            result = false;
        }
        
        if (value.getToTime().isBefore(LocalDateTime.now())) {
            message += "To time should be after current time.";
            result = false;
        }
        
        if (value.getToTime().isEqual(value.getFromTime()) ||
                value.getToTime().isBefore(value.getFromTime())) {
            message += "To time should be greater than from time.";
            result = false;
        }
    
        if (!message.isEmpty()) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate( message ).addConstraintViolation();
        }
        return result;
    }
}
