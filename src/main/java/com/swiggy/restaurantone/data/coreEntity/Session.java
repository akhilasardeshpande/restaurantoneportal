package com.swiggy.restaurantone.data.coreEntity;

import lombok.*;

import java.util.UUID;

/**
 * Created by balamuneeswaran.s on 1/5/17.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Session {
    private Long userId;
    private UUID sessionId;
    private String authDetails;
    private Long tokenExpirationTime;
    private String authType;
    private Long sessionExpiration;
}