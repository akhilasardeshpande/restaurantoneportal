package com.swiggy.restaurantone.data.coreEntity;

import com.swiggy.rest.entity.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by rajnishc on 5/5/17.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Audited
@Table(name = "city")
public class CityEntity extends BaseEntity {
    @Id
    public Long id;
    public String name;
}
