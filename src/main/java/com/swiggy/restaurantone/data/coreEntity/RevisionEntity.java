package com.swiggy.restaurantone.data.coreEntity;

import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;

/**
 * Created by rajnish.chandra on 09/03/19
 */
@MappedSuperclass
public class RevisionEntity implements Serializable {
        private static final long serialVersionUID = 8530213963961662300L;
        
        @Id
        @GeneratedValue
        @RevisionNumber
        private long id;
        
        @RevisionTimestamp
        private long timestamp;
        
        public long getId() {
            return id;
        }
        
        public void setId(long id) {
            this.id = id;
        }
        
        @Transient
        public Date getRevisionDate() {
            return new Date( timestamp );
        }
        
        public long getTimestamp() {
            return timestamp;
        }
        
        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }
        
        @Override
        public boolean equals(Object o) {
            if ( this == o ) {
                return true;
            }
            if ( !(o instanceof RevisionEntity) ) {
                return false;
            }
            
            final RevisionEntity that = (RevisionEntity) o;
            return id == that.id
                    && timestamp == that.timestamp;
        }
        
        @Override
        public String toString() {
            return "DefaultRevisionEntity(id = " + id
                    + ", revisionDate = " + DateFormat.getDateTimeInstance().format( getRevisionDate() ) + ")";
        }
}
