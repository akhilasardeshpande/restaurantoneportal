package com.swiggy.restaurantone.data.coreEntity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.swiggy.restaurantone.utils.LocalDateTimeDeserializer;
import com.swiggy.restaurantone.utils.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by rajnish.chandra on 20/02/19
 */
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "partner_disable_runinfo")
public class PartnerDisableRunInfoEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @Column(name = "partner_id")
    private Integer partnerId;
    
    @Column(name = "from_time")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Type(
            type = "com.swiggy.restaurantone.data.coreEntity.LocalDateTimeType"
    )
    private LocalDateTime fromTime;
    
    @Column(name = "to_time")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Type(
            type = "com.swiggy.restaurantone.data.coreEntity.LocalDateTimeType"
    )
    private LocalDateTime toTime;
    
    @Column(name = "reason")
    private String reason;

    @Column(name = "disposition_id")
    private Long dispositionId;
    
    @Column(name = "total_restaurants")
    private Integer totalRestaurants;
    
    @Column(name = "active_restaurants")
    private Integer activeRestaurants;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "updated_by")
    private String updatedBy;
    
    @Column(name = "created_at")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Type(
            type = "com.swiggy.restaurantone.data.coreEntity.LocalDateTimeType"
    )
    private LocalDateTime createdAt;
    
    @Column(name = "updated_at")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Type(
            type = "com.swiggy.restaurantone.data.coreEntity.LocalDateTimeType"
    )
    private LocalDateTime updatedAt;
    
    @Column(name = "status_message")
    private String statusMessage;
    
    @Column(name = "status_code")
    private int statusCode;
    
    @Column(name = "is_active")
    private boolean isActive = false;
}