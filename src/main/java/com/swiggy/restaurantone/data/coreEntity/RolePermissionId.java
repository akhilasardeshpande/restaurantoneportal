package com.swiggy.restaurantone.data.coreEntity;

import java.io.Serializable;

/**
 * Created by rajnishc on 5/14/17.
 */
public class RolePermissionId implements Serializable {
    private Long roleId;
    private Long permissionId;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((roleId == null) ? 0 : roleId.hashCode());
        result = prime * result + ((permissionId == null) ? 0 : permissionId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RolePermissionId other = (RolePermissionId) obj;
        if (roleId == null) {
            if (other.roleId != null)
                return false;
        } else if (!roleId.equals(other.roleId))
            return false;
        if (permissionId != other.permissionId)
            return false;
        return true;
    }
}
