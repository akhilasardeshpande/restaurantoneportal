package com.swiggy.restaurantone.data.coreEntity;

import com.swiggy.rest.entity.BaseEntity;
import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by balamuneeswaran.s on 1/3/17.
 */
@Entity
@Table(name = "restaurantUserMap")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Audited
@Builder
public class RestaurantUserMapEntity extends BaseEntity {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Long id;

    public Long restaurantId;

    public Long userId;

    @Column(name = "created_at", nullable = true, updatable = false)
    protected Date createdAt = new Date();

    @Column(name = "updated_at", nullable = true)
    protected Date updatedAt = new Date();

    @Column(name = "created_by", nullable = false, updatable = false)
    protected String createdBy = "";

    @Column(name = "updated_by")
    protected String updatedBy = "";

    @PrePersist
    protected void onCreate() {
        updatedAt = createdAt = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updatedAt = new java.util.Date();
    }
}

