package com.swiggy.restaurantone.data.coreEntity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by balamuneeswaran.s on 1/5/17.
 */
@Entity
@Table(name = "session")
@Getter
@Setter
public class SessionEntity {
    @javax.persistence.Id
    public Long Id;
    public Long UserId;
    public String Properties;
}
