package com.swiggy.restaurantone.data.coreEntity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.swiggy.rest.entity.CoreEntity;
import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by balamuneeswaran.s on 1/3/17.
 */

@Entity
@Table(name = "user")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Audited
public class UserEntity extends CoreEntity {
    @Column(updatable = false)
    public String name;
    public String contactNumber;
    @Column(updatable = false, unique = true)
    public String email;
    public Long role;
    public String properties;

    @ManyToMany(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    @JoinTable(name = "user_permission",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"))
    @JsonBackReference(value = "users")
    public List<PermissionEntity> permissions;
    @Column(name = "team_id")
    public Long teamId;
    public Date doj;
    public Date dob;
    @Column(name = "city")
    public Long cityId;
}
