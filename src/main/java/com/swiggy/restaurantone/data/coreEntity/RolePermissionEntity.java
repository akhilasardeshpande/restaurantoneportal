package com.swiggy.restaurantone.data.coreEntity;

import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;

/**
 * Created by rajnishc on 5/14/17.
 */
@Entity
@Table(name = "role_permission")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Audited
@IdClass(RolePermissionId.class)
public class RolePermissionEntity {
    @Id
    @Column(name = "role_id")
    Long roleId;

    @Id
    @Column(name = "permission_id")
    Long permissionId;
}
