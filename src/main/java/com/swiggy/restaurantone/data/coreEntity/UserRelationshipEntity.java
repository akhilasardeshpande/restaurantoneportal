package com.swiggy.restaurantone.data.coreEntity;

import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by balamuneeswaran.s on 1/4/17.
 */
@Entity
@Table(name = "userRelationship")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Audited
@IdClass(UserRelationshipId.class)
public class UserRelationshipEntity {
    @Id
    public Long userId;

    @Id
    public Long childUserId;

    @Column(name = "is_direct_parent", nullable = false, updatable = false)
    public boolean isDirectParent;

    @Column(name = "created_at", nullable = true, updatable = false)
    protected Date createdAt = new Date();

    @Column(name = "updated_at", nullable = true)
    protected Date updatedAt = new Date();

    @Column(name = "created_by", nullable = false, updatable = false)
    protected String createdBy = "";

    @Column(name = "updated_by")
    protected String updatedBy = "";

    @PrePersist
    protected void onCreate() {
        updatedAt = createdAt = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updatedAt = new java.util.Date();
    }
}
