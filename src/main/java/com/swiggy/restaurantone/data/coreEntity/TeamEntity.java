package com.swiggy.restaurantone.data.coreEntity;

import com.swiggy.rest.entity.BaseEntity;
import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by rajnishc on 5/5/17.
 */
@Entity
@Table(name = "team")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Audited
public class TeamEntity extends BaseEntity{
    public String name;
}
