package com.swiggy.restaurantone.data.coreEntity;
import org.hibernate.envers.RevisionListener;

/**
 * Created by rajnish.chandra on 09/03/19
 */
public class CustomRevisionListener implements RevisionListener {
    
    @Override
    public void newRevision(Object revisionEntity) {
    }
}