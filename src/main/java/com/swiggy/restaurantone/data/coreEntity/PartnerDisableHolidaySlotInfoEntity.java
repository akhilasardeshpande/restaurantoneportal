package com.swiggy.restaurantone.data.coreEntity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.swiggy.restaurantone.utils.LocalDateTimeDeserializer;
import com.swiggy.restaurantone.utils.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by rajnish.chandra on 20/02/19
 */
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Table(name = "partner_disable_holiday_slot_info")
public class PartnerDisableHolidaySlotInfoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "runinfo_id")
    private Long runInfoId;
    
    @Column(name = "restaurant_id")
    private Long restaurantId;
    
    @Column(name = "holiday_slot_id")
    private Long holidaySlotId;
    
    @Column(name = "from_time")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Type(
            type = "com.swiggy.restaurantone.data.coreEntity.LocalDateTimeType"
    )
    private LocalDateTime fromTime;
    
    @Column(name = "to_time")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Type(
            type = "com.swiggy.restaurantone.data.coreEntity.LocalDateTimeType"
    )
    private LocalDateTime toTime;
    
    @Column(name = "updated_at")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Type(
            type = "com.swiggy.restaurantone.data.coreEntity.LocalDateTimeType"
    )
    private LocalDateTime updatedAt;
    
    @Column(name = "updated_by")
    private String updatedBy;
    
    @Column(name = "created_at")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Type(
            type = "com.swiggy.restaurantone.data.coreEntity.LocalDateTimeType"
    )
    private LocalDateTime createdAt;
    
    @Column(name = "created_by")
    private String createdBy;
    
    @Column(name = "is_active")
    private Boolean isActive;
    
    @Column(name = "retry")
    private int retryCount = 0;
    
    @Column(name = "error_code")
    private int errorCode;
}