package com.swiggy.restaurantone.data.coreEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by rajnish.chandra on 20/06/18
 */
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Table(name = "rms_users_audit")
public class RMSUserEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    Long id;
    
    @Column(name = "rms_id")
    Long rmsId;
    
    @Column(name = "user_id")
    Long userId;
    
    @Column(name = "mobile_no")
    String mobileNo;
    
    String name;
    
    @Column(name = "created_by")
    String createdBy;
    
    @Column(name = "updated_by")
    String updatedBy;
    
    @Column(name = "is_active")
    Boolean isActive;
    
    @Column(name = "is_primary")
    Boolean isPrimary;
    
    @Column(name = "registration_complete")
    Boolean registrationComplete;
    
    @Column(name = "is_editable")
    Boolean isEditable;
    
    @Column(name = "role_id")
    Long roleId;
    
    @Column(name = "activation_date")
    Date activationDate;
    
    @Column(name = "deactivation_date")
    Date deactivationDate;
    
    @Column(name = "created_at")
    Date createdAt;
    
    @Column(name = "updated_at")
    Date updatedAt;
}
