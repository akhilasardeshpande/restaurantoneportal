package com.swiggy.restaurantone.data.coreEntity;


import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by rajnish.chandra on 09/03/19
 */

@Entity
@RevisionEntity(CustomRevisionListener.class)
@Table(name = "TBL_REVINFO")
public class CustomRevisionEntity extends DefaultRevisionEntity {
    private static final long serialVersionUID = 1L;
}
