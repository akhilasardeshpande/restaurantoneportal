package com.swiggy.restaurantone.data.coreEntity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.swiggy.rest.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by shahbaz.khalid on 3/9/17.
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Audited
@Table(name = "Permission")
public class PermissionEntity extends BaseEntity {
    public String permission;
    @ManyToMany(mappedBy = "permissions")
    @JsonManagedReference(value = "users")
    @JsonIgnore
    public List<UserEntity> users;

    @ManyToMany(mappedBy = "permissions")
    @JsonManagedReference(value = "permissions")
    @JsonIgnore
    public List<RoleEntity> roles;
}
