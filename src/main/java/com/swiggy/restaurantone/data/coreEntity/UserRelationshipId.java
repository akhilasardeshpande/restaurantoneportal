package com.swiggy.restaurantone.data.coreEntity;

import java.io.Serializable;

/**
 * Created by balamuneeswaran.s on 1/12/17.
 */
public class UserRelationshipId implements Serializable {

    private Long userId;
    private Long childUserId;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((userId == null) ? 0 : userId.hashCode());
        result = prime * result + ((childUserId == null) ? 0 : childUserId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserRelationshipId other = (UserRelationshipId) obj;
        if (userId == null) {
            if (other.userId != null)
                return false;
        } else if (!userId.equals(other.userId))
            return false;
        if (childUserId != other.childUserId)
            return false;
        return true;
    }
}