package com.swiggy.restaurantone.data.coreEntity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by balamuneeswaran.s on 1/3/17.
 */
@Getter
@Setter
public class UserRestaurantId implements Serializable {

    private Long userId;
    private Long restaurantId;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((userId == null) ? 0 : userId.hashCode());
        result = prime * result + ((restaurantId == null) ? 0 : restaurantId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UserRestaurantId other = (UserRestaurantId) obj;
        if (userId == null) {
            if (other.userId != null)
                return false;
        } else if (!userId.equals(other.userId))
            return false;
        if (restaurantId != other.restaurantId)
            return false;
        return true;
    }
}
