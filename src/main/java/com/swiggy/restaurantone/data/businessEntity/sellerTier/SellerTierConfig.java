package com.swiggy.restaurantone.data.businessEntity.sellerTier;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.Date;
import java.util.List;

/**
 * Created by shahbaz.khalid on 6/23/17.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SellerTierConfig {

    private String startDate;
    private String endDate;
    private String goLiveDate;
    private String refreshDate;
    private String uploadedBy;
    private List<SellerTierData> data;
}