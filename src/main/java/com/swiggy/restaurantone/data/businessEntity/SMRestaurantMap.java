package com.swiggy.restaurantone.data.businessEntity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Created by rajnishc on 5/29/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class SMRestaurantMap {
    @JsonProperty(value = "user_email")
    String email;
    @JsonProperty(value = "restaurant_id")
    Long restaurantId;
}
