package com.swiggy.restaurantone.data.businessEntity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.Date;

/**
 * Created by rajnish.chandra on 21/06/18
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class RMSUser {
    Long id;
    
    @JsonProperty(value = "user_id")
    Long userId;
    
    @JsonProperty(value = "mobile_no")
    String mobileNo;
    
    String name;
    
    @JsonProperty(value = "created_by")
    String createdBy;
    
    @JsonProperty(value = "updated_by")
    String updatedBy;
    
    @JsonProperty(value = "is_active")
    boolean isActive;
    
    @JsonProperty(value = "is_primary")
    boolean isPrimary;
    
    @JsonProperty(value = "registration_complete")
    boolean registrationComplete;
    
    @JsonProperty(value = "is_editable")
    boolean isEditable;
    
    @JsonProperty(value = "role_id")
    Long roleId;
    
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonProperty(value = "activation_date")
    Date activationDate;
    
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonProperty(value = "deactivation_date")
    Date deactivationDate;
    
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonProperty(value = "created_at")
    Date createdAt;
    
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonProperty(value = "updated_at")
    Date updatedAt;
}
