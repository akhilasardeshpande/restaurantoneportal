package com.swiggy.restaurantone.data.businessEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Max;

/**
 * Created by balamuneeswaran.s on 1/5/17.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Pagination {
    public int pageNumber = 1;
    @Max(500)
    public int size = 100;
    public String sortColumn = "id";
    public boolean isAscending = true;
}
