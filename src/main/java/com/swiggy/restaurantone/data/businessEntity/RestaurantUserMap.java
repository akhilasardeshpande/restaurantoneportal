package com.swiggy.restaurantone.data.businessEntity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiggy.rest.bo.BusinessEntity;
import lombok.*;

import javax.validation.constraints.NotNull;

/**
 * Created by balamuneeswaran.s on 1/15/17.
 */
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@Getter
@Setter
public class RestaurantUserMap extends BusinessEntity {
    @NotNull
    public Long id;
    @NotNull
    public Long restaurantId;
    @NotNull
    public Long userId;

    private String updatedBy;

    @JsonIgnore
    @Override
    public String getUniqueId() {
        return id != null ? id.toString() : null;
    }
}
