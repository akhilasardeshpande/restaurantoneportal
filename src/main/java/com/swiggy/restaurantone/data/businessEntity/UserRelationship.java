package com.swiggy.restaurantone.data.businessEntity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiggy.rest.bo.BusinessEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Created by balamuneeswaran.s on 1/15/17.
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserRelationship extends BusinessEntity {
    @NotNull
    public Long userId;

    @NotNull
    public Long childUserId;

    public String updatedBy;

    @JsonIgnore
    @Override
    public String getUniqueId() {
        final int prime = 31;
        int result = 17;
        result = prime * result
                + ((userId == null) ? 0 : userId.hashCode());
        result = prime * result + ((childUserId == null) ? 0 : childUserId.hashCode());
        return String.valueOf(result);
    }
}
