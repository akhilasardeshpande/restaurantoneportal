package com.swiggy.restaurantone.data.businessEntity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.swiggy.restaurantone.data.ApplicationEntity.ValidPartnerDisableRequestAnnotation;
import com.swiggy.restaurantone.utils.LocalDateTimeDeserializer;
import com.swiggy.restaurantone.utils.LocalDateTimeSerializer;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by rajnish.chandra on 20/02/19
 */
@Data
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@ValidPartnerDisableRequestAnnotation()
public class PartnerOrRestaurantDisableRequest {
    @JsonProperty(value = "partner_id")
    private Integer partnerId;
    
    @JsonProperty(value = "restaurant_ids")
    @Size(max = 80000, message = "Max limit of restaurant ids is 80000")
    private List<Long> rests;
    
    @JsonProperty("from_time")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime fromTime;
    
    @JsonProperty("to_time")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime toTime;

    @JsonProperty("disposition_id")
    private Long dispositionId = 1L;
    
    @JsonProperty(value = "updated_by")
    private String updatedBy;
    
    @JsonProperty(value = "reason")
    private String reason;
    
    @JsonProperty(value = "send_communication")
    private boolean sendComm;
}