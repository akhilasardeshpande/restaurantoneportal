package com.swiggy.restaurantone.data.businessEntity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiggy.rest.bo.BusinessEntity;
import lombok.*;

import javax.validation.constraints.NotNull;

/**
 * Created by rajnishc on 5/5/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class City extends BusinessEntity {
    @NotNull
    public Long id;
    public String name;

    @JsonIgnore
    public String getUniqueId() {
        return this.id != null ? this.id.toString() : null;
    }
}