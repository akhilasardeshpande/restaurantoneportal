package com.swiggy.restaurantone.data.businessEntity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiggy.rest.bo.BusinessEntity;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by rajnishc on 5/12/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class Role extends BusinessEntity{
    public Long id;
    @NotNull
    public String name;
    @NotNull
    public Long level;
    @Valid
    public List<Permission> permissions;

    @JsonIgnore
    public String getUniqueId() {
        return this.id != null ? this.id.toString() : null;
    }
}
