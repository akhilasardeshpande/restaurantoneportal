package com.swiggy.restaurantone.data.businessEntity;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.swiggy.rest.bo.BusinessEntity;
import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;

/**
 * Created by balamuneeswaran.s on 1/3/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class User extends BusinessEntity {
    @NotEmpty
    @Pattern(regexp = "^[a-zA-Z ]*$", message = "Only alphabets are allowed")
    public String name;
    public Long id;
    @NotEmpty
    @Pattern(regexp = "^[0-9]{10}$", message = "Not a valid contact number")
    public String contactNumber;
    @NotNull
    @Pattern(regexp = "^[a-zA-Z0-9._%+-]+(@swiggy.in|@external.swiggy.in|@swiggyosp.in)$", message = "not a valid email")
    public String email;
    @NotNull
    @Valid
    public Role role;
    public String properties;
    public List<Permission> permissions;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Past
    public Date doj;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Past
    public Date dob;
    @NotNull
    public Manager manager;
    @NotNull
    public City city;
    @NotNull
    public Team team;

    private String updatedBy;

    @JsonIgnore
    public String getUniqueId() {
        return this.id != null ? this.id.toString() : null;
    }

}
