package com.swiggy.restaurantone.data.businessEntity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by alok.singhal on 9/27/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class Batch {
        protected Long id;
        protected Long scheduleId;
        protected Long itemId;
        protected Long restaurantId;
        protected String state;
        protected Long expiryTime;
        protected Long confirmationWaitTime;
        protected Long batchSize;
        protected Long countOrdersAssigned;
        protected String ordersList;
        protected Long sequenceId;
        protected String itemImageUrl;
        protected String name;
        protected Long prepTime;
        protected Date placedTimeStamp;
        protected boolean expiryAck;
}
