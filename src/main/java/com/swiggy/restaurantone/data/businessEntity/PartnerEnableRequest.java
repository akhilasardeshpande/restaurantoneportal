package com.swiggy.restaurantone.data.businessEntity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by rajnish.chandra on 27/02/19
 */
@Data
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PartnerEnableRequest {
    @JsonProperty("run_info_id")
    private long runInfoId;
    
    @JsonProperty(value = "updated_by")
    private String updatedBy;
}