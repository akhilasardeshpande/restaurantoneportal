package com.swiggy.restaurantone.data.businessEntity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RestaurantSpoc implements Serializable {
    private static final long serialVersionUID = -6567144221841636336L;

    @NotNull
    @NonNull
    @JsonProperty("restaurant_id")
    private Long restaurantId;

    @JsonProperty("sm_name")
    private String smName;

    @JsonProperty("sm_mobile")
    private String smMobile;

    @JsonProperty("sm_email")
    private String smEmail;

    @JsonProperty("asm_name")
    private String asmName;

    @JsonProperty("asm_mobile")
    private String asmMobile;

    @JsonProperty("asm_email")
    private String asmEmail;

    @JsonProperty("support_email")
    private String supportEmail;

    @JsonProperty("support_phone")
    private String supportPhone;

    @JsonIgnore
    private String updatedBy;
}
