package com.swiggy.restaurantone.data.businessEntity;

import lombok.*;

import java.util.List;

/**
 * Created by balamuneeswaran.s on 1/5/17.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserCriteria {
    public List<Long> idList;
    public List<String> emailList;
    public List<String> mobileNumberList;
    public Pagination pagination;
}
