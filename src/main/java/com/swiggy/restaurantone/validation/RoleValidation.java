package com.swiggy.restaurantone.validation;

import com.swiggy.restaurantone.data.businessEntity.Role;
import com.swiggy.restaurantone.data.businessEntity.ValidationResult;
import com.swiggy.restaurantone.service.RoleService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by rajnishc on 5/14/17.
 */
@Component
public class RoleValidation {
    @Autowired
    private RoleService roleService;

    @Autowired
    private PermissionValidator permissionValidator;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public ValidationResult isValidRole(Role role) {
        if(role == null || roleService.findRoleName(role.getName()) == null){
            String message = "Role is not valid: " + role;
            return sendResult(message, false);
        }
        return sendResult("", true);
    }
    private ValidationResult sendResult(String message, boolean isValid) {
        logger.error(message);
        return ValidationResult.builder()
                .Message(message)
                .isValid(isValid)
                .build();
    }

    public List<ValidationResult> postValidation(Role role) {
        List<ValidationResult> errors = new LinkedList<>();

        if(roleService.findRoleName(role.getName()) != null) {
            errors.add(new ValidationResult("Role already exists", false));
        }
        return errors;
    }

    public List<ValidationResult> putValidation(Role role) {
        List<ValidationResult> errors = new LinkedList<>();
        Role currentRole;

        if(role.getId() == null  ) {
            errors.add(new ValidationResult("Id validation failed", false));
        }

        if(roleService.findRoleById(role.getId()) == null) {
            errors.add(new ValidationResult("Role does not exist", false));
        }

        currentRole = roleService.findRoleById(role.getId());

        if(currentRole.getName().equals(role.name) && currentRole.level == role.level && currentRole.getPermissions().size() == role.permissions.size() && CollectionUtils.subtract(currentRole.getPermissions(), role.getPermissions()).isEmpty()) {
            errors.add(new ValidationResult("Role already exists", false));
        }
        List<ValidationResult> permissionErrors = permissionValidator.validate(role.permissions);

        if(!permissionErrors.isEmpty())
            errors.addAll(permissionErrors);

        return errors;
    }

}
