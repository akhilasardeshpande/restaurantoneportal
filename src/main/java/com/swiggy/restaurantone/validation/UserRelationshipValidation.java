package com.swiggy.restaurantone.validation;

import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.data.businessEntity.UserCriteria;
import com.swiggy.restaurantone.data.businessEntity.UserRelationship;
import com.swiggy.restaurantone.service.RoleService;
import com.swiggy.restaurantone.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by balamuneeswaran.s on 1/16/17.
 */
@Component
public class UserRelationshipValidation {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public boolean validate(List<UserRelationship> userRelationshipList) {
        for (UserRelationship userRelationShip:
                userRelationshipList) {
            if(!validate(userRelationShip)) {
                return false;
            }
        }
        return true;
    }

    private boolean validate(UserRelationship userRelationship) {
        UserCriteria criteria = new UserCriteria();
        criteria.idList = new ArrayList<>();
        criteria.idList.add(userRelationship.childUserId);
        criteria.idList.add(userRelationship.userId);
        List<User> users = userService.getUsersByCriteria(criteria);

        User parent = users.stream().filter(user -> user.id.equals(userRelationship.userId)).findAny().orElse(null);
        User child = users.stream().filter(user -> user.id.equals(userRelationship.childUserId)).findAny().orElse(null);
        if(parent == null || child == null ) {
            logger.error("User Relationship in Invalid: ", userRelationship);
            return false;
        }
        return true;
    }
}
