package com.swiggy.restaurantone.validation;

import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.data.businessEntity.UserCriteria;
import com.swiggy.restaurantone.data.businessEntity.ValidationResult;
import com.swiggy.restaurantone.service.CityService;
import com.swiggy.restaurantone.service.TeamService;
import com.swiggy.restaurantone.service.UserService;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by balamuneeswaran.s on 1/16/17.
 */
@Setter
@Component
public class UserValidation {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RoleValidation roleValidation;

    @Autowired
    private UserService userService;

    @Autowired
    private CityService cityService;

    @Autowired
    private TeamService teamService;

    @Value("${contactEditEnabled}")
    private boolean contactEditEnabled;

    @Value("${rolesEditEnabled}")
    private boolean rolesEditEnabled;

//    public List<ValidationResult> validate(List<User> users) {
//        for (User user:
//             users) {
//            ValidationResult validationResult = validate(user, false);
//            if(!validationResult.isValid()) {
//                return validationResult;
//            }
//        }
//        return new LinkedList<>(.add())ValidationResult.builder()
//                .Message("")
//                .isValid(true)
//                .build();
//    }

    public List<ValidationResult> validate(User user, boolean isAdd) {
        return fieldValidator(user, isAdd);
    }

    private ValidationResult sendResult(String message, boolean isValid) {
        logger.error(message);
        return ValidationResult.builder()
                .Message(message)
                .isValid(isValid)
                .build();
    }
    private boolean emailValidator(Long id, String email) {
        User user = getUser(id);
        return user.email.equalsIgnoreCase(email);
    }

    private boolean nameValidator(Long id, String name) {
        User user = getUser(id);
        return user.name.equalsIgnoreCase(name);
    }

    private boolean cityValidator(Long id) {
        return cityService.findCity(id) != null;
    }

    private boolean userValidator(Long id) {
        return userService.find(id) != null;
    }

    private boolean teamValidator(Long id) {
        return id != null && teamService.find(id) != null;
    }

    private boolean userValidator(String email) {
        UserCriteria userCriteria = new UserCriteria();
        userCriteria.emailList = new ArrayList<>();
        userCriteria.emailList.add(email);
        return userService.getUsersByCriteria(userCriteria).isEmpty();
    }

    private User getUser(String email) {
        UserCriteria userCriteria = new UserCriteria();
        userCriteria.emailList = new ArrayList<>();
        userCriteria.emailList.add(email);
        List<User> list = userService.getUsersByCriteria(userCriteria);
        if(!list.isEmpty())
            return list.get(0);
        return null;
    }

    private User getUser(Long id) {
        return userService.find(id);
    }

    private List<ValidationResult> fieldValidator(User user, boolean isAdd) {
        List<ValidationResult> errors = new LinkedList<>();

        if (!isAdd && !userValidator(user.id)) {
            errors.add(new ValidationResult("Invalid user id", false));
        }

        if (!isAdd && !emailValidator(user.id, user.email)) {
            errors.add(new ValidationResult("Trying to edit immutable field (Email) " + user.email, false));
        }

        if (!isAdd && !nameValidator(user.id, user.name)) {
            errors.add(new ValidationResult("Trying to edit immutable field (Name) " + user.name, false));
        }

        if (!roleValidation.isValidRole(user.role).isValid()) {
            errors.add(new ValidationResult("Invalid Role", false));
        }

        if (!cityValidator(user.city.getId())) {
            errors.add(new ValidationResult("Invalid City", false));
        }

        if (!userValidator(user.manager.getId())) {
            errors.add(new ValidationResult("Invalid Manager details", false));
        }

        if (!teamValidator(user.team.getId())) {
            errors.add(new ValidationResult("Invalid Team", false));
        }
        return errors;
    }

    private boolean isEmptyOrNull(Object field) {
        return field == null || field instanceof String && ((String) field).isEmpty();
    }
}
