package com.swiggy.restaurantone.validation;

import com.swiggy.restaurantone.data.businessEntity.RestaurantUserMap;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.service.RestaurantUserMapService;
import com.swiggy.restaurantone.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by balamuneeswaran.s on 1/16/17.
 */
@Component
public class RestaurantUserMapValidation {
    @Autowired
    private UserService userService;

    @Autowired
    private RoleValidation roleValidation;

    @Autowired
    private RestaurantUserMapService restaurantUserMapService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public boolean validate(List<RestaurantUserMap> restaurantUserMaps) {
        return restaurantUserMaps.stream().allMatch(this::validate);
    }

    public boolean validate(RestaurantUserMap restaurantUserMap) {
        User user = userService.find(restaurantUserMap.userId);
        return user!= null && roleValidation.isValidRole(user.role).isValid();
    }

    private boolean validateId(List<RestaurantUserMap> restaurantUserMaps) {
        return restaurantUserMaps.stream().allMatch(restaurantUserMap ->   restaurantUserMapService.getRestaurantById(restaurantUserMap) != null);

    }
}