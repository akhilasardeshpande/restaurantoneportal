package com.swiggy.restaurantone.validation;

import com.swiggy.restaurantone.data.businessEntity.Permission;
import com.swiggy.restaurantone.data.businessEntity.ValidationResult;
import com.swiggy.restaurantone.service.PermissionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by rajnishc on 5/14/17.
 */
@Component
public class PermissionValidator {
    @Autowired
    private PermissionService permissionService;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private ValidationResult isValidPermission(Long id) {
        if(permissionService.findPermissionEntity(id) == null){
            String message = "Permission is not valid: " + id;
            return sendResult(message, false);
        }
        return null;
    }
    private ValidationResult sendResult(String message, boolean isValid) {
        logger.error(message);
        return ValidationResult.builder()
                .Message(message)
                .isValid(isValid)
                .build();
    }

    public List<ValidationResult> validate(List<Permission> permissions) {
        List<ValidationResult> errors = new LinkedList<>();

        Iterator iterator = permissions.iterator();
        while (iterator.hasNext()) {
            Permission permission = (Permission) iterator.next();
            ValidationResult result = isValidPermission(permission.getId());
            if(result != null)
            errors.add(result);
        }
        return errors;
    }
}
