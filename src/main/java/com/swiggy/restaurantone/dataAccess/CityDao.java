package com.swiggy.restaurantone.dataAccess;

import com.swiggy.restaurantone.data.coreEntity.CityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by rajnishc on 5/5/17.
 */
@Repository
public interface CityDao extends JpaRepository<CityEntity, Long> {
    CityEntity findById(Long id);
}
