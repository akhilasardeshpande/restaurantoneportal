package com.swiggy.restaurantone.dataAccess;

import com.swiggy.rest.dao.CrudDao;
import com.swiggy.restaurantone.data.coreEntity.RestaurantUserMapEntity;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by balamuneeswaran.s on 1/3/17.
 */
public interface RestaurantUserMapDao extends CrudDao<RestaurantUserMapEntity, Long> {
    public List<RestaurantUserMapEntity> findByUserIdIn(List<Long> id);
    RestaurantUserMapEntity findByUserIdAndAndRestaurantId(Long userId, Long restaurantId);
    @Transactional
    public List<RestaurantUserMapEntity> deleteByRestaurantId(Long restaurantId);
    public List<RestaurantUserMapEntity> findByUserId(Long id);
    public RestaurantUserMapEntity findById(Long id);
    public List<RestaurantUserMapEntity> findByRestaurantId(Long id);
    @Transactional
    public Long deleteByUserId(Long id);
}
