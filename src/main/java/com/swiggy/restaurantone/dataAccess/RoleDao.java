package com.swiggy.restaurantone.dataAccess;

import com.swiggy.restaurantone.data.coreEntity.RoleEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by balamuneeswaran.s on 1/16/17.
 */
@Repository
public interface RoleDao extends CrudRepository<RoleEntity, Long> {
    List<RoleEntity> findAll();
    RoleEntity findByRoleIgnoreCase(String name);

}
