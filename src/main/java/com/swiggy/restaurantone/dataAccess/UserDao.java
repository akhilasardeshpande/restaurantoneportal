package com.swiggy.restaurantone.dataAccess;

import com.swiggy.rest.dao.CrudDao;
import com.swiggy.restaurantone.data.coreEntity.UserEntity;

import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by balamuneeswaran.s on 1/3/17.
 */

public interface UserDao extends CrudDao<UserEntity, Long> {
    List<UserEntity> findByIdInOrEmailInOrContactNumberIn(List<Long> id, List<String> email, List<String> number, Pageable pageable);
    UserEntity findByName(String name);
    List<UserEntity>  findTop15ByNameStartingWithIgnoreCase(String chars);
    UserEntity findByEmail(String email);
}
