package com.swiggy.restaurantone.dataAccess;

import com.swiggy.rest.dao.CrudDao;
import com.swiggy.restaurantone.data.coreEntity.PermissionEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by shahbaz.khalid on 5/2/17.
 */
@Repository
public interface PermissionDao extends CrudDao<PermissionEntity, Long>{
    List<PermissionEntity> findAll();
}
