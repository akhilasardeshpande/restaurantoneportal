package com.swiggy.restaurantone.dataAccess;

import com.swiggy.restaurantone.data.coreEntity.PartnerDisableRunInfoEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Created by rajnish.chandra on 20/02/19
 */
@Repository
public interface PartnerDisableRunInfoDao extends JpaRepository<PartnerDisableRunInfoEntity, Long> {
    List<PartnerDisableRunInfoEntity> findAllByOrderByIdDesc(Pageable pageable);
}
