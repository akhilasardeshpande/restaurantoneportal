package com.swiggy.restaurantone.dataAccess;

import com.swiggy.rest.dao.CrudDao;
import com.swiggy.restaurantone.data.coreEntity.RMSUserEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by rajnish.chandra on 20/06/18
 */
@Repository
public interface RMSUserAuditDao extends CrudDao<RMSUserEntity, Long> {
}
