package com.swiggy.restaurantone.dataAccess;

import com.swiggy.restaurantone.data.coreEntity.PartnerDisableHolidaySlotInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by rajnish.chandra on 20/02/19
 */
@Repository
public interface PartnerDisableHolidaySlotInfoDao extends JpaRepository<PartnerDisableHolidaySlotInfoEntity, Long> {
    List<PartnerDisableHolidaySlotInfoEntity> findAllByRunInfoIdAndIsActiveTrue(Long id);
    PartnerDisableHolidaySlotInfoEntity findByRunInfoIdAndRestaurantId(Long runId, Long restaurantId);
    List<PartnerDisableHolidaySlotInfoEntity> findByRestaurantIdInAndIsActive(List<Long> restIds, boolean isActive);
    int countByRunInfoIdAndIsActiveTrue(Long id);
}