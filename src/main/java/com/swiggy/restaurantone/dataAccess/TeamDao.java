package com.swiggy.restaurantone.dataAccess;

import com.swiggy.restaurantone.data.coreEntity.TeamEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by rajnishc on 5/5/17.
 */
@Repository
public interface TeamDao extends JpaRepository<TeamEntity, Long> {
    TeamEntity findById(Long id);
}
