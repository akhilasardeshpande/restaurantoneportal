package com.swiggy.restaurantone.dataAccess;

import com.swiggy.restaurantone.data.coreEntity.Session;
import com.swiggy.restaurantone.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by balamuneeswaran.s on 1/5/17.
 */
@Repository
public class SessionDao {

    @Autowired
    private CacheService cacheService;

    public void saveSessionList(String key, List<Session> sessions) {
        cacheService.set(key, sessions);
    }

    public List<Session> getSessionList(String key) {
        List sessions = (List<Session>) cacheService.get(key);
        return sessions == null ? new ArrayList<>() : sessions;
    }

    public void deleteSessionList(String key) {
        cacheService.delete(key);
    }
}
