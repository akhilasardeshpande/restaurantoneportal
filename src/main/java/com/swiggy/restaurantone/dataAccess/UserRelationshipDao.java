package com.swiggy.restaurantone.dataAccess;

import com.swiggy.restaurantone.data.coreEntity.UserRelationshipEntity;
import com.swiggy.restaurantone.data.coreEntity.UserRelationshipId;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by balamuneeswaran.s on 1/4/17.
 */

public interface UserRelationshipDao extends JpaRepository<UserRelationshipEntity, UserRelationshipId> {
    List<UserRelationshipEntity> findByUserId(Long id);

    List<UserRelationshipEntity> findByChildUserId(Long id);

    List<UserRelationshipEntity> findByUserIdAndIsDirectParent(Long id, boolean isDirectParent);

    List<UserRelationshipEntity> findByChildUserIdAndIsDirectParent(Long id, boolean isDirectParent);

    @Transactional
    Long deleteByUserId(Long id);

    @Transactional
    Long deleteByChildUserId(Long id);
}
