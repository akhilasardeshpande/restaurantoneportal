package com.swiggy.restaurantone.dataAccess;

import com.swiggy.rest.dao.CrudDao;
import com.swiggy.restaurantone.data.coreEntity.RolePermissionEntity;

import javax.transaction.Transactional;

/**
 * Created by rajnishc on 5/14/17.
 */

public interface RolePermissionDao extends CrudDao<RolePermissionEntity, Long> {
    @Transactional
    Long deleteByRoleId(Long id);
}
