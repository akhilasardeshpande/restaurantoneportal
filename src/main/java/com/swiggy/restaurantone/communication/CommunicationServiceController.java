package com.swiggy.restaurantone.communication;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/api/v1")
public class CommunicationServiceController extends AbstractController {

    @Autowired
    private CommunicationService communicationService;

    @PostMapping(value = "/template", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> createTemplate(@RequestBody Object requestBody) {
        log.info("Request received. API: {}, RequestBody: {}", "CREATE_TEMPLATE", requestBody);
        return this.executeTask(() -> communicationService.createTemplate(requestBody));
    }

    @PutMapping(value = "/template", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> updateTemplate(@RequestBody Object requestBody) {
        log.info("Request received. API: {}, RequestBody: {}", "UPDATE_TEMPLATE", requestBody);
        return this.executeTask(() -> communicationService.updateTemplate(requestBody));
    }

    @GetMapping(value = "/template/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> getTemplateById(@PathVariable(value = "id") String id) {
        log.info("Request received. API: {}, Id: {}", "GET_TEMPLATE_BY_ID", id);
        return this.executeTask(() -> communicationService.getTemplateById(id));
    }

    @GetMapping(value = "/template/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> getAllTemplates(@RequestParam(value = "type") String type,
                                       @RequestParam(value = "name", required = false) String name) {
        log.info("Request received. API: {}, Type: {}, Name: {}", "GET_ALL_TEMPLATES", type, name);
        return this.executeTask(() -> communicationService.getAllTemplates(type, name));
    }

    @GetMapping(value = "/template/searchName", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> searchTemplate(@RequestParam(name = "name") String name, @RequestParam(name = "type") String type) {
        log.info("Request received. API: {}, Type: {}, Name: {}", "SEARCH_TEMPLATE", type, name);
        return this.executeTask(() -> communicationService.getAllTemplates(type, name));
    }

    @GetMapping(value = "/event/component/{componentName}/event-name/{eventName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> getAllEventsByComponentAndName(@PathVariable(value = "componentName") String componentName,
                                                      @PathVariable(value = "eventName") String eventName) throws CommunicationServiceException {

        log.info("Request received. API: {}, componentName: {}, EventName: {}", "GET_ALL_EVENTS_BY_COMPONENT_AND_NAME", componentName, eventName);

        return this.executeTask(() -> communicationService.getAllEventsByComponentAndName(componentName, eventName));
    }

    @GetMapping(value = "/event", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> getAllEvents() throws CommunicationServiceException {

        log.info("Request received. API: {}", "GET_ALL_EVENTS");

        return this.executeTask(() -> communicationService.getAllEvents());
    }

    @GetMapping(value = "/event/component/{componentName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> getAllEventsByComponent(@PathVariable(value = "componentName") String componentName) throws CommunicationServiceException {

        log.info("Request received. API: {}, componentName: {}", "GET_ALL_EVENTS_BY_COMPONENT", componentName);

        return this.executeTask(() -> communicationService.getAllEventsByComponent(componentName));
    }

    @PostMapping(value = "/event", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> createEvent(@RequestBody Object requestBody) throws CommunicationServiceException {
        log.info("Request received. API: {}, componentName: {}", "CREATE_EVENT", requestBody);
        return this.executeTask(() -> communicationService.createEvent(requestBody));
    }

    @DeleteMapping(value = "/event", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> deleteEvent(@RequestBody Object requestBody) throws CommunicationServiceException {
        log.info("Request received. API: {}, componentName: {}", "DELETE_EVENT", requestBody);
        return this.executeTask(() -> communicationService.deleteEvent(requestBody));
    }


}
