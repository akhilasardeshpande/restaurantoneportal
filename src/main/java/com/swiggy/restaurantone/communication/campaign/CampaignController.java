package com.swiggy.restaurantone.communication.campaign;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = {"/campaign", "/api/comm-engine/campaign"})
public class CampaignController extends AbstractController {
    @Autowired
    private CampaignService campaignService;

    @RequestMapping(value = {"", "/"}, method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> updateCampaign(@RequestBody Object requestBody) {
        log.info("Request received. API: {}, RequestBody: {}", "UPDATE_CAMPAIGN", requestBody);
        return this.executeTask(() -> campaignService.updateCampaign(requestBody));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> getCampaignById(@PathVariable(value = "id") Long id) {
        log.info("Request received. API: {}, Id: {}", "GET_CAMPAIGN_BY_ID", id);
        return this.executeTask(() -> campaignService.getCampaignById(id));
    }

    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> getAllCampaigns() {
        log.info("Request received. API: {}", "GET_ALL_CAMPAIGNS");
        return this.executeTask(() -> campaignService.getAllCampaigns());
    }

    @RequestMapping(value = "/{campaignId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> deleteCampaignByCampaignId(@PathVariable(value = "campaignId") Long campaignId) {
        log.info("Request received. API: {}, CampaignId: {}", "GET_CAMPAIGN_BY_CAMPAIGN_ID", campaignId);
        return this.executeTask(() -> campaignService.deleteCampaignByCampaignId(campaignId));
    }

    @RequestMapping(value = "/facade", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> campaignFacade(@RequestBody Object requestBody){
        log.info("Request received. API: {}, RequestBody: {}", "CAMPAIGN_FACADE", requestBody);
        return this.executeTask(() -> campaignService.campaignFacade(requestBody));
    }
}