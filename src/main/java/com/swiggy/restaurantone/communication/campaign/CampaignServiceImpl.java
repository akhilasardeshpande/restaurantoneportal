package com.swiggy.restaurantone.communication.campaign;

import com.swiggy.restaurantone.communication.CommEngineResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.IOException;

@Slf4j
@Service
public class CampaignServiceImpl implements CampaignService {
    @Autowired
    private CampaignServiceClient campaignServiceClient;

    @Override
    public Object getAllCampaigns() throws CampaignServiceException {
        Object responseBody;
        try {
            Response<CommEngineResponse> response = campaignServiceClient.getAllCampaigns().execute();
            responseBody = extractAndValidateResponseBody(response);
        } catch (IOException e) {
            throw new CampaignServiceException(e.getMessage(), e.getCause());
        }
        return responseBody;
    }

    @Override
    public Object getCampaignById(Long id) throws CampaignServiceException {
        Object responseBody;
        try {
            Response<CommEngineResponse> response = campaignServiceClient.getCampaignById(id).execute();
            responseBody = extractAndValidateResponseBody(response);
        } catch (IOException e) {
            throw new CampaignServiceException(e.getMessage(), e.getCause());
        }
        return responseBody;
    }

    @Override
    public Object deleteCampaignByCampaignId(Long campaignId) throws CampaignServiceException {
        Object responseBody;
        try {
            Response<CommEngineResponse> response = campaignServiceClient.deleteCampaignByCampaignId(campaignId)
                    .execute();
            responseBody = extractAndValidateResponseBody(response);
        } catch (IOException e) {
            throw new CampaignServiceException(e.getMessage(), e.getCause());
        }
        return responseBody;
    }

    @Override
    public Object updateCampaign(Object campaign) throws CampaignServiceException {
        Object responseBody;
        try {
            Response<CommEngineResponse> response = campaignServiceClient.updateCampaign(campaign).execute();
            responseBody = extractAndValidateResponseBody(response);
        } catch (IOException e) {
            throw new CampaignServiceException(e.getMessage(), e.getCause());
        }
        return responseBody;
    }

    @Override
    public Object campaignFacade(Object requestBody) throws CampaignServiceException {
        Object responseBody;
        try {
            Response<CommEngineResponse> response = campaignServiceClient.campaignFacade(requestBody).execute();
            responseBody = extractAndValidateResponseBody(response);
        } catch (IOException e) {
            throw new CampaignServiceException(e.getMessage(), e.getCause());
        }
        return responseBody;
    }

    private Object extractAndValidateResponseBody(Response<CommEngineResponse> response) {
        if (response == null || !response.isSuccessful() || response.body() == null) {
            log.error("Empty or no response from Campaign Service: {}", response);
            throw new CampaignServiceException(String.format("Empty or no response from Campaign Service: %s",
                    response));
        }
        log.info("Success response from Campaign Service: {}", response.body());
        return response.body().getData();
    }
}
