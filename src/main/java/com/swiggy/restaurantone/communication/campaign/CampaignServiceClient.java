package com.swiggy.restaurantone.communication.campaign;

import com.swiggy.restaurantone.communication.CommEngineResponse;
import retrofit2.Call;
import retrofit2.http.*;

public interface CampaignServiceClient {
    @Headers({"Content-Type: application/json"})
    @GET("/campaign")
    Call<CommEngineResponse> getAllCampaigns();

    @Headers({"Content-Type: application/json"})
    @PUT("/campaign")
    Call<CommEngineResponse> updateCampaign(@Body Object requestBody);

    @Headers({"Content-Type: application/json"})
    @GET("/campaign/{id}")
    Call<CommEngineResponse> getCampaignById(@Path("id") Long id);

    @Headers({"Content-Type: application/json"})
    @DELETE("/campaign/{campaignId}")
    Call<CommEngineResponse> deleteCampaignByCampaignId(@Path("campaignId") Long campaignId);

    @Headers({"Content-Type: application/json"})
    @POST("/campaign/facade")
    Call<CommEngineResponse> campaignFacade(@Body Object body);
}
