package com.swiggy.restaurantone.communication.campaign;

public class CampaignServiceException extends RuntimeException {
    public CampaignServiceException(String message) {
        super(message);
    }

    public CampaignServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
