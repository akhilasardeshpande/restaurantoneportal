package com.swiggy.restaurantone.communication.campaign;

import com.swiggy.restaurantone.communication.campaign.CampaignServiceException;

public interface CampaignService {
    Object getAllCampaigns()throws CampaignServiceException;

    Object getCampaignById(final Long id) throws CampaignServiceException;

    Object deleteCampaignByCampaignId(final Long campaignId) throws CampaignServiceException;

    Object updateCampaign(final Object campaign) throws CampaignServiceException;

    Object campaignFacade(final Object requestBody) throws CampaignServiceException;
}
