package com.swiggy.restaurantone.communication.notification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@RestController
@RequestMapping(value = "/rest-one/v1")
public class NotificationTriggerController extends AbstractController {
    @Autowired
    private AuthorizationHelper authorizationHelper;

    @Autowired
    private NotificationTriggerService commEventService;

    @RequestMapping(method = RequestMethod.POST,
            value = "/seller-tier/ExcelUploadNotification",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> fileUploadedTrigger(@RequestBody @Valid UploadedFileRequestBody requestBody,
                                           BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            return this.getFailureResponse(bindingResult.toString());
        }

        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(
                    () -> commEventService.sendAppNotificationEvent(requestBody.getS3url()));
        }
        return this.getFailureResponse("Unauthorized");
    }

    @Data
    @NoArgsConstructor
    private static class UploadedFileRequestBody implements Serializable {
        @NotNull
        @JsonProperty("s3url")
        private String s3url;
    }
}

