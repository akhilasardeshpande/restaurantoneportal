package com.swiggy.restaurantone.communication.notification;

import com.swiggy.backlit.batteries.prometheus.Instrumentation.InstrumentMetrics;
import com.swiggy.commons.Json;
import com.swiggy.kafka.clients.producer.Producer;
import com.swiggy.restaurantone.communication.CommunicationService;
import com.swiggy.restaurantone.exceptions.PublisherException;
import com.swiggy.restaurantone.pojos.commEngine.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class NotificationsService {
    private static final String COMPONENT_ROP = "ROP";
    private static final String VENDOR_USER = "VENDOR_USER";

    @Autowired
    @Qualifier("kafka-txn-producer")
    private Producer kafkaTxnProducer;

    @Value("${kafka.txn.producer.comm-event.topic.name}")
    private String commTopic;

    @Autowired
    private CommunicationService communicationService;

    @Async
    @InstrumentMetrics
    public void notifyRestaurant(Long restaurantId, Map<String, Object> params) {
        try {
            CommEngineEvent commEngineEvent = createCommEngineEvent(restaurantId, params);
            String commEngineEventStr = Json.serialize(commEngineEvent);
            log.info("sending comm event to confluent kafka.Message: {}", commEngineEventStr);
            kafkaTxnProducer.send(commTopic, commEngineEventStr);
        } catch (IllegalArgumentException | PublisherException e) {
            throw new NotificationsServiceException(e.getMessage(), e.getCause());
        }
    }

    @Async
    @InstrumentMetrics
    public void sendEventToComm(CommEngineEvent commEngineEvent) {
        try {
            String commEngineEventStr = Json.serialize(commEngineEvent);
            log.info("sending comm event to confluent kafka.Message: {}", commEngineEventStr);
            kafkaTxnProducer.send(commTopic, commEngineEventStr);
        } catch (IllegalArgumentException | PublisherException e) {
            throw new NotificationsServiceException(e.getMessage(), e.getCause());
        }
    }

    @InstrumentMetrics
    public void sendApiEmail(String eventName, String emailId,
                             Map<String, Object> commEventMessage, Map<String, Object> commEventMeta) {
        try {
            CommEventHeader commEventHeader = CommEventHeader.builder()
                    .component(COMPONENT_ROP)
                    .eventName(eventName)
                    .build();
            CommEventUser commEventUser = CommEventUser.builder()
                    .type(VENDOR_USER)
                    .email(CommEventEmail.builder().email(Collections.singletonList(emailId)).build())
                    .build();
            CommEventTarget commEventTarget = CommEventTarget.builder()
                    .commEventUsers(Collections.singletonList(commEventUser)).build();
            CommEventPostRequest commEventPostRequest = CommEventPostRequest.builder()
                    .requestId(UUID.randomUUID().toString())
                    .commEventHeader(commEventHeader)
                    .commEventTarget(commEventTarget)
                    .commEventMessage(commEventMessage)
                    .commEventMeta(commEventMeta)
                    .build();
            communicationService.sendCommunicationEvent(commEventPostRequest);
        } catch (Exception e) {
            throw new NotificationsServiceException(e.getMessage(), e.getCause());
        }
    }

    @InstrumentMetrics
    public void sendEmail(String eventName, String emailId, List<String> attachments, Map<String, Object> ctx) {
        User user = User.builder()
                .type(VENDOR_USER)
                .emailId(emailId)
                .build();
        List<UserInfo> userInfo = new ArrayList<>();
        userInfo.add(UserInfo.builder()
                .user(user)
                .ctx(Collections.EMPTY_MAP)
                .metaInfo(Collections.EMPTY_MAP)
                .build());
        Map<String, Object> metaInfo = new HashMap<>();
        if (CollectionUtils.isNotEmpty(attachments)) {
            Map<String, List<String>> attachment = Collections.singletonMap("attachments", attachments);
            metaInfo.put("email", attachment);
        }
        CommEngineEvent commEngineEvent = CommEngineEvent.builder()
                .identificationId(UUID.randomUUID().toString())
                .name(eventName)
                .type("GENERAL")
                .component(COMPONENT_ROP)
                .metaInformation(metaInfo)
                .ctx(ctx)
                .users(userInfo)
                .build();
        try {
            String commEngineEventStr = Json.serialize(commEngineEvent);
            log.info("sending comm event to confluent kafka.Message: {}", commEngineEventStr);
            kafkaTxnProducer.send(commTopic, commEngineEventStr);
        } catch (IllegalArgumentException | PublisherException e) {
            throw new NotificationsServiceException(e.getMessage(), e.getCause());
        }
    }

    private static CommEngineEvent createCommEngineEvent(Long restId, Map<String, Object> Params) {
        return CommEngineEvent.builder()
                .identificationId(UUID.randomUUID().toString())
                .type("GENERAL")
                .component("VENDOR")
                .name((String) Params.get("event_name"))
                .ctx(Params)
                .metaInformation(Collections.EMPTY_MAP)
                .userRetrieval(UserRetrieval.builder()
                        .type("REFERENCE_IDS")
                        .idType("RESTAURANT")
                        .resolve("VENDOR_USER")
                        .key("PRIMARY")
                        .ids(Collections.singletonList(restId))
                        .build())
                .build();
    }
}
