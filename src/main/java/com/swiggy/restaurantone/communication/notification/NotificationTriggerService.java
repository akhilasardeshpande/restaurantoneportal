package com.swiggy.restaurantone.communication.notification;

import com.swiggy.backlit.batteries.prometheus.Instrumentation.InstrumentMetrics;
import com.swiggy.restaurantone.Constant.CommEventConstants;
import com.swiggy.restaurantone.pojos.commEngine.CommEngineEvent;
import com.swiggy.restaurantone.pojos.commEngine.User;
import com.swiggy.restaurantone.pojos.commEngine.UserInfo;
import com.swiggy.restaurantone.service.AmazonS3ClientService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.swiggy.restaurantone.Constant.CommEventConstants.*;
import static com.swiggy.restaurantone.utils.FileUtils.parseCSVFile;

@Service
@Slf4j
public class NotificationTriggerService {
    @Autowired
    AmazonS3ClientService amazonS3ClientService;

    @Autowired
    NotificationsService notificationsService;

    @Value("${commEngine.appNotification.validation.dateFormat}")
    private String appNotificationDateFormat;

    @InstrumentMetrics
    public String sendAppNotificationEvent(String s3Url) throws CommEventFieldException {
        BufferedReader bufferedReader = amazonS3ClientService.getFileFromS3(s3Url);
        List<List<String>> records = parseCSVFile(bufferedReader);

        Map<String, Integer> fieldMapping = new HashMap<>();
        List<String> dynamicFields = new ArrayList<>();
        List<String> attrFields = new ArrayList<>();
        List<String> headerRow = records.get(0);

        extractCommEventFieldInfo(fieldMapping, dynamicFields, attrFields, headerRow);
        validateMandatoryEventFields(fieldMapping);

        for (int row = 1; row < records.size(); row++) {
            List<String> fields = records.get(row);
            CommEngineEvent commEngineEvent = getCommEngineEvent(fieldMapping, dynamicFields, attrFields, row, fields);
            if (commEngineEvent != null) {
                notificationsService.sendEventToComm(commEngineEvent);
            }
        }
        return "success sending app notification events to CommEngine";
    }

    private CommEngineEvent getCommEngineEvent(Map<String, Integer> fieldMapping,
                                               List<String> dynamicFields, List<String> attrFields,
                                               int row, List<String> fields) {
        CommEngineEvent commEngineEvent = null;
        try {
            final Map<String, Object> metaInformation = extractMetaInformation(fieldMapping, attrFields, row, fields);
            final Map<String, Object> ctx = extractCtx(fieldMapping, dynamicFields, row, fields);
            final long user_id = Long.parseLong(fields.get(fieldMapping.get(USER_ID)));
            String user_type = fields.get(fieldMapping.get(USER_TYPE));
            final String component = fields.get(fieldMapping.get(COMPONENT));
            final Map<String, Object> appNotificationMetaInformation = new HashMap();
            appNotificationMetaInformation.put(META_INFO_FIELD_APP_NOTIFICATION, metaInformation);
            String eventName = fields.get(fieldMapping.get(EVENT_NAME));

            commEngineEvent = createCommEngineEventPayload(ctx, user_id, user_type, component,
                    appNotificationMetaInformation, eventName);
        } catch (NumberFormatException | NullPointerException e) {
            log.error(e.getMessage(), e.getStackTrace());
        }
        return commEngineEvent;
    }

    private CommEngineEvent createCommEngineEventPayload(Map<String, Object> ctx, long user_id, String user_type,
                                                         String component,
                                                         Map<String, Object> appNotificationMetaInformation,
                                                         String eventName) {
        User user = User.builder()
                .userId(user_id)
                .type(user_type)
                .build();
        List<UserInfo> userInfo = new ArrayList<>();
        userInfo.add(UserInfo.builder()
                .user(user)
                .ctx(new HashMap<>())
                .metaInfo(new HashMap<>())
                .build());
        return CommEngineEvent.builder()
                .identificationId(UUID.randomUUID().toString())
                .name(eventName)
                .type("GENERAL")
                .component(component)
                .metaInformation(appNotificationMetaInformation)
                .ctx(ctx)
                .users(userInfo)
                .build();
    }

    private void validateMandatoryEventFields(Map<String, Integer> fieldMapping) {
        for (String mandatoryCommEventField : CommEventConstants.MANDATORY_COMM_EVENT_FIELDS) {
            if (!fieldMapping.containsKey(mandatoryCommEventField)) {
                log.error("Mandatory field is missing --> " + mandatoryCommEventField);
                throw new CommEventFieldException("Mandatory field is missing --> " + mandatoryCommEventField);
            }
        }
    }

    private Map<String, Object> extractCtx(Map<String, Integer> fieldMapping, List<String> dynamicFields, int row, List<String> fields) {
        Map<String, Object> ctx = new HashMap<>();
        for (String field : dynamicFields) {
            if (fields.get(fieldMapping.get(field)).equals("") || fields.get(fieldMapping.get(field)) == null) {
                throw new CommEventFieldException(("Ctx fields is missing in row " + row));
            }
            ctx.put(field, fields.get(fieldMapping.get(field)));
        }
        return ctx;
    }

    private Map<String, Object> extractMetaInformation(Map<String, Integer> fieldMapping, List<String> attrFields, int row, List<String> fields) {
        Map<String, Object> metaInformation = new HashMap<>();
        if (isValidDuration(fields.get(fieldMapping.get(APP_NOTIFICATION_START_DATE)), fields.get(fieldMapping.get(APP_NOTIFICATION_END_DATE)))) {
            metaInformation.put(APP_NOTIFICATION_START_DATE, fields.get(fieldMapping.get(APP_NOTIFICATION_START_DATE)));
            metaInformation.put(APP_NOTIFICATION_END_DATE, fields.get(fieldMapping.get(APP_NOTIFICATION_END_DATE)));
        } else {
            throw new CommEventFieldException("Duration(end_date < start_date) is not valid in row " + row);
        }
        for (String field : attrFields) {
            String value = fields.get(fieldMapping.get(field));
            if (StringUtils.isEmpty(value)) {
                throw new CommEventFieldException("Attribute field is missing in row " + row);
            }
            Object parsedValue = parseMetaInformationField(value);
            metaInformation.put(field, parsedValue);
        }
        return metaInformation;
    }

    private Object parseMetaInformationField(String field) {
        if (field.contains(",")) {
            List<Object> filedValues = new ArrayList<>();
            String[] values = field.split(",");
            for (String value : values) {
                if (NumberUtils.isDigits(value)) {
                    filedValues.add(Integer.parseInt(value));
                } else {
                    filedValues.add(value);
                }
            }
            return filedValues;
        } else {
            return field;
        }
    }

    private boolean isValidDuration(String startDate, String endDate) {
        final SimpleDateFormat sdf = new SimpleDateFormat(appNotificationDateFormat);
        Date parsedStartDate = null;
        Date parsedEndDate = null;
        try {
            parsedStartDate = sdf.parse(startDate);
            parsedEndDate = sdf.parse(endDate);
        } catch (ParseException e) {
            log.error(e.getMessage(), e.getCause());
        }
        assert parsedEndDate != null && parsedStartDate != null;
        return parsedEndDate.after(parsedStartDate);
    }

    private void extractCommEventFieldInfo(Map<String, Integer> fieldMapping, List<String> dynamicFields, List<String>
            attrFields, List<String> headerRow) {
        for (int col = 0; col < headerRow.size(); col++) {
            if (headerRow.get(col).startsWith(COMM_EVENT_ATTR_PREFIX)) {
                String trimString = COMM_EVENT_ATTR_PREFIX + "_";
                String metaFields = headerRow.get(col).substring(trimString.length());
                attrFields.add(metaFields);
                fieldMapping.put(metaFields, col);
            } else if (headerRow.get(col).startsWith(COMM_EVENT_DYNAMICFIELD_PREFIX)) {
                String trimString = COMM_EVENT_DYNAMICFIELD_PREFIX + "_";
                String dynamicField = headerRow.get(col).substring(trimString.length());
                dynamicFields.add(dynamicField);
                fieldMapping.put(dynamicField, col);
            } else {
                fieldMapping.put(headerRow.get(col), col);
            }
        }
    }
}
