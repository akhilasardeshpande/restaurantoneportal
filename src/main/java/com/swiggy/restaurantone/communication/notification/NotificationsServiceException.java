package com.swiggy.restaurantone.communication.notification;

public class NotificationsServiceException extends RuntimeException {
    public NotificationsServiceException(String message) {
        super(message);
    }

    public NotificationsServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
