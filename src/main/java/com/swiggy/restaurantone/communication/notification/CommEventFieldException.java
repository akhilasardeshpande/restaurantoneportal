package com.swiggy.restaurantone.communication.notification;

public class CommEventFieldException extends RuntimeException{
    public CommEventFieldException(String message) {
        super(message);
    }

    public CommEventFieldException(String message, Throwable cause) {
        super(message, cause);
    }
}
