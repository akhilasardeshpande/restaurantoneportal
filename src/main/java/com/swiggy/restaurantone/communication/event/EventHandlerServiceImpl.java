package com.swiggy.restaurantone.communication.event;

import com.swiggy.restaurantone.communication.CommEngineResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import retrofit2.Response;

import java.io.IOException;

@Slf4j
@Configuration
public class EventHandlerServiceImpl implements EventHandlerService {
    @Autowired
    private EventHandlerApiClient eventHandlerApiClient;

    @Override
    public Object getAllEventsByPod(String pod) throws EventHandlerServiceException {
        Object responseBody;
        try {
            Response<CommEngineResponse> response = eventHandlerApiClient.getAllEventsByPods(pod).execute();
            responseBody = extractAndValidateResponseBody(response);
        } catch (IOException e) {
            throw new EventHandlerServiceException(e.getMessage(), e.getCause());
        }
        return responseBody;
    }

    @Override
    public Object getAllTemplateVariablesByPod(String pod) throws EventHandlerServiceException {
        Object responseBody;
        try {
            Response<CommEngineResponse> response = eventHandlerApiClient.getAllTemplateVariablesByPod(pod).execute();
            responseBody = extractAndValidateResponseBody(response);
        } catch (IOException e) {
            throw new EventHandlerServiceException(e.getMessage(), e.getCause());
        }
        return responseBody;
    }

    private Object extractAndValidateResponseBody(Response<CommEngineResponse> response) {
        if (response == null || !response.isSuccessful() || response.body() == null) {
            log.error("Empty or no response from Event Handler Service: {}", response);
            throw new EventHandlerServiceException(String.format("Empty or no response from Event Handler Service: %s",
                    response));
        }
        log.info("Success response from Event Handler Service: {}", response.body());
        return response.body().getData();
    }

    @Override
    public Object getAllEvents() throws EventHandlerServiceException {
        Object responseBody;
        try {
            Response<CommEngineResponse> response = eventHandlerApiClient.getAllEvents().execute();
            responseBody = extractAndValidateResponseBody(response);
        } catch (IOException e) {
            throw new EventHandlerServiceException(e.getMessage(), e.getCause());
        }
        return responseBody;
    }

    @Override
    public Object createPODEvent(String pod,Object requestBody) throws EventHandlerServiceException {
        Object responseBody;
        try {
            Response<CommEngineResponse> response = eventHandlerApiClient.createPODEvent(pod, requestBody).execute();
            responseBody = extractAndValidateResponseBody(response);
        } catch (IOException e) {
            throw new EventHandlerServiceException(e.getMessage(), e.getCause());
        }
        return responseBody;
    }
}
