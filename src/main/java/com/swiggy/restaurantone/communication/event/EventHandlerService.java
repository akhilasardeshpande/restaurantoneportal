package com.swiggy.restaurantone.communication.event;

import com.swiggy.restaurantone.communication.event.EventHandlerServiceException;

public interface EventHandlerService {
    Object getAllEventsByPod(String pod) throws EventHandlerServiceException;

    Object getAllTemplateVariablesByPod(String pod) throws EventHandlerServiceException;

    Object getAllEvents() throws EventHandlerServiceException;

    Object createPODEvent(String pod, final Object requestBody) throws EventHandlerServiceException;
}
