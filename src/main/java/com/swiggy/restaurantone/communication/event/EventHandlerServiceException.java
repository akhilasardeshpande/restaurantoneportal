package com.swiggy.restaurantone.communication.event;

public class EventHandlerServiceException extends RuntimeException {
    public EventHandlerServiceException(String message) {
        super(message);
    }

    public EventHandlerServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
