package com.swiggy.restaurantone.communication.event;

import com.swiggy.restaurantone.communication.CommEngineResponse;
import retrofit2.Call;
import retrofit2.http.*;

public interface EventHandlerApiClient {
    @Headers({"Content-Type: application/json"})
    @GET("/event-pod/{pod}")
    Call<CommEngineResponse> getAllEventsByPods(@Path("pod") String pod);

    @Headers({"Content-Type: application/json"})
    @GET("/template-variable-pod/{pod}")
    Call<CommEngineResponse> getAllTemplateVariablesByPod(@Path("pod") String pod);

    @Headers({"Content-Type: application/json"})
    @GET("/event-pod")
    Call<CommEngineResponse> getAllEvents();

    @Headers({"Content-Type: application/json"})
    @POST("/event-pod/{pod}")
    Call<CommEngineResponse> createPODEvent(@Path("pod") String pod, @Body Object body);
}
