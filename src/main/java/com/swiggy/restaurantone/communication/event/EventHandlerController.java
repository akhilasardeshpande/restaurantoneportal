package com.swiggy.restaurantone.communication.event;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
public class EventHandlerController extends AbstractController {
    @Autowired
    private EventHandlerService eventHandlerService;

    @RequestMapping(value = "/event-pod/{pod}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> getAllEventsByPod(@PathVariable(value = "pod") String pod) {
        log.info("Request received. API: {}, Pod: {}", "GET_ALL_EVENTS_BY_POD", pod);
        return this.executeTask(() -> eventHandlerService.getAllEventsByPod(pod));
    }
    @RequestMapping(value = "/template-variable-pod/{pod}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> getAllTemplateVariablesByPod(@PathVariable(value = "pod") String pod) {
        log.info("Request received. API: {}, Pod: {}", "GET_ALL_TEMPLATE_VARIABLES_BY_POD", pod);
        return this.executeTask(() -> eventHandlerService.getAllTemplateVariablesByPod(pod));
    }
    @RequestMapping(value = "/event-pod", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> getAllEvents() {
        log.info("Request received. API: {}, Pod: {}", "GET_ALL_EVENTS");
        return this.executeTask(() -> eventHandlerService.getAllEvents());
    }

    @RequestMapping(value = "/event-pod/{pod}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> createPODEvent(@PathVariable(value = "pod") String pod, @RequestBody Object requestBody) {
        log.info("Request received. API: {}, Pod: {}, RequestBody: {}", "CREATE_EVENT_IN_POD", pod, requestBody);
        return this.executeTask(() -> eventHandlerService.createPODEvent(pod, requestBody));
    }
}
