package com.swiggy.restaurantone.communication;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CommEngineResponse<T> {
    @JsonProperty(value = "statusCode")
    Integer code;

    @JsonProperty(value = "statusMessage")
    String message;

    @JsonProperty(value = "data")
    T data;
}