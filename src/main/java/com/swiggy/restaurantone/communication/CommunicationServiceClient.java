package com.swiggy.restaurantone.communication;

import org.springframework.web.bind.annotation.RequestParam;
import retrofit2.Call;
import retrofit2.http.*;

public interface CommunicationServiceClient {

    @Headers({"Content-Type: application/json"})
    @POST("/api/v1/template")
    Call<CommEngineResponse> createTemplate(@Body Object requestBody);

    @Headers({"Content-Type: application/json"})
    @PUT("/template")
    Call<CommEngineResponse> updateTemplate(@Body Object requestBody);

    @Headers({"Content-Type: application/json"})
    @GET("/api/v1/template/{id}")
    Call<CommEngineResponse> getTemplateById(@Path("id") String id);

    @Headers({"Content-Type: application/json"})
    @GET("/api/v1/template/search")
    Call<CommEngineResponse> searchTemplate(@Query("name") String name, @Query("type") String type);

    @Headers({"Content-Type: application/json"})
    @POST("/api/v1/template/sendTestMessage")
    Call<CommEngineResponse> sendTestMessage(@Body Object requestBody);

    @Headers({"Content-Type: application/json"})
    @GET("/api/v1/template")
    Call<CommEngineResponse> getAllTemplates(@RequestParam(value = "type", required = false) String type,
                                             @RequestParam(value = "name", required = false) String name);

    @Headers({"Content-Type: application/json"})
    @GET("/api/v1/event")
    Call<CommEngineResponse> getAllEvents();


    @Headers({"Content-Type: application/json"})
    @GET("/api/v1/event/component/{componentName}/event-name/{eventName}")
    Call<CommEngineResponse> getAllEventsByComponentAndName(@Path(value = "componentName") String componentName,
                                                            @Path(value = "eventName") String eventName);


    @Headers({"Content-Type: application/json"})
    @GET("/api/v1/event/component/{componentName}")
    Call<CommEngineResponse> getAllEventsByComponent(@Path(value = "componentName") String componentName);

    @Headers({"Content-Type: application/json"})
    @POST("/api/v1/event")
    Call<CommEngineResponse> createEvent(@Body Object requestBody);


    @Headers({"Content-Type: application/json"})
    @DELETE("/api/v1/event")
    Call<CommEngineResponse> deleteEvent(@Body Object requestBody);

    @Headers({"Content-Type: application/json"})
    @POST("/api/v1/communication")
    Call<CommEngineResponse> sendCommunicationEvent(@Body Object requestBody);
}
