package com.swiggy.restaurantone.communication;

public class CommunicationServiceException extends RuntimeException {
    public CommunicationServiceException(String message) {
        super(message);
    }

    public CommunicationServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
