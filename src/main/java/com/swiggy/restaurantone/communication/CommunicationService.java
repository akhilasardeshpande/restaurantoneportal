package com.swiggy.restaurantone.communication;

public interface CommunicationService {
    Object getAllTemplates(String type, String name) throws CommunicationServiceException;

    Object getTemplateById(String id) throws CommunicationServiceException;

    Object updateTemplate(Object requestBody) throws CommunicationServiceException;

    Object createTemplate(Object requestBody) throws CommunicationServiceException;

    Object sendTestMessage(Object requestBody) throws CommunicationServiceException;

    Object getAllEvents() throws CommunicationServiceException;

    Object getAllEventsByComponent(String componentName) throws CommunicationServiceException;

    Object getAllEventsByComponentAndName(String componentName, String eventName) throws CommunicationServiceException;

    Object createEvent(Object requestBody) throws CommunicationServiceException;

    Object deleteEvent(Object requestBody) throws CommunicationServiceException;

    Object sendCommunicationEvent(Object requestBody) throws CommunicationServiceException;
}
