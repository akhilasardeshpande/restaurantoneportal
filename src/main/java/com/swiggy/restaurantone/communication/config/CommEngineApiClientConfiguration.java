package com.swiggy.restaurantone.communication.config;

import com.swiggy.restaurantone.apiClients.ApiClientBuilder;
import com.swiggy.restaurantone.communication.CommunicationServiceClient;
import com.swiggy.restaurantone.communication.campaign.CampaignServiceClient;
import com.swiggy.restaurantone.communication.event.EventHandlerApiClient;
import com.swiggy.restaurantone.communication.template.TemplateServiceClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class CommEngineApiClientConfiguration {
    @Value("${hostname.commEngine.templateService}")
    private String templateServiceHostName;

    @Value("${communication-service.host-name}")
    private String communicationServiceHostName;

    @Value("${communication-service.timeout}")
    private String communicationServiceTimeout;


    @Value("${timeout.commEngine.templateService}")
    private String templateServiceTimeout;

    @Value("${hostname.commEngine.campaignService}")
    private String campaignServiceHostName;
    @Value("${timeout.commEngine.campaignService}")
    private String campaignServiceTimeout;

    @Value("${hostname.commEngine.eventHandler}")
    private String eventHandlerHostName;
    @Value("${timeout.commEngine.eventHandler}")
    private String eventHandlerTimeout;

    @Bean
    public TemplateServiceClient templateServiceClient() {
        return ApiClientBuilder.builder().
                baseUrl(templateServiceHostName).
                loggingInterceptor(new HttpLoggingInterceptor()).
                connectTimeout(Long.parseLong(templateServiceTimeout), TimeUnit.MILLISECONDS).
                readTimeout(Long.parseLong(templateServiceTimeout), TimeUnit.MILLISECONDS).
                writeTimeout(Long.parseLong(templateServiceTimeout), TimeUnit.MILLISECONDS).
                build(TemplateServiceClient.class);
    }

    @Bean
    public CommunicationServiceClient communicationServiceClient() {
        return ApiClientBuilder.builder().
                baseUrl(communicationServiceHostName).
                loggingInterceptor(new HttpLoggingInterceptor()).
                connectTimeout(Long.parseLong(communicationServiceTimeout), TimeUnit.MILLISECONDS).
                readTimeout(Long.parseLong(communicationServiceTimeout), TimeUnit.MILLISECONDS).
                writeTimeout(Long.parseLong(communicationServiceTimeout), TimeUnit.MILLISECONDS).
                build(CommunicationServiceClient.class);
    }

    @Bean
    public CampaignServiceClient campaignServiceClient() {
        return ApiClientBuilder.builder().
                baseUrl(campaignServiceHostName).
                loggingInterceptor(new HttpLoggingInterceptor()).
                connectTimeout(Long.parseLong(campaignServiceTimeout), TimeUnit.MILLISECONDS).
                readTimeout(Long.parseLong(campaignServiceTimeout), TimeUnit.MILLISECONDS).
                writeTimeout(Long.parseLong(campaignServiceTimeout), TimeUnit.MILLISECONDS).
                build(CampaignServiceClient.class);
    }

    @Bean
    public EventHandlerApiClient eventHandlerClient() {
        return ApiClientBuilder.builder().
                baseUrl(eventHandlerHostName).
                loggingInterceptor(new HttpLoggingInterceptor()).
                connectTimeout(Long.parseLong(eventHandlerTimeout), TimeUnit.MILLISECONDS).
                readTimeout(Long.parseLong(eventHandlerTimeout), TimeUnit.MILLISECONDS).
                writeTimeout(Long.parseLong(eventHandlerTimeout), TimeUnit.MILLISECONDS).
                build(EventHandlerApiClient.class);
    }
}
