package com.swiggy.restaurantone.communication.template;

import com.swiggy.restaurantone.communication.CommEngineResponse;
import retrofit2.Call;
import retrofit2.http.*;

public interface TemplateServiceClient {

    @Headers({"Content-Type: application/json"})
    @POST("/template")
    Call<CommEngineResponse> createTemplate(@Body Object requestBody);

    @Headers({"Content-Type: application/json"})
    @PUT("/template/")
    Call<CommEngineResponse> updateTemplate(@Body Object requestBody);

    @Headers({"Content-Type: application/json"})
    @GET("/template/{id}")
    Call<CommEngineResponse> getTemplateById(@Path("id") String id);

    @Headers({"Content-Type: application/json"})
    @GET("/template/search")
    Call<CommEngineResponse> searchTemplate(@Query("name") String name, @Query("type") String type);

    @Headers({"Content-Type: application/json"})
    @GET("/template/history/{id}")
    Call<CommEngineResponse> templateHistoryById(@Path("id") Long id, @Query("page") Integer page,
                                     @Query("per-page") Integer perPage);

    @Headers({"Content-Type: application/json"})
    @POST("/template/sendTestMessage")
    Call<CommEngineResponse> sendTestMessage(@Body Object requestBody);

}
