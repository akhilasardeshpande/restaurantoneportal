package com.swiggy.restaurantone.communication.template;

public interface TemplateService {

    Object getAllTemplates(String type, String name) throws TemplateServiceException;

    Object getTemplateById(String id) throws TemplateServiceException;

    Object updateTemplate(Object requestBody) throws TemplateServiceException;

    Object createTemplate(Object requestBody) throws TemplateServiceException;

    Object sendTestMessage(Object requestBody) throws TemplateServiceException;

    Object getTemplateHistoryById(Long templateId, Integer page, Integer perPage) throws TemplateServiceException;
}
