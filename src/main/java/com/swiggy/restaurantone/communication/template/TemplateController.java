package com.swiggy.restaurantone.communication.template;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = {"/template", "/api/comm-engine/template"})
public class TemplateController extends AbstractController {
    @Autowired
    private TemplateService templateService;

    @RequestMapping(value = {"", "/"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> createTemplate(@RequestBody Object requestBody) {
        log.info("Request received. API: {}, RequestBody: {}", "CREATE_TEMPLATE", requestBody);
        return this.executeTask(() -> templateService.createTemplate(requestBody));
    }

    @RequestMapping(value = {"", "/"}, method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> updateTemplate(@RequestBody Object requestBody) {
        log.info("Request received. API: {}, RequestBody: {}", "UPDATE_TEMPLATE", requestBody);
        return this.executeTask(() -> templateService.updateTemplate(requestBody));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> getTemplateById(@PathVariable(value = "id") String id) {
        log.info("Request received. API: {}, Id: {}", "GET_TEMPLATE_BY_ID", id);
        return this.executeTask(() -> templateService.getTemplateById(id));
    }

    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> getAllTemplates(@RequestParam(value = "type", required = false) String type,
                                       @RequestParam(value = "name", required = false) String name) {
        log.info("Request received. API: {}, Type: {}, Name: {}", "GET_ALL_TEMPLATES", type, name);
        return this.executeTask(() -> templateService.getAllTemplates(type, name));
    }

    @RequestMapping(value = "/searchName", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> searchTemplate(@RequestParam(name = "name") String name, @RequestParam(name = "type") String type) {
        log.info("Request received. API: {}, Type: {}, Name: {}", "SEARCH_TEMPLATE", type, name);
        return this.executeTask(() -> templateService.getAllTemplates(type, name));
    }

    @RequestMapping(value = "/sendTestMessage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> sendTestMessage(@RequestBody Object requestBody) {
        log.info("Request received. API: {}, RequestBody: {}", "SEND_TEST_MESSAGE", requestBody);
        return this.executeTask(() -> templateService.sendTestMessage(requestBody));
    }

    @RequestMapping(value = "/history/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> getTemplateHistoryById(@PathVariable("id") Long id,
                                              @RequestParam("page") Integer page,
                                              @RequestParam("per-page") Integer perPage) {
        log.info("Request received. API: {}, Id: {}, Page: {}, PerPage: {}", "GET_TEMPLATE_HISTORY_BY_ID", id, page, perPage);
        return this.executeTask(() -> templateService.getTemplateHistoryById(id, page, perPage));
    }
}
