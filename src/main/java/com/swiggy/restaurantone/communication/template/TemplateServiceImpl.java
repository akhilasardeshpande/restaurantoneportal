package com.swiggy.restaurantone.communication.template;

import com.swiggy.restaurantone.communication.CommEngineResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.IOException;

@Slf4j
@Service
public class TemplateServiceImpl implements TemplateService {
    @Autowired
    private TemplateServiceClient templateServiceClient;

    @Override
    public Object getAllTemplates(String type, String name) throws TemplateServiceException {
        Object responseBody;
        try {
            Response<CommEngineResponse> response = templateServiceClient.searchTemplate(name, type).execute();
            responseBody = extractAndValidateResponseBody(response);
        } catch (IOException e) {
            throw new TemplateServiceException(e.getMessage(), e.getCause());
        }
        return responseBody;
    }

    @Override
    public Object getTemplateById(String id) throws TemplateServiceException {
        Object responseBody;
        try {
            Response<CommEngineResponse> response = templateServiceClient.getTemplateById(id).execute();
            responseBody = extractAndValidateResponseBody(response);
        } catch (IOException e) {
            throw new TemplateServiceException(e.getMessage(), e.getCause());
        }
        return responseBody;
    }

    @Override
    public Object updateTemplate(Object requestBody) throws TemplateServiceException {
        Object responseBody;
        try {
            Response<CommEngineResponse> response = templateServiceClient.updateTemplate(requestBody).execute();
            responseBody = extractAndValidateResponseBody(response);
        } catch (IOException e) {
            throw new TemplateServiceException(e.getMessage(), e.getCause());
        }
        return responseBody;
    }


    @Override
    public Object createTemplate(Object requestBody) throws TemplateServiceException {
        try {
            Response<CommEngineResponse> response = templateServiceClient.createTemplate(requestBody).execute();
            if (!response.isSuccessful()) {
                if (response.errorBody() != null) {
                    return response.errorBody().string();
                }
                throw new TemplateServiceException(String.format("Empty or no response from Template Service: %s",
                        response));
            } else {
                return response.body().getData();
            }
        } catch (IOException e) {
            throw new TemplateServiceException(e.getMessage(), e.getCause());
        }
    }

    @Override
    public Object sendTestMessage(Object requestBody) throws TemplateServiceException {
        Object responseBody;
        try {
            Response<CommEngineResponse> response = templateServiceClient.sendTestMessage(requestBody).execute();
            responseBody = extractAndValidateResponseBody(response);
        } catch (IOException e) {
            throw new TemplateServiceException(e.getMessage(), e.getCause());
        }
        return responseBody;
    }

    @Override
    public Object getTemplateHistoryById(Long templateId, Integer page, Integer perPage) throws TemplateServiceException {
        Object responseBody;
        try {
            Response<CommEngineResponse> response = templateServiceClient.templateHistoryById(templateId, page, perPage).execute();
            responseBody = extractAndValidateResponseBody(response);
        } catch (IOException e) {
            throw new TemplateServiceException(e.getMessage(), e.getCause());
        }
        return responseBody;
    }

    private Object extractAndValidateResponseBody(Response<CommEngineResponse> response) {
        if (response == null || !response.isSuccessful() || response.body() == null) {
            log.error("Empty or no response from Template Service: {}", response);
            throw new TemplateServiceException(String.format("Empty or no response from Template Service: %s",
                    response));
        }
        log.info("Success response from Template Service: {}", response.body());
        return response.body().getData();
    }
}
