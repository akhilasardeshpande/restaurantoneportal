package com.swiggy.restaurantone.communication;

import com.swiggy.restaurantone.annotations.NudgeInstrument;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.Objects;
import java.util.function.Supplier;

@Slf4j
@Service
public class CommunicationServiceImpl implements CommunicationService {

    @Autowired
    private CommunicationServiceClient communicationServiceClient;

    @Override
    public Object getAllTemplates(String type, String name) throws CommunicationServiceException {
        return callCommunicationServiceClient(() -> communicationServiceClient.searchTemplate(name, type));
    }

    @Override
    public Object getTemplateById(String id) throws CommunicationServiceException {
        return callCommunicationServiceClient(() -> communicationServiceClient.getTemplateById(id));
    }

    @Override
    public Object updateTemplate(Object requestBody) throws CommunicationServiceException {
        return callCommunicationServiceClient(() -> communicationServiceClient.updateTemplate(requestBody));
    }

    @Override
    @NudgeInstrument
    public Object createTemplate(Object requestBody) throws CommunicationServiceException {
        return callCommunicationServiceClient(() -> communicationServiceClient.createTemplate(requestBody));
    }

    @Override
    public Object sendTestMessage(Object requestBody) throws CommunicationServiceException {
        return callCommunicationServiceClient(() -> communicationServiceClient.sendTestMessage(requestBody));
    }

    @Override
    public Object getAllEvents() throws CommunicationServiceException {
        return callCommunicationServiceClient(() -> communicationServiceClient.getAllEvents());
    }

    @Override
    public Object getAllEventsByComponent(String componentName) throws CommunicationServiceException {
        return callCommunicationServiceClient(() -> communicationServiceClient.getAllEventsByComponent(componentName));
    }

    @Override
    public Object getAllEventsByComponentAndName(String componentName, String eventName) throws CommunicationServiceException {
        return callCommunicationServiceClient(() -> communicationServiceClient.getAllEventsByComponentAndName(componentName, eventName));
    }


    @Override
    public Object createEvent(Object requestBody) throws CommunicationServiceException {
        return callCommunicationServiceClient(() -> communicationServiceClient.createEvent(requestBody));
    }

    @Override
    public Object deleteEvent(Object requestBody) throws CommunicationServiceException {
        return callCommunicationServiceClient(() -> communicationServiceClient.deleteEvent(requestBody));
    }

    @Override
    public Object sendCommunicationEvent(Object requestBody) throws CommunicationServiceException {
        return callCommunicationServiceClient(() -> communicationServiceClient.sendCommunicationEvent(requestBody));
    }

    private Object callCommunicationServiceClient(Supplier<Call<CommEngineResponse>> supplier) throws CommunicationServiceException {
        Object responseBody;
        try {
            Response<CommEngineResponse> response = supplier.get().execute();
            responseBody = extractAndValidateResponseBody(response);
        } catch (IOException e) {
            throw new CommunicationServiceException(e.getMessage(), e.getCause());
        }
        return responseBody;
    }

    private Object extractAndValidateResponseBody(Response<CommEngineResponse> response) {
        if (Objects.isNull(response) || !response.isSuccessful() || Objects.isNull(response.body())) {
            log.error("Empty or no response from Communication Service: {}", response);
            throw new CommunicationServiceException(String.format("Empty or no response from Communication Service: %s",
                    response));
        }
        log.info("Success response from Communication Service: {}", response.body());
        return response.body().getData();
    }
}
