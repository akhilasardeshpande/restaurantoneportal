package com.swiggy.restaurantone.headlesscms;

import com.swiggy.restaurantone.apiClients.ApiClientBuilder;

import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class HeadlessCmsApiClientConfiguration {

    @Value("${headlesscms-service.timeout}")
    private String headlessCMSServiceTimeout;

    @Value("${headlesscms-service.host-name}")
    private String headlessCMSServiceHostName;

    @Bean
    public HeadlessCMSServiceClient headlessCMSAPIService() {
        return ApiClientBuilder.builder().
                baseUrl(headlessCMSServiceHostName).
                loggingInterceptor(new HttpLoggingInterceptor()).
                connectTimeout(Long.parseLong(headlessCMSServiceTimeout), TimeUnit.MILLISECONDS).
                readTimeout(Long.parseLong(headlessCMSServiceTimeout), TimeUnit.MILLISECONDS).
                writeTimeout(Long.parseLong(headlessCMSServiceTimeout), TimeUnit.MILLISECONDS).
                build(HeadlessCMSServiceClient.class);
    }
}
