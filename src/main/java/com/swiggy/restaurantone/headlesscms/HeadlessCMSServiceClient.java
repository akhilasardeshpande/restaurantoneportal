package com.swiggy.restaurantone.headlesscms;

import org.json.simple.JSONObject;
import retrofit2.Call;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Body;
import retrofit2.http.Path;

import javax.validation.Valid;

public interface HeadlessCMSServiceClient {

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/v1/schema/create")
    Call<HeadlessCMSResponse> createSchema(@Valid @Body JSONObject data);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/v1/schema/update")
    Call<HeadlessCMSResponse> updateSchema(@Valid @Body JSONObject data);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/v1/schema/fetch/all")
    Call<HeadlessCMSResponse> getAllSchemas();

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/v1/schema/{schemaId}")
    Call<HeadlessCMSResponse> getSchemaById(@Path(value = "schemaId") String schemaId);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/v1/content/create")
    Call<HeadlessCMSResponse> createContent(@Valid @Body JSONObject data);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/v1/content/{contentId}")
    Call<HeadlessCMSResponse> getContentById(@Path(value = "contentId") String contentId);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/v1/content/update/publish")
    Call<HeadlessCMSResponse> publishContent(@Valid @Body JSONObject data);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/v1/content/update/archive")
    Call<HeadlessCMSResponse> archiveContent(@Valid @Body JSONObject data);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/v1/content/update")
    Call<HeadlessCMSResponse> updateContent(@Valid @Body JSONObject data);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/v1/collection/create")
    Call<HeadlessCMSResponse> createCollection(@Valid @Body JSONObject data);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/v1/collection/update")
    Call<HeadlessCMSResponse> updateCollection(@Valid @Body JSONObject data);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/v1/collection/reorder")
    Call<HeadlessCMSResponse> reorderCollection(@Valid @Body JSONObject data);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/v1/collection/fetch/all")
    Call<HeadlessCMSResponse> getAllCollections();

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/v1/collection/{collectionId}")
    Call<HeadlessCMSResponse> getCollectionById(@Path(value = "collectionId") String collectionId);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/v1/space/create")
    Call<HeadlessCMSResponse> createSpace(@Valid @Body JSONObject data);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/v1/space/fetch/all")
    Call<HeadlessCMSResponse> getAllSpaces();

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/v1/space/list/{spaceId}")
    Call<HeadlessCMSResponse> getAllSpacesBySpaceId(@Path(value = "spaceId") String spaceId);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/v1/space/details/{spaceId}")
    Call<HeadlessCMSResponse> getSpaceDetails(@Path(value = "spaceId") String spaceId);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/v1/space/update")
    Call<HeadlessCMSResponse> updateSpace(@Valid @Body JSONObject data);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/v1/space/reorder")
    Call<HeadlessCMSResponse> reorderSpace(@Valid @Body JSONObject data);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @HTTP(method = "DELETE", path = "/v1/space/delete", hasBody = true)
    Call<HeadlessCMSResponse> deleteSpace(@Valid @Body JSONObject data);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/v1/segment")
    Call<HeadlessCMSResponse> getAllSegments(@Valid @Body JSONObject data);
}
