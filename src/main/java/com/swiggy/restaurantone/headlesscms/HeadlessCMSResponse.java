package com.swiggy.restaurantone.headlesscms;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.Valid;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
public class HeadlessCMSResponse<T>{

    @JsonProperty(value = "statusCode")
    private int code;

    @JsonProperty(value = "statusMessage")
    private String message;

    @JsonProperty(value = "data")
    @Valid
    protected T data;

    @JsonProperty(value = "error")
    String error;
}

