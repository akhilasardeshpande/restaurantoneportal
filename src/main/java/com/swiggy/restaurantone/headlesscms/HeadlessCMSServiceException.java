package com.swiggy.restaurantone.headlesscms;

public class HeadlessCMSServiceException extends RuntimeException {

    public HeadlessCMSServiceException(String message) {
        super(message);
    }

    public  HeadlessCMSServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
