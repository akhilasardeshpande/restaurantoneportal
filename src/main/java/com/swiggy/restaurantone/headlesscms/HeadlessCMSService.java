package com.swiggy.restaurantone.headlesscms;

import org.json.simple.JSONObject;

public interface HeadlessCMSService {

     HeadlessCMSResponse createSchema(JSONObject request) throws HeadlessCMSServiceException;

     HeadlessCMSResponse updateSchema(JSONObject request) throws HeadlessCMSServiceException;

     HeadlessCMSResponse getAllSchemas() throws HeadlessCMSServiceException;

     HeadlessCMSResponse getSchemaById(String schemaIds) throws HeadlessCMSServiceException;

     HeadlessCMSResponse createContent(JSONObject request) throws HeadlessCMSServiceException;

     HeadlessCMSResponse getContentById(String contentId) throws HeadlessCMSServiceException;

     HeadlessCMSResponse publishContent(JSONObject request) throws HeadlessCMSServiceException;

     HeadlessCMSResponse archiveContent(JSONObject request) throws HeadlessCMSServiceException;

     HeadlessCMSResponse updateContent(JSONObject request) throws HeadlessCMSServiceException;

     HeadlessCMSResponse createCollection(JSONObject request) throws HeadlessCMSServiceException;

     HeadlessCMSResponse updateCollection(JSONObject request) throws HeadlessCMSServiceException;

     HeadlessCMSResponse reorderCollection(JSONObject request) throws HeadlessCMSServiceException;

     HeadlessCMSResponse getAllCollections() throws HeadlessCMSServiceException;

     HeadlessCMSResponse getCollectionById(String collectionId) throws HeadlessCMSServiceException;

     HeadlessCMSResponse createSpace(JSONObject request) throws HeadlessCMSServiceException;

     HeadlessCMSResponse getAllSpaces() throws HeadlessCMSServiceException;

     HeadlessCMSResponse getAllSpacesBySpaceId(String spaceId) throws HeadlessCMSServiceException;

     HeadlessCMSResponse getSpaceDetails(String spaceId) throws HeadlessCMSServiceException;

     HeadlessCMSResponse updateSpace(JSONObject request) throws HeadlessCMSServiceException;

     HeadlessCMSResponse reorderSpace(JSONObject request) throws HeadlessCMSServiceException;

     HeadlessCMSResponse deleteSpace(JSONObject request) throws HeadlessCMSServiceException;

     HeadlessCMSResponse getAllSegments(JSONObject request) throws HeadlessCMSServiceException;
}
