package com.swiggy.restaurantone.headlesscms;

import com.google.api.client.http.HttpStatusCodes;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/v1")
@Slf4j
public class HeadlessCMSController {

    private AuthorizationHelper authorizationHelper;

    private HeadlessCMSServiceImpl headlessCMSService;

    @Value("${headlessCMSPermission}")
    private Long headlessCMSPermission;

    @Autowired
    public HeadlessCMSController(AuthorizationHelper authorizationHelper, HeadlessCMSServiceImpl headlessCMSService) {
        this.authorizationHelper = authorizationHelper;
        this.headlessCMSService = headlessCMSService;
    }

    @PostMapping(value = "/schema/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse createSchema(@Valid @RequestBody JSONObject request) {
        log.info("Request received. API: {}, RequestBody: {}", "CREATE_SCHEMA", request);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.createSchema(request);
    }

    @PostMapping(value = "/schema/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse updateSchema(@Valid @RequestBody JSONObject request) {
        log.info("Request received. API: {}, RequestBody: {}", "UPDATE_SCHEMA", request);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.updateSchema(request);
    }

    @GetMapping (value = "/schema/fetch/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse getAllSchemas() {
        log.info("Request received. API: {}", "GET_ALL_SCHEMAS");
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.getAllSchemas();
    }

    @GetMapping (value = "/schema/{schemaId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse getSchemaById(@PathVariable String schemaId) {
        log.info("Request received. API: {}, SchemaId: {}", "GET_SCHEMA_BY_ID", schemaId);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.getSchemaById(schemaId);
    }

    @PostMapping (value = "/content/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse createContent(@Valid @RequestBody JSONObject request) {
        log.info("Request received. API: {}, RequestBody: {}", "CREATE_CONTENT", request);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.createContent(request);
    }

    @GetMapping (value = "/content/{contentId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse getContentById(@PathVariable String contentId) {
        log.info("Request received. API: {}, ContentId: {}", "GET_CONTENT_BY_ID", contentId);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.getContentById(contentId);
    }

    @PostMapping (value = "/content/update/publish", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse publishContent(@Valid @RequestBody JSONObject request) {
        log.info("Request received. API: {}, RequestBody: {}", "PUBLISH_CONTENT", request);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.publishContent(request);
    }

    @PostMapping (value = "/content/update/archive", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse archiveContent(@Valid @RequestBody JSONObject request) {
        log.info("Request received. API: {}, RequestBody: {}", "ARCHIVE_CONTENT", request);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.archiveContent(request);
    }

    @PostMapping (value = "/content/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse updateContent(@Valid @RequestBody JSONObject request) {
        log.info("Request received. API: {}, RequestBody: {}", "UPDATE_CONTENT", request);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.updateContent(request);
    }

    @PostMapping (value = "/collection/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse createCollection(@Valid @RequestBody JSONObject request) {
        log.info("Request received. API: {}, RequestBody: {}", "CREATE_COLLECTION", request);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.createCollection(request);
    }

    @PostMapping (value = "/collection/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse updateCollection(@Valid @RequestBody JSONObject request) {
        log.info("Request received. API: {}, RequestBody: {}", "UPDATE_COLLECTION", request);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.updateCollection(request);
    }
    @PostMapping (value = "/collection/reorder", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse reorderCollection(@Valid @RequestBody JSONObject request) {
        log.info("Request received. API: {}, RequestBody: {}", "REORDER_COLLECTION", request);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.reorderCollection(request);
    }

    @GetMapping (value = "/collection/fetch/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse getAllCollections() {
        log.info("Request received. API: {}", "GET_ALL_COLLECTIONS");
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.getAllCollections();
    }

    @GetMapping (value = "/collection/{collectionId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse getCollectionById(@PathVariable String collectionId) {
        log.info("Request received. API: {}, CollectionId: {}", "GET_COLLECTION_BY_ID", collectionId);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.getCollectionById(collectionId);
    }

    @PostMapping (value = "/space/create", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse createSpace(@Valid @RequestBody JSONObject request) {
        log.info("Request received. API: {}, RequestBody: {}", "CREATE_SPACE", request);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.createSpace(request);
    }

    @GetMapping (value = "/space/fetch/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse getAllSpaces() {
        log.info("Request received. API: {}", "GET_ALL_SPACES");
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.getAllSpaces();
    }

    @GetMapping (value = "/space/list/{spaceId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse getAllSpacesBySpaceId(@PathVariable String spaceId) {
        log.info("Request received. API: {}, SpaceId: {}", "GET_ALL_SPACES_BY_SPACE_ID", spaceId);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.getAllSpacesBySpaceId(spaceId);
    }

    @GetMapping (value = "/space/details/{spaceId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse getSpaceDetails(@PathVariable String spaceId) {
        log.info("Request received. API: {}, SpaceId: {}", "GET_SPACE_DETAILS", spaceId);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.getSpaceDetails(spaceId);
    }

    @PostMapping (value = "/space/update", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse updateSpace(@Valid @RequestBody JSONObject request) {
        log.info("Request received. API: {}, RequestBody: {}", "UPDATE_SPACE", request);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.updateSpace(request);
    }

    @PostMapping (value = "/space/reorder", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse reorderSpace(@Valid @RequestBody JSONObject request) {
        log.info("Request received. API: {}, RequestBody: {}", "REORDER_SPACE", request);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.reorderSpace(request);
    }

    @DeleteMapping (value = "/space/delete", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse deleteSpace(@Valid @RequestBody JSONObject request) {
        log.info("Request received. API: {}, RequestBody: {}", "DELETE_SPACE", request);
        if(!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED,"Unauthorized", null , null);

        return headlessCMSService.deleteSpace(request);
    }

    @GetMapping(value = "/segment", produces = MediaType.APPLICATION_JSON_VALUE)
    public HeadlessCMSResponse getAllSegments() {
        log.info("Request received. API: {}", "GET_ALL_SEGMENTS");
        if (!(authorizationHelper.authenticateUserSession() && authorizationHelper.checkPermissions(headlessCMSPermission)))
            return new HeadlessCMSResponse(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED, "Unauthorized", null, null);

        User user = authorizationHelper.fetchUser();
        JSONObject request = new JSONObject();
        request.put("user_email", user.email);
        return headlessCMSService.getAllSegments(request);
    }

}
