package com.swiggy.restaurantone.headlesscms;

import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;

import java.util.Objects;
import java.util.function.Supplier;

@Slf4j
@Service
public class HeadlessCMSServiceImpl implements HeadlessCMSService{

    @Autowired
    AuthorizationHelper authorizationHelper;

    @Autowired
    private HeadlessCMSServiceClient headlessCMSServiceClient;

    private HeadlessCMSResponse callHeadlessCMSServiceClient(Supplier<Call<HeadlessCMSResponse>> supplier) throws HeadlessCMSServiceException {
        HeadlessCMSResponse responseBody;
        try {
            Response<HeadlessCMSResponse> response = supplier.get().execute();
            responseBody = extractAndValidateResponseBody(response);
        } catch (Exception e) {
            throw new HeadlessCMSServiceException(e.getMessage(), e.getCause());
        }
        return responseBody;
    }

    private HeadlessCMSResponse extractAndValidateResponseBody(Response<HeadlessCMSResponse> response) {

        if (Objects.isNull(response) || !response.isSuccessful() || Objects.isNull(response.body())) {
            log.error("Empty or no response from HeadlessCMS Service: {}", response);
            throw new HeadlessCMSServiceException(String.format("Empty or no response from HeadlessCMS Service: %s",
                    response));
        }
        log.info("Success response from HeadlessCMS Service: {}", response.body());
        return response.body();
    }

    private void updateRequestBody(JSONObject request){
        JSONObject updatedBy = new JSONObject();
        User user = authorizationHelper.fetchUser();
        String name = user.getName();
        String email = user.getEmail();
        updatedBy.put("name", name);
        updatedBy.put("email", email);
        request.put("updatedBy", updatedBy);
    }

    public HeadlessCMSResponse createSchema(JSONObject request) throws HeadlessCMSServiceException{
        updateRequestBody(request);
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.createSchema(request));
    }

    public HeadlessCMSResponse updateSchema(JSONObject request) throws HeadlessCMSServiceException{
        updateRequestBody(request);
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.updateSchema(request));
    }

    public HeadlessCMSResponse getAllSchemas() throws HeadlessCMSServiceException{
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.getAllSchemas());
    }

    public HeadlessCMSResponse getSchemaById(String schemaId) throws HeadlessCMSServiceException{
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.getSchemaById(schemaId));
    }

    public HeadlessCMSResponse createContent(JSONObject request) throws HeadlessCMSServiceException{
        updateRequestBody(request);
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.createContent(request));
    }

    public HeadlessCMSResponse getContentById(String contentId) throws HeadlessCMSServiceException {
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.getContentById(contentId));
    }

    public HeadlessCMSResponse publishContent(JSONObject request) throws HeadlessCMSServiceException {
        updateRequestBody(request);
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.publishContent(request));
    }

    public HeadlessCMSResponse archiveContent(JSONObject request) throws HeadlessCMSServiceException {
        updateRequestBody(request);
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.archiveContent(request));
    }

    public HeadlessCMSResponse updateContent(JSONObject request) throws HeadlessCMSServiceException {
        updateRequestBody(request);
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.updateContent(request));
    }

    public HeadlessCMSResponse createCollection(JSONObject request) throws HeadlessCMSServiceException {
        updateRequestBody(request);
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.createCollection(request));
    }

    public HeadlessCMSResponse updateCollection(JSONObject request) throws HeadlessCMSServiceException {
        updateRequestBody(request);
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.updateCollection(request));
    }

    public HeadlessCMSResponse reorderCollection(JSONObject request) throws HeadlessCMSServiceException {
        updateRequestBody(request);
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.reorderCollection(request));
    }

    public HeadlessCMSResponse getAllCollections() throws HeadlessCMSServiceException {
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.getAllCollections());
    }

    public HeadlessCMSResponse getCollectionById(String collectionId) throws HeadlessCMSServiceException {
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.getCollectionById(collectionId));
    }

    public HeadlessCMSResponse createSpace(JSONObject request) throws HeadlessCMSServiceException {
        updateRequestBody(request);
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.createSpace(request));
    }

    public HeadlessCMSResponse getAllSpaces() throws HeadlessCMSServiceException {
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.getAllSpaces());
    }

    public HeadlessCMSResponse getAllSpacesBySpaceId(String spaceId) throws HeadlessCMSServiceException {
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.getAllSpacesBySpaceId(spaceId));
    }

    public HeadlessCMSResponse getSpaceDetails(String spaceId) throws HeadlessCMSServiceException {
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.getSpaceDetails(spaceId));
    }

    public HeadlessCMSResponse updateSpace(JSONObject request) throws HeadlessCMSServiceException {
        updateRequestBody(request);
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.updateSpace(request));
    }

    public HeadlessCMSResponse reorderSpace(JSONObject request) throws HeadlessCMSServiceException {
        updateRequestBody(request);
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.reorderSpace(request));
    }

    public HeadlessCMSResponse deleteSpace(JSONObject request) throws HeadlessCMSServiceException {
        updateRequestBody(request);
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.deleteSpace(request));
    }

    public HeadlessCMSResponse getAllSegments(JSONObject request) throws HeadlessCMSServiceException {
        return callHeadlessCMSServiceClient(() -> headlessCMSServiceClient.getAllSegments(request));
    }
}
