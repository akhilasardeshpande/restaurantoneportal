package com.swiggy.restaurantone.controller;

import com.amazonaws.HttpMethod;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.AmazonS3ClientService;
import com.swiggy.restaurantone.service.SellerTierService;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

/**
 * Created by shahbaz.khalid on 5/25/17.
 */

@RestController
@RequestMapping(value = "/rest-one/v1/seller-tier")
public class SellerTierController extends AbstractController {

    @Autowired
    private SellerTierService sellerTierService;

    @Autowired
    private AuthorizationHelper authorizationHelper;

    @Autowired
    private AmazonS3ClientService amazonS3ClientService;

    @RequestMapping(method = RequestMethod.GET,
            value = "/metadata",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> getMetadata() {
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(
                    () -> sellerTierService.getMetadata());
        }
        return this.getFailureResponse("Unauthorized");
    }

    @RequestMapping(method = RequestMethod.POST,
            value = "/benchmark",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> uploadBenchMark(@RequestBody JSONObject request) {
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(
                    () -> sellerTierService.uploadBenchmark(request));
        }
        return this.getFailureResponse("Unauthorized");
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/benchmark/{cityId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> getBenchMark(@PathVariable Long cityId) {
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(
                    () -> sellerTierService.getBenchmark(cityId));
        }
        return this.getFailureResponse("Unauthorized");
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/sample",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> getSampleFile() {
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(
                    () -> sellerTierService.getSampleFile());
        }
        return this.getFailureResponse("Unauthorized");
    }

    @RequestMapping(method = RequestMethod.POST,
            value = "/data",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> saveRestaurantMetric(@RequestParam("file") MultipartFile file,
                                            @RequestParam("startDate") String startDate,
                                            @RequestParam("endDate") String endDate,
                                            @RequestParam("goLiveDate") String goLiveDate,
                                            @RequestParam("refreshDate") String refreshDate) {
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(
                    () -> sellerTierService.saveRestaurantDetails(file.getInputStream(), startDate, endDate, goLiveDate, refreshDate));
        }
        return this.getFailureResponse("Unauthorized");
    }

    @RequestMapping(method = RequestMethod.GET,
            value = "/data",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> getUploadedData() {
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(
                    () -> sellerTierService.getUploadedTier());
        }
        return this.getFailureResponse("Unauthorized");
    }

    @RequestMapping(method = RequestMethod.PUT,
            value = "/preSignedUrl",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> generatePreSignedUrl(@RequestBody SignedURLRequestBody requestBody) {
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(() -> this.amazonS3ClientService.getPreSignedUrl(requestBody.getFileName(),
                    requestBody.getContentType(), HttpMethod.PUT));
        }
        return this.getFailureResponse("Unauthorized");
    }

    @RequestMapping(method = RequestMethod.POST,
            value = "/fileUploadNotification",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> sellerTieringFileUploadNotification(@RequestParam("s3url") String s3Url,
                                                           @RequestParam("month") String month,
                                                           @RequestParam("year") String year,
                                                           @RequestParam("type") String type) {
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(
                    () -> sellerTierService.fileUploadUpdate(s3Url, month, year, type));
        }
        return this.getFailureResponse("Unauthorized");
    }

    @Data
    @NoArgsConstructor
    private static class SignedURLRequestBody implements Serializable {
        @JsonProperty("fileName")
        private String fileName;

        @JsonProperty("contentType")
        private String contentType;
    }

}
