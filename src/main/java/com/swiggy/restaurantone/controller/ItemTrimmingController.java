package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.ItemTrimmingService;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/rest-one/v1/item-trimming")
@CommonsLog
public class ItemTrimmingController extends AbstractController {
    private AuthorizationHelper authorizationHelper;
    private ItemTrimmingService itemTrimmingService;

    @Autowired
    public ItemTrimmingController(AuthorizationHelper authorizationHelper, ItemTrimmingService itemTrimmingService) {
        this.authorizationHelper = authorizationHelper;
        this.itemTrimmingService = itemTrimmingService;
    }

    @RequestMapping(method = RequestMethod.POST,
            value = "/data",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> itemTrimming(@RequestParam("file") MultipartFile file) {
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(
                    () -> itemTrimmingService.applyHolidaySlot(file.getInputStream()));
        }
        return this.getFailureResponse("Unauthorized");
    }
}
