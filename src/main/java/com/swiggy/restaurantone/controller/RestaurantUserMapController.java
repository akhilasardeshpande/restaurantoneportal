package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.data.businessEntity.RestaurantUserMap;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.RestaurantUserMapService;
import com.swiggy.restaurantone.service.UserService;
import com.swiggy.restaurantone.validation.RestaurantUserMapValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by balamuneeswaran.s on 1/15/17.
 */
@RestController
@RequestMapping(value = "/rest-one/restaurantusermap")
public class RestaurantUserMapController extends AbstractController {

    private final RestaurantUserMapService restaurantUserMapService;

    private final RestaurantUserMapValidation restaurantUserMapValidation;

    private final AuthorizationHelper authorizationHelper;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserService userService;

    @Autowired
    public RestaurantUserMapController(RestaurantUserMapService restaurantUserMapService, RestaurantUserMapValidation restaurantUserMapValidation, AuthorizationHelper authorizationHelper) {
        this.restaurantUserMapService = restaurantUserMapService;
        this.restaurantUserMapValidation = restaurantUserMapValidation;
        this.authorizationHelper = authorizationHelper;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> getRestaurants(@RequestParam("userId") Long id) {
        if (!authorizationHelper.authenticateUserSession()) {
            return new Response<RestaurantUserMap>(401, "Unauthorized");
        }
        return this.executeTask(() -> restaurantUserMapService.getRestaurantsByUserId(id));
    }

    @RequestMapping(
            value = "/parent/{user_id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> getRestaurantsByParentId(@PathVariable(value = "user_id") Long id) {
        if (!authorizationHelper.authenticateUserSession()) {
            return new Response<RestaurantUserMap>(401, "Unauthorized");
        }
        return this.executeTask(() -> restaurantUserMapService.getRestaurantsByParentUserId(id));
    }

    @RequestMapping(
            value = "/rms-login",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> getRestaurantForPartnerLogin(@RequestParam("userId") Long userId) {
        if (!authorizationHelper.authenticateUserSession()) {
            return new Response<Long>(401, "Unauthorized");
        }
        return this.executeTask(() -> restaurantUserMapService.getRestaurantForRmsUser(userId));
    }
    @RequestMapping(
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> updateRestaurantUserMap(@RequestBody List<RestaurantUserMap> restaurantUserMaps) {
        List<Long> restaurantIds = restaurantUserMaps
                .stream()
                .map(restaurantUserMap -> restaurantUserMap.restaurantId)
                .collect(Collectors.toList());

        List<Long> userIds = restaurantUserMaps
                .stream()
                .map(restaurantUserMap -> restaurantUserMap.userId)
                .collect(Collectors.toList());

        Set<Long> userIdSet = new HashSet<>();
        userIdSet.addAll(userIds);
        userIds.clear();
        userIds.addAll(userIdSet);
        logger.info("userIds " + userIds);
        logger.info("RestaurantIds " + restaurantIds);
        boolean authorized  = authorizationHelper.authForAddPoc() || authorizationHelper.checkAuthorization(userIds, restaurantIds, true);

        if (!authorized) {
            return new Response<RestaurantUserMap>(401, "Unauthorized");
        }
        if (!restaurantUserMapValidation.validate(restaurantUserMaps)) {
            return new Response<RestaurantUserMap>(400,"Bad Request");
        }
        return this.executeTask(() -> _updateRestaurantUserMap(restaurantUserMaps));
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> addRestaurantUserMap(@RequestBody List<RestaurantUserMap> restaurantUserMaps) {
        List<Long> restaurantIds = restaurantUserMaps
                .stream()
                .map(restaurantUserMap -> restaurantUserMap.restaurantId)
                .collect(Collectors.toList());

        List<Long> userIds = restaurantUserMaps
                .stream()
                .map(restaurantUserMap -> restaurantUserMap.userId)
                .collect(Collectors.toList());

        Set<Long> userIdSet = new HashSet<>();
        userIdSet.addAll(userIds);
        userIds.clear();
        userIds.addAll(userIdSet);
        logger.info("userIds " + userIds);
        logger.info("RestaurantIds " + restaurantIds);
        boolean authorized  = authorizationHelper.authForAddPoc() || authorizationHelper.checkAuthorization(userIds, restaurantIds, true);

        if (!authorized) {
            return new Response<RestaurantUserMap>(401, "Unauthorized");
        }
        if (!restaurantUserMapValidation.validate(restaurantUserMaps)) {
            return new Response<RestaurantUserMap>(400,"Bad Request");
        }
        return this.executeTask(() -> restaurantUserMapService.addUserRestaurantMap(restaurantUserMaps.get(0)));
    }

    private List<RestaurantUserMap> _updateRestaurantUserMap(List<RestaurantUserMap> restaurantUserMaps) {
        return restaurantUserMapService.updateRestaurantUserMap(restaurantUserMaps);
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public Response<?> getUsersFromRestaurants(@RequestParam("restaurantId") Long id){
        List<RestaurantUserMap> restaurantUserMaps =restaurantUserMapService.getUsersByRestaurantsId(id);
        return this.executeTask(() -> restaurantUserMaps.stream().map(restaurantUserMap -> userService.find(restaurantUserMap.userId)).collect(Collectors.toList()) );

    }

    @RequestMapping(value = "/is-parent/{rest_id}",method = RequestMethod.GET)
    public Response<?> checkParent(@PathVariable(value = "rest_id") Long restId){
        return this.executeTask(() -> authorizationHelper.checkAuthorization(null, restId, false));
    }
}
