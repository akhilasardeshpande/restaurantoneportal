package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.RMSRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by shahbaz.khalid on 4/24/17.
 */

@RestController
@RequestMapping(value = "/rest-one/rms-role")
public class RMSRoleController extends AbstractController {

    @Autowired
    private RMSRoleService rmsRoleService;

    @Autowired
    private AuthorizationHelper authorizationHelper;

    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> getRoles() {
        if (!(authorizationHelper.checkRMSAuthorization() || authorizationHelper.checkRMSReadOnlyAuthorization())) {
            return new Response<User>(401, "Unauthorized");
        }
        return this.executeTask(
                () -> rmsRoleService.getRoles());
    }

    public <T> Response<?> getSuccessResponse(T t) {
        return (Response<?>) t;
    }
}
