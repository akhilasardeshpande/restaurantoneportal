package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.exceptions.InvalidHeadersException;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.pojos.igccAttribution.UploadResponse;
import com.swiggy.restaurantone.response.PresignedUrlResponse;
import com.swiggy.restaurantone.service.IgccAttributionService;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/rest-one/v1/igcc_attribution")
@CommonsLog
public class IgccAttributionController extends AbstractController {

    private AuthorizationHelper authorizationHelper;
    private IgccAttributionService igccAttributionService;

    @Autowired
    public IgccAttributionController(AuthorizationHelper authorizationHelper, IgccAttributionService igccAttributionService) {
        this.authorizationHelper = authorizationHelper;
        this.igccAttributionService = igccAttributionService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/data", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> igccAttribution(@RequestParam("key") final String key) {
        if(authorizationHelper.authenticateUserSession()) {
            try {
                UploadResponse igccUploadResponse = igccAttributionService.syncIgccAttribution(key);
                return this.getSuccessResponse(igccUploadResponse);
            } catch (InvalidHeadersException  e) {
                return this.getFailureResponse(e.getMessage());
            }
        }
        return this.getFailureResponse("Unauthorized");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get_presigned_url", produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> presignedUrl() {
        if(authorizationHelper.authenticateUserSession()) {
            try {
                PresignedUrlResponse presignedUrl = igccAttributionService.getPresignedUrl();
                return this.getSuccessResponse(presignedUrl);
            } catch (Exception e) {
                return this.getFailureResponse(e.getMessage());
            }
        }
        return this.getFailureResponse("Unauthorized");
    }
}
