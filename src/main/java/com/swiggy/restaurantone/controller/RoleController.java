package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.data.businessEntity.Role;
import com.swiggy.restaurantone.data.businessEntity.ValidationResult;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.RoleService;
import com.swiggy.restaurantone.validation.RoleValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by rajnishc on 5/12/17.
 */
@RequestMapping("/rest-one/role")
@RestController
public class RoleController extends AbstractController{
    @Autowired
    private RoleService roleService;

    @Autowired
    private AuthorizationHelper authorizationHelper;

    @Autowired
    private RoleValidation roleValidation;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Response<?> getRoles() {
        if(authorizationHelper.authenticateUserSession())
            return this.executeTask(()->roleService.findAll());
        return this.getFailureResponse("Unauthorized");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Response<?> getRoleById(@PathVariable Long id) {
        if(authorizationHelper.authenticateUserSession())
            return this.executeTask(()->roleService.findRoleById(id));
        return this.getFailureResponse("Unauthorized");
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public Response<?> addRole(@Valid @RequestBody Role role) {
        List<ValidationResult> validationResult = roleValidation.postValidation(role);
        if(!validationResult.isEmpty()) {
            return new Response<>(400, validationResult);
        }
        if(authorizationHelper.authenticateUserSession() && authorizationHelper.authForAddUser()) {
            return this.executeTask(() -> roleService.addRole(role));
        }
        return this.getFailureResponse("Unauthorized");
    }

    @RequestMapping(value = "/", method = RequestMethod.PUT)
    public Response<?> editRole(@Valid @RequestBody Role role) {
        List<ValidationResult> validationResult = roleValidation.putValidation(role);
        if(!validationResult.isEmpty()) {
            return new Response<>(400, validationResult);
        }
        if(authorizationHelper.authenticateUserSession() && authorizationHelper.authForAddUser()) {
            return this.executeTask(() -> roleService.editRole(role));
        }
        return this.getFailureResponse("Unauthorized");
    }
}
