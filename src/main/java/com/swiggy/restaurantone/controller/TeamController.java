package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by rajnishc on 5/12/17.
 */
@RestController
@RequestMapping(value = "/rest-one/team")
public class TeamController extends AbstractController{

    @Autowired
    private TeamService teamService;

    @Autowired
    private AuthorizationHelper authorizationHelper;
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Response<?> getAllTeams() {
        if (authorizationHelper.authenticateUserSession())
            return this.executeTask(() -> teamService.findAll());
        return this.getFailureResponse("Unauthorized");
    }

}
