package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.RMSPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by shahbaz.khalid on 3/30/17.
 */

@RestController
@RequestMapping(value = "/rest-one/rms-permission")
public class RMSPermissionController extends AbstractController {

    @Autowired
    private RMSPermissionService rmsPermissionService;

    @Autowired
    private AuthorizationHelper authorizationHelper;

    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> getPermissions() {
        if (!(authorizationHelper.checkRMSAuthorization() || authorizationHelper.checkRMSReadOnlyAuthorization())) {
            return new Response<User>(401, "Unauthorized");
        }
        return this.executeTask(
                () -> rmsPermissionService.getPermissions());
    }

    public <T> Response<?> getSuccessResponse(T t) {
        return (Response<?>) t;
    }
}
