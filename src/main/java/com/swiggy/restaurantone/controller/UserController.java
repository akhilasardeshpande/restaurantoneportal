package com.swiggy.restaurantone.controller;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.data.businessEntity.*;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.UserService;
import com.swiggy.restaurantone.validation.UserValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.QueryParam;
import java.util.List;

/**
 * Created by balamuneeswaran.s on 1/3/17.
 */
@RestController
@RequestMapping(value = "/rest-one/user")
public class UserController extends AbstractController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthorizationHelper authorizationHelper;

    @Autowired
    private UserValidation userValidation;

    @RequestMapping(value = "/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> get(@Validated @NotNull @JsonSubTypes.Type(Long.class) @PathVariable("id") Long id) {
        if (!authorizationHelper.authenticateUserSession()) {
            return new Response<User>(401, "Unauthorized");
        }
        return this.executeTask(() -> userService.find(id));
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> getManager(@QueryParam("name") String name) {
        if (!authorizationHelper.authenticateUserSession()) {
            return new Response<User>(401, "Unauthorized");
        }
        return this.executeTask(() -> userService.findNamesLike(name));
    }

    @RequestMapping(method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> update(@Valid @RequestBody User user) {
        List<ValidationResult> errors = userValidation.validate(user, false);
        if(!errors.isEmpty()) {
            return new Response<>(400, errors);
        }
        if (!authorizationHelper.authForEditUser(user)) {
            return new Response<User>(401, "Unauthorized");
        }
        return this.executeTask(() -> userService.updateUser(user));
    }

    @RequestMapping(value = "/fetchall",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> getAllUser(@RequestBody UserCriteria criteria) {
        if (criteria.pagination == null) {
            criteria.pagination = new Pagination();
        }
        if (!authorizationHelper.authenticateUserSession()) {
            return new Response<User>(401, "Unauthorized");
        }

        return this.executeTask(() -> userService.getUsersByCriteria(criteria));
    }

    @RequestMapping(value = "/add-user", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> addUser(@Validated @Valid @RequestBody User user){
        if (!authorizationHelper.authenticateUserSession() && !authorizationHelper.authForAddUser()) {
            return new Response<UserRelationship>(401, "Unauthorized");
        }
        List<ValidationResult> validationResult = userValidation.validate(user, true);
        if(!validationResult.isEmpty()) {
            return new Response<>(400, validationResult);
        }

        return this.executeTask(() -> userService.addUser(user));
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="There was an error processing the request body. One of the format is incorrect")
    public String handleMessageNotReadableException(HttpServletRequest request, HttpMessageNotReadableException exception) {
        return exception.getMessage();
    }

}
