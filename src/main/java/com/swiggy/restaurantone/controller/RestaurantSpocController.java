package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.RestaurantSpocService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/rest-one/restaurant-spoc")
public class RestaurantSpocController extends AbstractController {
    private final RestaurantSpocService restaurantSpocService;
    private final AuthorizationHelper authorizationHelper;
    
    @Autowired
    public RestaurantSpocController(RestaurantSpocService restaurantSpocService,
                                    AuthorizationHelper authorizationHelper) {
        this.restaurantSpocService = restaurantSpocService;
        this.authorizationHelper = authorizationHelper;
    }
    
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> getRestaurantSpoc(@RequestParam(value = "restaurant_id") Long restaurantId,
                                         @RequestParam(required = false, name = "userType") String userType) {
        return this.executeTask(() -> restaurantSpocService.getRestaurantSpoc(restaurantId, userType));
    }
    
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> updateRestaurantsSpoc(@RequestParam("file") MultipartFile file,
                                             @RequestHeader("userId") String userId) {
        return this.executeTask(() -> restaurantSpocService.updateRestaurantsSpoc(file.getInputStream(), userId));
    }
    
}
