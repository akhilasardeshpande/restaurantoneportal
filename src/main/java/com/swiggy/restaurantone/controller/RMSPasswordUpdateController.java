package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.RMSUserService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by rajnishc on 8/3/17.
 */

@RestController
@RequestMapping(value = "/rest-one/rms-password")
public class RMSPasswordUpdateController extends AbstractController{
    @Autowired
    private RMSUserService rmsUserService;

    @Autowired
    private AuthorizationHelper authorizationHelper;

    @RequestMapping(value = "/restaurants",
                    method = RequestMethod.POST)
    public Response<?> uploadRestaurantsForPasswordChange(@RequestParam("file") MultipartFile file,
                                                          @RequestParam("email") String email,
                                                          @RequestParam("type_of_user")Short typeOfUser) {
        if (file.isEmpty()) {
            return this.getFailureResponse("Empty file");
        }
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(
                    () -> rmsUserService.forcePasswordChange(file, email, typeOfUser));
        }
        return this.getFailureResponse("Unauthorized");
    }

    @RequestMapping(value = "/changePassword",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> changePassword(@RequestBody JSONObject requestObject) {
        if (!authorizationHelper.checkRMSAuthorization()) {
            return new Response<User>(401, "Unauthorized");
        }
        return this.executeTask(
                () -> rmsUserService.changePassword(requestObject));
    }

    @RequestMapping(value = "/generate/",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> generatePassword(@RequestBody JSONObject requestObject) {
        if (!authorizationHelper.checkRMSAuthorization()) {
            return new Response<User>(401, "Unauthorized");
        }
        return this.executeTask(
                () -> rmsUserService.getPolicyAndGeneratePassword(requestObject));
    }

    public <T> Response<?> getSuccessResponse(T t) {
        return (Response<?>) t;
    }
}
