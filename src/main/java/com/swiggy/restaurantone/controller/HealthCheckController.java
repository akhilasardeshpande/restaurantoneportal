package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by rajnish.chandra on 08/01/19
 */
@RestController
public class HealthCheckController extends AbstractController {
    @RequestMapping(value = "/health-check", method = RequestMethod.GET)
    public Response<?> healthCheck() {
        return this.getSuccessResponse("Success");
    }

    @RequestMapping(value = "/health", method = RequestMethod.GET)
    public Response<?> healthCheckRock() {
        return this.getSuccessResponse("Success");
    }
}
