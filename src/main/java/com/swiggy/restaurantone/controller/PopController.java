package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.PopService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by alok.singhal on 9/20/17.
 */
@RestController
@RequestMapping(value = "/pop")
public class PopController extends AbstractController {
    @Autowired
    private PopService popService;

    @Autowired
    private AuthorizationHelper authorizationHelper;

    @RequestMapping(value = "/areas", method = RequestMethod.GET)
    public Response<?> getAreas() {
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(() -> popService.getAllAreas());
        }
        return this.getFailureResponse("Unauthorized");
    }

    @RequestMapping(value = "/batch/unConfirmed", method = RequestMethod.GET)
    public Response<?> getUnconfirmedBatches(@RequestParam List<Long> areaIds) {
        if (authorizationHelper.authenticateUserSession()) {
            return this.executeTask(() -> popService.getUnconfirmedBatches(areaIds));
        }
        return this.getFailureResponse("Unauthorized");
    }

    @RequestMapping(value = "/batch/", method = RequestMethod.PUT)
    public Response<?> batchActions(@RequestBody JSONObject jsonObject) {
        if (authorizationHelper.authenticateUserSession()) {
            return this.executeTask(() -> popService.batchActions(jsonObject));
        }
        return this.getFailureResponse("Unauthorized");
    }

}
