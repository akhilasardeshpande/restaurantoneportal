package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by rajnishc on 5/15/17.
 */
@RestController
@RequestMapping(value = "/rest-one/city")
public class CityController extends AbstractController {
    @Autowired
    private CityService cityService;

    @Autowired
    private AuthorizationHelper authorizationHelper;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Response<?> getCities() {
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(() -> cityService.findAllCities());
        }
        return this.getFailureResponse("Unauthorized");
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Response<?> getCity(@PathVariable("id") Long id) {

        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(() -> cityService.findCity(id));
        }
        return this.getFailureResponse("Unauthorized");
    }
}
