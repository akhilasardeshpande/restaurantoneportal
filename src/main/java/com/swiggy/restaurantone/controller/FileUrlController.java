package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.pojos.communication_service.UploadUrlRequest;
import com.swiggy.restaurantone.service.FileUrlService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping(value = "/api/v1/")
public class FileUrlController extends AbstractController {

    @Autowired
    private AuthorizationHelper authorizationHelper;

    @Autowired
    private FileUrlService fileUrlService;

    @PostMapping(value = "/uploadUrl", produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Response<?> fetchUploadUrl(@RequestBody UploadUrlRequest cdnRequest) {
        log.info("CDN Api Request {}", cdnRequest);
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(() -> fileUrlService.fetchUploadUrl(cdnRequest));
        }
        return this.getFailureResponse("Unauthorized");
    }

}
