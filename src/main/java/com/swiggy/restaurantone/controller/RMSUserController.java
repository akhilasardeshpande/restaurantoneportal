package com.swiggy.restaurantone.controller;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.RMSUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.json.simple.JSONObject;

import javax.validation.constraints.NotNull;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;

/**
 * Created by shahbaz.khalid on 3/22/17.
 */
@RestController
@RequestMapping(value = "/rest-one/rms-user")
public class RMSUserController extends AbstractController {

    @Autowired
    private RMSUserService rmsUserService;

    @Autowired
    private AuthorizationHelper authorizationHelper;

    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> getAllRMSUser(@QueryParam("user_id") Long user_id,
                                     @QueryParam("email") String email,
                                     @QueryParam("name") String name,
                                     @QueryParam("mobile_no") String mobile_no,
                                     @QueryParam("rest_id") Long rest_id,
                                     @QueryParam("offset") @DefaultValue("0") Integer offset,
                                     @QueryParam("limit") @DefaultValue("10") Integer limit) {
        if (authorizationHelper.checkRMSAuthorization() || authorizationHelper.checkRMSReadOnlyAuthorization() || authorizationHelper.checkAuthorization(null, rest_id, false)) {
            return this.executeTask(() -> rmsUserService.getAllUsers(user_id, email, name, mobile_no, rest_id, offset, limit));
        }
        return new Response<User>(401, "Unauthorized");
    }

    @RequestMapping(value = "/{user_id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> getRMSUser(@Validated @NotNull @JsonSubTypes.Type(Long.class) @PathVariable("user_id") Long userId) {
        if (!(authorizationHelper.checkRMSAuthorization() || authorizationHelper.checkRMSReadOnlyAuthorization())) {
            return new Response<User>(401, "Unauthorized");
        }
        return this.executeTask(() -> rmsUserService.getUsersDetails(userId));
    }

    @RequestMapping(method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> addRMSUser(@RequestBody JSONObject userDetails) {
        if (!authorizationHelper.checkRMSAuthorization()) {
            return new Response<User>(401, "Unauthorized");
        }
        return this.executeTask(
                () -> rmsUserService.addRMSUser(userDetails));
    }

    @RequestMapping(value = "/v2/create-user",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> addRMSUserV2(@RequestBody JSONObject userDetails) {
        if (!authorizationHelper.checkRMSAuthorization()) {
            return new Response<User>(401, "Unauthorized");
        }
        return this.executeTask(
                () -> rmsUserService.addRMSUserV2(userDetails));
    }

    @RequestMapping(value="/{user_id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> updateRMSUser(@Validated @NotNull @JsonSubTypes.Type(Long.class) @PathVariable("user_id") Long userId,
                                     @RequestBody JSONObject userDetails) {

        if (!authorizationHelper.checkRMSAuthorization()) {
            return new Response<User>(401, "Unauthorized");
        }
        return this.executeTask(
                () -> rmsUserService.updateRMSUser(userId, userDetails));
    }

    @RequestMapping(value = "/{user_id}/restaurants",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> addRestaurantToUser(@Validated @NotNull @JsonSubTypes.Type(Long.class) @PathVariable("user_id") Long userId,
                                           @RequestBody JSONObject restaurantId) {
        if (!authorizationHelper.checkRMSAuthorization()) {
            return new Response<User>(401, "Unauthorized");
        }
        return this.executeTask(
                () -> rmsUserService.addRestaurantToUser(userId, restaurantId));
    }

    @RequestMapping(value = "/{user_id}/restaurants/{rest_id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> removeRestaurantFromUser(@Validated @NotNull @JsonSubTypes.Type(Long.class) @PathVariable("user_id") Long userId,
                                                @Validated @NotNull @JsonSubTypes.Type(Long.class) @PathVariable("rest_id") Long restaurantId,
                                                @RequestParam("updated_by") String updatedBy) {
        if (!authorizationHelper.checkRMSAuthorization()) {
            return new Response<User>(401, "Unauthorized");
        }
        return this.executeTask(
                () -> rmsUserService.removeRestaurantFromUser(userId, restaurantId, updatedBy));
    }

    @RequestMapping(value = "/{user_id}/restaurants",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> getRestaurantsForUser(@Validated @NotNull @JsonSubTypes.Type(Long.class) @PathVariable("user_id") Long userId) {
        if (!(authorizationHelper.checkRMSAuthorization() || authorizationHelper.checkRMSReadOnlyAuthorization())) {
            return new Response<User>(401, "Unauthorized");
        }
        return this.executeTask(
                () -> rmsUserService.getRestaurantsForUser(userId));
    }


    @RequestMapping(value = "/logout",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> invalidateSessions(@RequestBody JSONObject requestObject) {
        if (!authorizationHelper.checkRMSAuthorization()) {
            return new Response<User>(401, "Unauthorized");
        }
        return this.executeTask(
                () -> rmsUserService.logoutUser(requestObject));
    }

    public <T> Response<?> getSuccessResponse(T t) {
        return (Response<?>) t;
    }
}
