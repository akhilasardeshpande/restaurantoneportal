package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.pojos.requests.letterbox.UploadRequest;
import com.swiggy.restaurantone.service.LetterboxService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import java.io.IOException;

@RestController
@Slf4j
@RequestMapping(value = "/letterbox")
public class LetterboxController extends AbstractController {

    @Autowired
    private AuthorizationHelper authorizationHelper;

    @Autowired
    private LetterboxService letterboxService;

    @Value("${letterboxPermission}")
    private Long letterboxPermission;

    @Value("${bulkUploadPermission}")
    private Long bulkUploadPermission;

    @RequestMapping(value = "/templates", method = RequestMethod.GET)
    public Response<?> getTemplates() throws IOException {
        log.info("Request received for get templates");
        if (!(authorizationHelper.authenticateUserSession() && checkBulkUploadViewPermission())) {
            return this.getFailureResponse("Unauthorized");
        }
        return letterboxService.getTemplates();
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public Response<?> upload(@RequestBody UploadRequest uploadRequest) throws IOException {
        log.info("Request received to upload file : {}", uploadRequest);
        if (!(authorizationHelper.authenticateUserSession() && checkBulkUploadPermission())) {
            return this.getFailureResponse("Unauthorized");
        }
        return letterboxService.upload(uploadRequest);
    }

    @RequestMapping(value = "/presigned-url", method = RequestMethod.GET)
    public Response<?> presignedUrl(
            @RequestParam(value = "fileName") final String fileName,
            @RequestParam(value = "uploadType") final String uploadType
    ) throws IOException {
        log.info("Request received for get presigned url- filename:{}, uploadType:{}", fileName, uploadType);
        if (!(authorizationHelper.authenticateUserSession() && checkBulkUploadViewPermission())) {
            return this.getFailureResponse("Unauthorized");
        }
        return letterboxService.getPresignedUrl(fileName, uploadType);
    }

    @RequestMapping(value = "/audit-logs", method = RequestMethod.GET)
    public Response<?> userAuditLogs() throws IOException {
        log.info("Request received for get audit logs");
        if (!(authorizationHelper.authenticateUserSession() && checkBulkUploadViewPermission())) {
            return this.getFailureResponse("Unauthorized");
        }
        return letterboxService.getUserAuditLogs();
    }

    @RequestMapping(value = "/uploads-status", method = RequestMethod.GET)
    public Response<?> userUploadStatus(
            @RequestParam("uploadType") final String uploadType,
            @RequestParam("page") final int page
    ) throws IOException {
        log.info("Request received for get upload-status - uploadType:{}, page:{}", uploadType, page);
        if (!(authorizationHelper.authenticateUserSession() && checkBulkUploadViewPermission())) {
            return this.getFailureResponse("Unauthorized");
        }
        return letterboxService.getUserUploadStatus(uploadType, page);
    }

    public boolean checkBulkUploadPermission() {
        return authorizationHelper.checkPermissions(bulkUploadPermission);
    }

    public boolean checkBulkUploadViewPermission() {
        return authorizationHelper.checkPermissions(letterboxPermission) || authorizationHelper.checkPermissions(bulkUploadPermission);
    }

}
