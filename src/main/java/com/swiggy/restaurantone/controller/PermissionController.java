package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by rajnishc on 5/15/17.
 */
@RequestMapping(value = "/rest-one/permission")
@RestController
public class PermissionController extends AbstractController{
    @Autowired
    private PermissionService permissionService;

    @Autowired
    private AuthorizationHelper authorizationHelper;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public Response<?> getPermissions() {
        if(!authorizationHelper.authenticateUserSession()) {
            return this.getFailureResponse("Unauthorized");
        }
        return this.executeTask(() -> permissionService.findAll());
    }
}
