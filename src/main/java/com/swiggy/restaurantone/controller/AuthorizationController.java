package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.data.ApplicationEntity.RmsUserAuthInfo;
import com.swiggy.restaurantone.data.ApplicationEntity.UserPermissionInfo;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by rajnishc on 8/4/17.
 */
@RestController

public class AuthorizationController extends AbstractController{
    private final AuthorizationHelper authorizationHelper;

    @Autowired
    public AuthorizationController(AuthorizationHelper authorizationHelper) {
        this.authorizationHelper = authorizationHelper;
    }

    @RequestMapping(value = "/rest-one/auth/",
                    method = RequestMethod.POST,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public Response checkAuth(@RequestBody UserPermissionInfo userPermissionInfo) {
        return this.executeTask(() -> authorizationHelper.checkPermissions(userPermissionInfo.getPermissionId()));
    }

    @RequestMapping(value = "/rest-one/auth/restaurants",
                    method = RequestMethod.POST,
                    produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response checkAuthForRestaurant(@RequestBody RmsUserAuthInfo rmsUserAuthInfo) {
        return this.executeTask(() ->
                authorizationHelper.authorizeUserForRestaurant(rmsUserAuthInfo));
    }
}
