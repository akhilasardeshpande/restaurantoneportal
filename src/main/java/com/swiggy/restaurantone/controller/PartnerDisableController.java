package com.swiggy.restaurantone.controller;

import com.google.api.client.http.HttpStatusCodes;
import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.data.businessEntity.PartnerEnableRequest;
import com.swiggy.restaurantone.data.businessEntity.PartnerOrRestaurantDisableRequest;
import com.swiggy.restaurantone.data.coreEntity.PartnerDisableRunInfoEntity;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.helper.RestaurantHolidaySlotHelper;
import com.swiggy.restaurantone.pojos.response.RestaurantHolidaySlotDisposition;
import com.swiggy.restaurantone.service.PartnerDisableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by rajnish.chandra on 19/02/19
 */
@RestController
@RequestMapping(value = "/rest-one/partner")
public class PartnerDisableController extends AbstractController {
    
    @Value("${partnerDisablePermission}")
    private Long partnerDisablePermission;
    
    private final AuthorizationHelper authorizationHelper;
    private final PartnerDisableService partnerDisableService;
    private final RestaurantHolidaySlotHelper restaurantHolidaySlotHelper;
    
    @Autowired
    public PartnerDisableController(AuthorizationHelper authorizationHelper,
                                    PartnerDisableService partnerDisableService,
                                    RestaurantHolidaySlotHelper restaurantHolidaySlotHelper) {
        this.authorizationHelper = authorizationHelper;
        this.partnerDisableService = partnerDisableService;
        this.restaurantHolidaySlotHelper = restaurantHolidaySlotHelper;

    }
    
    @RequestMapping(value = "/disable", method = RequestMethod.POST)
    public Response<?> disablePartner(@NotNull @Valid @RequestBody
                                              PartnerOrRestaurantDisableRequest partnerOrRestaurantDisableRequest) {
        if (!authorizationHelper.checkPermissions(partnerDisablePermission)) {
            return new Response<>(0, HttpStatusCodes.STATUS_CODE_UNAUTHORIZED, "Unauthorized");
        }
        return new Response<>(1, partnerDisableService.disablePartner(partnerOrRestaurantDisableRequest),
                "success");
    }
    
    @RequestMapping(value = "/enable", method = RequestMethod.POST)
    public Response<?> enablePartner(@Valid @RequestBody PartnerEnableRequest partnerEnableRequest) {
        if (!authorizationHelper.checkPermissions(partnerDisablePermission)) {
            return new Response<>(0, HttpStatusCodes.STATUS_CODE_UNAUTHORIZED, "Unauthorized");
        }
        PartnerDisableRunInfoEntity partnerDisableRunInfoEntity = partnerDisableService.getRunIfo(
                partnerEnableRequest.getRunInfoId());
        
        if (ObjectUtils.isEmpty(partnerDisableRunInfoEntity)) {
            return new Response<>(0, 400, "Run id does not exist");
        }
        
        return new Response<>(1, partnerDisableService.enablePartner(partnerEnableRequest),
                "success");
    }
    
    @RequestMapping(value = "/enable-disable/run-info/{id}", method = RequestMethod.GET)
    public Response<?> getRunInfo(@Valid @NotNull @Min(value = 1) @PathVariable Long id) {
        if (!authorizationHelper.checkPermissions(partnerDisablePermission)) {
            return new Response<>(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED, "Unauthorized");
        }
        if (ObjectUtils.isEmpty(id)) {
            return new Response<>(0, 400, "Bad request");
        }
        
        PartnerDisableRunInfoEntity partnerDisableRunInfoEntity = partnerDisableService.getRunIfo(id);
        if (ObjectUtils.isEmpty(partnerDisableRunInfoEntity)) {
            return new Response<>(0, partnerDisableRunInfoEntity, "No data available");
        }
        return new Response<>(1, partnerDisableRunInfoEntity, "success");
    }
    
    @RequestMapping(value = "/enable-disable/run-info/all", method = RequestMethod.GET)
    public Response<?> getAllRunInfo() {
        if (!authorizationHelper.checkPermissions(partnerDisablePermission)) {
            return new Response<>(0, HttpStatusCodes.STATUS_CODE_UNAUTHORIZED, "Unauthorized");
        }
        List<PartnerDisableRunInfoEntity> partnerDisableRunInfoEntities = partnerDisableService.getAllRunIfo();
        
        if (ObjectUtils.isEmpty(partnerDisableRunInfoEntities)) {
            return new Response<>(0, partnerDisableRunInfoEntities, "No data available");
        }
        return new Response<>(1, partnerDisableRunInfoEntities, "success");
    }

    @RequestMapping(value = "/holidayslot/dispositions", method = RequestMethod.GET)
    public Response<?> getRestaurantHolidaySlotDispositions() {
        if (!authorizationHelper.checkPermissions(partnerDisablePermission)) {
            return new Response<>(0, HttpStatusCodes.STATUS_CODE_UNAUTHORIZED, "Unauthorized");
        }
        List<RestaurantHolidaySlotDisposition> restaurantHolidaySlotDispositions =
                restaurantHolidaySlotHelper.getDispositions();

        if (ObjectUtils.isEmpty(restaurantHolidaySlotDispositions)) {
            return new Response<>(0, restaurantHolidaySlotDispositions, "No data available");
        }
        return new Response<>(1, restaurantHolidaySlotDispositions, "success");
    }
}