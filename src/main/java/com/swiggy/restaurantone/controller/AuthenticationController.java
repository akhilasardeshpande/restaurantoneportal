package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.data.ApplicationEntity.LoginUserInfo;
import com.swiggy.restaurantone.helper.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by balamuneeswaran.s on 1/5/17.
 */
@RestController
public class AuthenticationController extends AbstractController {
    @Autowired
    private AuthenticationHelper authenticationHelper;

    @RequestMapping(value = "rest-one/login",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> login(@RequestBody LoginUserInfo loginInfo) {
        return this.executeTask(() -> authenticationHelper.login(loginInfo));
    }

    @RequestMapping(value = "rest-one/logout",
            method = RequestMethod.POST
    )
    public Response<?> logout() {
        return this.executeTask(() -> authenticationHelper.logout());
    }
}
