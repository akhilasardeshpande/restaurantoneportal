package com.swiggy.restaurantone.controller;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.data.businessEntity.UserRelationship;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.service.UserRelationshipService;
import com.swiggy.restaurantone.service.UserService;
import com.swiggy.restaurantone.validation.UserRelationshipValidation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by balamuneeswaran.s on 1/15/17.
 */
@RestController
@RequestMapping(value = "/rest-one/userrelationship")
public class UserRelationshipController extends AbstractController {

    private final UserRelationshipService userRelationshipService;

    private final UserRelationshipValidation userRelationshipValidation;

    private final AuthorizationHelper authorizationHelper;

    @Autowired
    private UserService userService;
    @Autowired
    private HttpServletRequest httpServletRequest;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public UserRelationshipController(UserRelationshipService userRelationshipService, UserRelationshipValidation userRelationshipValidation, AuthorizationHelper authorizationHelper) {
        this.userRelationshipService = userRelationshipService;
        this.userRelationshipValidation = userRelationshipValidation;
        this.authorizationHelper = authorizationHelper;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> getImmediateChildUsers(@RequestParam("parentUserId") Long id,
                                              @RequestParam("onlyImmediateChildren") boolean onlyImmediateChildren) {
        if (!authorizationHelper.authenticateUserSession()) {
            return new Response<UserRelationship>(401, "Unauthorized");
        }
        if (onlyImmediateChildren) {
            return this.executeTask(() -> userRelationshipService.GetImmediateChildUsersByUserId(id));
        }
        return this.executeTask(() -> userRelationshipService.GetAllChildUsersByUserId(id));
    }

    @RequestMapping(
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public Response<?> updateUserRelationship(@RequestBody List<UserRelationship> userRelationships) {
        List<Long> userIds = userRelationships
                .stream()
                .map(userRelationship -> userRelationship.userId)
                .collect(Collectors.toList());

        userIds.addAll(userRelationships
                .stream()
                .map(userRelationship -> userRelationship.childUserId)
                .collect(Collectors.toList()));
        Set<Long> usersSet =  new HashSet<>();
        usersSet.addAll(userIds);
        userIds.clear();
        userIds.addAll(usersSet);
        logger.info("All childs in user relationship " + userIds);
        if (!(authorizationHelper.checkAuthorization(userIds, null, true)) ||
                !(authorizationHelper.authenticateUserSession() && authorizationHelper.authForAddPoc())) {
            return new Response<UserRelationship>(401, "Unauthorized");
        }
        return this.executeTask(() -> validateAndUpdate(userRelationships));
    }

    private List<UserRelationship> validateAndUpdate(List<UserRelationship> userRelationships) {
        if (!userRelationshipValidation.validate(userRelationships)) {
            throw new IllegalArgumentException("Validation Error");
        }
        return userRelationshipService.UpdateUserRelationship(userRelationships);
    }

    @RequestMapping(value = "/immediateParent",method = RequestMethod.GET)
    public Response<?> getImmediateParent(){
        List<UserRelationship> userRelationship = userRelationshipService.GetImmediateParentUsersByUserId( Long.parseLong(httpServletRequest.getHeader("userId")));
        return this.executeTask(() -> userRelationship.stream().map(userRelationship1 ->  userService.find(userRelationship1.userId)).collect(Collectors.toList()));
    }

    @RequestMapping(value = "/is-parent/{id}",method = RequestMethod.GET)
    public Response<?> checkParent(@PathVariable(value = "id") Long id){
        Long parentId = Long.parseLong(httpServletRequest.getHeader("userId"));
        return this.executeTask(() -> userRelationshipService.isParent(parentId, id));
    }
}
