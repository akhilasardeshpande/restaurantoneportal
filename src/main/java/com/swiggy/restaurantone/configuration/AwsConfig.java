package com.swiggy.restaurantone.configuration;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AwsConfig {


    @Value("${s3.aws.secret}")
    private String s3AWSSecret;

    @Value("${s3.aws.accesskey}")
    private String s3AWSAccesskey;

    @Bean
    public AmazonS3 s3client() {
        return new AmazonS3Client(credential());
    }

    @Bean
    public AWSCredentials credential() {
        return new BasicAWSCredentials(s3AWSAccesskey, s3AWSSecret);
    }

}

