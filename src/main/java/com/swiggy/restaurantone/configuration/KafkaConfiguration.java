package com.swiggy.restaurantone.configuration;

import com.swiggy.commons.Json;
import com.swiggy.kafka.clients.configs.ConsumerConfig;
import com.swiggy.kafka.clients.configs.ProducerConfig;
import com.swiggy.kafka.clients.configs.Topic;
import com.swiggy.kafka.clients.consumer.Consumer;
import com.swiggy.kafka.clients.producer.Producer;
import com.swiggy.restaurantone.kafka.consumers.RestaurantPocKafkaEventHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Amareshwar.Bhat on 10/03/2020.
 */
@Configuration
@ConfigurationProperties
@Slf4j
public class KafkaConfiguration {

    @Value("${kafka.batch.consumer.cms-restaurant-poc.clientId}")
    private String cmsRestaurantPocTopicClientId;

    @Bean
    @ConfigurationProperties(prefix = "kafka.batch.producer.spoc-create-event.topic")
    public Topic spocCreateTopic() {
        return BeanUtils.instantiateClass(Topic.class);
    }

    @Bean
    @ConfigurationProperties(prefix = "kafka.batch.consumer.cms-restaurant-poc.topic")
    public Topic cmsRestaurantPocTopic() {
        return BeanUtils.instantiateClass(Topic.class);
    }

    @Bean
    @ConfigurationProperties(prefix = "kafka.txn.producer.comm-event.topic")
    public Topic commEventTopic() {
        return BeanUtils.instantiateClass(Topic.class);
    }

    @Bean
    @ConfigurationProperties(prefix = "kafka.rcc.topic")
    public Topic rccTopic() {
        return BeanUtils.instantiateClass(Topic.class);
    }

    @Bean
    @ConfigurationProperties(prefix = "kafka.batch.producer")
    public ProducerConfig kafkaBatchProducerConfig() {
        return BeanUtils.instantiateClass(ProducerConfig.class);
    }

    @Bean
    @ConfigurationProperties(prefix = "kafka.batch.consumer")
    public ConsumerConfig batchConsumerConfig() {
        return BeanUtils.instantiateClass(ConsumerConfig.class);
    }

    @Bean
    @ConfigurationProperties(prefix = "kafka.txn.producer")
    public ProducerConfig kafkaTxnProducerConfig() {
        return BeanUtils.instantiateClass(ProducerConfig.class);
    }

    @Bean(name = "kafka-batch-producer")
    @Autowired
    public Producer kafkaBatchProducer(ProducerConfig kafkaBatchProducerConfig) {
        Map<String, Topic> topics = new HashMap<>();
        topics.put(rccTopic().getName(), rccTopic());
        topics.put(spocCreateTopic().getName(), spocCreateTopic());
        log.info("kafka batch producer {}", Json.serialize(kafkaBatchProducerConfig));
        return new Producer(kafkaBatchProducerConfig.toBuilder().topics(topics).build());
    }

    @Bean(name = "kafka-txn-producer")
    @Autowired
    public Producer kafkaTxnProducer(ProducerConfig kafkaTxnProducerConfig) {
        Map<String, Topic> topics = new HashMap<>();
        topics.put(commEventTopic().getName(), commEventTopic());
        log.info("kafka-txn-producer {}", Json.serialize(kafkaTxnProducerConfig));
        return new Producer(kafkaTxnProducerConfig.toBuilder().topics(topics).build());
    }

    @Bean
    @Autowired
    public Consumer cmsRestaurantPocTopicConsumer(RestaurantPocKafkaEventHandler restaurantPocKafkaEventHandler, @Qualifier("kafka-batch-producer") Producer kafkaBatchProducer) {
        return new Consumer(batchConsumerConfig().toBuilder().topic(cmsRestaurantPocTopic()).clientId(cmsRestaurantPocTopicClientId).build(), restaurantPocKafkaEventHandler, kafkaBatchProducer);

    }

}
