package com.swiggy.restaurantone.restaurantEvent.configs;

import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.vendorconfig.FeatureConfigActionHandler;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.vendorconfig.PrepTimeConfigUpdateActionHandler;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.compliance.OrderComplianceActionHandler;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.compliance.RestaurantComplianceStatusActionHandler;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.holidaySlot.HolidaySlotRemoveHandler;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.rcc.RccAddActionHandler;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.rcc.RccRemoveActionHandler;
import com.swiggy.restaurantone.restaurantEvent.services.ActionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.EnumMap;

@Configuration
public class ActionConfig {

    @Autowired
    private RccAddActionHandler rccAddActionHandler;

    @Autowired
    private RccRemoveActionHandler rccRemoveActionHandler;

    @Autowired
    private HolidaySlotRemoveHandler holidaySlotRemoveHandler;

    @Autowired
    private OrderComplianceActionHandler orderComplianceActionHandler;

    @Autowired
    private RestaurantComplianceStatusActionHandler restaurantComplianceStatusActionHandler;

    @Autowired
    private PrepTimeConfigUpdateActionHandler prepTimeConfigUpdateActionHandler;

    @Autowired
    private FeatureConfigActionHandler featureConfigActionHandler;

    public EnumMap<ActionEnum, ActionHandler> getPrepTimeConfigActionHandler() {
        EnumMap<ActionEnum, ActionHandler> actionHandlerEnumMap = new EnumMap<>(ActionEnum.class);
        actionHandlerEnumMap.put(ActionEnum.UPDATE, prepTimeConfigUpdateActionHandler);
        return actionHandlerEnumMap;
    }

    public EnumMap<ActionEnum, ActionHandler> getFeatureConfigActionHandler() {
        EnumMap<ActionEnum, ActionHandler> actionHandlerEnumMap = new EnumMap<>(ActionEnum.class);
        actionHandlerEnumMap.put(ActionEnum.UPDATE, featureConfigActionHandler);
        return actionHandlerEnumMap;
    }

    public EnumMap<ActionEnum, ActionHandler> getRccActionHandler() {
        EnumMap<ActionEnum, ActionHandler> rccActionHandlerEnumMap = new EnumMap<>(ActionEnum.class);
        rccActionHandlerEnumMap.put(ActionEnum.ADD, rccAddActionHandler);
        rccActionHandlerEnumMap.put(ActionEnum.REMOVE, rccRemoveActionHandler);
        return rccActionHandlerEnumMap;
    }

    @Bean("eventActionHandlerMap")
    public EnumMap<EventTypeEnum,EnumMap <ActionEnum, ActionHandler>> getEventActionHandler() {
        EnumMap<EventTypeEnum, EnumMap<ActionEnum, ActionHandler>> eventActionHandler = new EnumMap<>(EventTypeEnum.class);
        eventActionHandler.put(EventTypeEnum.RCC,  getRccActionHandler());
        eventActionHandler.put(EventTypeEnum.PREP_TIME_CONFIG, getPrepTimeConfigActionHandler());
        eventActionHandler.put(EventTypeEnum.FEATURE_CONFIG, getFeatureConfigActionHandler());
        eventActionHandler.put(EventTypeEnum.HOLIDAY_SLOT, getHolidaySlotActionHandler());
        eventActionHandler.put(EventTypeEnum.RESTAURANT_COMPLIANCE_STATUS, getRestaurantComplianceStatusActionHandler());
        eventActionHandler.put(EventTypeEnum.ORDER_COMPLIANCE, getOrderComplianceActionHandler());
        return eventActionHandler;
    }

    public EnumMap<ActionEnum, ActionHandler> getHolidaySlotActionHandler() {
        EnumMap<ActionEnum, ActionHandler> holidaySlotActionHandlerEnumMap = new EnumMap<>(ActionEnum.class);
        holidaySlotActionHandlerEnumMap.put(ActionEnum.REMOVE, holidaySlotRemoveHandler);
        return holidaySlotActionHandlerEnumMap;
    }

    public EnumMap<ActionEnum, ActionEnum> getRccUndoAction() {
        EnumMap<ActionEnum, ActionEnum> rccUndoActions = new EnumMap<>(ActionEnum.class);
        rccUndoActions.put(ActionEnum.ADD, ActionEnum.REMOVE);
        rccUndoActions.put(ActionEnum.REMOVE, ActionEnum.ADD);
        return rccUndoActions;
    }

    @Bean("undoActionEnumMap")
    public EnumMap<EventTypeEnum, EnumMap<ActionEnum, ActionEnum>> getUndoAction() {
        EnumMap<EventTypeEnum, EnumMap<ActionEnum, ActionEnum>> undoActionMap = new EnumMap<>(EventTypeEnum.class);
        undoActionMap.put(EventTypeEnum.RCC, getRccUndoAction());
        return undoActionMap;
    }

    public EnumMap<ActionEnum, ActionHandler> getRestaurantComplianceStatusActionHandler() {
        EnumMap<ActionEnum, ActionHandler> restComplianceStatusActionHandler = new EnumMap<>(ActionEnum.class);
        restComplianceStatusActionHandler.put(ActionEnum.ADD, restaurantComplianceStatusActionHandler);
        return restComplianceStatusActionHandler;
    }

    public EnumMap<ActionEnum, ActionHandler> getOrderComplianceActionHandler() {
        EnumMap<ActionEnum, ActionHandler> actionHandlerMap = new EnumMap<>(ActionEnum.class);
        actionHandlerMap.put(ActionEnum.ADD, orderComplianceActionHandler);
        return actionHandlerMap;
    }
}
