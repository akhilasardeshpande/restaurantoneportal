package com.swiggy.restaurantone.restaurantEvent.configs;

import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.restaurantEvent.handlers.eventHandler.FeatureConfigEventHandler;
import com.swiggy.restaurantone.restaurantEvent.handlers.eventHandler.HolidaySlotEventHandler;
import com.swiggy.restaurantone.restaurantEvent.handlers.eventHandler.PrepTimeConfigEventHandler;
import com.swiggy.restaurantone.restaurantEvent.handlers.eventHandler.RccEventHandler;
import com.swiggy.restaurantone.restaurantEvent.handlers.eventHandler.compliance.OrderComplianceHandler;
import com.swiggy.restaurantone.restaurantEvent.handlers.eventHandler.compliance.RestaurantComplianceStatusEventHandler;
import com.swiggy.restaurantone.restaurantEvent.services.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

@Configuration
public class EventConfig {

    @Autowired
    private RccEventHandler rccEventHandler;

    @Autowired
    private HolidaySlotEventHandler holidaySlotEventHandler;

    @Autowired
    private RestaurantComplianceStatusEventHandler restaurantComplianceStatusHandler;

    @Autowired
    private OrderComplianceHandler orderComplianceHandler;

    @Autowired
    private PrepTimeConfigEventHandler prepTimeConfigEventHandler;

    @Autowired
    private FeatureConfigEventHandler featureConfigEventHandler;

    @Bean("undoEventSet")
    public Set<EventTypeEnum> getUnDoRestaurantEvent() {
        Set<EventTypeEnum> undoEventTypeEnumSet = new HashSet<>();
        undoEventTypeEnumSet.add(EventTypeEnum.RCC);
        return undoEventTypeEnumSet;
    }

    @Bean
    public Set<ActionEnum> getPrepTimeConfigAction() {
        return EnumSet.of(ActionEnum.UPDATE);
    }

    @Bean
    public Set<ActionEnum> getFeatureConfigAction() {
        return EnumSet.of(ActionEnum.UPDATE);
    }

    @Bean
    public Set<ActionEnum> getRccAction() {
        Set<ActionEnum> rccActions = new HashSet<>();
        rccActions.add(ActionEnum.ADD);
        rccActions.add(ActionEnum.REMOVE);
        rccActions.add(ActionEnum.UNDO);
        return rccActions;
    }

    @Bean
    public Set<ActionEnum> getHolidaySlotAction() {
        Set<ActionEnum> rccActions = new HashSet<>();
        rccActions.add(ActionEnum.REMOVE);
        return rccActions;
    }

    @Bean
    public Set<ActionEnum> getRestaurantComplianceStatusAction(){
        Set<ActionEnum> restaurantComplianceStatusActions = new HashSet<>();
        restaurantComplianceStatusActions.add(ActionEnum.ADD);
        return restaurantComplianceStatusActions;
    }


    @Bean
    public Set<ActionEnum> getOrderComplianceAction(){
        Set<ActionEnum> orderComplianceActions = new HashSet<>();
        orderComplianceActions.add(ActionEnum.ADD);
        return orderComplianceActions;
    }

    @Bean("eventEventHandlerEnumMap")
    public EnumMap<EventTypeEnum, EventHandler> getEventHandler() {
        EnumMap<EventTypeEnum, EventHandler> restaurantEventHandlerEnumMap = new EnumMap<>(EventTypeEnum.class);
        restaurantEventHandlerEnumMap.put(EventTypeEnum.RCC, rccEventHandler);
        restaurantEventHandlerEnumMap.put(EventTypeEnum.PREP_TIME_CONFIG, prepTimeConfigEventHandler);
        restaurantEventHandlerEnumMap.put(EventTypeEnum.FEATURE_CONFIG, featureConfigEventHandler);
        restaurantEventHandlerEnumMap.put(EventTypeEnum.HOLIDAY_SLOT, holidaySlotEventHandler);
        restaurantEventHandlerEnumMap.put(EventTypeEnum.RESTAURANT_COMPLIANCE_STATUS, restaurantComplianceStatusHandler);
        restaurantEventHandlerEnumMap.put(EventTypeEnum.ORDER_COMPLIANCE, orderComplianceHandler);
        return restaurantEventHandlerEnumMap;
    }
}
