package com.swiggy.restaurantone.restaurantEvent.dao;

import com.swiggy.restaurantone.restaurantEvent.entity.EventEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Component
public interface EventDao extends JpaRepository<EventEntity, Long> {
    EventEntity findById(Long id);

    List<EventEntity> findAllByTypeAndCreatedAtBetween(String type, LocalDateTime fromTime, LocalDateTime toTime);

    @Transactional
    @Modifying
    @Query("update EventEntity e SET e.status=:status , e.undoAllowed=:undoAllowed, e.action=:action, e.updatedAt=:updatedAt WHERE e.id=:id")
    Integer updateStatus(@Param("id") Long id, @Param("undoAllowed") boolean undoAllowed, @Param("status") String status, @Param("action") String action, @Param("updatedAt") LocalDateTime updatedAt);

}
