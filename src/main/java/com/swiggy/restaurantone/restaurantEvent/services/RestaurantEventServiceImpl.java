package com.swiggy.restaurantone.restaurantEvent.services;

import com.swiggy.commons.Json;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.restaurantEvent.dao.EventDao;
import com.swiggy.restaurantone.restaurantEvent.entity.EventEntity;
import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventStatusEnum;
import com.swiggy.restaurantone.restaurantEvent.mapper.EventEntityMapper;
import com.swiggy.restaurantone.restaurantEvent.pojos.businessObject.EventBo;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantEventRequest;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantUpdateEventRequest;
import com.swiggy.restaurantone.restaurantEvent.pojos.response.RestaurantEventDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;


@Slf4j
@Service
public class RestaurantEventServiceImpl implements RestaurantEventService {


    @Autowired
    EventEntityMapper eventEntityMapper;

    @Autowired
    private Set<EventTypeEnum> undoEventSet;

    @Autowired
    private EventDao eventDao;

    @Autowired
    private AsyncEventHandler asyncEventHandler;

    @Autowired
    private EnumMap<EventTypeEnum, EnumMap<ActionEnum, ActionEnum>> undoActionEnumMap;


    @Autowired
    private AuthorizationHelper authorizationHelper;

    @Override
    public Long createRestaurantEvent(RestaurantEventRequest restaurantEventRequest) {
        boolean undoAllowed = undoEventSet.contains(restaurantEventRequest.getType());
        User user = authorizationHelper.fetchUser();
        Long eventId = persistInitiatedEvent(restaurantEventRequest, undoAllowed, user.email);
        if (Objects.nonNull(eventId)) {
            asyncEventHandler.createEvent(restaurantEventRequest, undoAllowed, eventId, user);
        }
        return eventId;
    }


    private Long persistInitiatedEvent(RestaurantEventRequest restaurantEventRequest, boolean undoAllowed, String userEmail) {
        EventEntity eventEntity = EventEntityMapper.businessToEntity(
                restaurantEventRequest, undoAllowed, EventStatusEnum.INITIATED.name(), userEmail);

        EventEntity responseEntity = eventDao.save(eventEntity);
        if (Objects.nonNull(responseEntity)) {
            log.info("successfully persisted INITIATED event. Event:{}, eventId:{}", Json.serialize(restaurantEventRequest), responseEntity.getId());
            return responseEntity.getId();
        }
        return null;
    }


    public boolean updateEventWithAction(RestaurantUpdateEventRequest restaurantUpdateEvent, Long eventId) {
        EventEntity eventEntity = eventDao.findById(eventId);
        EventBo eventBo = EventEntityMapper.convertEntityToBO(eventEntity);
        EventTypeEnum type = eventBo.getType();
        ActionEnum action = restaurantUpdateEvent.getAction();
        User user = authorizationHelper.fetchUser();
        if (ActionEnum.UNDO.equals(action)) {
            if (eventBo.isUndoAllowed()) {
                action = undoActionEnumMap.get(type)
                        .get(eventBo.getAction());
            } else {
                log.error("undo action is not allowed for event id: {}", eventBo.getId());
                return false;
            }
        }

        asyncEventHandler.updateEvent(restaurantUpdateEvent, eventId, eventBo, type, action, user);
        return true;
    }

    public RestaurantEventDetails getEventDetailsById(Long Id) {
        return  EventEntityMapper.convertEntityToEventDetails(eventDao.findById(Id));
    }


    public List<RestaurantEventDetails> getEventDetailsByType(EventTypeEnum type, LocalDateTime fromTime, LocalDateTime toTime) {

        List<EventEntity> eventEntityList = eventDao.findAllByTypeAndCreatedAtBetween(type.name(), fromTime, toTime);
        List<RestaurantEventDetails> restaurantEventDetailsList = new ArrayList<>();
        for (EventEntity eventEntity : eventEntityList) {
            RestaurantEventDetails restaurantEventDetails = EventEntityMapper.convertEntityToEventDetails(eventEntity);
            restaurantEventDetailsList.add(restaurantEventDetails);
        }
        return restaurantEventDetailsList;
    }
}
