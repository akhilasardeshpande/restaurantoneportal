package com.swiggy.restaurantone.restaurantEvent.services;

import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.restaurantEvent.exceptions.ActionHandlerException;

import java.io.IOException;

public interface ActionHandler {
    boolean handler(Object meta, User user, Long eventId) throws IOException, ActionHandlerException;
}
