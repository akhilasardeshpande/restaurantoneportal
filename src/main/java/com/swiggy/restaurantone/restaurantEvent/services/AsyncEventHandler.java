package com.swiggy.restaurantone.restaurantEvent.services;

import com.swiggy.commons.Json;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.restaurantEvent.dao.EventDao;
import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventStatusEnum;
import com.swiggy.restaurantone.restaurantEvent.factory.EventHandlerFactory;
import com.swiggy.restaurantone.restaurantEvent.instrumentation.InstrumentServiceImpl;
import com.swiggy.restaurantone.restaurantEvent.pojos.businessObject.EventBo;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantEventRequest;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantUpdateEventRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Objects;

@Service
@Slf4j
public class AsyncEventHandler {

    @Autowired
    private EventHandlerFactory eventHandlerFactory;

    @Autowired
    private InstrumentServiceImpl instrumentService;

    @Autowired
    private EventDao eventDao;

    @Async
    public void createEvent(RestaurantEventRequest restaurantEventRequest, boolean undoAllowed, Long eventId, User user) {
        EventHandler eventHandler = eventHandlerFactory.getEventHandler(restaurantEventRequest.getType());
        boolean response = eventHandler.handler(restaurantEventRequest.getMeta(), restaurantEventRequest.getAction(), user, eventId);
        if (response) {
            updateStatus(EventStatusEnum.SUCCESS.name(), eventId, undoAllowed, restaurantEventRequest.getAction());
            instrumentService.doInstrument(restaurantEventRequest, undoAllowed, eventId, EventStatusEnum.SUCCESS.name(), user.email);
        } else {
            updateStatus(EventStatusEnum.FAILURE.name(), eventId, Boolean.FALSE, restaurantEventRequest.getAction());
            instrumentService.doInstrument(restaurantEventRequest, Boolean.FALSE, eventId, EventStatusEnum.FAILURE.name(), user.email);
        }
    }

    public void updateStatus(String status, Long eventId, boolean undoAllowed, ActionEnum action) {
        Integer updatedRows = eventDao.updateStatus(eventId, undoAllowed, status, action.name(), LocalDateTime.now());

        if (Objects.nonNull(updatedRows) && updatedRows.equals(1)) {
            log.info("successfully updated the status. eventId: {} , status {}, undoAllowed:{}", eventId, status, undoAllowed);
        } else if (Objects.nonNull(updatedRows) && updatedRows.equals(0)) {
            log.info("event Id doesn't exist.eventId:{} ", eventId);
        } else {
            log.error("unable to update the status.  eventId: {} , status {}, undoAllowed: {}", eventId, status, undoAllowed);
        }
    }

    @Async
    public void updateEvent(RestaurantUpdateEventRequest restaurantUpdateEvent, Long eventId, EventBo eventBo, EventTypeEnum type, ActionEnum action, User user) {
        updateStatus(EventStatusEnum.INITIATED.name(), eventId, eventBo.isUndoAllowed(), action);
        Object meta = Json.deserialize(eventBo.getMeta(), Object.class);

        EventHandler eventHandler = eventHandlerFactory.getEventHandler(type);
        boolean response = eventHandler.handler(meta, action, user, eventId);

        String status = response ? EventStatusEnum.SUCCESS.name() : EventStatusEnum.FAILURE.name();
        if (restaurantUpdateEvent.getAction().equals(ActionEnum.UNDO)) {
            updateStatus(status, eventId, Boolean.FALSE, action);
            instrumentService.doInstrument(eventBo, action, status, Boolean.FALSE, user.email);
        } else {
            updateStatus(status, eventId, eventBo.isUndoAllowed(), action);
            instrumentService.doInstrument(eventBo, action, status, eventBo.isUndoAllowed(), user.email);
        }
    }
}
