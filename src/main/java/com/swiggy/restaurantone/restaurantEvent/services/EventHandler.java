package com.swiggy.restaurantone.restaurantEvent.services;

import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;

public interface EventHandler {

    boolean handler(Object meta, ActionEnum action, User user, Long eventId);
}
