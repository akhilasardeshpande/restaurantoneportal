package com.swiggy.restaurantone.restaurantEvent.services;

import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantEventRequest;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantUpdateEventRequest;
import com.swiggy.restaurantone.restaurantEvent.pojos.response.RestaurantEventDetails;

import java.time.LocalDateTime;
import java.util.List;


public interface RestaurantEventService {
    Long createRestaurantEvent(RestaurantEventRequest restaurantEventRequest);

    boolean updateEventWithAction(RestaurantUpdateEventRequest restaurantUpdateEvent, Long eventId);

    List<RestaurantEventDetails> getEventDetailsByType(EventTypeEnum type, LocalDateTime fromTime, LocalDateTime toTime);
}
