package com.swiggy.restaurantone.restaurantEvent.instrumentation;

import com.swiggy.dp.ClientConfig;
import com.swiggy.dp.client.DPClient;
import com.swiggy.dp.event.EventRegistry;
import com.swiggy.restaurantone.compliance.ComplianceUploadDPEvent;
import com.swiggy.restaurantone.compliance.OrderComplianceDPEvent;
import com.swiggy.restaurantone.compliance.RestaurantComplianceStatusDPEvent;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.ConfigUploadStatusEvent;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.NudgeDetailsEvent;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.RestaurantConfigUploadEvent;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.RopRestaurantEvent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class DataPlatformConfig {

    @Value("${dp.appName}")
    private String appName;

    @Value(("${dp.appVersion}"))
    private String appVersion;

    @Value("${dp.asyncBatchSize}")
    private int batchSize;

    @Value("${dp.sinkEndPoint}")
    private String sinkEndPoint;

    @Value("${dp.socketTimeout}")
    private int socketTimeOut;

    @Value("${dp.messagePerSecond}")
    private int messagesPerSecond;

    @Value("${dp.maxQueueSize}")
    private int maxQueueSize;

    @Value("${dp.eventCount}")
    private int eventCount;

    @Value("${dp.isDiskBacked}")
    private boolean isDiskBacked;

    @Value("${dp.restaurantEventSchema.version}")
    private String schemaVersion;

    @Value("${dp.nudgeDetailsEventSchema.version}")
    private String nudgeDetailsEventSchemaVersion;

    @Value("${dp.restaurantComplianceStatusEventSchema.version}")
    private String restaurantComplianceStatusSchemaVersion;

    @Value("${dp.orderComplianceEventSchema.version}")
    private String orderComplianceSchemaVersion;

    @Value("${dp.complianceUploadEventSchema.version}")
    private String complianceUploadEventSchemaVersion;

    @Value("${dp.restaurantConfigUploadEventSchema.version}")
    private String restaurantConfigUploadEventSchemaVersion;

    @Value("${dp.configUpdateStatusEventSchemaVersion.version}")
    private String configUpdateStatusEventSchemaVersion;

    @Bean
    DPClient dpClient() {
        ClientConfig config = new ClientConfig();
        config.APP_NAME = appName;
        config.APP_VERSION = appVersion;
        config.ASYNC_BATCH_SIZE = batchSize;
        config.SINK_END_POINT = sinkEndPoint;
        config.SOCKET_TIMEOUT = socketTimeOut;
        config.MESSAGE_PER_SECOND = messagesPerSecond;
        config.MAX_QUEUE_SIZE = maxQueueSize;
        config.IS_DISK_BACKED = isDiskBacked;
        EventRegistry eventRegistry = new EventRegistry(eventCount);
        eventRegistry.register(NudgeDetailsEvent.class, nudgeDetailsEventSchemaVersion);
        eventRegistry.register(RopRestaurantEvent.class, schemaVersion);
        eventRegistry.register(OrderComplianceDPEvent.class, orderComplianceSchemaVersion);
        eventRegistry.register(RestaurantComplianceStatusDPEvent.class, restaurantComplianceStatusSchemaVersion);
        eventRegistry.register(ComplianceUploadDPEvent.class, complianceUploadEventSchemaVersion);
        eventRegistry.register(ConfigUploadStatusEvent.class, configUpdateStatusEventSchemaVersion);
        eventRegistry.register(RestaurantConfigUploadEvent.class, restaurantConfigUploadEventSchemaVersion);
        return new DPClient(config, eventRegistry);
    }
}
