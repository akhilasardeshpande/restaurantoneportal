package com.swiggy.restaurantone.restaurantEvent.instrumentation;

import com.swiggy.commons.Json;
import com.swiggy.dp.client.DPClient;
import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.mapper.InstrumentEventMapper;
import com.swiggy.restaurantone.restaurantEvent.pojos.businessObject.EventBo;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.ConfigUploadStatusEvent;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.NudgeDetailsEvent;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.RestaurantConfigUploadEvent;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.RopRestaurantEvent;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantEventRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
@Slf4j
public class InstrumentServiceImpl implements InstrumentService {

    @Autowired
    DPClient dpClient;

    @Override
    public void doInstrument(RestaurantEventRequest restaurantEventRequest, boolean undoAllowed, Long eventId, String status, String createdBy) {
        RopRestaurantEvent ropRestaurantEvent = InstrumentEventMapper.convertToRestaurantEvent(restaurantEventRequest, status, undoAllowed, eventId, createdBy);
        instrumentRestaurantEvent(ropRestaurantEvent);
    }

    @Override
    public void doInstrument(EventBo eventBo, ActionEnum action, String status, boolean undoAllowed, String createdBy) {
        RopRestaurantEvent ropRestaurantEvent = InstrumentEventMapper.convertToRestaurantEvent(eventBo, action, status, undoAllowed, createdBy);
        instrumentRestaurantEvent(ropRestaurantEvent);
    }

    @Override
    public void doInstrument(Long eventId, String request, String status) {
        RopRestaurantEvent ropRestaurantEvent = InstrumentEventMapper.convertToRestaurantEvent(eventId, request, status);
        instrumentRestaurantEvent(ropRestaurantEvent);
    }

    @Override
    public void doInstrument(List<ConfigUploadStatusEvent> configUploadStatusEventList) {
            try {
                for (ConfigUploadStatusEvent configUploadStatusEvent: configUploadStatusEventList) {
                    dpClient.send(configUploadStatusEvent);
                }
            } catch (Exception exception) {
                log.error("Error while doing instrumentation for ConfigUploadStatusEvent: {}, error:{}", Json.serialize(configUploadStatusEventList),
                        exception.getMessage());
            }
    }

    @Override
    public void doInstrument(RestaurantConfigUploadEvent restaurantConfigUploadEvent) {
        try {
            dpClient.send(restaurantConfigUploadEvent);
        } catch (Exception e) {
            log.error("Error while doing instrumentation for VendorConfigUploadEvent: {}, error:{}", Json.serialize(restaurantConfigUploadEvent), e.getMessage());
        }
    }

    @Override
    public void doInstrumentNudgeEvent(NudgeDetailsEvent nudgeDetailsEvent) {
        try {
            dpClient.send(nudgeDetailsEvent);
            log.info("NudgeDetailsEvent: {} successfully send to instrumentation.", Json.serialize(nudgeDetailsEvent));
        } catch (Exception e) {
            log.error("Error while doing instrumentation for NudgeDetailsEvent: {}, error:{}", Json.serialize(nudgeDetailsEvent), e.getMessage());
        }
    }

    @Override
    public void instrumentRestaurantEvent(RopRestaurantEvent ropRestaurantEvent) {
        try {
            dpClient.send(ropRestaurantEvent);
            log.info("Restaurant event: {} successfully send to instrumentation.", Json.serialize(ropRestaurantEvent));
        } catch (IOException e) {
            log.error("Error while doing instrumentation for Restaurant Event: {}, error:{}", Json.serialize(ropRestaurantEvent), e.getMessage());
        }
    }
}
