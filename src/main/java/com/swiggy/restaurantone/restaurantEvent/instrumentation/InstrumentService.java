package com.swiggy.restaurantone.restaurantEvent.instrumentation;

import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.pojos.businessObject.EventBo;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.ConfigUploadStatusEvent;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.NudgeDetailsEvent;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.RestaurantConfigUploadEvent;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.RopRestaurantEvent;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantEventRequest;

import java.util.List;

public interface InstrumentService {
    void instrumentRestaurantEvent(RopRestaurantEvent ropRestaurantEvent);

    void doInstrument(RestaurantEventRequest restaurantEventRequest, boolean undoAllowed, Long eventId, String status, String createdBy);

    void doInstrument(EventBo eventBo, ActionEnum action, String status, boolean undoAllowed, String createdBy);

    void doInstrument(Long eventId, String request, String status);

    void doInstrument(List<ConfigUploadStatusEvent> configUploadStatusEvent);

    void doInstrumentNudgeEvent(NudgeDetailsEvent nudgeDetailsEvent);

    void doInstrument(RestaurantConfigUploadEvent restaurantConfigUploadEvent);
}
