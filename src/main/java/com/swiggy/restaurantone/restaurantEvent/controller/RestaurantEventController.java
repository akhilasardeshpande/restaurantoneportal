package com.swiggy.restaurantone.restaurantEvent.controller;

import com.swiggy.commons.Json;
import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.Constant.ResponseConstants;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantEventRequest;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantUpdateEventRequest;
import com.swiggy.restaurantone.restaurantEvent.pojos.response.RestaurantEventDetails;
import com.swiggy.restaurantone.restaurantEvent.pojos.response.RestaurantEventResponse;
import com.swiggy.restaurantone.restaurantEvent.services.RestaurantEventServiceImpl;
import com.swiggy.restaurantone.utils.LocalDateTimeDeserializer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/v1")
@Slf4j
public class RestaurantEventController extends AbstractController {

    @Autowired
    private AuthorizationHelper authorizationHelper;

    @Autowired
    private RestaurantEventServiceImpl restaurantEventService;

    @RequestMapping(value = "/restaurant-event", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<RestaurantEventResponse> createRestaurantEvent(@RequestBody RestaurantEventRequest restaurantEvent) {

        int statusCode = ResponseConstants.FAILURE_STATUS_CODE;
        String statusMessage;
        RestaurantEventResponse data = null;

        try {
            if (authorizationHelper.authenticateUserSession()) {
                log.info("Restaurant Event creation request. RestaurantEvent: {}", Json.serialize(restaurantEvent));

                Long eventId = restaurantEventService.createRestaurantEvent(restaurantEvent);
                statusCode = Objects.isNull(eventId) ? ResponseConstants.FAILURE_STATUS_CODE : ResponseConstants.SUCCESS_STATUS_CODE;
                statusMessage = Objects.isNull(eventId) ? "Error while creating restaurant event" : "Successfully restaurant event created";
                data = Objects.isNull(eventId) ? null : RestaurantEventResponse.builder().id(eventId).build();

            } else {
                log.error("restaurant event creation request is raised by unauthorized user. RestaurantEvent:{} ", Json.serialize(restaurantEvent));
                statusMessage = "Unauthorized user";
            }
        } catch (Exception ex) {
            log.error("Exception occurred while creating the restaurant event. RestaurantEvent:{} , message:{}, cause:{} ",
                    Json.serialize(restaurantEvent), ex.getMessage(), ex.getCause());
            statusMessage = "Exception occurred while creating the restaurant event";
        }

        return new Response<>(statusCode, data, statusMessage);
    }

    @RequestMapping(value = "/restaurant-event/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> updateRestaurantEvent(@PathVariable(value = "id") Long id, @RequestBody RestaurantUpdateEventRequest restaurantUpdateEvent) {

        int statusCode = ResponseConstants.FAILURE_STATUS_CODE;
        String statusMessage;

        try {
            if (authorizationHelper.authenticateUserSession()) {
                log.info("Restaurant update Event request. RestaurantUpdateEvent: {}, eventId: {}", Json.serialize(restaurantUpdateEvent), id);
                boolean response = restaurantEventService.updateEventWithAction(restaurantUpdateEvent, id);
                statusCode = response ? ResponseConstants.SUCCESS_STATUS_CODE : ResponseConstants.FAILURE_STATUS_CODE;
                statusMessage = response ? "Successfully event updated" : "Error while updating restaurant event";

            } else {
                log.error("restaurant updating event request is raised by unauthorized user. RestaurantUpdateEvent:{} , eventId:{} ",
                        Json.serialize(restaurantUpdateEvent), id);
                statusMessage = "Unauthorized user";
            }
        } catch (Exception ex) {
            log.error("Exception occurred while updating the event. RestaurantUpdateEvent:{} ,eventId:{}, message:{}, cause:{}",
                    Json.serialize(restaurantUpdateEvent), id, ex.getMessage(), ex.getCause());
            statusMessage = "Exception occurred while updating the event with action";
        }
        return new Response<>(statusCode, null, statusMessage);
    }

    @RequestMapping(value = "/restaurant-event/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> getRestaurantEvent(@PathVariable(value = "id") Long id) {
        int statusCode = ResponseConstants.FAILURE_STATUS_CODE;
        String statusMessage = "success";
        RestaurantEventDetails data = null;
        try {
            if (authorizationHelper.authenticateUserSession()) {
                log.info("Restaurant get Event request. eventId: {}", id);
                data = restaurantEventService.getEventDetailsById(id);
                statusCode = ResponseConstants.SUCCESS_STATUS_CODE;
            } else {
                log.error("restaurant updating event request is raised by unauthorized user. RestaurantGetEvent , eventId:{} ",
                        id);
                statusMessage = "Unauthorized user";
            }
        } catch (Exception ex) {
            log.error("Exception occurred while getting the event.eventId:{}, message:{}, cause:{}",
                    id, ex.getMessage(), ex.getCause());
            statusMessage = "Exception occurred while getting the event";
        }
        return new Response<>(statusCode, data, statusMessage);
    }

    @RequestMapping(value = "/restaurant-event", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Response<?> getRestaurantEvent(@RequestParam("type") EventTypeEnum type,
                                          @RequestParam("fromTime") String fromTime,
                                          @RequestParam("toTime") String toTime) {


        int statusCode = ResponseConstants.FAILURE_STATUS_CODE;
        String statusMessage;
        List<RestaurantEventDetails> data = null;

        try {
            if (authorizationHelper.authenticateUserSession()) {
                log.info("Event fetch request received for event type: {}, fromTime:{} - toTime{} ", type, fromTime, toTime);
                LocalDateTime localFromTime = LocalDateTimeDeserializer.convertStringToLocalDateTime(fromTime);
                LocalDateTime localToTime = LocalDateTimeDeserializer.convertStringToLocalDateTime(toTime);
                data = restaurantEventService.getEventDetailsByType(type, localFromTime, localToTime);
                statusCode = Objects.isNull(data) ? ResponseConstants.FAILURE_STATUS_CODE : ResponseConstants.SUCCESS_STATUS_CODE;
                statusMessage = Objects.isNull(data) ? "Error while fetching restaurant events" : "Successfully events fetched";

            } else {
                log.error("Restaurant fetch event request is requested by Unauthorized user. event Type: {}", type);
                statusMessage = "Unauthorized user";
            }
        } catch (Exception ex) {
            log.error("Exception occurred while fetching the events. event type: {}, fromTime:{} - toTime{}. message:{}, cause:{} ",
                    type, fromTime, toTime, ex.getMessage(), ex.getCause());
            statusMessage = "Exception occurred while fetching the events";
        }
        return new Response<>(statusCode, data, statusMessage);
    }
}