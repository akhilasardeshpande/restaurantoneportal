package com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestaurantConfigUploadEvent implements Serializable {

    @JsonProperty(value = "job_event_id")
    private Long id;

    @JsonProperty(value = "file_url")
    private String fileUrl;

    @JsonProperty(value = "email")
    private String email;

    @JsonProperty("no_of_records")
    private Long records;

    @JsonProperty("status")
    private String status;

    @JsonProperty(value = "timestamp")
    private Long timestamp;

    @JsonProperty(value = "date")
    private String date;
}
