package com.swiggy.restaurantone.restaurantEvent.pojos.restaurant;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestaurantDetails {

    @JsonProperty(value = "include")
    private InclusiveRestaurants inclusiveRestaurants;

    @JsonProperty(value = "exclude")
    private ExclusiveRestaurants exclusiveRestaurants;

}
