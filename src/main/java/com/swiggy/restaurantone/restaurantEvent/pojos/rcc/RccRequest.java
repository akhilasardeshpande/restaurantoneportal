package com.swiggy.restaurantone.restaurantEvent.pojos.rcc;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RccRequest {
    @JsonProperty(value = "restaurant_ids")
    List<Long> restaurantIds;

    @JsonProperty(value = "requested_by")
    String requestedBy;

    @JsonProperty(value = "is_set_rcc")
    boolean setRcc;
}
