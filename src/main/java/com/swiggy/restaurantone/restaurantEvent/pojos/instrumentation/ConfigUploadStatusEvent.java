package com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConfigUploadStatusEvent implements Serializable {

    @JsonProperty(value = "job_event_id")
    private Long id;

    @JsonProperty(value = "record_no")
    private Long recordNo;

    @JsonProperty(value = "restaurant_id")
    private Long restaurantId;

    @JsonProperty(value = "key")
    private String key;

    @JsonProperty(value = "value")
    private String value;

    @JsonProperty("message")
    private String message;

    @JsonProperty("status")
    private String status;

    @JsonProperty(value = "timestamp")
    private Long timestamp;

    @JsonProperty(value = "date")
    private String date;
}
