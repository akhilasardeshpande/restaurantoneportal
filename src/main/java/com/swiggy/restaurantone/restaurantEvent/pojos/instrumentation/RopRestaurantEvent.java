package com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RopRestaurantEvent implements Serializable {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "type")
    private EventTypeEnum type;

    @JsonProperty(value = "description")
    private String description;

    @JsonProperty("status")
    private String status;

    @JsonProperty(value = "undoAllowed")
    private boolean undoAllowed;

    @JsonProperty(value = "action")
    private ActionEnum action;

    @JsonProperty("timestamp")
    private Long timestamp;

    @JsonProperty(value = "created_by")
    private String createdBy;

    @JsonProperty(value = "meta")
    private String meta;

    @JsonProperty(value = "action_request")
    private String actionRequest;

    @JsonProperty(value = "action_request_status")
    private String actionRequestStatus;

}
