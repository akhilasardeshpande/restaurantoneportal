package com.swiggy.restaurantone.restaurantEvent.pojos.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventStatusEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.utils.LocalDateTimeDeserializer;
import com.swiggy.restaurantone.utils.LocalDateTimeSerializer;
import lombok.*;
import org.hibernate.annotations.Type;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestaurantEventDetails {

    @JsonProperty(value = "id")
    private Long id;

    @JsonProperty(value = "type")
    private EventTypeEnum type;

    @JsonProperty(value = "description")
    private String description;

    @JsonProperty("status")
    private EventStatusEnum status;

    @JsonProperty(value = "undoAllowed")
    private boolean undoAllowed;

    @JsonProperty(value = "action")
    private ActionEnum action;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Type(
            type = "com.swiggy.restaurantone.data.coreEntity.LocalDateTimeType"
    )
    @JsonProperty(value = "createdAt")
    private LocalDateTime createdAt;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Type(
            type = "com.swiggy.restaurantone.data.coreEntity.LocalDateTimeType"
    )
    @JsonProperty(value = "updatedAt")
    private LocalDateTime updatedAt;

    @JsonProperty(value = "createdBy")
    private String createdBy;

    @JsonProperty(value = "updatedBy")
    private String updatedBy;

}
