package com.swiggy.restaurantone.restaurantEvent.pojos.businessObject;

import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class EventBo {

    private Long id;

    private EventTypeEnum type;

    private String description;

    private String createdBy;

    private String status;

    private boolean undoAllowed;

    private ActionEnum action;

    private String updatedBy;

    private String meta;
}
