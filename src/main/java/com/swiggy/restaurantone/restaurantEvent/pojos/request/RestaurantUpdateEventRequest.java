package com.swiggy.restaurantone.restaurantEvent.pojos.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestaurantUpdateEventRequest {

    @JsonProperty(value = "action")
    private ActionEnum action;

}
