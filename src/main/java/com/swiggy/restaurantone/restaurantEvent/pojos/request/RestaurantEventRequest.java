package com.swiggy.restaurantone.restaurantEvent.pojos.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestaurantEventRequest {

    @NonNull
    @JsonProperty(value = "type")
    private EventTypeEnum type;

    @NonNull
    @JsonProperty(value = "action")
    private ActionEnum action;

    @NonNull
    @JsonProperty(value = "description")
    private String description;

    @NonNull
    @JsonProperty(value = "meta")
    private Object meta;
}
