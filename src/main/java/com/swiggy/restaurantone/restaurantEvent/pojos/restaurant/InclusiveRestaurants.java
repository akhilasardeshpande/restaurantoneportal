package com.swiggy.restaurantone.restaurantEvent.pojos.restaurant;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InclusiveRestaurants {

    @JsonProperty(value = "isAllCitiesSelected")
    private boolean isAllCitiesSelected;

    @JsonProperty(value = "cityIds")
    private List<Integer> cityIds;

    @JsonProperty(value = "partnerTypes")
    private List<Integer> partnerTypes;

    @JsonProperty(value = "restaurantIds")
    private List<Long> restaurantIds;
}
