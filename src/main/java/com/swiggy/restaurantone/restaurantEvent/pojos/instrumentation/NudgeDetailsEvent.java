package com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NudgeDetailsEvent implements Serializable {

    @JsonProperty(value = "nudge_id")
    private String nudgeId;

    @JsonProperty(value = "created_at")
    private String createdAt;

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "sub_type")
    private String subType;

    @JsonProperty(value = "type")
    private String type;

    @JsonProperty(value = "id")
    private String id;

    @JsonProperty(value = "content")
    private Map<String, Object> content;

    @JsonProperty(value = "nudge_json")
    private String nudgeJson;

    @JsonProperty(value = "createdBy")
    private String createdBy;

}
