package com.swiggy.restaurantone.restaurantEvent.mapper;

import com.swiggy.restaurantone.pojos.requests.rms.RestaurantToggleStatusRMS;

public class RemoveHolidayActionMapper {

    public static RestaurantToggleStatusRMS getRestaurantToggleStatusRMS(Long restaurantId) {
        return RestaurantToggleStatusRMS
                .builder()
                .restaurantId(restaurantId)
                .isOpen(true)
                .build();
    }
}
