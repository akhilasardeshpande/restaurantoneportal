package com.swiggy.restaurantone.restaurantEvent.mapper;

import com.swiggy.commons.Json;
import com.swiggy.restaurantone.restaurantEvent.entity.EventEntity;
import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventStatusEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.restaurantEvent.pojos.businessObject.EventBo;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantEventRequest;
import com.swiggy.restaurantone.restaurantEvent.pojos.response.RestaurantEventDetails;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;

@Component
public class EventEntityMapper {


    public static EventEntity businessToEntity(RestaurantEventRequest request, boolean undoAllowed, String status, String user) {
        return EventEntity.builder().action(request.getAction().name())
                .description(request.getDescription())
                .type(request.getType().name())
                .createdAt(LocalDateTime.now(ZoneId.of("Asia/Kolkata")))
                .updatedAt(LocalDateTime.now(ZoneId.of("Asia/Kolkata")))
                .createdBy(user)
                .updatedBy(user)
                .status(status)
                .undoAllowed(undoAllowed)
                .meta(Json.serialize(request.getMeta()))
                .build();
    }

    public static EventBo convertEntityToBO(EventEntity eventEntity) {
        return EventBo.builder()
                .action(ActionEnum.valueOf(eventEntity.getAction()))
                .id(eventEntity.getId())
                .status(eventEntity.getStatus())
                .type(EventTypeEnum.valueOf(eventEntity.getType()))
                .undoAllowed(eventEntity.isUndoAllowed())
                .meta(eventEntity.getMeta())
                .createdBy(eventEntity.getCreatedBy()).build();
    }

    public static RestaurantEventDetails convertEntityToEventDetails(EventEntity eventEntity) {
        return RestaurantEventDetails.builder()
                .action(ActionEnum.valueOf(eventEntity.getAction()))
                .id(eventEntity.getId())
                .description(eventEntity.getDescription())
                .status(EventStatusEnum.valueOf(eventEntity.getStatus()))
                .type(EventTypeEnum.valueOf(eventEntity.getType()))
                .undoAllowed(eventEntity.isUndoAllowed())
                .createdAt(eventEntity.getCreatedAt())
                .updatedAt(eventEntity.getUpdatedAt())
                .createdBy(eventEntity.getCreatedBy())
                .updatedBy(eventEntity.getUpdatedBy())
                .build();
    }
}
