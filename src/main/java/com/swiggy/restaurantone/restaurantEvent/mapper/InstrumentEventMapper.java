package com.swiggy.restaurantone.restaurantEvent.mapper;

import com.swiggy.commons.Json;
import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.pojos.businessObject.EventBo;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.RopRestaurantEvent;
import com.swiggy.restaurantone.restaurantEvent.pojos.request.RestaurantEventRequest;
import org.springframework.stereotype.Service;

@Service
public class InstrumentEventMapper {

    public static RopRestaurantEvent convertToRestaurantEvent(RestaurantEventRequest restaurantEventRequest, String status, boolean undoAllowed, Long eventId, String createdBy) {
        long currentTimeStamp = System.currentTimeMillis();
        return RopRestaurantEvent.builder().
                action(restaurantEventRequest.getAction())
                .id(eventId)
                .description(restaurantEventRequest.getDescription())
                .status(status)
                .timestamp(currentTimeStamp)
                .undoAllowed(undoAllowed)
                .type(restaurantEventRequest.getType())
                .meta(Json.serialize(restaurantEventRequest.getMeta()))
                .createdBy(createdBy)
                .build();
    }

    public static RopRestaurantEvent convertToRestaurantEvent(EventBo eventBo, ActionEnum action, String status, boolean undoAllowed, String createdBy) {
        long currentTimeStamp = System.currentTimeMillis();
        return RopRestaurantEvent.builder()
                .action(action)
                .id(eventBo.getId())
                .status(status)
                .timestamp(currentTimeStamp)
                .type(eventBo.getType())
                .undoAllowed(undoAllowed)
                .description(eventBo.getDescription())
                .createdBy(createdBy)
                .build();
    }

    public static RopRestaurantEvent convertToRestaurantEvent(Long eventId, String actionRequest, String actionStatus) {
        long currentTimeStamp = System.currentTimeMillis();
        return RopRestaurantEvent.builder()
                .id(eventId)
                .actionRequest(actionRequest)
                .actionRequestStatus(actionStatus)
                .timestamp(currentTimeStamp)
                .build();
    }
}
