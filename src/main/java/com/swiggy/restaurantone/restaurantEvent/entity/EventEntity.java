package com.swiggy.restaurantone.restaurantEvent.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.swiggy.restaurantone.utils.LocalDateTimeDeserializer;
import com.swiggy.restaurantone.utils.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Table(name = "restaurant_event")
public class EventEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "type")
    private String type;

    @Column(name = "description")
    private String description;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "status")
    private String status;

    @Column(name = "undo_allowed")
    private boolean undoAllowed;

    @Column(name = "action")
    private String action;

    @Column(name = "updated_by")
    private String updatedBy;

    @Column(name = "created_at")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Type(
            type = "com.swiggy.restaurantone.data.coreEntity.LocalDateTimeType"
    )
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Type(
            type = "com.swiggy.restaurantone.data.coreEntity.LocalDateTimeType"
    )
    private LocalDateTime updatedAt;

    @Column(name = "meta")
    private String meta;

}


