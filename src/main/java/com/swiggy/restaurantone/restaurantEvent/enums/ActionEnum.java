package com.swiggy.restaurantone.restaurantEvent.enums;

public enum ActionEnum {
    ADD("ADD"), UPDATE("UPDATE"), REMOVE("REMOVE"), UNDO("UNDO");

    private final String value;

    ActionEnum(final String status) {
        this.value = status;
    }

    public String getValue() {
        return value;
    }
}