package com.swiggy.restaurantone.restaurantEvent.enums;

public enum EventTypeEnum {
    RCC("RCC"),
    PREP_TIME_CONFIG("PREP_TIME_CONFIG"),
    FEATURE_CONFIG("FEATURE_CONFIG"),
    HOLIDAY_SLOT("HOLIDAY_SLOT"),
    ORDER_COMPLIANCE("ORDER_COMPLIANCE"),
    RESTAURANT_COMPLIANCE_STATUS("RESTAURANT_COMPLIANCE_STATUS");

    private final String value;

    EventTypeEnum(final String status) {
        this.value = status;
    }

    public String getValue() {
        return value;
    }
}
