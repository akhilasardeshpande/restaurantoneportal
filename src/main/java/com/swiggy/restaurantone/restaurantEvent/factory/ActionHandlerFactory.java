package com.swiggy.restaurantone.restaurantEvent.factory;

import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.DefaultActionHandler;
import com.swiggy.restaurantone.restaurantEvent.services.ActionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.EnumMap;
import java.util.Objects;

@Component
public class ActionHandlerFactory {

    @Autowired
    EnumMap<EventTypeEnum, EnumMap<ActionEnum, ActionHandler>> eventActionHandlerMap;

    @Autowired
    private DefaultActionHandler defaultActionHandler;

    public ActionHandler getActionHandler(EventTypeEnum eventType, ActionEnum action) {
        ActionHandler actionHandler = eventActionHandlerMap.get(eventType).get(action);
        return Objects.nonNull(actionHandler) ? actionHandler : defaultActionHandler;
    }
}
