package com.swiggy.restaurantone.restaurantEvent.factory;

import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.restaurantEvent.services.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.EnumMap;

@Component
public class EventHandlerFactory {

    @Autowired
    private EnumMap<EventTypeEnum, EventHandler> eventEventHandlerEnumMap;

    public EventHandler getEventHandler(EventTypeEnum restaurantEvent) {
        return eventEventHandlerEnumMap.get(restaurantEvent);
    }
}
