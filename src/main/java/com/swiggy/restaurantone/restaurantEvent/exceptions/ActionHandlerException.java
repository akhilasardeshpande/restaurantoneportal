package com.swiggy.restaurantone.restaurantEvent.exceptions;

public class ActionHandlerException extends RuntimeException {

    public ActionHandlerException(String message) {
        super(message);
    }

    public ActionHandlerException(String message, Throwable cause) {
        super(message, cause);
    }
}
