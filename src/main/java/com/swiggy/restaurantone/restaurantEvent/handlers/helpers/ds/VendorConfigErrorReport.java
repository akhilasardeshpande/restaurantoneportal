package com.swiggy.restaurantone.restaurantEvent.handlers.helpers.ds;

import com.opencsv.CSVWriter;
import lombok.Getter;

import java.io.Closeable;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

public class VendorConfigErrorReport implements Closeable {

    @Getter
    private String errorFilePath;
    private CSVWriter writer;
    private final static String FILE_FORMAT = "/tmp/vendor-config-report-%s.csv";

    public VendorConfigErrorReport() throws IOException {
        errorFilePath = String.format(FILE_FORMAT, UUID.randomUUID());
        writer = new CSVWriter(new FileWriter(errorFilePath));
    }

    public void addRowError(long startNo, long endNo, String errorMessage) throws IOException {
        writer.writeNext(new String[]{String.valueOf(startNo), String.valueOf(endNo), errorMessage});
        writer.flush();
    }

    @Override
    public void close() throws IOException {
        writer.close();
    }
}
