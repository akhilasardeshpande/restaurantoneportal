package com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler;

import com.swiggy.backlit.batteries.prometheus.Instrumentation.InstrumentMetrics;
import com.swiggy.commons.Json;
import com.swiggy.restaurantone.response.SRSResponse;
import com.swiggy.restaurantone.restaurantEvent.exceptions.ActionHandlerException;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.ExclusiveRestaurants;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.InclusiveRestaurants;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.RestaurantDetails;
import com.swiggy.restaurantone.service.SRSAPIService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.IOException;
import java.util.*;

@Service
@Slf4j
public class RestaurantResolverAction {
    @Autowired
    private SRSAPIService srsapiService;

    public List<Long> getRestaurants(RestaurantDetails restaurantDetails) throws IOException, ActionHandlerException {

        InclusiveRestaurants inclusiveRestaurants = restaurantDetails.getInclusiveRestaurants();
        if (Objects.isNull(inclusiveRestaurants)) {
            return null;
        }
        ExclusiveRestaurants exclusiveRestaurants = restaurantDetails.getExclusiveRestaurants();
        List<Integer> cityIds = inclusiveRestaurants.getCityIds();
        List<Integer> partnerTypes = inclusiveRestaurants.getPartnerTypes();
        List<Long> inclusiveRestaurantIds = inclusiveRestaurants.getRestaurantIds();
        List<Long> exclusiveRestaurantIds = Objects.nonNull(exclusiveRestaurants) ? exclusiveRestaurants.getRestaurantIds() : null;
        boolean isAllCitiesSelected = inclusiveRestaurants.isAllCitiesSelected();
        List<Long> restaurantIdFromCityIdsAndPartnerType = null;
        List<Long> allDailyRestaurants = getAllDailyRestaurants();


        if (CollectionUtils.isNotEmpty(cityIds) || CollectionUtils.isNotEmpty(partnerTypes)) {
            restaurantIdFromCityIdsAndPartnerType = isAllCitiesSelected ?
                    getRestaurantsFromPartnerTypesAndCityIds(partnerTypes, null) :
                    getRestaurantsFromPartnerTypesAndCityIds(partnerTypes, cityIds);

        }

        return filterRestaurant(restaurantIdFromCityIdsAndPartnerType, inclusiveRestaurantIds, exclusiveRestaurantIds, allDailyRestaurants);

    }

    @Retryable(
            value = {IOException.class},
            maxAttempts = 4,
            backoff = @Backoff(
                    delay = 100,
                    maxDelay = 4000,
                    multiplier = 2.0
            )
    )
    @InstrumentMetrics
    public List<Long> getRestaurantsFromPartnerTypesAndCityIds(List<Integer> partnerTypes, List<Integer> cityIds) throws IOException, ActionHandlerException {

        try {
            Response<SRSResponse<List<Long>>> srsResponse = srsapiService.findByCityIdsAndPartnerTypes(cityIds, partnerTypes, true)
                    .execute();
            if (Objects.nonNull(srsResponse.body()) && srsResponse.isSuccessful()) {
                return srsResponse.body().getData();
            } else {
                log.error("Error while fetching cities and partner types restaurant list form SRS. SRS Response:{} ", Json.serialize(srsResponse));
                throw new ActionHandlerException("Error occurred while fetching cities and partner types restaurant API response from SRS");
            }
        } catch (IOException ex) {
            log.info("Exception occurred while fetching cities and partner types restaurant list form SRS. message:{} and cause: {}", ex.getMessage(), ex.getCause());
            throw ex;
        }
    }

    @InstrumentMetrics
    public List<Long> getAllDailyRestaurants() throws IOException, ActionHandlerException {

        try {
            Response<SRSResponse<List<Long>>> srsResponse = srsapiService.getAllDailyRestaurants().execute();
            if (Objects.nonNull(srsResponse.body()) && srsResponse.isSuccessful()) {
                return srsResponse.body().getData();
            } else {
                log.error("Error while fetching daily restaurant list form SRS. SRS Response:{} ", Json.serialize(srsResponse));
                throw new ActionHandlerException("Error occurred while fetching daily restaurant API response from SRS");
            }
        } catch (IOException ex) {
            log.info("Exception occurred while fetching daily restaurant list form SRS. message:{} and cause: {}", ex.getMessage(), ex.getCause());
            throw ex;
        }
    }


    private List<Long> filterRestaurant(List<Long> restaurantIdFromCityIdsAndPartnerType,
                                        List<Long> inclusiveRestaurantIds,
                                        List<Long> exclusiveRestaurantIds,
                                        List<Long> allDailyRestaurants) {

        Set<Long> result = new HashSet<>();

        if (CollectionUtils.isNotEmpty(restaurantIdFromCityIdsAndPartnerType)) {
            Set<Long> restaurantIdFromCityIdsAndPartnerTypeSet = new HashSet<>(restaurantIdFromCityIdsAndPartnerType);
            result.addAll(restaurantIdFromCityIdsAndPartnerTypeSet);
        }

        if (CollectionUtils.isNotEmpty(inclusiveRestaurantIds)) {
            Set<Long> inclusiveRestaurantIdsSet = new HashSet<>(inclusiveRestaurantIds);
            result.addAll(inclusiveRestaurantIdsSet);
        }

        if (CollectionUtils.isNotEmpty(exclusiveRestaurantIds)) {
            Set<Long> exclusiveRestaurantIdsSet = new HashSet<>(exclusiveRestaurantIds);
            result.removeAll(exclusiveRestaurantIdsSet);
        }

        if (CollectionUtils.isNotEmpty(allDailyRestaurants)) {
            Set<Long> allDailyRestaurantsSet = new HashSet<>(allDailyRestaurants);
            result.removeAll(allDailyRestaurantsSet);
        }

        return new ArrayList<>(result);
    }
}
