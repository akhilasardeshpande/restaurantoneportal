package com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.rcc;

import com.google.common.collect.Lists;
import com.swiggy.commons.Json;
import com.swiggy.kafka.clients.producer.Producer;
import com.swiggy.restaurantone.restaurantEvent.enums.EventStatusEnum;
import com.swiggy.restaurantone.restaurantEvent.instrumentation.InstrumentServiceImpl;
import com.swiggy.restaurantone.restaurantEvent.pojos.rcc.RccRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class RccCommonActionHandler {

    @Autowired
    @Qualifier("kafka-batch-producer")
    private Producer kafkaBatchProducer;

    @Value("${kafka.rcc.topic.name}")
    private String rccTopicName;

    @Value("${rccRestaurantBatchSize}")
    private Integer rccRestaurantBatches;

    @Autowired
    private InstrumentServiceImpl instrumentService;

    public void sendRequestToRcc(List<Long> rccRestaurantIds, boolean isSetRcc, String requestedBy, Long eventId) {

        final List<List<Long>> restaurantPartitions = Lists.partition(rccRestaurantIds, rccRestaurantBatches);
        for (List<Long> rccRestaurantBatch : restaurantPartitions) {
            RccRequest rccRequest = RccRequest.builder().restaurantIds(rccRestaurantBatch).setRcc(isSetRcc).requestedBy(requestedBy).build();
            String rccRequestString = Json.serialize(rccRequest);
            kafkaBatchProducer.send(rccTopicName, rccRequestString);
            instrumentService.doInstrument(eventId, rccRestaurantBatch.toString(), EventStatusEnum.SUCCESS.name());
            log.info("successfully publish rcc event. event:{}", rccRequestString);
        }
    }
}
