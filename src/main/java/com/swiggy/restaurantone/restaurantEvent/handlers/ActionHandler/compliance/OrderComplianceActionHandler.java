package com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.compliance;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.commons.Json;
import com.swiggy.restaurantone.compliance.ComplianceService;
import com.swiggy.restaurantone.compliance.ComplianceServiceException;
import com.swiggy.restaurantone.compliance.ComplianceType;
import com.swiggy.restaurantone.compliance.OrderComplianceRequest;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.restaurantEvent.exceptions.ActionHandlerException;
import com.swiggy.restaurantone.restaurantEvent.services.ActionHandler;
import com.swiggy.restaurantone.service.AmazonS3ClientService;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.io.BufferedReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.swiggy.restaurantone.utils.FileUtils.parseCSVFile;

@Component
@Slf4j
public class OrderComplianceActionHandler implements ActionHandler {

    private static final String UNSAFE_PACKAGING = "UNSAFE_PACKAGING";

    @Autowired
    private AmazonS3ClientService amazonS3ClientService;

    @Autowired
    private ComplianceService complianceService;

    @Value("${compliance.processing.delayMillis}")
    private long processingDelayDurationMillis;

    @Override
    public boolean handler(Object meta, User user, Long eventId) throws ActionHandlerException {
        List<String[]> failedInstances = new ArrayList<>();
        try {
            OrderComplianceUpload upload = Json.deserialize(Json.serialize(meta), OrderComplianceUpload.class);
            BufferedReader bufferedReader = amazonS3ClientService.getFileFromS3(upload.getInclude().getUploadUrl());
            List<List<String>> records = parseCSVFile(bufferedReader);
            complianceService.sendComplianceUploadEventToDP(eventId, upload.getInclude().getUploadUrl(),
                    user.getEmail(), records.size(), ComplianceType.OrderCompliance);
            for (List<String> complianceData : records) {
                OrderComplianceRequest request = null;
                try {
                    String orderId = complianceData.get(0);
                    request = OrderComplianceRequest.builder()
                            .type(UNSAFE_PACKAGING)
                            .orderId(Long.valueOf(orderId))
                            .build();
                    complianceService.createOrderCompliance(request);
                    log.info("success processing order compliance: {}", request);
                    complianceService.sendOrderComplianceEventToDP(eventId, request, true);
                    Thread.sleep(processingDelayDurationMillis);
                } catch (ComplianceServiceException ex) {
                    log.error("Failure processing order compliance: {}, error: {}", ex.getMessage(),
                            ex.getStackTrace());
                    failedInstances.add(new String[]{complianceData.get(0), ex.getMessage()});
                    complianceService.sendOrderComplianceEventToDP(eventId, request, false);
                } catch (InterruptedException ex) {
                    log.error("Failure putting thread to sleep: {}, error: {}", ex.getMessage(), ex.getStackTrace());
                } catch (Exception ex) {
                    log.error("Failure processing order compliance: {}, error: {}", ex.getMessage(),
                            ex.getStackTrace());
                    failedInstances.add(new String[]{complianceData.get(0), "invalid data"});
                    complianceService.sendOrderComplianceEventToDP(eventId, request, false);
                }
            }
            if (!failedInstances.isEmpty()) {
                complianceService.reportProcessingFailure(user, failedInstances,
                        "processing failure", ComplianceType.OrderCompliance);
            }
            return true;
        } catch (ActionHandlerException ex) {
            log.error("Exception while processing order compliance. Message: {} , cause: {}",
                    ex.getMessage(), ex.getCause());
            complianceService.reportProcessingFailure(user, Collections.EMPTY_LIST,
                    "upload failure", ComplianceType.OrderCompliance);
        }
        return false;
    }

    @Data
    @NoArgsConstructor
    private static class OrderComplianceUpload implements Serializable {
        @NotNull
        @JsonProperty("include")
        private Include include;

        @Data
        @NoArgsConstructor
        private static class Include implements Serializable {
            @NotNull
            @JsonProperty("uploadUrl")
            private String uploadUrl;
        }
    }
}
