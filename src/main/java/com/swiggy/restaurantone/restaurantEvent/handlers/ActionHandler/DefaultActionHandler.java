package com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler;

import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.restaurantEvent.exceptions.ActionHandlerException;
import com.swiggy.restaurantone.restaurantEvent.services.ActionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class DefaultActionHandler implements ActionHandler {

    @Override
    public boolean handler(Object meta, User user, Long eventId) throws IOException, ActionHandlerException {
        log.info("No Action is defined, hence doing noting. eventId: {}", eventId);
        return false;
    }
}
