package com.swiggy.restaurantone.restaurantEvent.handlers.helpers;

import com.swiggy.commons.Json;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.ds.VendorConfigBulkIterator;
import com.swiggy.restaurantone.service.AmazonS3ClientService;
import com.swiggy.restaurantone.vendorconfig.pojo.request.upload.VendorConfigBulkUploadRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.swiggy.restaurantone.Constant.VendorConfigConstants.SUCCESS;

@Component
@Slf4j
public class VendorConfigDocParser {

    private AmazonS3ClientService amazonS3ClientService;

    @Autowired
    public VendorConfigDocParser(AmazonS3ClientService amazonS3ClientService) {
        this.amazonS3ClientService = amazonS3ClientService;
    }

    public String validateBulkRequest(Object meta) {
        try {
            VendorConfigBulkUploadRequest vendorConfigBulkUploadRequest =
                    Json.deserialize(Json.serialize(meta), VendorConfigBulkUploadRequest.class);
            String s3Url = vendorConfigBulkUploadRequest.getUrl();
            if (StringUtils.isEmpty(s3Url)) {
                return "empty s3Url";
            }
            VendorConfigBulkIterator bulkIterator = getIterator(s3Url);
            if (bulkIterator == null) {
                return "Unable to read file from url";
            }
            if (bulkIterator.isEmpty()) {
                return "File is empty";
            }
            return SUCCESS;
        } catch (Exception ex) {
            log.error("Exception while validating config job. Stacktrace: {}",
                    ExceptionUtils.getStackTrace(ex));
            return ex.getMessage();
        }
    }

    public String getS3Url(Object meta) {
        VendorConfigBulkUploadRequest vendorConfigBulkUploadRequest =
                Json.deserialize(Json.serialize(meta), VendorConfigBulkUploadRequest.class);
        return vendorConfigBulkUploadRequest.getUrl();
    }

    public VendorConfigBulkIterator getIterator(String s3Url) {
        return new VendorConfigBulkIterator(amazonS3ClientService.getFileFromS3(s3Url));
    }
}
