package com.swiggy.restaurantone.restaurantEvent.handlers.helpers.validation;

public interface VendorConfigValidationHelper {

    String validateRowHeaders(String[] rowHeaders);

    String validateRowForErrors(String[] expectedRowHeaders, String[] data);

    Object getValue(String rowHeader, String value);
}
