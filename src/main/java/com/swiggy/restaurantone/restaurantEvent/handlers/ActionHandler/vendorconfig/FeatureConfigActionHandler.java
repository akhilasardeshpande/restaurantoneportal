package com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.vendorconfig;

import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.VendorConfigDocParser;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.email.VendorConfigUpdateEmailHelper;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.instrumentation.VendorConfigInstrumentHelper;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.validation.FeatureConfigValidationHelper;
import com.swiggy.restaurantone.vendorconfig.pojo.request.update.VendorConfigUpdateRequest;
import com.swiggy.restaurantone.vendorconfig.service.VendorConfigService;
import com.swiggy.restaurantone.vendorconfig.service.VendorConfigServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.swiggy.restaurantone.Constant.VendorConfigConstants.FEATURE_VENDOR_CONFIG_TYPE;
import static com.swiggy.restaurantone.Constant.VendorConfigConstants.SUCCESS;

@Component
@Slf4j
public class FeatureConfigActionHandler extends VendorConfigActionHandler {

    private FeatureConfigValidationHelper validationHelper;
    private VendorConfigService vendorConfigService;

    @Autowired
    public FeatureConfigActionHandler(VendorConfigDocParser vendorConfigDocParser,
                                      VendorConfigServiceImpl vendorConfigServiceImpl,
                                      VendorConfigUpdateEmailHelper emailHelper,
                                      VendorConfigInstrumentHelper instrumentHelper,
                                      FeatureConfigValidationHelper validationHelper) {
        super(vendorConfigDocParser, emailHelper, instrumentHelper, validationHelper);
        this.validationHelper = validationHelper;
        this.vendorConfigService = vendorConfigServiceImpl;
    }

    @Override
    public String getType() {
        return FEATURE_VENDOR_CONFIG_TYPE;
    }

    @Override
    public VendorConfigUpdateRequest getVendorConfigServiceUpdateRequest(String[] data, String[] expectedRows) {
        int ridIndex = 0;
        int projectNameIndex = 1;
        int featureNameIndex = 2;
        int valueIndex = 3;
        VendorConfigUpdateRequest vendorConfigServiceUpdateRequest =
                VendorConfigUpdateRequest.builder().build();
        vendorConfigServiceUpdateRequest.setKey(data[featureNameIndex].trim() + ":" + data[ridIndex].trim());
        vendorConfigServiceUpdateRequest.setConfig(data[projectNameIndex].trim());
        Object value = validationHelper.getValue(expectedRows[valueIndex], data[valueIndex]);
        vendorConfigServiceUpdateRequest.setValue(value);
        return vendorConfigServiceUpdateRequest;
    }

    public String uploadConfig(List<VendorConfigUpdateRequest> list) {
        HashMap<String, List<VendorConfigUpdateRequest> > projectMap = new HashMap<>();
        for (VendorConfigUpdateRequest request: list) {
            if (!projectMap.containsKey(request.getConfig())) {
                projectMap.put(request.getConfig(), new ArrayList<>());
            }
            projectMap.get(request.getConfig()).add(request);
        }
        for (Map.Entry<String, List<VendorConfigUpdateRequest>> projectSet: projectMap.entrySet()) {
            String validationMessage = vendorConfigService.updateConfig(
                    this.getClass().getSimpleName(), projectSet.getKey(), projectSet.getValue()).getMessage();
            if (!validationMessage.equalsIgnoreCase(SUCCESS)) {
                return validationMessage;
            }
        }
        return SUCCESS;
    }
}
