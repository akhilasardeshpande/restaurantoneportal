package com.swiggy.restaurantone.restaurantEvent.handlers.eventHandler.compliance;

import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.restaurantEvent.enums.ActionEnum;
import com.swiggy.restaurantone.restaurantEvent.enums.EventTypeEnum;
import com.swiggy.restaurantone.restaurantEvent.factory.ActionHandlerFactory;
import com.swiggy.restaurantone.restaurantEvent.services.ActionHandler;
import com.swiggy.restaurantone.restaurantEvent.services.EventHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class OrderComplianceHandler implements EventHandler {
    private ActionHandlerFactory actionHandlerFactory;

    private AuthorizationHelper authorizationHelper;

    @Autowired
    public OrderComplianceHandler(ActionHandlerFactory actionHandlerFactory, AuthorizationHelper authorizationHelper) {
        this.actionHandlerFactory = actionHandlerFactory;
        this.authorizationHelper = authorizationHelper;
    }

    @Value("${orderCompliancePermission}")
    private Long orderCompliancePermission;

    @Override
    public boolean handler(Object meta, ActionEnum action, User user, Long eventId) {
        if (authorizationHelper.isAuthorizedUser(user, orderCompliancePermission)) {
            ActionHandler actionHandler = actionHandlerFactory.getActionHandler(EventTypeEnum.ORDER_COMPLIANCE, action);
            try {
                return actionHandler.handler(meta, user, eventId);
            } catch (Exception ex) {
                log.info("Exception occurred while executing the action {} ,  message {} and cause",
                        actionHandler.getClass().getName(), ex.getMessage(), ex.getCause());
            }
        } else {
            log.error("User is not authorized to perform the action: {} for non-compliance order", action);
        }
        return false;
    }
}
