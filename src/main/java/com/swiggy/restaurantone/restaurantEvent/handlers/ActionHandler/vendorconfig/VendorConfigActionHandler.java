package com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.vendorconfig;

import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.restaurantEvent.exceptions.ActionHandlerException;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.ds.VendorConfigBulkIterator;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.VendorConfigDocParser;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.ds.VendorConfigErrorReport;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.email.VendorConfigUpdateEmailHelper;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.instrumentation.VendorConfigInstrumentHelper;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.validation.VendorConfigValidationHelper;
import com.swiggy.restaurantone.restaurantEvent.services.ActionHandler;
import com.swiggy.restaurantone.vendorconfig.pojo.request.update.VendorConfigUpdateRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.swiggy.restaurantone.Constant.VendorConfigConstants.*;

@Slf4j
public abstract class VendorConfigActionHandler implements ActionHandler {


    private VendorConfigDocParser docParser;
    private VendorConfigUpdateEmailHelper emailHelper;
    private VendorConfigInstrumentHelper instrumentHelper;
    private VendorConfigValidationHelper validationHelper;

    public VendorConfigActionHandler(VendorConfigDocParser docParser,
                                     VendorConfigUpdateEmailHelper emailHelper,
                                     VendorConfigInstrumentHelper instrumentHelper,
                                     VendorConfigValidationHelper validationHelper) {
        this.docParser = docParser;
        this.emailHelper = emailHelper;
        this.instrumentHelper = instrumentHelper;
        this.validationHelper = validationHelper;
    }

    @Override
    public boolean handler(Object meta, User user, Long eventId) throws IOException, ActionHandlerException {
        try {
            String validationMessage = docParser.validateBulkRequest(meta);
            String s3Url = docParser.getS3Url(meta);
            if (!validationMessage.equalsIgnoreCase(SUCCESS)) {
                return returnFailureAction(user, s3Url, eventId, validationMessage, getType());
            }
            VendorConfigBulkIterator bulkIterator = docParser.getIterator(s3Url);
            String[] rowHeaders = bulkIterator.getRowHeader();
            validationMessage = validationHelper.validateRowHeaders(rowHeaders);
            if (!validationMessage.equalsIgnoreCase(SUCCESS)) {
                return returnFailureAction(user, s3Url, eventId, validationMessage, getType());
            }
            return handleRecords(s3Url, user, eventId, bulkIterator);
        } catch (Exception ex) {
            log.error("Exception while processing prep time config job. Stacktrace: {}",
                    ExceptionUtils.getStackTrace(ex));
            throw new ActionHandlerException(ex.getMessage(), ex);
        }
    }

    public boolean handleRecords(String s3Url, User user, Long eventId, VendorConfigBulkIterator bulkIterator) {
        try {
            boolean handleResult = SUCCESS_RESULT;
            long lineNo = 1L, startIndex = 1L;
            String[] rowHeaders = bulkIterator.getRowHeader();
            List<VendorConfigUpdateRequest> list = new ArrayList<>();
            VendorConfigErrorReport vendorConfigErrorReport = new VendorConfigErrorReport();
            while (!bulkIterator.isEmpty()) {
                String[] data = bulkIterator.getCurrentRow();
                String validateMessage = validationHelper.validateRowForErrors(rowHeaders, data);
                if (!validateMessage.equalsIgnoreCase(SUCCESS)) {
                    handleResult = handleResult && FAILURE_RESULT;
                    vendorConfigErrorReport.addRowError(lineNo, lineNo, validateMessage);
                    continue;
                }
                list.add(getVendorConfigServiceUpdateRequest(data, rowHeaders));
                if (list.size() == BATCH_SIZE) {
                    handleResult = handleResult && executeRequests(eventId,
                            vendorConfigErrorReport, list, startIndex, startIndex + BATCH_SIZE, getType());
                    startIndex = lineNo + 1L;
                }
                lineNo++;
            }
            handleResult = handleResult && executeRequests(eventId, vendorConfigErrorReport,
                    list, startIndex, startIndex + list.size(), getType());
            vendorConfigErrorReport.close();
            handleInstrumentationAndEmail(handleResult == SUCCESS_RESULT, user, s3Url,
                    lineNo, eventId, vendorConfigErrorReport.getErrorFilePath(), getType());
            return handleResult;
        } catch (Exception ex) {
            log.error("Exception while processing prep time config job. Stacktrace: {}",
                    ExceptionUtils.getStackTrace(ex));
            generateErrorReport(user, s3Url, String.valueOf(eventId), ex.getMessage(), getType());
            return FAILURE_RESULT;
        }
    }

    protected boolean returnFailureAction(User user, String s3Url, Long eventId, String message, String vendorConfigType) {
        log.error("Exception while processing prep time config job. Stacktrace: {}", message);
        emailHelper.generateErrorEmailReport(user, s3Url, String.valueOf(eventId), message, vendorConfigType);
        instrumentHelper.instrumentFailedFileConfigUpdate(user.getEmail(), eventId, s3Url);
        return FAILURE_RESULT;
    }

    protected void handleInstrumentationAndEmail(boolean success, User user, String s3Url, Long records, Long eventId, String errorFilePath, String vendorConfigType) {
        if (success) {
            emailHelper.generateSuccessEmailReport(user, s3Url, eventId.toString(), vendorConfigType);
            instrumentHelper.instrumentSuccessConfigUpdate(user.getEmail(), eventId, records, s3Url);
        } else {
            emailHelper.generateErrorEmailReport(user, s3Url, eventId.toString(), FAILURE, errorFilePath, vendorConfigType);
            instrumentHelper.instrumentFailedFileConfigUpdate(user.getEmail(), eventId, records, s3Url);
        }
    }

    public boolean executeRequests(Long eventId, VendorConfigErrorReport writer, List<VendorConfigUpdateRequest> list, long startIndex, long endIndex, String vendorConfigType)
            throws IOException {
        String message = uploadConfig(list);
        instrumentHelper.instrumentConfigUploadUpdateEvent(eventId,
                (message.equals(SUCCESS)?SUCCESS:FAILURE), startIndex, list, message);
        list.clear();
        if (!message.equals(SUCCESS)) {
            writer.addRowError(startIndex, endIndex, message);
            return FAILURE_RESULT;
        }
        return SUCCESS_RESULT;
    }

    protected void generateErrorReport(User user, String s3Url, String eventId, String message, String vendorConfigType) {
        emailHelper.generateErrorEmailReport(user, s3Url, String.valueOf(eventId), message, vendorConfigType);
    }

    public abstract String uploadConfig(List<VendorConfigUpdateRequest> list);

    public abstract String getType();

    public abstract VendorConfigUpdateRequest getVendorConfigServiceUpdateRequest(String[] data, String[] expectedRows);

}
