package com.swiggy.restaurantone.restaurantEvent.handlers.helpers.validation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.swiggy.restaurantone.Constant.VendorConfigConstants.RID;
import static com.swiggy.restaurantone.Constant.VendorConfigConstants.SUCCESS;

@Component
@Slf4j
public class FeatureConfigValidationHelper implements VendorConfigValidationHelper {

    private static final int featureExpectedLength = 4;

    @Override
    public String validateRowHeaders(String[] rowHeaders) {
        if (rowHeaders == null || rowHeaders.length != featureExpectedLength || !rowHeaders[0].equalsIgnoreCase(RID)) {
            return "Csv upload has wrong format";
        }
        return SUCCESS;
    }

    public String validateRowForErrors(String[] expectedRowHeaders, String[] data) {
        if (data.length != featureExpectedLength) {
            return String.format("Unexpected no. of values has %d required %d", data.length, featureExpectedLength);
        }
        return SUCCESS;
    }

    public Object getValue(String rowHeader, String value) {
        if (Boolean.parseBoolean(value)) {
            return true;
        }
        if (parseFalseBoolean(value)) {
            return false;
        }
        try {
            return Integer.valueOf(value);
        } catch (Exception ex) {
            log.debug("Not a integer {}", value);
        }
        try {
            return Double.valueOf(value);
        } catch (Exception ex) {
            log.debug("Not a double {}", value);
        }
        return value;
    }

    public boolean parseFalseBoolean(String s) {
        return ((s != null) && s.equalsIgnoreCase("false"));
    }
}
