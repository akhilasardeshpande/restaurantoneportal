package com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.rcc;

import com.swiggy.commons.Json;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.restaurantEvent.exceptions.ActionHandlerException;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.RestaurantResolverAction;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.RestaurantDetails;
import com.swiggy.restaurantone.restaurantEvent.services.ActionHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class RccRemoveActionHandler implements ActionHandler {

    @Autowired
    private RestaurantResolverAction restaurantResolverAction;

    @Autowired
    private RccCommonActionHandler rccCommonActionHandler;

    @Override
    public boolean handler(Object meta, User user, Long eventId) throws IOException, ActionHandlerException {
        try {
            RestaurantDetails restaurantDetails = Json.deserialize(Json.serialize(meta), RestaurantDetails.class);
            List<Long> restaurantIds = restaurantResolverAction.getRestaurants(restaurantDetails);
            if (CollectionUtils.isNotEmpty(restaurantIds)) {
                String requestedBy = user.email;
                rccCommonActionHandler.sendRequestToRcc(restaurantIds, Boolean.FALSE, requestedBy, eventId);
                return true;
            }
            if (Objects.nonNull(restaurantIds) && restaurantIds.isEmpty()) {
                log.info("resolved restaurants is empty for RCC.  metaInfo: {}", Json.serialize(meta));
                return true;
            }
        } catch (IOException | ActionHandlerException ex) {
            log.info("Exception while occurred to perform RCC Remove action. Exception:{}, Message: {} , cause: {}",
                    ex.getClass().getName(), ex.getMessage(), ex.getCause());
            throw ex;
        }
        return false;
    }
}
