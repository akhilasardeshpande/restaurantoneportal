package com.swiggy.restaurantone.restaurantEvent.handlers.helpers.ds;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.BufferedReader;
import java.io.IOException;

@Slf4j
public class VendorConfigBulkIterator {

    private BufferedReader bufferedReader;
    private boolean empty;
    @Getter
    private String[] rowHeader;
    @Getter
    private String[] currentRow;

    public VendorConfigBulkIterator(BufferedReader bufferedReader) {
        this.bufferedReader = bufferedReader;
        checkEmpty();
    }

    private void checkEmpty() {
        if (bufferedReader == null) {
            empty = true;
            return;
        }
        computeNext();
        rowHeader = currentRow;
        empty = (rowHeader == null);
    }

    public boolean isEmpty() {
        computeNext();
        return empty;
    }

    public void computeNext() {
        try {
            String row = "";
            while (row != null && row.equals(StringUtils.EMPTY)) {
                row = bufferedReader.readLine();
            }
            if (row == null) {
                empty = true;
                currentRow = null;
            } else {
                currentRow = row.split(",");
            }
        } catch (IOException e) {
            log.error("IOException at reading doc.. {}", ExceptionUtils.getStackTrace(e));
            empty = false;
        }
    }


}
