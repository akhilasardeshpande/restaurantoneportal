package com.swiggy.restaurantone.restaurantEvent.handlers.helpers.email;

import com.amazonaws.HttpMethod;
import com.swiggy.backlit.batteries.prometheus.Instrumentation.InstrumentMetrics;
import com.swiggy.restaurantone.communication.notification.NotificationsService;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.service.AmazonS3ClientService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class VendorConfigUpdateEmailHelper {

    private static final String SUCCESS = "success";
    private static final String SUCCESS_EMAIL_SUBJECT = "%s Config Update Success for Event Id %s";
    private static final String ERROR_EMAIL_SUBJECT = "%s Config Update Error for Event Id %s";
    private static final String VENDOR_CONFIG_FAILURE_COMM_EVENT = "prep_time_config_update_event";
    private static final String CONTENT_TYPE_TEXT_CSV = "text/csv";
    private static final String NONE = "NONE";
    private static final String EMPTY_FILE = "";
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

    private NotificationsService notificationsService;
    private AmazonS3ClientService amazonS3ClientService;

    @Autowired
    public VendorConfigUpdateEmailHelper(NotificationsService notificationsService,
                                         AmazonS3ClientService amazonS3ClientService) {
        this.notificationsService = notificationsService;
        this.amazonS3ClientService = amazonS3ClientService;
    }

    public void generateErrorEmailReport(User user, String s3Url, String eventId, String message, String vendorConfigType) {
        generateEmailReport(user, EMPTY_FILE, String.format(vendorConfigType, ERROR_EMAIL_SUBJECT, eventId), message, s3Url, vendorConfigType);
    }

    public void generateErrorEmailReport(User user, String s3Url, String eventId, String message, String fileName, String vendorConfigType) {
        generateEmailReport(user, fileName, String.format(vendorConfigType, ERROR_EMAIL_SUBJECT, eventId), message, s3Url, vendorConfigType);
    }

    public void generateSuccessEmailReport(User user, String s3Url, String eventId, String vendorConfigType) {
        generateEmailReport(user, EMPTY_FILE, String.format(vendorConfigType, SUCCESS_EMAIL_SUBJECT, eventId), SUCCESS, s3Url, vendorConfigType);
    }

    private String uploadAndGetPreSignedUrl(String filePath, String uploadFileName) {
        try {
            if (StringUtils.isEmpty(filePath)) {
                return NONE;
            }
            File file = new File(filePath);
            amazonS3ClientService.Upload(uploadFileName, file, CONTENT_TYPE_TEXT_CSV);
            return amazonS3ClientService.getPreSignedUrl(uploadFileName, null,
                    HttpMethod.GET);
        } catch (Exception ex) {
            log.error("Error writing prep time config processing failure to csv file: {}",
                    filePath);
            return NONE;
        }
    }

    @InstrumentMetrics
    public void generateEmailReport(User user, String fileName, String subject, String message, String s3Url, String vendorConfigType) {
        try {
            String time = sdf.format(new Timestamp(System.currentTimeMillis()));
            String preSignedUrl = uploadAndGetPreSignedUrl(fileName,
                    String.format("%s-config-report-%s.csv", vendorConfigType, time));
            Map<String, Object> ctx = new HashMap<>();
            ctx.put("subject", subject);
            ctx.put("message", message);
            ctx.put("s3Url", s3Url);
            ctx.put("errorUrl", preSignedUrl);
            notificationsService.sendApiEmail(VENDOR_CONFIG_FAILURE_COMM_EVENT, user.getEmail(), ctx, new HashMap<>());
        } catch (Exception e) {
            log.error("Error sending prep time config processing failure report: {}", ExceptionUtils.getStackTrace(e));
        }
    }

}
