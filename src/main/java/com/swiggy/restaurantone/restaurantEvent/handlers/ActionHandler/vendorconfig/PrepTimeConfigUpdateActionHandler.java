package com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.vendorconfig;

import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.VendorConfigDocParser;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.email.VendorConfigUpdateEmailHelper;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.instrumentation.VendorConfigInstrumentHelper;
import com.swiggy.restaurantone.restaurantEvent.handlers.helpers.validation.RestaurantConfigValidationHelper;
import com.swiggy.restaurantone.restaurantEvent.services.ActionHandler;
import com.swiggy.restaurantone.vendorconfig.pojo.request.update.VendorConfigUpdateRequest;
import com.swiggy.restaurantone.vendorconfig.service.VendorConfigService;
import com.swiggy.restaurantone.vendorconfig.service.VendorConfigServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.swiggy.restaurantone.Constant.VendorConfigConstants.PREP_TIME_VENDOR_CONFIG_TYPE;

@Service
@Slf4j
public class PrepTimeConfigUpdateActionHandler extends VendorConfigActionHandler implements ActionHandler {

    private RestaurantConfigValidationHelper validationHelper;
    private VendorConfigService vendorConfigService;

    @Autowired
    public PrepTimeConfigUpdateActionHandler(VendorConfigDocParser vendorConfigDocParser,
                                             VendorConfigServiceImpl vendorConfigServiceImpl,
                                             VendorConfigUpdateEmailHelper emailHelper,
                                             VendorConfigInstrumentHelper instrumentHelper,
                                             RestaurantConfigValidationHelper validationHelper) {
        super(vendorConfigDocParser, emailHelper, instrumentHelper, validationHelper);
        this.validationHelper = validationHelper;
        this.vendorConfigService = vendorConfigServiceImpl;
    }

    @Override
    public String getType() {
        return PREP_TIME_VENDOR_CONFIG_TYPE;
    }

    public VendorConfigUpdateRequest getVendorConfigServiceUpdateRequest(String[] data, String[] expectedRows) {
        int ridIndex = 0;
        VendorConfigUpdateRequest vendorConfigServiceUpdateRequest =
                VendorConfigUpdateRequest.builder().build();
        vendorConfigServiceUpdateRequest.setKey(data[ridIndex]);
        Map<String, Object> hashMap = new HashMap<>();
        for (int valueIndex = 1; valueIndex < expectedRows.length; valueIndex++) {
            String rowData = data[valueIndex].trim();
            if (rowData.isEmpty()) {
                continue;
            }
            hashMap.put(expectedRows[valueIndex],
                    validationHelper.getValue(expectedRows[valueIndex], rowData));
        }
        vendorConfigServiceUpdateRequest.setValue(hashMap);
        return vendorConfigServiceUpdateRequest;
    }

    public String uploadConfig(List<VendorConfigUpdateRequest> list) {
        return vendorConfigService.uploadRestaurantConfig(list).getMessage();
    }

}
