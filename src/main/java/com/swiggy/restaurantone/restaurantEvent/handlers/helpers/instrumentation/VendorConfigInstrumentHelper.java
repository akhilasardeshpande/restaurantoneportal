package com.swiggy.restaurantone.restaurantEvent.handlers.helpers.instrumentation;

import com.swiggy.restaurantone.restaurantEvent.instrumentation.InstrumentService;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.ConfigUploadStatusEvent;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.RestaurantConfigUploadEvent;
import com.swiggy.restaurantone.vendorconfig.pojo.request.update.VendorConfigUpdateRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class VendorConfigInstrumentHelper {

    private static final String FAILED = "FAILED";
    private static final String SUCCESS = "SUCCESS";
    private static final long NO_RECORDS = 0;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private InstrumentService instrumentService;

    @Autowired
    public VendorConfigInstrumentHelper(InstrumentService instrumentService) {
        this.instrumentService = instrumentService;
    }

    public void instrumentFailedFileConfigUpdate(String email, Long eventId, String fileUrl) {
        instrumentFailedFileConfigUpdate(email, eventId, NO_RECORDS, fileUrl);
    }

    public void instrumentFailedFileConfigUpdate(String email, Long eventId, Long records, String fileUrl) {
        instrumentService.doInstrument(
                RestaurantConfigUploadEvent.builder()
                   .id(eventId)
                   .status(FAILED)
                   .email(email)
                   .fileUrl(fileUrl)
                   .records(records)
                   .timestamp(System.currentTimeMillis())
                   .date(sdf.format(new Date()))
                   .build()
        );
    }

    public void instrumentSuccessConfigUpdate(String email, Long eventId, Long records, String fileUrl) {
        instrumentService.doInstrument(
                RestaurantConfigUploadEvent.builder()
                        .id(eventId)
                        .status(SUCCESS)
                        .email(email)
                        .fileUrl(fileUrl)
                        .records(records)
                        .timestamp(System.currentTimeMillis())
                        .date(sdf.format(new Date()))
                        .build()
        );
    }

    public void instrumentConfigUploadUpdateEvent(
            Long eventId, String status, Long startNo, List<VendorConfigUpdateRequest> list, String message) {
        try {
            String date = sdf.format(new Date());
            Long timestamp = System.currentTimeMillis();
            List<ConfigUploadStatusEvent> configUploadStatusEventList = new ArrayList<>();
            list.forEach(vendorConfigServiceUpdateRequest -> {
                Long rid = getRid(vendorConfigServiceUpdateRequest);
                if (vendorConfigServiceUpdateRequest.getValue() instanceof Map) {
                    Map<String, Object> valueMap =
                            (Map<String, Object>) vendorConfigServiceUpdateRequest.getValue();
                    Long recordNo = startNo;
                    for (String key: valueMap.keySet()) {
                        configUploadStatusEventList.add(
                                ConfigUploadStatusEvent.builder()
                                        .restaurantId(rid)
                                        .id(eventId)
                                        .key(key)
                                        .timestamp(timestamp)
                                        .date(date)
                                        .recordNo(recordNo)
                                        .status(status)
                                        .message(message)
                                        .value(valueMap.get(key).toString())
                                        .build()
                        );
                        recordNo++;
                    }
                }
            });
            instrumentService.doInstrument(configUploadStatusEventList);
        } catch (Exception ex) {
            log.error("Error in instrumentConfigUploadUpdateEventFailure {}", ExceptionUtils.getStackTrace(ex));
        }
    }

    private Long getRid(VendorConfigUpdateRequest vendorConfigServiceUpdateRequest) {
        try {
            return Long.valueOf(vendorConfigServiceUpdateRequest.getKey());
        } catch (Exception ex) {
            log.error("Rid conversion error {}", ExceptionUtils.getStackTrace(ex));
            return 0L;
        }
    }

}
