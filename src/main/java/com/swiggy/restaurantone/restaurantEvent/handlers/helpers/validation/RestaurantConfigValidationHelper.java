package com.swiggy.restaurantone.restaurantEvent.handlers.helpers.validation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

import static com.swiggy.restaurantone.Constant.VendorConfigConstants.RID;

@Component
@Slf4j
public class RestaurantConfigValidationHelper implements VendorConfigValidationHelper {

    private static final String SUCCESS = "success";

    public enum DataType {
        STRING,
        INT;
    }

    private static final Map<String, DataType> regexMap;

    static {
        regexMap = new HashMap<>();
        regexMap.put("[0-9][0-9]:[0-9][0-9]-[0-9][0-9]:[0-9][0-9]", DataType.INT);
        regexMap.put(".*", DataType.STRING);
    }

    @Override
    public String validateRowHeaders(String[] rowHeaders) {
        if (rowHeaders == null || rowHeaders.length < 1 || !rowHeaders[0].equalsIgnoreCase(RID)) {
            return "Csv upload has wrong format, rid must come first";
        }
        return SUCCESS;
    }

    public String validateRowForErrors(String[] expectedRowHeaders, String[] data) {
        if (Objects.isNull(data)) {
            return "Empty Record sent for validation";
        }
        if (data.length != expectedRowHeaders.length) {
            return String.format("Unexpected no. of values has %d required %d", data.length, expectedRowHeaders.length);
        }
        for (int valueNo = 1; valueNo < expectedRowHeaders.length; valueNo++) {
            String rowData = data[valueNo].trim();
            if (rowData.isEmpty()) {
                continue;
            }
            String rowHeader = expectedRowHeaders[valueNo];
            String result = SUCCESS;
            for (String regex: regexMap.keySet()) {
                if(Pattern.matches(regex, rowHeader)) {
                    switch (regexMap.get(regex)) {
                        case INT:
                            result = checkNumber(rowData, valueNo);
                            break;
                        case STRING:
                        default:
                            result = checkString(rowData, valueNo);
                            break;
                    }
                }
            }
            if (!result.equals(SUCCESS)) {
                return result;
            }
        }
        return SUCCESS;
    }

    public Object getValue(String rowHeader, String value) {
        for (String regex: regexMap.keySet()) {
            if(Pattern.matches(regex, rowHeader)) {
                switch (regexMap.get(regex)) {
                    case INT:
                        return Integer.parseInt(value);
                    case STRING:
                        return value;
                }
            }
        }
        return value;
    }

    private String checkNumber(String rowData,int valueNo) {
        try {
            Integer.parseInt(rowData);
        } catch (Exception ex) {
            return String.format("Invalid value at valueNo %d", valueNo);
        }
        return SUCCESS;
    }

    private String checkString(String rowData, int valueNo) {
        return SUCCESS;
    }


}
