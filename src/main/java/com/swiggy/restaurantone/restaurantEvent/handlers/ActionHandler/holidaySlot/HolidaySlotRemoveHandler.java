package com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.holidaySlot;

import com.swiggy.backlit.batteries.prometheus.Instrumentation.InstrumentMetrics;
import com.swiggy.commons.Json;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.RestaurantHolidaySlotHelper;
import com.swiggy.restaurantone.pojos.requests.rms.RestaurantToggleStatusRMS;
import com.swiggy.restaurantone.response.CommonsResponse;
import com.swiggy.restaurantone.restaurantEvent.enums.EventStatusEnum;
import com.swiggy.restaurantone.restaurantEvent.exceptions.ActionHandlerException;
import com.swiggy.restaurantone.restaurantEvent.handlers.ActionHandler.RestaurantResolverAction;
import com.swiggy.restaurantone.restaurantEvent.instrumentation.InstrumentServiceImpl;
import com.swiggy.restaurantone.restaurantEvent.mapper.RemoveHolidayActionMapper;
import com.swiggy.restaurantone.restaurantEvent.pojos.restaurant.RestaurantDetails;
import com.swiggy.restaurantone.restaurantEvent.services.ActionHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class HolidaySlotRemoveHandler implements ActionHandler {

    @Autowired
    private RestaurantResolverAction restaurantResolverAction;

    @Autowired
    private RestaurantHolidaySlotHelper restaurantHolidaySlotHelper;

    @Autowired
    private InstrumentServiceImpl instrumentService;

    @Override
    public boolean handler(Object meta, User user, Long eventId) throws IOException, ActionHandlerException {
        try {
            RestaurantDetails restaurantDetails = Json.deserialize(Json.serialize(meta), RestaurantDetails.class);
            List<Long> restaurantIds = restaurantResolverAction.getRestaurants(restaurantDetails);
            if (CollectionUtils.isNotEmpty(restaurantIds)) {
                String requestedBy = user.email;
                removeHolidaySlot(restaurantIds, requestedBy, eventId);
                return true;
            }
            if (Objects.nonNull(restaurantIds) && restaurantIds.isEmpty()) {
                log.info("resolved restaurants is empty for Holiday slot.  metaInfo: {}", Json.serialize(meta));
                return true;
            }
        } catch (IOException | ActionHandlerException ex) {
            log.info("Exception while occurred to perform Holiday Remove action. Message: {} , cause: {}", ex.getMessage(), ex.getCause());
            throw ex;
        }
        return false;
    }

    @InstrumentMetrics
    private void removeHolidaySlot(List<Long> restaurantIds, String updatedBy, Long evenId) throws IOException {

        for (Long restaurantId : restaurantIds) {
            try {
                CommonsResponse<Boolean> response = restaurantHolidaySlotHelper.openRestaurantFromSelfServe(restaurantId, updatedBy);
                if (Objects.nonNull(response) && Boolean.TRUE.equals(response.getData())) {
                    RestaurantToggleStatusRMS restaurantToggleStatusRMS = RemoveHolidayActionMapper.getRestaurantToggleStatusRMS(restaurantId);
                    restaurantHolidaySlotHelper.updateRestaurantStatusInRMSCache(restaurantToggleStatusRMS);
                    instrumentService.doInstrument(evenId, restaurantId.toString(), EventStatusEnum.SUCCESS.name());
                    log.info("Successfully removed holiday slot for restaurant Id: {}", restaurantId);
                } else {
                    instrumentService.doInstrument(evenId, restaurantId.toString(), EventStatusEnum.FAILURE.name());
                    log.error("Error while removing holiday slot for restaurantId:{},  Response: {}", restaurantId, response);
                }
            } catch (IOException ex) {
                instrumentService.doInstrument(evenId, restaurantId.toString(), EventStatusEnum.FAILURE.name());
                log.error("Exception occurred while removing holiday Slot. restaurantId:{},  message. {}, cause: {}"
                        , restaurantId, ex.getMessage(), ex.getCause());
                throw ex;
            }
        }
    }
}
