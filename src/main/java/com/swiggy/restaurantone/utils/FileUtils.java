package com.swiggy.restaurantone.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
public class FileUtils {

    public static File convertMultipartToFile(MultipartFile multipartFile, String dir) throws IOException {

        String fileName = new StringBuilder(dir).append(multipartFile.getOriginalFilename()).toString();
        File file = new File(fileName);
        multipartFile.transferTo(file);
        return file;
    }

    public static List<List<String>> parseCSVFile(BufferedReader reader) {
        List<List<String>> contents = new ArrayList<>();
        try {
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
            for (CSVRecord csvRecord : csvParser) {
                List<String> record = getListFromIterator(csvRecord.iterator());
                contents.add(record);
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause(), e.getStackTrace());
        }
        return contents;
    }

    private static <T> List<T> getListFromIterator(Iterator<T> iterator) {
        Iterable<T> iterable = () -> iterator;
        return StreamSupport
                .stream(iterable.spliterator(), false)
                .collect(Collectors.toList());
    }
}

