package com.swiggy.restaurantone.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by rajnish.chandra on 20/02/19
 */
public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {
    
    private static DateTimeFormatter FORMATTER = DateTimeFormatter.ISO_DATE_TIME;
    
    @Override
    public LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        return LocalDateTime.parse(p.getValueAsString(), FORMATTER);
    }

    public static LocalDateTime convertStringToLocalDateTime(String time){
       return LocalDateTime.parse(time, FORMATTER);
    }
}