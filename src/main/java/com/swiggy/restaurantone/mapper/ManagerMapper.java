package com.swiggy.restaurantone.mapper;

import com.swiggy.restaurantone.data.businessEntity.Manager;
import com.swiggy.restaurantone.data.coreEntity.UserEntity;
import com.swiggy.restaurantone.dataAccess.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by rajnishc on 5/5/17.
 */
@Component
public class ManagerMapper {
    @Autowired
    private UserDao userDao;

    public Manager convertToBusinessEntity(Long managerId) {
        UserEntity manager = userDao.findOne(managerId);
        return manager == null
                ? null
                : Manager.builder().id(manager.getId()).name(manager.getName()).build();
    }

    public Manager convertToBusinessEntity(UserEntity userEntity) {
        return userEntity == null
                ? null
                : Manager.builder().id(userEntity.getId()).name(userEntity.getName()).build();
    }

    public List<Manager> convertToBusinessEntity(List<UserEntity> userEntity) {
        return userEntity.stream().map(this::convertToBusinessEntity).collect(Collectors.toList());
    }
}
