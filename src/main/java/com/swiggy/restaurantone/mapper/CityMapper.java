package com.swiggy.restaurantone.mapper;

import com.swiggy.restaurantone.data.businessEntity.City;
import com.swiggy.restaurantone.data.coreEntity.CityEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by rajnishc on 5/6/17.
 */
@Component
public class CityMapper {
    public City entityToBusiness(CityEntity cityEntity) {
        return cityEntity == null
                ? null
                : City.builder()
                .id(cityEntity.getId())
                .name(cityEntity.getName())
                .build();

    }

    public List<City> entityToBusiness(List<CityEntity> cityEntities) {
        return cityEntities.stream().map(this::entityToBusiness).collect(Collectors.toList());
    }
}
