package com.swiggy.restaurantone.mapper;

import com.swiggy.restaurantone.data.businessEntity.RMSUser;
import com.swiggy.restaurantone.data.coreEntity.RMSUserEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

/**
 * Created by rajnish.chandra on 21/06/18
 */
@Component
public class RMSUserMapper {
    public RMSUserEntity businessToEntity(RMSUser rmsUser) {
        if (ObjectUtils.isEmpty(rmsUser)) {
            return null;
        }
        return RMSUserEntity.builder()
                .rmsId(rmsUser.getId())
                .mobileNo(rmsUser.getMobileNo())
                .name(rmsUser.getName())
                .createdAt(rmsUser.getCreatedAt())
                .createdBy(rmsUser.getCreatedBy())
                .updatedAt(rmsUser.getUpdatedAt())
                .updatedBy(rmsUser.getUpdatedBy())
                .activationDate(rmsUser.getActivationDate())
                .deactivationDate(rmsUser.getDeactivationDate())
                .registrationComplete(rmsUser.isRegistrationComplete())
                .isActive(rmsUser.isActive())
                .isEditable(rmsUser.isEditable())
                .isPrimary(rmsUser.isPrimary())
                .roleId(rmsUser.getRoleId())
                .build();
    }
}
