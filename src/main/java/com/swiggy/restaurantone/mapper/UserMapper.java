package com.swiggy.restaurantone.mapper;

import com.swiggy.rest.service.AbstractMappingConverter;
import com.swiggy.restaurantone.data.businessEntity.Permission;
import com.swiggy.restaurantone.data.businessEntity.Role;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.data.businessEntity.UserRelationship;
import com.swiggy.restaurantone.data.coreEntity.UserEntity;
import com.swiggy.restaurantone.helper.PermissionHelper;
import com.swiggy.restaurantone.service.CityService;
import com.swiggy.restaurantone.service.RoleService;
import com.swiggy.restaurantone.service.TeamService;
import com.swiggy.restaurantone.service.UserRelationshipService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by balamuneeswaran.s on 1/3/17.
 */
@Component
public class UserMapper extends AbstractMappingConverter<User, UserEntity> {

    private final HttpServletRequest request;


    private final PermissionMapper permissionMapper;

    @Autowired
    private PermissionHelper permissionHelper;

    @Autowired
    private UserRelationshipService userRelationshipService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private CityService cityService;

    @Autowired
    private ManagerMapper managerMapper;

    @Autowired
    private RoleService roleService;

    @Autowired
    public UserMapper(HttpServletRequest request, PermissionMapper permissionMapper) {
        this.request = request;
        this.permissionMapper = permissionMapper;
    }

    public List<UserEntity> businessToEntity(List<User> users) {

        List<UserEntity> userEntities = new ArrayList<>();
        for (User user : users) {
            userEntities.add(this.businessToEntity(user));
        }
        return userEntities;
    }

    @Override
    public Function<User, UserEntity> businessToEntity() {
        return user -> {
            return businessToEntity(user);
        };
    }

    @Override
    public Function<UserEntity, User> entityToBusinessObject() {
        return userEntity -> {
            return entityToBusinessObject(userEntity);
        };
    }

    public UserEntity businessToEntity(User user) {
        if(user == null)
            return null;
        UserEntity userEntity = UserEntity.builder().build();
        try {
            userEntity.setId(user.id);
            userEntity.name = user.name;
            userEntity.contactNumber = user.contactNumber;
            userEntity.email = user.email;
            userEntity.properties = user.properties;
            userEntity.permissions = permissionHelper.getPermissions(user.permissions);
            userEntity.role = roleService.findRoleName(user.role.getName()).getId();
            userEntity.dob=user.dob;
            userEntity.doj=user.doj;
            userEntity.cityId = user.getCity() != null ? user.getCity().getId() : null;
            userEntity.teamId = user.getTeam() != null ? user.getTeam().getId() : null;
            if(StringUtils.isNotEmpty(user.getUpdatedBy())){
                userEntity.setUpdatedBy(user.getUpdatedBy());
                userEntity.setCreatedBy(user.getUpdatedBy());
            }else{
                userEntity.setUpdatedBy(request.getHeader("userId"));
                userEntity.setCreatedBy(request.getHeader("userId"));
            }
        } catch (Exception e) {
            userEntity = null;
        }

        return userEntity;
    }

    public List<User> entityToBusinessObject(List<UserEntity> entities) {

        List<User> users = new ArrayList<>();
        for (UserEntity entity : entities
                ) {
            users.add(this.entityToBusinessObject(entity));
        }
        return users;
    }

    public User entityToBusinessObject(UserEntity entity) {
        if(entity == null)
            return null;
        User user = new User();
        user.name = entity.name;
        user.contactNumber = entity.contactNumber;
        user.email = entity.email;
        user.id = entity.getId();
        Role role = roleService.findRoleById(entity.role);
        user.role = role;
        List<Permission> userPermissions = permissionMapper.entityToBusinessObject(entity.getPermissions());
        List<Permission> rolePermissions = role.getPermissions();
        user.permissions = Stream.concat(userPermissions.stream(), rolePermissions.stream())
                .distinct()
                .collect(Collectors.toList());
        user.properties = entity.properties;
        List<UserRelationship> list = userRelationshipService.GetImmediateParentUsersByUserId(entity.getId());
        Long managerId = !list.isEmpty() ? list.get(0).userId : null;
        if(managerId != null) {
            user.manager = managerMapper.convertToBusinessEntity(managerId);
        } else {
            user.manager = null;
        }
        user.team = entity.teamId != null ? teamService.find(entity.teamId) : null;
        user.dob=entity.dob;
        user.doj=entity.doj;
        user.city = entity.cityId != null ? cityService.findCity(entity.cityId) : null;
        return user;
    }

    public UserEntity businessRequestToEntity(User user) {
        UserEntity userEntity = UserEntity.builder().build();
        try {
            userEntity.setId(user.id);
            userEntity.name = user.name;
            userEntity.contactNumber = user.contactNumber;
            userEntity.email = user.email;
            userEntity.properties = user.properties;
            userEntity.role = roleService.findRoleName(user.role.getName()).getId();
            userEntity.dob=user.dob;
            userEntity.doj=user.doj;
            userEntity.cityId = user.getCity() != null ? user.getCity().getId() : null;
            userEntity.teamId = user.getTeam() != null ? user.getTeam().getId() : null;
            if(StringUtils.isNotEmpty(user.getUpdatedBy())){
                userEntity.setUpdatedBy(user.getUpdatedBy());
            }else{
                userEntity.setUpdatedBy(request.getHeader("userId"));
            }
        } catch (Exception e) {
            userEntity = null;
        }

        return userEntity;
    }
}
