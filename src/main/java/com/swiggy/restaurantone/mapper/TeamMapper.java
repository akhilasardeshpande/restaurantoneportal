package com.swiggy.restaurantone.mapper;

import com.swiggy.restaurantone.data.businessEntity.Team;
import com.swiggy.restaurantone.data.coreEntity.TeamEntity;
import org.springframework.stereotype.Component;

/**
 * Created by rajnishc on 5/5/17.
 */
@Component
public class TeamMapper  {

    public Team entityToBusiness(TeamEntity teamEntity) {
        return teamEntity == null
                ? null
                : Team.builder()
                .id(teamEntity.getId())
                .name(teamEntity.getName())
                .build();

    }
}
