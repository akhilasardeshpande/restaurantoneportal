package com.swiggy.restaurantone.mapper;

import com.swiggy.restaurantone.data.businessEntity.UserRelationship;
import com.swiggy.restaurantone.data.coreEntity.UserRelationshipEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by balamuneeswaran.s on 1/4/17.
 */
@Component
public class UserRelationshipMapper {

    @Autowired
    public HttpServletRequest request;

    public List<UserRelationshipEntity> businessToEntity(List<UserRelationship> users) {

        List<UserRelationshipEntity> userRelationshipEntities = new ArrayList<>();
        for (UserRelationship user : users
                ) {
            userRelationshipEntities.add(this.businessToEntity(user));
        }
        return userRelationshipEntities;
    }

    public UserRelationshipEntity businessToEntity(UserRelationship user) {
        String updatedBy;
        if(StringUtils.isNotEmpty(user.getUpdatedBy())){
            updatedBy = user.getUpdatedBy();
        }else{
            updatedBy = request.getHeader("userId");
        }
        return UserRelationshipEntity.builder()
                .userId(user.userId)
                .childUserId(user.childUserId)
                .updatedBy(updatedBy)
                .updatedAt(new Date())
                .build();

    }

    public UserRelationship entityToBusinessObject(UserRelationshipEntity userRelationshipEntity) {
        UserRelationship userRelationship = new UserRelationship();
        userRelationship.userId = userRelationshipEntity.userId;
        userRelationship.childUserId = userRelationshipEntity.childUserId;
        userRelationship.updatedBy = userRelationshipEntity.getUpdatedBy();
        return userRelationship;
    }

    public List<UserRelationship> entityToBusinessObject(List<UserRelationshipEntity> users) {

        List<UserRelationship> userRelationships = new ArrayList<>();
        for (UserRelationshipEntity user : users
                ) {
            userRelationships.add(this.entityToBusinessObject(user));
        }
        return userRelationships;
    }
}
