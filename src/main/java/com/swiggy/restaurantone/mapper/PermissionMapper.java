package com.swiggy.restaurantone.mapper;

import com.swiggy.restaurantone.data.businessEntity.Permission;
import com.swiggy.restaurantone.data.coreEntity.PermissionEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by shahbaz.khalid on 3/10/17.
 */
@Component
public class PermissionMapper {

    public List<Permission> entityToBusinessObject(List<PermissionEntity> permissions) {
        return Optional.ofNullable(permissions).map(List::stream).orElseGet(Stream::empty)
                .map(p -> new Permission(p.getId(), p.getPermission()))
                .collect(Collectors.toList());
    }
}
