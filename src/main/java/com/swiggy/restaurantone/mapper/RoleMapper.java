package com.swiggy.restaurantone.mapper;

import com.swiggy.restaurantone.data.businessEntity.Role;
import com.swiggy.restaurantone.data.coreEntity.RoleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by rajnishc on 5/12/17.
 */
@Component
public class RoleMapper {

    @Autowired
    private PermissionMapper permissionMapper;

    public Role entityToBusiness(RoleEntity roleEntity) {
        return roleEntity == null
                ? null
                : Role.builder()
                .id(roleEntity.getId())
                .name(roleEntity.getRole())
                .level(roleEntity.level)
                .permissions(permissionMapper.entityToBusinessObject(roleEntity.getPermissions()))
                .build();

    }

    public RoleEntity businessToEntity(Role role) {
        return RoleEntity
                .builder()
                .id(role.getId())
                .role(role.getName())
                .level(role.level)
                .build();
    }

    public RoleEntity businessRequestToEntity(Role role) {
        return RoleEntity
                .builder()
                .role(role.getName())
                .level(role.level)
                .build();
    }
}
