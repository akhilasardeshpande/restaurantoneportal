package com.swiggy.restaurantone.mapper;

import com.swiggy.rest.service.AbstractMappingConverter;
import com.swiggy.restaurantone.data.businessEntity.RestaurantUserMap;
import com.swiggy.restaurantone.data.coreEntity.RestaurantUserMapEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

/**
 * Created by balamuneeswaran.s on 1/4/17.
 */
@Component
public class UserRestaurantMapMapper extends AbstractMappingConverter<RestaurantUserMap, RestaurantUserMapEntity> {

    private final HttpServletRequest request;

    @Autowired
    public UserRestaurantMapMapper(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public Function<RestaurantUserMap, RestaurantUserMapEntity> businessToEntity() {
        return restaurantUserMap -> {
            return businessToEntity(restaurantUserMap);
        };
    }

    @Override
    public Function<RestaurantUserMapEntity, RestaurantUserMap> entityToBusinessObject() {
        return entity -> {
            return entityToBusinessObject(entity);
        };
    }

    public List<RestaurantUserMapEntity> businessToEntity(List<RestaurantUserMap> restaurantUserMaps) {

        List<RestaurantUserMapEntity> userEntities = new ArrayList<RestaurantUserMapEntity>();
        for (RestaurantUserMap map : restaurantUserMaps
                ) {
            userEntities.add(this.businessToEntity(map));
        }
        return userEntities;
    }

    public RestaurantUserMapEntity businessToEntity(RestaurantUserMap restaurantUserMap) {
        RestaurantUserMapEntity userRestaurantMapEntity = new RestaurantUserMapEntity();
        userRestaurantMapEntity.setId(restaurantUserMap.id);
        userRestaurantMapEntity.userId = restaurantUserMap.userId;
        userRestaurantMapEntity.restaurantId = restaurantUserMap.restaurantId;
        if(StringUtils.isNotEmpty(restaurantUserMap.getUpdatedBy())){
            userRestaurantMapEntity.setUpdatedBy(restaurantUserMap.getUpdatedBy());
        }else{
            userRestaurantMapEntity.setUpdatedBy(request.getHeader("userId"));
        }
        userRestaurantMapEntity.setUpdatedAt(new Date());
        return userRestaurantMapEntity;
    }

    public List<RestaurantUserMap> entityToBusinessObject(List<RestaurantUserMapEntity> userRestaurantMapEntities) {

        List<RestaurantUserMap> restaurantUserMaps = new ArrayList<RestaurantUserMap>();
        for (RestaurantUserMapEntity map : userRestaurantMapEntities
                ) {
            restaurantUserMaps.add(this.entityToBusinessObject(map));
        }
        return restaurantUserMaps;
    }

    private RestaurantUserMap entityToBusinessObject(RestaurantUserMapEntity userRestaurantMapEntity) {
        RestaurantUserMap restaurantUserMap = new RestaurantUserMap();
        restaurantUserMap.id = userRestaurantMapEntity.getId();
        restaurantUserMap.userId = userRestaurantMapEntity.userId;
        restaurantUserMap.restaurantId = userRestaurantMapEntity.restaurantId;
        return restaurantUserMap;
    }
}
