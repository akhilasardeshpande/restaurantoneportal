package com.swiggy.restaurantone.pojos.ItemTrimming;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Builder
@ToString
public class ItemData implements Serializable {
    private static final long serialVersionUID = -6567144551841636336L;

    @NotNull
    @NonNull
    private String restaurantId;

    @NotNull
    @NonNull
    private String itemId;

    @NotNull
    @NonNull
    private String itemName;

}
