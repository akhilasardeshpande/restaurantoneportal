package com.swiggy.restaurantone.pojos.igccAttribution;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UploadResponse<T> {
    @JsonProperty(value = "statusMessage")
    private String message;

    @JsonProperty(value = "statusCode")
    private int statusCode;

    @JsonProperty(value = "data")
    T data;
}