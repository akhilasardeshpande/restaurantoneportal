package com.swiggy.restaurantone.pojos.commEngine;

import lombok.*;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by rajnish.chandra on 2019-05-24
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UserInfo implements Serializable {
    private User user;
    private Map<String, Object> ctx;
    private Map<String, Object> metaInfo;
}
