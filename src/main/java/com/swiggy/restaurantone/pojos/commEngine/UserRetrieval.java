package com.swiggy.restaurantone.pojos.commEngine;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserRetrieval {
    private String type;

    private String idType;

    private String resolve;

    private List<?> ids;

    private String key;
}
