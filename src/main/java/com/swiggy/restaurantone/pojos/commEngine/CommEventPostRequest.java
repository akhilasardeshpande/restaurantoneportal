package com.swiggy.restaurantone.pojos.commEngine;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public class CommEventPostRequest {

    @JsonProperty(value = "request_id")
    private String requestId;

    @JsonProperty(value = "headers")
    private CommEventHeader commEventHeader;

    @JsonProperty(value = "message_context")
    private Map<String, Object> commEventMessage;

    @JsonProperty(value = "message_meta")
    private Map<String, Object> commEventMeta;

    @JsonProperty(value = "target_context")
    private CommEventTarget commEventTarget;

}
