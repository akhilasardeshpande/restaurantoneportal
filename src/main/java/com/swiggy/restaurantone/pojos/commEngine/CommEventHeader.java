package com.swiggy.restaurantone.pojos.commEngine;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public class CommEventHeader {

    @JsonProperty(value = "component")
    private String component;

    @JsonProperty(value = "event_name")
    private String eventName;
}
