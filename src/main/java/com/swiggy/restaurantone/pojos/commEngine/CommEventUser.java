package com.swiggy.restaurantone.pojos.commEngine;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public class CommEventUser {

    @JsonProperty(value = "type")
    private String type;

    @JsonProperty(value = "email")
    private CommEventEmail email;
}
