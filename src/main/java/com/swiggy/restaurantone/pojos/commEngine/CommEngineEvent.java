package com.swiggy.restaurantone.pojos.commEngine;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@Builder
public class CommEngineEvent {
    private String identificationId;
    
    private String type;
    
    private Map<String, Object> ctx;
    
    private Object metaInformation;
    
    private String component;
    
    private String name;
    
    private UserRetrieval userRetrieval;
    
    private List<UserInfo> users;
    
}
