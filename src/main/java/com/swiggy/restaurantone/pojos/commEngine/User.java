package com.swiggy.restaurantone.pojos.commEngine;

import lombok.*;

/**
 * Created by rajnish.chandra on 2019-05-24
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class User {
    Long userId;
    String emailId;
    String mobileNumber;
    String type = "GENERAL";
}