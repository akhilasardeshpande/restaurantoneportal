package com.swiggy.restaurantone.pojos.requests.rms;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.swiggy.restaurantone.utils.LocalDateTimeDeserializer;
import com.swiggy.restaurantone.utils.LocalDateTimeSerializer;
import lombok.*;

import java.time.LocalDateTime;

/**
 * Created by rajnish.chandra on 2019-05-15
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RestaurantToggleStatusRMS {
    
    @JsonProperty("isOpen")
    private boolean isOpen;
    
    private Long restaurantId;
    
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime fromTime;
    
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime toTime;
}
