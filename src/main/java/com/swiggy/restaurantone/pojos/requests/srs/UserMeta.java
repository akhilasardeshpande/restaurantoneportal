package com.swiggy.restaurantone.pojos.requests.srs;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rajnish.chandra on 20/02/19
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
@Setter
public class UserMeta {
    @JsonProperty("source")
    private String source;
    
    @JsonProperty("meta")
    private Map<String,Object> meta = new HashMap<>();
    
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        String metaString = null;
        try {
            metaString =  mapper.writeValueAsString(meta);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "{ \"source\" : \"" + source  + "\" , \"meta\" : " + metaString;
    }
    
}
