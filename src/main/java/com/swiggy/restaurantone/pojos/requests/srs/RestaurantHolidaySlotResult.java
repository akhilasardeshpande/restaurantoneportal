package com.swiggy.restaurantone.pojos.requests.srs;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.swiggy.restaurantone.utils.LocalDateTimeDeserializer;
import com.swiggy.restaurantone.utils.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Created by rajnish.chandra on 20/02/19
 */
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class RestaurantHolidaySlotResult {
    @JsonProperty("id")
    private Long id;
    
    @JsonProperty("restaurant_id")
    private Long restaurantId;
    
    @JsonProperty("from_time")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime fromTime;
    
    @JsonProperty("to_time")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime toTime;
    
    @JsonProperty("updated_by")
    UserMeta updatedBy;
}
