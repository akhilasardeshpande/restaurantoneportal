package com.swiggy.restaurantone.pojos.requests.srs;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.swiggy.restaurantone.utils.LocalDateTimeDeserializer;
import com.swiggy.restaurantone.utils.LocalDateTimeSerializer;
import lombok.*;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class RestaurantHolidaySlotRequest {
    
    @JsonProperty("restaurant_id")
    private Long restaurantId;
    
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonProperty("from_time")
    private LocalDateTime fromTime;
    
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonProperty("to_time")
    private LocalDateTime toTime;

    @JsonProperty("disposition_id")
    private Long dispositionId;
}
