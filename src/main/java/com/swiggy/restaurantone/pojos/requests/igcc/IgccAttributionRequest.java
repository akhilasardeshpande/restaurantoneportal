package com.swiggy.restaurantone.pojos.requests.igcc;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import java.io.Serializable;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IgccAttributionRequest implements Serializable {

    private final static long serialVersionUID = -1126148065191389910L;

    @JsonProperty("file")
    private String file;

    @JsonProperty("bucket")
    private String bucket;

}
