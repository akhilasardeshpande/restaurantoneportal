package com.swiggy.restaurantone.pojos.requests.cms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ItemTrimmingRequest implements Serializable {
    private final static long serialVersionUID = -1126148065191389910L;

    @JsonProperty("data")
    @Valid
    private List<ItemTrimmingData> data = null;

    @JsonProperty("user_meta")
    @Valid
    private UserMeta userMeta;

    @Data
    @Builder
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ItemTrimmingData implements Serializable {
        private final static long serialVersionUID = 2405935277562085373L;

        @NotNull
        @NonNull
        @JsonProperty("item_id")
        public String itemId;

        @NotNull
        @NonNull
        @JsonProperty("from_time")
        public String fromTime;

        @NotNull
        @NonNull
        @JsonProperty("to_time")
        public String toTime;
    }

    @Data
    @Builder
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class UserMeta implements Serializable {
        private final static long serialVersionUID = -2453773735320508296L;
        @JsonProperty("source")
        public String source;

        @JsonProperty("meta")
        @Valid
        public Meta meta;

        @Data
        @Builder
        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class Meta implements Serializable {
            private final static long serialVersionUID = 332171587881104737L;

            @JsonProperty("userid")
            public String userid;

            @JsonProperty("reason")
            public String reason;
        }

    }

}
