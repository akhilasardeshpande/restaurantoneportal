package com.swiggy.restaurantone.pojos.requests.srs;

import lombok.*;

import java.util.List;

/**
 * Created by rajnish.chandra on 22/02/19
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PartnerTypeSearchRequest {
    List<Integer> partnerTypes;
}
