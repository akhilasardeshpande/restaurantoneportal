
package com.swiggy.restaurantone.pojos.branchIO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExtraData implements Serializable {
    private final static long serialVersionUID = -4687188565561498147L;

    @JsonProperty("restaurantID")
    public Long restaurantID;
}
