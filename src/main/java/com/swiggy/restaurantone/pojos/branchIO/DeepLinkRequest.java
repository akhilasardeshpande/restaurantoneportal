
package com.swiggy.restaurantone.pojos.branchIO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import javax.validation.Valid;
import java.io.Serializable;

@lombok.Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeepLinkRequest implements Serializable {
    private final static long serialVersionUID = -1048875215294404196L;

    @JsonProperty("branch_key")
    public String branchKey;

    @JsonProperty("channel")
    public String channel;

    @JsonProperty("feature")
    public String feature;

    @JsonProperty("campaign")
    public String campaign;

    @JsonProperty("data")
    @Valid
    public Data data;
}
