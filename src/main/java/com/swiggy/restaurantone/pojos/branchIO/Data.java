
package com.swiggy.restaurantone.pojos.branchIO;

import java.io.Serializable;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Builder;

@lombok.Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Data implements Serializable {
    private final static long serialVersionUID = -4165947605813672457L;

    @JsonProperty("$og_title")
    public String orgTitle;

    @JsonProperty("$og_description")
    public String orgDescription;

    @JsonProperty("$og_image_url")
    public String orgImageUrl;

    @JsonProperty("$desktop_url")
    public String desktopUrl;

    @JsonProperty("$android_url")
    public String androidUrl;

    @JsonProperty("$ios_url")
    public String iosUrl;

    @JsonProperty("screenName")
    public String screenName;

    @JsonProperty("extraData")
    @Valid
    public ExtraData extraData;
}
