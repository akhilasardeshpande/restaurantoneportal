package com.swiggy.restaurantone.pojos.communication_service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UploadUrlResponse {

    @JsonProperty("cdn")
    String cdnUrl;

    @JsonProperty("signed_url")
    String preSignedUrl;
}

