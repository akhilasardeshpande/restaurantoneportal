package com.swiggy.restaurantone.apiClients;

import com.swiggy.restaurantone.helper.BasicAuthInterceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.util.concurrent.TimeUnit;

public class ApiClientBuilder {
    private HttpLoggingInterceptor loggingInterceptor;
    private BasicAuthInterceptor basicAuthInterceptor;
    private OkHttpClient.Builder httpClient;
    private Retrofit.Builder retrofitBuilder;
    private String baseUrl;

    private ApiClientBuilder() {
        this.httpClient = new OkHttpClient.Builder();
    }

    public static ApiClientBuilder builder() {
        return new ApiClientBuilder();
    }

    public ApiClientBuilder baseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    public ApiClientBuilder loggingInterceptor(HttpLoggingInterceptor loggingInterceptor) {
        this.loggingInterceptor = loggingInterceptor;
        this.loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        this.httpClient.addInterceptor(this.loggingInterceptor);
        return this;
    }

    public ApiClientBuilder basicAuthInterceptor(BasicAuthInterceptor basicAuthInterceptor) {
        this.basicAuthInterceptor = basicAuthInterceptor;
        this.httpClient.addInterceptor(this.basicAuthInterceptor);
        return this;
    }

    public ApiClientBuilder readTimeout(long timeout, TimeUnit unit) {
        this.httpClient.readTimeout(timeout, unit);
        return this;
    }

    public ApiClientBuilder connectTimeout(long timeout, TimeUnit unit) {
        this.httpClient.connectTimeout(timeout, unit);
        return this;
    }

    public ApiClientBuilder writeTimeout(long timeout, TimeUnit unit) {
        this.httpClient.writeTimeout(timeout, unit);
        return this;
    }

    public <T> T build(final Class<T> apiClient) {
        this.retrofitBuilder = new Retrofit.Builder()
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient.build());
        return retrofitBuilder.baseUrl(baseUrl).build().create(apiClient);
    }
}
