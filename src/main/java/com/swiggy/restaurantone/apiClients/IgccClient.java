package com.swiggy.restaurantone.apiClients;

import com.swiggy.restaurantone.compliance.OrderComplianceRequest;
import com.swiggy.restaurantone.compliance.OrderComplianceResponse;
import com.swiggy.restaurantone.compliance.RestaurantComplianceStatusRequest;
import com.swiggy.restaurantone.compliance.RestaurantComplianceStatusResponse;
import com.swiggy.restaurantone.pojos.igccAttribution.UploadResponse;
import com.swiggy.restaurantone.pojos.requests.igcc.IgccAttributionRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IgccClient {
    @Headers({"Content-Type: application/json;", "charset: utf-8"})
    @POST("/igcc-attribution/bulk")
    Call<UploadResponse> syncData(@Body IgccAttributionRequest igccAttributionRequest);

    @Headers({"Content-Type: application/json;", "charset: utf-8"})
    @POST("api/v1/compliance/order")
    Call<OrderComplianceResponse> createOrderCompliance(@Body OrderComplianceRequest request);

    @Headers({"Content-Type: application/json;", "charset: utf-8"})
    @POST("api/v1/compliance/restaurant")
    Call<RestaurantComplianceStatusResponse> createRestaurantComplianceStatus
            (@Body RestaurantComplianceStatusRequest request);
}
