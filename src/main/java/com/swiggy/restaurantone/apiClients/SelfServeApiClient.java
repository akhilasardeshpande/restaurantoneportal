package com.swiggy.restaurantone.apiClients;

import com.swiggy.restaurantone.pojos.requests.srs.RestaurantHolidaySlotRequest;
import com.swiggy.restaurantone.response.CommonsResponse;
import com.swiggy.restaurantone.response.selfserve.SelfServeHolidaySlotResponse;
import retrofit2.Call;
import retrofit2.http.*;

/**
 * Created by rajnish.chandra on 2019-05-15
 */
public interface SelfServeApiClient {
    @Headers("Content-Type: application/json")
    @POST(value = "/restaurant/close/{restaurant_id}")
    Call<CommonsResponse<SelfServeHolidaySlotResponse>>
    closeRestaurant(@Path(value = "restaurant_id") Long restaurantId,
                    @Body RestaurantHolidaySlotRequest restaurantHolidaySlotRequest,
                    @Header("user-meta") String userMeta);
    
    @Headers("Content-Type: application/json")
    @POST(value = "/restaurant/open/{restaurant_id}")
    Call<CommonsResponse<Boolean>>
    openRestaurant(@Path(value = "restaurant_id") Long restaurantId, @Header("user-meta") String userMeta);
}
