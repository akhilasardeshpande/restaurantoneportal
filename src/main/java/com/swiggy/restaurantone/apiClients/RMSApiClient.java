package com.swiggy.restaurantone.apiClients;

import com.swiggy.commons.response.Response;
import com.swiggy.restaurantone.pojos.requests.rms.RestaurantToggleStatusRMS;
import org.json.simple.JSONObject;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by rajnish.chandra on 2019-05-22
 */
public interface RMSApiClient {
    
    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/restaurant/slots/v1/toggleStatusOnCache")
    Call<Response<JSONObject>>
    updateRestaurantOpenStatusInCache(@Body RestaurantToggleStatusRMS restaurantToggleStatusRMS);
}
