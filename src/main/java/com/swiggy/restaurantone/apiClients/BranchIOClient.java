package com.swiggy.restaurantone.apiClients;

import com.swiggy.restaurantone.pojos.branchIO.DeepLinkRequest;
import com.swiggy.restaurantone.pojos.branchIO.DeepLinkResponse;
import org.json.simple.JSONObject;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface BranchIOClient {
    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/v1/url")
    Call<DeepLinkResponse> generateDeepLink(@Body DeepLinkRequest requestBody);
}
