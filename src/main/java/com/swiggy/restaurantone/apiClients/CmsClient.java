package com.swiggy.restaurantone.apiclients;

import com.swiggy.restaurantone.pojos.requests.cms.ItemTrimmingRequest;
import com.swiggy.restaurantone.response.ItemTrimmingResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface CmsClient {

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/item-holiday-slot/bulk")
    Call<ItemTrimmingResponse> markOOS(@Body ItemTrimmingRequest itemTrimmingRequest);

}
