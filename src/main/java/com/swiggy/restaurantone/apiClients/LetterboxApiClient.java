package com.swiggy.restaurantone.apiClients;

import com.swiggy.commons.response.Response;
import com.swiggy.restaurantone.pojos.requests.letterbox.UploadRequest;
import org.springframework.web.bind.annotation.RequestParam;
import retrofit2.http.Headers;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Body;
import retrofit2.http.Query;
import retrofit2.Call;

public interface LetterboxApiClient {

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/api/v1/uploads/templates")
    Call<Response> getTemplates();

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/api/v1/uploads/process-file")
    Call<Response> processUpload(@Body UploadRequest uploadRequest);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/api/v1/uploads/presigned-url")
    Call<Response> presignedUrl(@Query("fileName") final String fileName, @Query("uploadType") final String uploadType);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/api/v1/users/audit-logs")
    Call<Response> userAuditLogs(@Query("email") final String email);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/api/v1/users/uploads-status")
    Call<Response> userUploadsStatus(
            @Query("email") final String email, @Query("uploadType") final String uploadType, @Query("page") final int page
    );
}
