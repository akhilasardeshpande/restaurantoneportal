package com.swiggy.restaurantone.annotations.aspect;

import com.swiggy.commons.Json;
import com.swiggy.restaurantone.annotations.NudgeInstrument;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.restaurantEvent.instrumentation.InstrumentService;
import com.swiggy.restaurantone.restaurantEvent.pojos.instrumentation.NudgeDetailsEvent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Slf4j
@Component
public class NudgeInstrumentAspect {

    @Autowired
    private InstrumentService instrumentService;

    @Autowired
    private AuthorizationHelper authorizationHelper;

    @Pointcut("execution(public * *(..))")
    public void publicMethod(){}

    @Pointcut(value = "@annotation(com.swiggy.restaurantone.annotations.NudgeInstrument)")
    public void annotationPointCut(){

    }

    /**
     * @param pointcut After Pointcut object
     * @param result Object returned from after execution
     *
     * Use Annotation {@link NudgeInstrument} for Aspect on method
     * Instruments the nudge details required
     */
    @AfterReturning(value = "publicMethod() && annotationPointCut()", returning = "result")
    public void instrument(JoinPoint pointcut, Object result) {
        try {
            NudgeDetailsEvent nudgeDetailsEvent = Json.deserialize(Json.serialize(result), NudgeDetailsEvent.class);
            nudgeDetailsEvent.setCreatedBy(authorizationHelper.fetchUser().getEmail());
            nudgeDetailsEvent.setNudgeId(nudgeDetailsEvent.getId());
            nudgeDetailsEvent.setNudgeJson(Json.serialize(nudgeDetailsEvent.getContent()));
            nudgeDetailsEvent.setContent(null);
            instrumentService.doInstrumentNudgeEvent(nudgeDetailsEvent);
        } catch (Exception ex) {
            log.error("Error at nudge creation instrumentation {}", ExceptionUtils.getStackTrace(ex));
        }
    }

}
