package com.swiggy.restaurantone.kafka.consumers;

import com.newrelic.api.agent.Trace;
import com.swiggy.commons.Json;
import com.swiggy.kafka.clients.Record;
import com.swiggy.kafka.clients.consumer.Handler;
import com.swiggy.restaurantone.data.businessEntity.RestaurantSpoc;
import com.swiggy.restaurantone.service.RestaurantSpocService;
import liquibase.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RestaurantPocKafkaEventHandler implements Handler {

    @Value("${kafka.batch.consumer.cms-restaurant-poc.topic.name}")
    private String restaurantPocTopic;

    @Autowired
    private RestaurantSpocService restaurantSpocService;

    private String message;

    @Trace(metricName = "rop_restaurant_poc_kafka_listener", dispatcher = true)
    @Override
    public Status process(Record record) {
        log.info("Received restaurant poc event from kafka topic: {}", restaurantPocTopic);
        if (StringUtils.isEmpty(record.getValue())) {
            return Status.HARD_FAILURE;
        }
        try {
            RestaurantSpoc restaurantSpoc;
            message = record.getValue();
            log.info("restaurantPocTopic message: {}", message);
            restaurantSpoc = Json.deserialize(message, RestaurantSpoc.class);
            restaurantSpocService.updateSpoc(restaurantSpoc);
        } catch (IllegalArgumentException e) {
            log.error("Failed to deserialize restaurantPocTopic event: {} error: {}", message, e.getMessage());
            return Status.HARD_FAILURE;
        } catch (Exception e) {
            log.error("Exception while parsing restaurantPocTopic event: {}, error: {}", message, e.getMessage());
            return Status.HARD_FAILURE;
        }
        return Status.SUCCESS;
    }
}

