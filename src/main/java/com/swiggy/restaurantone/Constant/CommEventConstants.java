package com.swiggy.restaurantone.Constant;

import java.util.Arrays;
import java.util.List;

public class CommEventConstants {
    public static final String COMM_EVENT_ATTR_PREFIX = "attr";
    public static final String COMM_EVENT_DYNAMICFIELD_PREFIX = "plch";
    public static final String APP_NOTIFICATION_START_DATE = "start_date";
    public static final String APP_NOTIFICATION_END_DATE = "end_date";
    public static final String EVENT_NAME = "event_name";
    public static final String COMPONENT = "component";
    public static final String USER_TYPE = "user_type";
    public static final String USER_ID = "user_id";
    public static final String META_INFO_FIELD_APP_NOTIFICATION = "app_notification";
    public static final List<String> MANDATORY_COMM_EVENT_FIELDS = Arrays.asList(EVENT_NAME, COMPONENT, USER_TYPE,
            USER_ID,APP_NOTIFICATION_START_DATE,APP_NOTIFICATION_END_DATE);

}
