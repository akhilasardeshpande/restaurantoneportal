package com.swiggy.restaurantone.Constant;

/**
 * @author Asif
 */
public class ItemTrimmingConstants {

    public static final String SUCCESS = "success";
    public static final String VENDOR = "VENDOR";
    public static final Integer BATCH_SIZE = 50;
    public static final String CMS_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final Integer HEADER = 1;
    public static final String TO_TIME = "2050-01-01 01:01:01";
    public static final String REASON = "item trimming";
    public static final String COMM_EVENT_NAME_KEY = "event_name";
    public static final String ITEM_TRIMMING_COMM_EVENT = "item_trimming_comm_event";
    public static final String COMM_ITEM_NAMES = "item_names";
}
