package com.swiggy.restaurantone.Constant;

public class VendorConfigConstants {
    public static final String FEATURE_VENDOR_CONFIG_TYPE = "feature-config";
    public static final String PREP_TIME_VENDOR_CONFIG_TYPE = "prep-time";
    public static final String JOB_PROCESSING_FAILED = "Job processing failed..";
    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    public static final String RID = "rid";
    public static final String IGCC = "igcc";
    public static final String GLOBAL = "global";
    public static final String META_FEATURE = "metaprojectfeatures";
    public static final String PROJECT_NAMES = "metaprojectnames";
    public static final boolean SUCCESS_RESULT = true, FAILURE_RESULT = false;
    public static int BATCH_SIZE = 500;
}
