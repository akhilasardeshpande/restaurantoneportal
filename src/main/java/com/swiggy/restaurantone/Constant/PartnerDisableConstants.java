package com.swiggy.restaurantone.Constant;

/**
 * Created by rajnish.chandra on 2019-03-29
 */
public class PartnerDisableConstants {
    public static final int PARTNER_DISABLED_CODE = 0;
    public static final int PARTNER_ENABLED_CODE = 1;
    public static final int DISABLE_PENDING_CODE = 2;
    public static final int ENABLE_PENDING_CODE = 3;
    public static final int PARTNER_ENABLE_FAILED_CODE = 4;
    public static final int PARTNER_DISABLE_FAILED_CODE = 5;
    
    public static final String PARTNER_DISABLED = "Disabled";
    public static final String PARTNER_ENABLED = "Enabled";
    public static final String PARTNER_DISABLE_PENDING = "Disable pending";
    public static final String PARTNER_ENABLE_PENDING = "Enable Pending";
    public static final String PARTNER_ENABLE_FAILED = "Partner enable failed";
    public static final String PARTNER_DISABLE_FAILED = "Partner disable failed";
    
    public static final String PARTNER_DISABLE_EVENT_NAME = "partner-bulk-disable";
    public static final String PARTNER_ENABLE_EVENT_NAME = "partner-bulk-enable";
    
    public static final String RESTAURANT_CLOSE_EVENT_NAME = "restaurant-close-rop";
    public static final String RESTAURANT_OPEN_EVENT_NAME = "restaurant-open-rop";
}