package com.swiggy.restaurantone.Constant;

public class IgccAttributionConstants {

    public static final String IGCC_S3_FILE_NAME_PREFIX = "igcc_attribution_";
    public static final int SUCCESS_STATUS_CODE = 200;
}
