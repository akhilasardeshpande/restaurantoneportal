package com.swiggy.restaurantone.Constant;

/**
 * Created by shahbaz.khalid on 7/12/17.
 */
public enum Tier {
    Platinum,
    Gold,
    Silver,
    Bronze,
    Untiered
}
