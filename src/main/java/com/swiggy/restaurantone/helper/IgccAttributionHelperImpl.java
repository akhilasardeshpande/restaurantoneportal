package com.swiggy.restaurantone.helper;

import com.swiggy.restaurantone.pojos.requests.igcc.IgccAttributionRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import retrofit2.Response;
import java.io.IOException;
import com.swiggy.restaurantone.pojos.igccAttribution.UploadResponse;

import static com.swiggy.restaurantone.Constant.IgccAttributionConstants.*;

@Component
@Slf4j
public class IgccAttributionHelperImpl implements IgccAttributionHelper {

    @Value("${igcc.baseurl}")
    private String igccBaseUrl;

    @Override
    public UploadResponse syncIgccAttributionData(final String file, final String bucket) {
        UploadResponse uploadResponse = null;
        IgccAttributionRequest igccAttributionRequest = IgccAttributionRequest
                .builder()
                .file(file)
                .bucket(bucket).build();
        try {
            Response<UploadResponse> response = ClientHelper
                    .igccApiService(igccBaseUrl).syncData(igccAttributionRequest).execute();
            if (response == null || response.body().getStatusCode() != SUCCESS_STATUS_CODE) {
                log.error("Error from IGCC while calling save-igcc-attribution-events bulk API {}", file);
            }
            uploadResponse =  response.body();

        } catch (IOException ex) {
            log.error("Error from IGCC while calling save-igcc-attribution-events bulk API {} ", ex);
        }

        return  uploadResponse;
    }
}

