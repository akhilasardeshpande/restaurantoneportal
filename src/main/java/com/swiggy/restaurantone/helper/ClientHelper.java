package com.swiggy.restaurantone.helper;

import com.swiggy.restaurantone.Constant.Credentials;
import com.swiggy.restaurantone.apiClients.BranchIOClient;
import com.swiggy.restaurantone.apiclients.CmsClient;
import com.swiggy.restaurantone.apiClients.IgccClient;
import com.swiggy.restaurantone.apiClients.LetterboxApiClient;
import com.swiggy.restaurantone.apiClients.RMSApiClient;
import com.swiggy.restaurantone.apiClients.SelfServeApiClient;
import com.swiggy.restaurantone.service.HungerGameAPIService;
import com.swiggy.restaurantone.service.PlacingAPIService;
import com.swiggy.restaurantone.service.RMSAPIService;
import com.swiggy.restaurantone.service.SRSAPIService;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by shahbaz.khalid on 4/27/17.
 */
public class ClientHelper {
    
    private static OkHttpClient.Builder httpClient;
    private static Retrofit.Builder retrofitBuilder;

    static {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor(Credentials.USERNAME, Credentials.PASSWORD))
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
        retrofitBuilder = new Retrofit.Builder()
                .client(client)
                .addConverterFactory(JacksonConverterFactory.create());
    }

    public static RMSAPIService rmsAPIService(String baseUrl) {
        return retrofitBuilder.baseUrl(baseUrl)
                .build()
                .create(RMSAPIService.class);
    }

    public static SRSAPIService srsAPIService(String baseUrl) {
        return retrofitBuilder
                .baseUrl(baseUrl)
                .build()
                .create(SRSAPIService.class);
    }
    
    public static SelfServeApiClient selfServeApiService(String baseUrl) {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
    
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(
                        new BasicAuthInterceptor(Credentials.SELF_SERVE_USERNAME, Credentials.SELF_SERVE_PASSWORD))
                .connectTimeout(3, TimeUnit.SECONDS)
                .readTimeout(3, TimeUnit.SECONDS)
                .writeTimeout(3, TimeUnit.SECONDS)
                .build();
        return new Retrofit.Builder().client(httpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(SelfServeApiClient.class);
    }

    public static PlacingAPIService placingAPIService(String baseUrl) {
        return retrofitBuilder
                .baseUrl(baseUrl)
                .build()
                .create(PlacingAPIService.class);
    }

    public static CmsClient CmsApiService(String baseUrl) {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(1, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.SECONDS)
                .writeTimeout(1, TimeUnit.SECONDS)
                .build();
        return new Retrofit.Builder().client(httpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(CmsClient.class);
    }
    
    public static IgccClient igccApiService(String baseUrl) {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(1, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.SECONDS)
                .writeTimeout(1, TimeUnit.SECONDS)
                .build();
        return new Retrofit.Builder().client(httpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(IgccClient.class);
    }

    public static BranchIOClient branchIOClient(String baseUrl) {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build();
        return new Retrofit.Builder().client(httpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(BranchIOClient.class);
    }
    
    public static RMSApiClient rmsApiClient(String baseUrl) {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(new BasicAuthInterceptor(Credentials.RMS_USERNAME, Credentials.RMS_PASSWORD))
                .connectTimeout(1, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.SECONDS)
                .writeTimeout(1, TimeUnit.SECONDS)
                .build();
        return new Retrofit.Builder().client(httpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .build()
                .create(RMSApiClient.class);
    }

    public static HungerGameAPIService hgApiClient(String baseUrl) {
        return retrofitBuilder
                .baseUrl(baseUrl)
                .build()
                .create(HungerGameAPIService.class);
    }

    public static LetterboxApiClient letterboxApiClient(String baseUrl) {
        return retrofitBuilder.baseUrl(baseUrl).build().create(LetterboxApiClient.class);
    }
}
