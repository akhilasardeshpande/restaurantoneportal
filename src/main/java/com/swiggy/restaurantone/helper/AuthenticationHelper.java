package com.swiggy.restaurantone.helper;

import com.swiggy.commons.Json;
import com.swiggy.restaurantone.data.ApplicationEntity.LoginUserInfo;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.data.ApplicationEntity.UserAuthInfo;
import com.swiggy.restaurantone.data.businessEntity.UserCriteria;
import com.swiggy.restaurantone.data.coreEntity.Session;
import com.swiggy.restaurantone.service.*;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by balamuneeswaran.s on 1/5/17.
 */
@Setter
@Component
public class AuthenticationHelper {

    private final SessionService sessionService;

    private final UserService userService;

    private final GoogleAuthenticationService googleAuthentication;

    @Value("${user.expirationTime}")
    private Long expiryTime;

    private final HttpServletResponse httpServletResponse;

    private final HttpServletRequest httpServletRequest;

    @Autowired
    private CacheService cacheService;



    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public AuthenticationHelper(SessionService sessionService, UserService userService, GoogleAuthenticationService googleAuthentication, HttpServletResponse httpServletResponse, HttpServletRequest httpServletRequest) {
        this.sessionService = sessionService;
        this.userService = userService;
        this.googleAuthentication = googleAuthentication;
        this.httpServletResponse = httpServletResponse;
        this.httpServletRequest = httpServletRequest;
    }

    public User login(LoginUserInfo loginUserInfo) throws IOException{
        UserCriteria userCriteria = new UserCriteria();
        userCriteria.emailList = new ArrayList<>();
        UserAuthInfo userAuthInfo;
        String authType = loginUserInfo.getAuthType();
        IAuthentication authenticator = getAuthenticator(authType);
        userAuthInfo = authenticator != null ? authenticator.authenticate(loginUserInfo) : null;

        return Optional
                .ofNullable(userAuthInfo)
                .map((uAuthInfo) ->
                        {
                            userCriteria.emailList.add(uAuthInfo.getEmail());
                            User user = userService.getUsersByCriteria(userCriteria).get(0);
                            UUID sessionId = UUID.randomUUID();
                            Long expirationTime = System.currentTimeMillis() + expiryTime;
                            sessionService.createAndSaveSession(user.getId(), sessionId, Json.serialize(uAuthInfo), expirationTime, authType);
                            httpServletResponse.addHeader("sessionId", sessionId.toString());
                            httpServletResponse.addHeader("userId", user.getId().toString());
                            return user;
                        }
                ).orElse(null);

    }

    private IAuthentication getAuthenticator(String authType) {
        if (authType != null && authType.equals("Google")) {
            return googleAuthentication;
        }
        return null;
    }

    boolean reAuthentication(Session session) {
        if (System.currentTimeMillis() >= session.getSessionExpiration()) {
            sessionService.deleteSession(session);
            return false;
        }
        UserAuthInfo userAuthInfo = Json.deserialize(session.getAuthDetails(), UserAuthInfo.class);
        IAuthentication authenticator = getAuthenticator(session.getAuthType());
        String updatedAuth = authenticator != null ? authenticator.reAuthenticate(userAuthInfo) : null;
        if (updatedAuth != null) {
            session.setAuthDetails(updatedAuth);
            Long expirationTime = System.currentTimeMillis() + expiryTime;
            session.setTokenExpirationTime(expirationTime);
            sessionService.saveSession(session);
            return true;
        }
        return false;
    }

    public boolean logout() {
        try {
            Long userId = Long.parseLong(httpServletRequest.getHeader("userId"));
            UUID sessionId = UUID.fromString(httpServletRequest.getHeader("sessionId"));
            Session session = sessionService.getSession(userId, sessionId);
            sessionService.deleteSession(session);
            cacheService.delete("session_" + userId);
            cacheService.delete("user-" + userId);
            cacheService.delete("restaurantUserMap-" + userId);

            return true;
        } catch (Exception e) {
            logger.error("Error logging out user ", e.getMessage());
            return false;
        }

    }


}
