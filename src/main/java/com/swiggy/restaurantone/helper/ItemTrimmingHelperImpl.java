package com.swiggy.restaurantone.helper;

import com.swiggy.restaurantone.communication.notification.NotificationsServiceException;
import com.swiggy.restaurantone.pojos.ItemTrimming.ItemData;
import com.swiggy.restaurantone.pojos.requests.cms.ItemTrimmingRequest;
import com.swiggy.restaurantone.response.ItemTrimmingResponse;
import com.swiggy.restaurantone.communication.notification.NotificationsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import retrofit2.Response;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.swiggy.restaurantone.Constant.ItemTrimmingConstants.CMS_TIME_FORMAT;
import static com.swiggy.restaurantone.Constant.ItemTrimmingConstants.COMM_ITEM_NAMES;
import static com.swiggy.restaurantone.Constant.ItemTrimmingConstants.COMM_EVENT_NAME_KEY;
import static com.swiggy.restaurantone.Constant.ItemTrimmingConstants.ITEM_TRIMMING_COMM_EVENT;
import static com.swiggy.restaurantone.Constant.ItemTrimmingConstants.TO_TIME;
import static com.swiggy.restaurantone.Constant.ItemTrimmingConstants.REASON;
import static com.swiggy.restaurantone.Constant.ItemTrimmingConstants.SUCCESS;
import static com.swiggy.restaurantone.Constant.ItemTrimmingConstants.VENDOR;

@Component
@Slf4j
public class ItemTrimmingHelperImpl implements ItemTrimmingHelper {
    private final NotificationsService notificationsService;

    @Autowired
    public ItemTrimmingHelperImpl (NotificationsService notificationsService) {
        this.notificationsService = notificationsService;
    }

    @Value("${cms.baseurl}")
    private String cmsBaseUrl;

    @Override
    public Boolean applyBulkHolidaySlot(List<ItemData> itemDataList) {
        List<ItemTrimmingRequest.ItemTrimmingData> itemTrimmingData = new ArrayList<>();
        Boolean isCMSCallSuccess = false;

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(CMS_TIME_FORMAT);
        String fromTime = LocalDateTime.now().format(formatter);

        for (ItemData itemData : itemDataList) {
            ItemTrimmingRequest.ItemTrimmingData trimmingData =  ItemTrimmingRequest.ItemTrimmingData
                    .builder()
                    .itemId(itemData.getItemId())
                    .fromTime(fromTime)
                    .toTime(TO_TIME)
                    .build();
            itemTrimmingData.add(trimmingData);
        }
        ItemTrimmingRequest.UserMeta userMeta = ItemTrimmingRequest.UserMeta
                .builder()
                .source(VENDOR)
                .meta(ItemTrimmingRequest.UserMeta.Meta
                        .builder()
                        .userid(VENDOR)
                        .reason(REASON)
                        .build())
                .build();
        ItemTrimmingRequest itemTrimmingRequest = ItemTrimmingRequest
                .builder()
                .data(itemTrimmingData)
                .userMeta(userMeta).build();
        try {
            Response<ItemTrimmingResponse> itemTrimmingResponse = ClientHelper
                    .CmsApiService(cmsBaseUrl).markOOS(itemTrimmingRequest).execute();
            if (itemTrimmingResponse.body().getData() != null && itemTrimmingResponse.body().getMessage().equals(SUCCESS)) {
                isCMSCallSuccess = true;
            } else {
                log.error("Error while applying holiday slot for Items {} ", itemTrimmingRequest.getData());
            }

        } catch (IOException ex) {
            log.error("Error from CMS while calling item-holiday-slot bulk API {} ", ex);
        }

        return isCMSCallSuccess;
    }

    @Async
    public void applyHolidaySlotAndSendNotification (List<List<ItemData>> itemDataListBatches) {
        Map<String, List<String>> restaurantIdToItemNameMap = new HashMap<>();

        for (List<ItemData> itemDataIterator : itemDataListBatches) {
            if (applyBulkHolidaySlot(itemDataIterator)) {
                for (ItemData itemData : itemDataIterator) {
                    List<String> itemNameList = new ArrayList<>();
                    if (restaurantIdToItemNameMap.get(itemData.getRestaurantId()) != null) {
                        itemNameList = restaurantIdToItemNameMap.get(itemData.getRestaurantId());
                    }
                    itemNameList.add(itemData.getItemName());
                    restaurantIdToItemNameMap.put(itemData.getRestaurantId(),itemNameList);
                }
            }
        }
        sendItemTrimmingNotification(restaurantIdToItemNameMap);
    }

    @Override
    public void sendItemTrimmingNotification(Map <String, List<String> > restaurantIdToItemNameMap) {
        for (Map.Entry<String, List<String>> entry : restaurantIdToItemNameMap.entrySet()) {
            try {
                Map<String, Object> params = new HashMap<>();
                params.put(COMM_EVENT_NAME_KEY, ITEM_TRIMMING_COMM_EVENT);
                params.put(COMM_ITEM_NAMES, entry.getValue());
                notificationsService.notifyRestaurant(Long.parseLong(entry.getKey()), params);
            } catch (NotificationsServiceException ex) {
                log.error("Error sending item trimming notification {} ", ex.getMessage());
            }
        }
    }
}
