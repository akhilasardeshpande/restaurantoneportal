package com.swiggy.restaurantone.helper;

import com.swiggy.commons.Json;
import com.swiggy.kafka.clients.producer.Producer;
import com.swiggy.restaurantone.communication.notification.NotificationsService;
import com.swiggy.restaurantone.communication.notification.NotificationsServiceException;
import com.swiggy.restaurantone.data.businessEntity.RestaurantSpoc;
import com.swiggy.restaurantone.data.businessEntity.RestaurantUserMap;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.data.businessEntity.UserCriteria;
import com.swiggy.restaurantone.exceptions.PublisherException;
import com.swiggy.restaurantone.service.BranchIOService;
import com.swiggy.restaurantone.service.RestaurantUserMapService;
import com.swiggy.restaurantone.service.RoleService;
import com.swiggy.restaurantone.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
@Slf4j
public class RestaurantSpocHelperImpl implements RestaurantSpocHelper {
    private static final String NOTIFICATION_EVENT_NAME = "restaurant_spoc_update";

    @Autowired
    @Qualifier("kafka-batch-producer")
    private Producer kafkaBatchProducer;

    private final RestaurantUserMapService restaurantUserMapService;

    private final UserService userService;

    private final NotificationsService notificationsService;

    private final RoleService roleService;

    private final BranchIOService branchIOService;

    @Value("${kafka.batch.producer.spoc-create-event.topic.name}")
    private String spocCreateTopic;

    @Autowired
    public RestaurantSpocHelperImpl(RestaurantUserMapService restaurantUserMapService,
                                    UserService userService,
                                    NotificationsService notificationsService,
                                    RoleService roleService,
                                    BranchIOService branchIOService) {
        this.restaurantUserMapService = restaurantUserMapService;
        this.userService = userService;
        this.notificationsService = notificationsService;
        this.roleService = roleService;
        this.branchIOService = branchIOService;
    }

    /**
     * update restaurants spoc
     *
     * @param restaurantSpocs
     */
    @Override
    @Async
    public void updateRestaurantsSpoc(List<RestaurantSpoc> restaurantSpocs, String updatedBy) {
        for (RestaurantSpoc restaurantSpoc : restaurantSpocs) {
            try {
                restaurantSpoc.setUpdatedBy(updatedBy);
                log.info("sending restaurant spoc create event to confluent kafka message: {}", restaurantSpoc);
                kafkaBatchProducer.send(spocCreateTopic, Json.serialize(restaurantSpoc));
            } catch (PublisherException e) {
                log.error("Error publishing restaurant spoc create event. RestaurantSpoc: {}, Error: {}, StackTrace: {}",
                        restaurantSpoc, e.getMessage(), e.getCause());
            } catch (IllegalArgumentException e) {
                log.error("Error serializing restaurant spoc. RestaurantSpoc: {}, Error: {}, StackTrace: {}",
                        restaurantSpoc, e.getMessage(), e.getCause());
            }
        }
    }

    @Override
    public void sendSpocNotification(RestaurantSpoc restaurantSpoc) {
        // generate deep link
        String branchLink = null;
        try{
            branchLink = branchIOService.generateRestaurantSpocDeepLink(restaurantSpoc.getRestaurantId());
        }catch (Exception e){
            log.error("Error generating deep link for spoc. RestaurantSpoc: {}, Error: {}, StackTrace: {}",
                    restaurantSpoc, e.getMessage(), e.getCause());
        }
        try {
            Map<String, Object> params = new HashMap<>();
            params.put("event_name", NOTIFICATION_EVENT_NAME);
            params.put("sm_name", restaurantSpoc.getSmName());
            params.put("sm_mobile", restaurantSpoc.getSmMobile());
            params.put("sm_email", restaurantSpoc.getSmEmail());
            params.put("asm_name", restaurantSpoc.getAsmName());
            params.put("asm_mobile", restaurantSpoc.getAsmMobile());
            params.put("asm_email", restaurantSpoc.getAsmEmail());
            if(branchLink != null){
                params.put("branch_link", branchLink);
            }
            notificationsService.notifyRestaurant(restaurantSpoc.getRestaurantId(), params);
        } catch (NotificationsServiceException e) {
            log.error("Error sending spoc update notification. RestaurantSpoc: {}, Error: {}, StackTrace: {} ",
                    restaurantSpoc, e.getMessage(), e.getCause());
        }
    }

    /**
     * update SPOC
     *
     * @param newUser
     * @return
     */
    @Override
    public User updateSpoc(User newUser) {
        User updatedUser;
        User existingUser = userService.find(newUser.getEmail());
        if (existingUser == null) {
            updatedUser = userService.addUser(newUser);
        } else {
            newUser.setId(existingUser.getId());
            updatedUser = userService.updateUser(newUser);
        }
        return updatedUser;
    }

    /**
     * update restaurant SPOC mapping
     *
     * @param restaurantId
     * @param user
     * @return
     */
    @Override
    public RestaurantUserMap updateRestaurantSpocMap(Long restaurantId, User user) {
        RestaurantUserMap updatedRestaurantUserMap = null;
        Optional<RestaurantUserMap> restaurantAsmUserMapOptional = getSpocMapping(restaurantId, user.getRole().id);
        if (restaurantAsmUserMapOptional.isPresent()) {
            //update the mapping
            RestaurantUserMap existingMap = restaurantAsmUserMapOptional.get();
            updatedRestaurantUserMap = restaurantUserMapService.updateRestaurantUserMap(RestaurantUserMap.builder()
                    .id(existingMap.getId())
                    .restaurantId(restaurantId)
                    .updatedBy(user.getUpdatedBy())
                    .userId(user.getId()).build());
        } else {
            // create mapping
            updatedRestaurantUserMap = restaurantUserMapService.addUserRestaurantMap(RestaurantUserMap.builder()
                    .restaurantId(restaurantId)
                    .userId(user.id)
                    .updatedBy(user.getUpdatedBy())
                    .build());
        }
        return updatedRestaurantUserMap;
    }

    /**
     * Gets Asm user for a restaurant
     *
     * @param restaurantId
     * @return
     */
    @Override
    public Optional<User> getSpoc(Long restaurantId, Long roleId) {
        Optional<User> asmOptional = Optional.empty();
        try {
            List<User> users = getUsersForRestaurant(restaurantId);
            asmOptional = users.stream().filter(p -> p.role.id == roleId).findFirst();
        } catch (Exception e) {
            log.error("Error getting Asm User. RestaurantId: {}, Error: {}, StackTrace: {}",
                    restaurantId, e.getMessage(), e.getCause());
        }

        return asmOptional;
    }

    private Optional<RestaurantUserMap> getSpocMapping(Long restaurantId, Long roleId) {
        RestaurantUserMap restaurantUserMap = null;
        List<User> users = getUsersForRestaurant(restaurantId);
        Optional<User> smUserOptional = users.stream().filter(p -> p.role.id == roleId).findFirst();
        if (smUserOptional.isPresent()) {
            User user = smUserOptional.get();
            restaurantUserMap = restaurantUserMapService.getMappingByUserIdAndRestId(user.getId(), restaurantId);
        }
        return Optional.ofNullable(restaurantUserMap);
    }

    private List<User> getUsersForRestaurant(Long restaurantId) {
        List<Long> userIds = restaurantUserMapService.getUsersByRestaurantsId(restaurantId).
                stream().map(RestaurantUserMap::getUserId).filter(Objects::nonNull).collect(Collectors.toList());
        UserCriteria userFindCriteria = UserCriteria.builder().idList(userIds).build();
        List<User> users = userService.getUsersByCriteria(userFindCriteria);
        return users;
    }
}
