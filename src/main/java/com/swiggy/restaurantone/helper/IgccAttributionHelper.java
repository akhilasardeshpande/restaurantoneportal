package com.swiggy.restaurantone.helper;

import com.swiggy.restaurantone.pojos.igccAttribution.UploadResponse;

public interface IgccAttributionHelper {

    UploadResponse syncIgccAttributionData(final String fileName, final String bucket);
}
