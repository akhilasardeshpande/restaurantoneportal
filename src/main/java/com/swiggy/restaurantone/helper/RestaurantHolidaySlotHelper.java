package com.swiggy.restaurantone.helper;

import com.swiggy.commons.Json;
import com.swiggy.commons.response.Response;
import com.swiggy.restaurantone.apiClients.RMSApiClient;
import com.swiggy.restaurantone.pojos.requests.rms.RestaurantToggleStatusRMS;
import com.swiggy.restaurantone.pojos.requests.srs.RestaurantHolidaySlotRequest;
import com.swiggy.restaurantone.pojos.requests.srs.RestaurantHolidaySlotResult;
import com.swiggy.restaurantone.pojos.requests.srs.UserMeta;
import com.swiggy.restaurantone.pojos.response.RestaurantHolidaySlotDisposition;
import com.swiggy.restaurantone.response.CommonsResponse;
import com.swiggy.restaurantone.response.SRSResponse;
import com.swiggy.restaurantone.response.selfserve.SelfServeHolidaySlotResponse;
import com.swiggy.restaurantone.service.SRSAPIService;
import com.swiggy.restaurantone.apiClients.SelfServeApiClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class RestaurantHolidaySlotHelper {
    
    private final SRSAPIService srsapiService;
    private final SelfServeApiClient selfServeApiClient;
    private final RMSApiClient rmsApiClient;
    
    @Autowired
    public RestaurantHolidaySlotHelper(SRSAPIService srsapiService, SelfServeApiClient selfServeApiClient,
                                       RMSApiClient rmsApiClient) {
        this.srsapiService = srsapiService;
        this.selfServeApiClient = selfServeApiClient;
        this.rmsApiClient = rmsApiClient;
    }
    
    SRSResponse<RestaurantHolidaySlotResult> addHolidaySlotThroughSRS(Long restaurantId, String updatedBy,
                                                                      LocalDateTime fromTime,
                                                                      LocalDateTime toTime,
                                                                      Long dispositionId) throws IOException {

        RestaurantHolidaySlotRequest restaurantHolidaySlotRequest = getHolidaySlotRequest(restaurantId, fromTime,
                toTime, dispositionId);
        
            return srsapiService.addHolidaySlot(restaurantHolidaySlotRequest, Json.serialize(getUserMeta(updatedBy)))
                    .execute().body();
    }
    
    CommonsResponse<SelfServeHolidaySlotResponse> closeRestaurantSelfServe(Long restaurantId, String updatedBy,
                                                                           LocalDateTime fromTime,
                                                                           LocalDateTime toTime,
                                                                           Long dispositionId) throws IOException {
    
        RestaurantHolidaySlotRequest restaurantHolidaySlotRequest = getHolidaySlotRequest(restaurantId, fromTime,
                toTime, dispositionId);
        return selfServeApiClient.closeRestaurant(restaurantId, restaurantHolidaySlotRequest,
                Json.serialize(getUserMeta(updatedBy))).execute().body();
    }
    
    @Retryable(
            value = {IOException.class},
            maxAttempts = 4,
            backoff = @Backoff(
                    delay = 100,
                    maxDelay = 4000,
                    multiplier = 2.0
            )
    )
    SRSResponse deleteHolidaySlot(Long slotId, String updatedBy) throws IOException {
            return srsapiService.deleteHolidaySlot(slotId, Json.serialize(getUserMeta(updatedBy))).execute().body();
    }
    
    @Retryable(
            value = {IOException.class},
            maxAttempts = 4,
            backoff = @Backoff(
                    delay = 100,
                    maxDelay = 4000,
                    multiplier = 2.0
            )
    )
    public CommonsResponse<Boolean> openRestaurantFromSelfServe(Long restaurantId, String updatedBy)
            throws IOException {
        return selfServeApiClient.openRestaurant(restaurantId, Json.serialize(getUserMeta(updatedBy)))
                .execute()
                .body();
    }
    
    List<RestaurantHolidaySlotResult> getHolidaySlots(Long restaurantId) {
    
        try {
            SRSResponse<List<RestaurantHolidaySlotResult>> holidaySlotResultSRSResponse =  srsapiService
                    .getFutureHolidaySlotsOnRestaurant(restaurantId).execute().body();
            
            if (holidaySlotResultSRSResponse != null) {
                return holidaySlotResultSRSResponse.getData();
            }
        } catch (IOException e) {
            log.error("Error getting response from SRS. Error : {}", e.getMessage());
        }
        return new ArrayList<>();
    }
    
    private UserMeta getUserMeta(String updatedBy) {
        Map<String, Object> meta = new HashMap<>();
        meta.put("updatedBy", updatedBy);
    
        return UserMeta.builder()
                .source("ROP")
                .meta(meta)
                .build();
    }
    
    private RestaurantHolidaySlotRequest getHolidaySlotRequest(Long restaurantId, LocalDateTime fromTime,
                                                               LocalDateTime toTime, Long dispositionId) {
        return RestaurantHolidaySlotRequest.builder()
                .restaurantId(restaurantId)
                .fromTime(fromTime)
                .toTime(toTime)
                .dispositionId(dispositionId)
                .build();
    }
    
    public boolean updateRestaurantStatusInRMSCache(RestaurantToggleStatusRMS restaurantToggleStatusRMS) {
        Response response;
        boolean success = false;
        try {
            response = rmsApiClient.updateRestaurantOpenStatusInCache(restaurantToggleStatusRMS).execute().body();
            if (response.getStatusCode() == 1) {
                log.info("Restaurant status toggled in RMS. RestaurantId : {}",
                        restaurantToggleStatusRMS.getRestaurantId());
                success = true;
            }
        } catch (Exception ex) {
            log.error("Exception occurred while toggling status in RMS. Exception : {}", ex.getMessage());
        }
        return success;
    }

    public List<RestaurantHolidaySlotDisposition> getDispositions() {
        try {
            SRSResponse<List<RestaurantHolidaySlotDisposition>> restaurantHolidaySlotDispositions =  srsapiService
                    .getRestaurantHolidaySlotDispositions().execute().body();

            if (restaurantHolidaySlotDispositions != null) {
                return restaurantHolidaySlotDispositions.getData();
            }
        } catch (IOException e) {
            log.error("Error getting response from SRS. Error : {}", e.getMessage());
        }
        return new ArrayList<>();
    }
}