package com.swiggy.restaurantone.helper;

import com.swiggy.restaurantone.data.businessEntity.Permission;
import com.swiggy.restaurantone.data.coreEntity.PermissionEntity;
import com.swiggy.restaurantone.dataAccess.PermissionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by shahbaz.khalid on 4/25/17.
 */

@Component
public class PermissionHelper {

    @Autowired
    private PermissionDao permissionDao;
    public List<PermissionEntity> getPermissions(List<Permission> permissions) {
        return permissions.stream()
                .map(p -> permissionDao.findOne(p.getId()))
                        .collect(Collectors.toList());
    }
}
