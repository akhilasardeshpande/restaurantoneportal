package com.swiggy.restaurantone.helper;

import com.swiggy.restaurantone.data.ApplicationEntity.RmsUserAuthInfo;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.data.coreEntity.Session;
import com.swiggy.restaurantone.service.RestaurantUserMapService;
import com.swiggy.restaurantone.service.SessionService;
import com.swiggy.restaurantone.service.UserRelationshipService;
import com.swiggy.restaurantone.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by rajnishc on 1/21/17.
 */
@Component
public class AuthorizationHelper {
    private final SessionService sessionService;

    private final AuthenticationHelper authenticationHelper;

    private final RestaurantUserMapService restMap;

    private final UserRelationshipService userMap;


    @Value("${addUserPermission}")
    private Long addUserPermission;

    @Value("${rmsPermission}")
    private Long rmsPermission;

    @Value("${rmsReadOnlyPermission}")
    private Long rmsReadOnlyPermission;

    @Value("${editUserPermission}")
    private Long editUserPermission;

    @Value("${addSwiggyPoc}")
    private Long addSwiggyPocPermission;

    @Autowired
    private UserService userService;

    @Value("${user.expirationTime}")
    private Long expiryTime;

    @Value("${basicAuth}")
    private String plainCredentials;

    private final HttpServletRequest httpServletRequest;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public AuthorizationHelper(SessionService sessionService, AuthenticationHelper authenticationHelper, RestaurantUserMapService restMap, UserRelationshipService userMap, HttpServletRequest httpServletRequest) {
        this.sessionService = sessionService;
        this.authenticationHelper = authenticationHelper;
        this.restMap = restMap;
        this.userMap = userMap;
        this.httpServletRequest = httpServletRequest;
    }


    private boolean authForChild(Long childId, boolean self) {
        if (checkForBasicAuth()) {
            return true;
        }
        String userIdString = httpServletRequest.getHeader("userId");
        if (userIdString != null) {
            Long userId = Long.parseLong(userIdString);
            List<Long> childIds = new ArrayList<>();
            return childIds.add(childId) && authForChild(userId, childIds, self);
        }
        return false;
    }

    private boolean authForChild(Long id, List<Long> childIds, boolean self) {
        Set<Long> childSet = new HashSet<Long>(childIds);

        Set<Long> authChild = userMap.GetAllChildUsersByUserId(id)
                .stream()
                .map(userRel -> userRel.childUserId)
                .collect(Collectors.toSet());

        if (self) {
            authChild.add(id);
        }
        logger.info("Child ids" + authChild);
        logger.info("Child sets" + childSet);

        return authChild.containsAll(childSet);
    }

    private boolean authForRestaurant(Long restaurantId) {
        String userIdString = httpServletRequest.getHeader("userId");
        if (userIdString != null) {
            Long userId = Long.parseLong(userIdString);
            List<Long> childIds = new ArrayList<>();
            return childIds.add(restaurantId) && authForRestaurant(userId, childIds);
        }
        return false;
    }

    public boolean authForAddPoc() {
        return checkPermissions(addSwiggyPocPermission);
    }

    private boolean authForRestaurant(Long id, List<Long> restaurants) {
        Set<Long> restSet = new HashSet<Long>(restaurants);
        Set<Long> allRestSet = restMap.getRestaurantsByParentUserId(id);
        logger.info("All restaurants " + allRestSet);
        logger.info("Requested restaurants" + restSet);
        return allRestSet.containsAll(restSet);
    }

    public boolean authorizeUserForRestaurant(RmsUserAuthInfo rmsUserAuthInfo) {
        if (!checkForBasicAuth()) {
            return false;
        }
        User user = userService.find(rmsUserAuthInfo.getEmail());
        return user != null && authForRestaurant(user.getId(), rmsUserAuthInfo.getRestaurants());
    }

    public boolean checkAuthorization(List<Long> childIds, List<Long> restaurantIds, boolean self) {
        if ((childIds == null && restaurantIds == null) || checkForBasicAuth()) {
            return true;
        } else if (childIds == null) {
            return authForRestaurant(Long.parseLong(httpServletRequest.getHeader("userId")), restaurantIds);
        } else if (restaurantIds == null) {
            return authForChild(Long.parseLong(httpServletRequest.getHeader("userId")), childIds, self);
        } else {
            Long userId = Long.parseLong(httpServletRequest.getHeader("userId"));
            return authForChild(userId, childIds, self)
                    &&
                    authForRestaurant(userId, restaurantIds);
        }
    }

    public boolean checkRMSAuthorization() {
        return checkPermissions(rmsPermission);
    }

    public boolean checkRMSReadOnlyAuthorization() {
        return checkPermissions(rmsReadOnlyPermission);
    }

    public boolean checkPermissions(Long givenPermission) {
        String userIdString;
        Long userId;
        try {
            userIdString = httpServletRequest.getHeader("userId");
            if (userIdString.contains(".")) {
                return false;
            }
            userId = Long.parseLong(userIdString);
        } catch (Exception e) {
            logger.error("Error getting session info from header");
            return false;
        }
        User user = userService.find(userId);
        return user != null && user.getPermissions().stream().anyMatch(permission -> permission.getId().equals(givenPermission));

    }

    public boolean checkAuthorization(Long childId, Long restaurantId, boolean self) {
        List<Long> childIds = null;
        List<Long> restIds = null;
        if (childId != null) {
            childIds = new ArrayList<Long>();
            childIds.add(childId);
        }
        if (restaurantId != null) {
            restIds = new ArrayList<Long>();
            restIds.add(restaurantId);
        }
        return checkAuthorization(childIds, restIds, self);
    }


    public boolean authForAddUser() {
        Long userId;
        try {
            userId = Long.parseLong(httpServletRequest.getHeader("userId"));
        } catch (Exception e) {
            logger.error("Error getting session info from header");
            throw new IllegalArgumentException("Wrong values in header");
        }

        User user = userService.find(userId);
        return user != null && user.getPermissions().stream().anyMatch(permission -> permission.getId() == addUserPermission);
    }

    public boolean authForEditUser(User user) {
        Long userId;
        try {
            userId = Long.parseLong(httpServletRequest.getHeader("userId"));
        } catch (Exception e) {
            logger.error("Error getting session info from header");
            throw new IllegalArgumentException("Wrong values in header");
        }

        User currentUser = userService.find(userId);
        if (user.id.equals(currentUser.id)) {
            return editAuthForSelf(currentUser, user);
        }
        return authForChild(user.id, false) ||
                currentUser.role.level > user.role.level && currentUser.getPermissions().stream().anyMatch(permission -> permission.getId() == editUserPermission);
    }

    private boolean editAuthForSelf(User user, User newValue) {
        FieldChanges fieldChanges = new FieldChanges();
        return !(fieldChanges.isTeamChange(user, newValue) ||
                fieldChanges.isCityChange(user, newValue) ||
                fieldChanges.dojChange(user, newValue) ||
                fieldChanges.isManagerChange(user, newValue) ||
                fieldChanges.isRoleChange(user, newValue));
    }

    public boolean refreshAccess() {
        String sessionIdString = httpServletRequest.getHeader("sessionId");
        String userIdString = httpServletRequest.getHeader("userId");

        if (sessionIdString != null && userIdString != null) {
            UUID sessionId = UUID.fromString(sessionIdString);
            Long id = Long.parseLong(userIdString);
            return refreshAccess(sessionId, id);
        }
        return false;
    }

    private boolean refreshAccess(UUID sessionId, Long id) {
        Session session = sessionService.getSession(id, sessionId);
        return _refreshAccess(session);
    }

    private boolean _refreshAccess(Session session) {
        return session != null && authenticationHelper.reAuthentication(session);
    }

    public boolean authenticateUserSession() {

        if (checkForBasicAuth()) {
            return true;
        }
        String sessionIdString = null, userIdString = null;
        UUID sessionId;
        Long userId;
        try {
            sessionIdString = httpServletRequest.getHeader("sessionId");
            userIdString = httpServletRequest.getHeader("userId");
            sessionId = UUID.fromString(sessionIdString);
            if (userIdString.contains(".")) {
                return false;
            }
            userId = Long.parseLong(userIdString);
        } catch (Exception e) {
            logger.error("Error getting session info from header");
            return false;
        }
        Session session = sessionService.getSession(userId, sessionId);
        if (httpServletRequest.getMethod().equals("GET")) {
            return _authenticateUserSession(session);
        }
        return _authenticateUserSession(session) && refreshAccess(sessionId, userId);
    }

    public boolean authenticateUserSession(UUID sessionId, Long userId, boolean forceRefresh) {

        Session session = sessionService.getSession(userId, sessionId);
        if (!forceRefresh) {
            return _authenticateUserSession(session);
        }
        return _authenticateUserSession(session) && refreshAccess(sessionId, userId);
    }

    private boolean _authenticateUserSession(Session session) {
        if (session == null) {
            return false;
        } else {
            Long expiryTime = session.getTokenExpirationTime();
            boolean expired = System.currentTimeMillis() > expiryTime;

            return !expired || authenticationHelper.reAuthentication(session);
        }
    }

    private boolean checkForBasicAuth() {
        String auth = httpServletRequest.getHeader("Authorization");
        if (auth != null && auth.startsWith("Basic")) {
            String base64Credentials = auth.substring("Basic".length()).trim();
            String creds = new String(Base64.getDecoder().decode(base64Credentials));
            return plainCredentials.equals(creds);
        }
        return false;
    }

    public User fetchUser() {
        try {
            String userIdString = httpServletRequest.getHeader("userId");
            Long userId = Long.parseLong(userIdString);
            return userService.find(userId);
        } catch (Exception ex) {
            logger.error("Error getting session info from header");
            throw ex;
        }
    }

    public boolean isAuthorizedUser(User user, Long requiredPermission) {
        return user.getPermissions().stream().anyMatch(permission -> permission.id.equals(requiredPermission));
    }

    private class FieldChanges {
        boolean isTeamChange(User user, User newValue) {
            return !user.getTeam().getId().equals(newValue.getTeam().getId());
        }

        boolean isRoleChange(User user, User newValue) {
            return !user.getRole().getId().equals(newValue.getRole().getId());
        }

        boolean isContactChange(User user, User newValue) {
            return !user.getContactNumber().equals(newValue.getContactNumber());
        }

        boolean dobChange(User user, User newValue) {
            if (user.dob == null && newValue.dob != null || user.dob != null && newValue.dob == null) {
                return true;
            }
            if (user.dob == null && newValue.dob == null)
                return false;
            return user.dob.compareTo(newValue.dob) != 0;
        }

        boolean dojChange(User user, User newValue) {
            if (user.doj == null && newValue.doj != null || user.doj != null && newValue.doj == null)
                return true;

            return !(user.doj == null && newValue.doj == null) && user.doj.compareTo(newValue.doj) != 0;
        }

        boolean isCityChange(User user, User newValue) {
            if (user.city.getId() == null && newValue.city.getId() != null || user.city.getId() != null && newValue.city.getId() == null)
                return true;

            if (user.city.getId() == null && newValue.city.getId() == null)
                return false;

            return !user.city.getId().equals(newValue.city.getId());
        }

        boolean isManagerChange(User user, User newValue) {
            return !user.manager.getId().equals(newValue.manager.getId());
        }
    }

}
