package com.swiggy.restaurantone.helper;

import com.swiggy.restaurantone.data.businessEntity.RestaurantSpoc;
import com.swiggy.restaurantone.data.businessEntity.RestaurantUserMap;
import com.swiggy.restaurantone.data.businessEntity.User;

import java.util.List;
import java.util.Optional;

public interface RestaurantSpocHelper {
    void updateRestaurantsSpoc(List<RestaurantSpoc> restaurantSpocs, String updatedBy);
    Optional<User> getSpoc(Long restaurantId, Long roleId);
    void sendSpocNotification(RestaurantSpoc restaurantSpoc);
    User updateSpoc(User newUser);
    RestaurantUserMap updateRestaurantSpocMap(Long restaurantId, User user);
}
