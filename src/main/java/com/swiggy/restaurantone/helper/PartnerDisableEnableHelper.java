package com.swiggy.restaurantone.helper;

import com.swiggy.commons.Json;
import com.swiggy.restaurantone.Constant.PartnerDisableConstants;
import com.swiggy.restaurantone.apiClients.SelfServeApiClient;
import com.swiggy.restaurantone.data.businessEntity.PartnerEnableRequest;
import com.swiggy.restaurantone.data.businessEntity.PartnerOrRestaurantDisableRequest;
import com.swiggy.restaurantone.data.coreEntity.PartnerDisableHolidaySlotInfoEntity;
import com.swiggy.restaurantone.data.coreEntity.PartnerDisableRunInfoEntity;
import com.swiggy.restaurantone.dataAccess.PartnerDisableHolidaySlotInfoDao;
import com.swiggy.restaurantone.dataAccess.PartnerDisableRunInfoDao;
import com.swiggy.restaurantone.communication.notification.NotificationsServiceException;
import com.swiggy.restaurantone.pojos.commEngine.CommEngineEvent;
import com.swiggy.restaurantone.pojos.commEngine.User;
import com.swiggy.restaurantone.pojos.commEngine.UserInfo;
import com.swiggy.restaurantone.pojos.requests.rms.RestaurantToggleStatusRMS;
import com.swiggy.restaurantone.pojos.requests.srs.PartnerTypeSearchRequest;
import com.swiggy.restaurantone.response.CommonsResponse;
import com.swiggy.restaurantone.response.selfserve.SelfServeHolidaySlotResponse;
import com.swiggy.restaurantone.communication.notification.NotificationsService;
import com.swiggy.restaurantone.service.SRSAPIService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.time.LocalDateTime;
import java.util.*;

import static com.swiggy.restaurantone.Constant.ItemTrimmingConstants.COMM_EVENT_NAME_KEY;

@Component
@Slf4j
public class PartnerDisableEnableHelper {
    private final PartnerDisableHolidaySlotInfoDao partnerDisableHolidaySlotInfoDao;
    private final RestaurantHolidaySlotHelper restaurantHolidaySlotHelper;
    private final SRSAPIService srsapiService;
    private final PartnerDisableRunInfoDao partnerDisableRunInfoDao;
    private final NotificationsService notificationsService;
    
    private final int errorCode = 1;
    private final int socketTimedOutErrorCode = 2;
    
    @Value("${partnerDisableBatchSize}")
    private int partnerHolidaySlotBatchSize;
    @Value("${partner.disable.retry.count}")
    private int retryCount;
    
    private final Environment environment;
    
    @Autowired
    public PartnerDisableEnableHelper(PartnerDisableHolidaySlotInfoDao partnerDisableHolidaySlotInfoDao,
                                      RestaurantHolidaySlotHelper restaurantHolidaySlotHelper,
                                      SRSAPIService srsapiService, PartnerDisableRunInfoDao partnerDisableRunInfoDao,
                                      NotificationsService notificationsService,
                                      SelfServeApiClient selfServeApiClient, Environment environment) {
        this.partnerDisableHolidaySlotInfoDao = partnerDisableHolidaySlotInfoDao;
        this.restaurantHolidaySlotHelper = restaurantHolidaySlotHelper;
        this.srsapiService = srsapiService;
        this.partnerDisableRunInfoDao = partnerDisableRunInfoDao;
        this.notificationsService = notificationsService;
        this.environment = environment;
    }
    
    @Async
    public void closePartnerOrRestaurant(PartnerOrRestaurantDisableRequest partnerOrRestaurantDisableRequest,
                                         Long runId) {
        List<Integer> partnerIds = new ArrayList<>();
        partnerIds.add(partnerOrRestaurantDisableRequest.getPartnerId());
        
        if (!ObjectUtils.isEmpty(partnerOrRestaurantDisableRequest.getRests())) {
            applyHolidaySlotOnListOfRestaurants(partnerOrRestaurantDisableRequest.getRests(),
                    partnerOrRestaurantDisableRequest, runId, true);
        } else {
            List<Long> restaurantIds = null;
            try {
                PartnerTypeSearchRequest partnerTypeSearchRequest = new PartnerTypeSearchRequest(partnerIds);
                
                //Getting all restaurant ids for the given partner id
                restaurantIds = srsapiService.getRestaurantIdsFromPartnerId(partnerTypeSearchRequest).execute().body()
                        .getData();
                if (!ObjectUtils.isEmpty(restaurantIds)) {
                    applyHolidaySlotOnListOfRestaurants(restaurantIds, partnerOrRestaurantDisableRequest, runId,
                            false);
                } else {
                    log.error("No restaurant found for partner id {}", partnerOrRestaurantDisableRequest
                            .getPartnerId());
                }
            } catch (IOException e) {
                log.error("error while disabling partner. Error : {}", e.getMessage());
            }
    
            if (ObjectUtils.isEmpty(restaurantIds)) {
                PartnerDisableRunInfoEntity partnerDisableRunInfoEntity = partnerDisableRunInfoDao.findOne(runId);
                partnerDisableRunInfoEntity.setTotalRestaurants(0);
                partnerDisableRunInfoEntity.setStatusCode(PartnerDisableConstants.PARTNER_DISABLE_FAILED_CODE);
                partnerDisableRunInfoEntity.setStatusMessage(PartnerDisableConstants.PARTNER_DISABLE_FAILED);
                partnerDisableRunInfoDao.save(partnerDisableRunInfoEntity);
            }
        }
    }
    
    private void applyHolidaySlotOnListOfRestaurants(List<Long> restaurantIds,
                                                     PartnerOrRestaurantDisableRequest partnerOrRestaurantDisableRequest,
                                                     Long runId, boolean isRestaurant) {
        List<List<Long>> restaurantIdBatches = ListUtils.partition(restaurantIds, partnerHolidaySlotBatchSize);
        
        for (List<Long> restaurantIdIterator : restaurantIdBatches) {
            log.info("Closing restaurant in batch");
            boolean result = true;
            int count = 0;
            
            for (Long restaurantId : restaurantIdIterator) {
                log.info("Closing restaurant id : {}", restaurantId);
                result = result && addAndUpdateHolidaySlotForRestaurant(restaurantId, runId,
                        partnerOrRestaurantDisableRequest, isRestaurant);
            }
            
            log.info("Batch has been processed");
            
            while (count++ < retryCount && !result) {
                log.info("retry : count {}", count);
                result = addHolidaySlotRetryForBatch(restaurantIdIterator, partnerOrRestaurantDisableRequest,
                        isRestaurant);
            }
        }
        
        int activeRestaurants = partnerDisableHolidaySlotInfoDao.countByRunInfoIdAndIsActiveTrue(runId);
        PartnerDisableRunInfoEntity partnerDisableRunInfoEntity = partnerDisableRunInfoDao.findOne(runId);
    
        if (activeRestaurants == 0) {
            partnerDisableRunInfoEntity.setActive(false);
            partnerDisableRunInfoEntity.setStatusCode(PartnerDisableConstants.PARTNER_DISABLE_FAILED_CODE);
            partnerDisableRunInfoEntity.setStatusMessage(PartnerDisableConstants.PARTNER_DISABLE_FAILED);
        } else {
            partnerDisableRunInfoEntity.setActive(true);
            partnerDisableRunInfoEntity.setStatusCode(PartnerDisableConstants.PARTNER_DISABLED_CODE);
            partnerDisableRunInfoEntity.setStatusMessage(PartnerDisableConstants.PARTNER_DISABLED);
        }
        
        partnerDisableRunInfoEntity.setTotalRestaurants(restaurantIds.size());
        partnerDisableRunInfoEntity.setActiveRestaurants(activeRestaurants);

        partnerDisableRunInfoDao.save(partnerDisableRunInfoEntity);
        
        if (!ObjectUtils.isEmpty(partnerOrRestaurantDisableRequest.getPartnerId())) {
            sendCommEventToPartnerPoc(partnerOrRestaurantDisableRequest.getPartnerId(),
                    partnerOrRestaurantDisableRequest.getFromTime(), partnerOrRestaurantDisableRequest.getToTime(),
                    partnerOrRestaurantDisableRequest.getReason(), PartnerDisableConstants.PARTNER_DISABLE_EVENT_NAME);
        }
    }
    
    private boolean addAndUpdateHolidaySlotForRestaurant(long restaurantId, long runId,
                                                         PartnerOrRestaurantDisableRequest partnerOrRestaurantDisableRequest,
                                                         boolean isRestaurant) {
        boolean result = false;
        try {
            LocalDateTime currentTime = LocalDateTime.now().plusSeconds(5);
            LocalDateTime fromTime = isRestaurant ? currentTime :
                    partnerOrRestaurantDisableRequest.getFromTime().isBefore(LocalDateTime.now()) ?
                            currentTime : partnerOrRestaurantDisableRequest.getFromTime();

            CommonsResponse<SelfServeHolidaySlotResponse> response = restaurantHolidaySlotHelper
                    .closeRestaurantSelfServe(restaurantId,
                            partnerOrRestaurantDisableRequest.getUpdatedBy(), fromTime,
                            partnerOrRestaurantDisableRequest.getToTime(),
                            partnerOrRestaurantDisableRequest.getDispositionId());
            
            if (response != null && response.getData() != null) {
                log.info("Restaurant id : {}, has been closed", restaurantId);
                result = true;
                updateRestaurantCloseSuccessInfoInDB(response.getData(), runId,
                        partnerOrRestaurantDisableRequest.getUpdatedBy());
                toggleRestaurantStatusInCache(restaurantId, false, partnerOrRestaurantDisableRequest);
                sendOpenCloseEventToComm(restaurantId, partnerOrRestaurantDisableRequest.getFromTime(),
                        partnerOrRestaurantDisableRequest.getToTime(), partnerOrRestaurantDisableRequest.getReason(),
                        PartnerDisableConstants.RESTAURANT_CLOSE_EVENT_NAME);
            } else {
                log.info("Error while closing restaurant, restaurant id :{}", restaurantId);
                updateRestaurantCloseFailureInfoInDB(restaurantId, runId,
                        partnerOrRestaurantDisableRequest.getUpdatedBy(), errorCode);
            }
        } catch (IOException e) {
            int errorCode = e instanceof SocketTimeoutException ? socketTimedOutErrorCode : this.errorCode;
            
            updateRestaurantCloseFailureInfoInDB(restaurantId, runId, partnerOrRestaurantDisableRequest.getUpdatedBy(),
                    errorCode);
            log.error("Error while closing restaurant. restaurant id :{}. Error : {}", restaurantId, e.getMessage());
        }
        return result;
    }
    
    private void updateRestaurantCloseSuccessInfoInDB(SelfServeHolidaySlotResponse addHolidayResponse, Long runId,
                                                      String updatedBy) {
        PartnerDisableHolidaySlotInfoEntity partnerDisableHolidaySlotInfoEntity = PartnerDisableHolidaySlotInfoEntity
                .builder()
                .fromTime(addHolidayResponse.getFromTime())
                .toTime(addHolidayResponse.getToTime())
                .createdAt(LocalDateTime.now())
                .runInfoId(runId)
                .createdBy(updatedBy)
                .restaurantId(addHolidayResponse.getRestaurantId())
                .isActive(true)
                .build();
        partnerDisableHolidaySlotInfoDao.save(partnerDisableHolidaySlotInfoEntity);
    }
    
    private void updateRestaurantCloseFailureInfoInDB(Long restaurantId, Long runId, String updatedBy, int errorCode) {
        PartnerDisableHolidaySlotInfoEntity partnerDisableHolidaySlotInfoEntity = partnerDisableHolidaySlotInfoDao
                .findByRunInfoIdAndRestaurantId(runId, restaurantId);
        
        if (partnerDisableHolidaySlotInfoEntity != null) {
            partnerDisableHolidaySlotInfoEntity.setRetryCount(partnerDisableHolidaySlotInfoEntity.getRetryCount() + 1);
            partnerDisableHolidaySlotInfoEntity.setErrorCode(errorCode);
        } else {
            partnerDisableHolidaySlotInfoEntity = PartnerDisableHolidaySlotInfoEntity.builder()
                    .createdAt(LocalDateTime.now())
                    .runInfoId(runId)
                    .createdBy(updatedBy)
                    .restaurantId(restaurantId)
                    .isActive(false)
                    .errorCode(errorCode)
                    .retryCount(1)
                    .build();
        }
        partnerDisableHolidaySlotInfoDao.save(partnerDisableHolidaySlotInfoEntity);
    }
    
    private boolean addHolidaySlotRetryForBatch(List<Long> restaurantIds,
                                                PartnerOrRestaurantDisableRequest partnerOrRestaurantDisableRequest,
                                                boolean isRestaurant) {
        List<PartnerDisableHolidaySlotInfoEntity> partnerDisableHolidaySlotInfoEntities =
                partnerDisableHolidaySlotInfoDao.findByRestaurantIdInAndIsActive(restaurantIds, false);
        if (ObjectUtils.isEmpty(partnerDisableHolidaySlotInfoEntities)) {
            return true;
        }
        boolean result = true;
        
        for (PartnerDisableHolidaySlotInfoEntity partnerDisableRunInfoEntity : partnerDisableHolidaySlotInfoEntities) {
            long restaurantId = partnerDisableRunInfoEntity.getRestaurantId();
            long runId = partnerDisableRunInfoEntity.getRunInfoId();
            int errorCode = partnerDisableRunInfoEntity.getErrorCode();
            log.info("Retrying holiday slot for restaurant id : {}", restaurantId);
            
            if (errorCode == this.errorCode) {
                // If holiday slot request failed
                result = result && addAndUpdateHolidaySlotForRestaurant(restaurantId, runId,
                        partnerOrRestaurantDisableRequest, isRestaurant);
            } else if (errorCode == socketTimedOutErrorCode) {
                // If add holiday slot request timed out
            }
        }
        return result;
    }
    
    @Async
    public void deleteHolidaySlotsOnRestaurant(LocalDateTime toTime,
                                               PartnerEnableRequest partnerEnableRequest) {
        List<PartnerDisableHolidaySlotInfoEntity> holidaySlotInfoEntities = partnerDisableHolidaySlotInfoDao
                .findAllByRunInfoIdAndIsActiveTrue(partnerEnableRequest.getRunInfoId());
        PartnerDisableRunInfoEntity partnerDisableRunInfoEntity = partnerDisableRunInfoDao
                .findOne(partnerEnableRequest.getRunInfoId());
        boolean isRestaurant = partnerDisableRunInfoEntity.getPartnerId() != null;
        
        for (PartnerDisableHolidaySlotInfoEntity partnerDisableHolidaySlotInfoEntity : holidaySlotInfoEntities) {
            if (toTime.isAfter(LocalDateTime.now())) {
                openRestaurantAndUpdateStatus(partnerDisableRunInfoEntity,
                        partnerDisableHolidaySlotInfoEntity, isRestaurant, partnerEnableRequest);
            } else {
                partnerDisableHolidaySlotInfoEntity.setIsActive(false);
                partnerDisableHolidaySlotInfoDao.save(partnerDisableHolidaySlotInfoEntity);
            }
        }
        partnerDisableRunInfoEntity.setStatusCode(PartnerDisableConstants.PARTNER_ENABLED_CODE);
        partnerDisableRunInfoEntity.setStatusMessage(PartnerDisableConstants.PARTNER_ENABLED);
        partnerDisableRunInfoEntity.setActive(false);
        partnerDisableRunInfoDao.save(partnerDisableRunInfoEntity);
    }
    
    private void openRestaurantAndUpdateStatus(PartnerDisableRunInfoEntity partnerDisableRunInfoEntity,
                                               PartnerDisableHolidaySlotInfoEntity partnerDisableHolidaySlotInfoEntity,
                                               boolean isRestaurant, PartnerEnableRequest partnerEnableRequest) {
        long restaurantId = partnerDisableHolidaySlotInfoEntity.getRestaurantId();
        try {
            
            CommonsResponse<Boolean> response = restaurantHolidaySlotHelper
                    .openRestaurantFromSelfServe(restaurantId, partnerEnableRequest.getUpdatedBy());
            String event = isRestaurant ? PartnerDisableConstants.RESTAURANT_OPEN_EVENT_NAME :
                    PartnerDisableConstants.PARTNER_ENABLE_EVENT_NAME;

            if (response != null && response.getData().equals(Boolean.TRUE)) {
                log.info("Successfully opened restaurant {}", restaurantId);
                
                partnerDisableHolidaySlotInfoEntity.setIsActive(false);
                partnerDisableHolidaySlotInfoEntity.setUpdatedAt(LocalDateTime.now());
                partnerDisableHolidaySlotInfoEntity.setUpdatedBy(partnerEnableRequest.getUpdatedBy());
                toggleRestaurantStatusInCache(restaurantId, true, null);
                
                sendOpenCloseEventToComm(restaurantId, partnerDisableRunInfoEntity.getFromTime(),
                        partnerDisableRunInfoEntity.getToTime(), partnerDisableRunInfoEntity.getReason(), event);
                
                partnerDisableHolidaySlotInfoDao.save(partnerDisableHolidaySlotInfoEntity);
            } else {
                log.error("Error while opening restaurant {}.", restaurantId);
            }
        } catch (IOException e) {
            log.error("Error while opening restaurant {}. Error : {}", restaurantId, e.getMessage());
        }
    }
    
    private void toggleRestaurantStatusInCache(long restaurantId, boolean isOpen,
                                               PartnerOrRestaurantDisableRequest partnerOrRestaurantDisableRequest) {
        RestaurantToggleStatusRMS restaurantToggleStatusRMS;
        if (isOpen) {
            restaurantToggleStatusRMS = RestaurantToggleStatusRMS
                    .builder()
                    .restaurantId(restaurantId)
                    .isOpen(true)
                    .build();
        } else {
            restaurantToggleStatusRMS = RestaurantToggleStatusRMS
                    .builder()
                    .restaurantId(restaurantId)
                    .fromTime(partnerOrRestaurantDisableRequest.getFromTime())
                    .toTime(partnerOrRestaurantDisableRequest.getToTime())
                    .isOpen(false)
                    .build();
        }
        restaurantHolidaySlotHelper.updateRestaurantStatusInRMSCache(restaurantToggleStatusRMS);
    }
    
    private void sendOpenCloseEventToComm(long restaurantId, LocalDateTime fromTime, LocalDateTime toTime,
                                          String reason, String eventName) {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put(COMM_EVENT_NAME_KEY, eventName);
            params.put("fromTime", fromTime.toString().replace('T', ' '));
            params.put("toTime", toTime.toString().replace('T', ' '));
            params.put("reason", reason);
            
            notificationsService.notifyRestaurant(restaurantId, params);
        } catch (NotificationsServiceException ex) {
            log.error("Error sending restaurant open/close notification event to comm engine. Error :  {} ",
                    ex.getMessage());
        }
    }
    
    @Async
    public void sendCommEventToPartnerPoc(long partnerType, LocalDateTime fromTime, LocalDateTime toTime,
                                          String reason, String eventName) {
        Map<String, Object> params = new HashMap<>();
        params.put(COMM_EVENT_NAME_KEY, eventName);
        params.put("fromTime", fromTime.toString().replace('T', ' '));
        params.put("toTime", toTime.toString().replace('T', ' '));
        params.put("reason", reason);
        
        String phoneNumber = environment.getProperty("partner.poc.phone.type-" + partnerType);
        String emailId = environment.getProperty("partner.poc.email.type-" + partnerType);
        
        if (ObjectUtils.isEmpty(phoneNumber) && ObjectUtils.isEmpty(emailId)) {
            return;
        }
        
        User user = User.builder()
                .emailId(emailId)
                .mobileNumber(phoneNumber)
                .build();
        
        UserInfo userInfo = UserInfo.builder()
                .user(user)
                .metaInfo(new HashMap<>())
                .ctx(new HashMap<>())
                .build();
        List<UserInfo> users = new ArrayList<>();
        users.add(userInfo);
        
        CommEngineEvent commEngineEvent = CommEngineEvent.builder()
                .identificationId(UUID.randomUUID().toString())
                .type("GENERAL")
                .component("VENDOR")
                .name(eventName)
                .ctx(params)
                .users(users)
                .metaInformation(Collections.EMPTY_MAP)
                .userRetrieval(null)
                .build();
        try {
            notificationsService.sendEventToComm(commEngineEvent);
        } catch (NotificationsServiceException ex) {
            log.error("Error sending notification event to comm engine. Payload : {}, Error :  {} ",
                    Json.serialize(commEngineEvent), ex.getMessage());
        }
    }
}
