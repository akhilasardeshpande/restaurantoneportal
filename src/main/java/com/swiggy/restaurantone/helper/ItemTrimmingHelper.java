package com.swiggy.restaurantone.helper;

import com.swiggy.restaurantone.pojos.ItemTrimming.ItemData;

import java.util.List;
import java.util.Map;

public interface ItemTrimmingHelper {

    Boolean applyBulkHolidaySlot(List<ItemData> itemDataList);
    void applyHolidaySlotAndSendNotification(List<List<ItemData>> itemDataListBatches);
    void sendItemTrimmingNotification(Map<String, List<String> > restaurantIdToItemNameMap);

}
