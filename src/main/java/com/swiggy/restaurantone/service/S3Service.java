package com.swiggy.restaurantone.service;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.swiggy.restaurantone.Constant.IgccAttributionConstants;
import com.swiggy.restaurantone.response.PresignedUrlResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;

@Service
@Slf4j
public class S3Service {
    @Autowired
    private AmazonS3 s3Client;

    @Value("${igcc.s3.bucket}")
    private String igccS3Bucket;

    public PresignedUrlResponse getPresignedUrl() {

        String s3FileName = new StringBuilder(IgccAttributionConstants.IGCC_S3_FILE_NAME_PREFIX)
                .append(new Date().getTime()).append(".csv").toString();
        log.info("Generating presigned url for File Name : {}", s3FileName);
        try {
            java.util.Date expiration = new java.util.Date();
            long expTimeMillis = expiration.getTime();
            expTimeMillis += 1000 * 60 * 60;
            expiration.setTime(expTimeMillis);

            // Generate the pre-signed URL.
            System.out.println("Generating pre-signed URL.");

            GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(igccS3Bucket, s3FileName);
            generatePresignedUrlRequest
                    .withMethod(HttpMethod.PUT)
                    .withExpiration(expiration)
                    .withContentType("text/csv");
                    //.addRequestParameter("Content-Type", "text/csv");
            URL url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);
            PresignedUrlResponse response = new PresignedUrlResponse();
            response.setUrl(url.toURI().toString());
            response.setKey(s3FileName);
            return response;
        } catch (AmazonS3Exception | URISyntaxException ex) {
            log.error("Error while creating Pre signed URL for file {}", s3FileName);
        }
        return null;
    }
}
