package com.swiggy.restaurantone.service;

import com.swiggy.commons.response.ErrorResponse;
import com.swiggy.commons.response.Response;
import com.swiggy.restaurantone.helper.ClientHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by shahbaz.khalid on 4/23/17.
 */

@Component
public class RMSRoleService {

    @Value("${baseurl.rmsint}")
    private String rmsBaseUrl;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public Response getRoles() {
        Response response;
        try {
            response = ClientHelper.rmsAPIService(rmsBaseUrl).getRoles().execute().body();
        } catch (Exception ex) {
            response = new ErrorResponse(ex.getMessage());
            logger.error("unknown error occurred");
        }
        return response;
    }
}
