package com.swiggy.restaurantone.service;

import com.swiggy.annotations.cache.annotations.CacheDefaults;
import com.swiggy.commons.Json;
import com.swiggy.restaurantone.data.businessEntity.UserRelationship;
import com.swiggy.restaurantone.data.coreEntity.UserRelationshipEntity;
import com.swiggy.restaurantone.dataAccess.UserRelationshipDao;
import com.swiggy.restaurantone.mapper.UserRelationshipMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by balamuneeswaran.s on 1/15/17.
 */
@CacheDefaults(prefix = "userRelationship", deserialize = UserRelationship.class)
@Service
public class UserRelationshipService {
    
    private final UserRelationshipDao userRelationshipDao;
    
    private final UserRelationshipMapper userRelationshipMapper;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    
    @Autowired
    public UserRelationshipService(UserRelationshipDao userRelationshipDao, UserRelationshipMapper userRelationshipMapper) {
        this.userRelationshipDao = userRelationshipDao;
        this.userRelationshipMapper = userRelationshipMapper;
    }
    
    public List<UserRelationship> GetImmediateChildUsersByUserId(Long id) {
        logger.debug("GetImmediateChildUsersByUserId Input: " + id);
        List<UserRelationshipEntity> userRelationshipEntities = userRelationshipDao.findByUserIdAndIsDirectParent(id, true);
        List<UserRelationship> userRelationships = userRelationshipMapper.entityToBusinessObject(userRelationshipEntities);
        logger.debug("GetImmediateChildUsersByUserId Output: " + Json.serialize(userRelationships));
        return userRelationships;
    }
    
    public List<UserRelationship> GetImmediateParentUsersByUserId(Long id) {
        logger.debug("GetImmediateParentUsersByUserId Input: " + id);
        List<UserRelationship> userRelationships = userRelationshipDao.findByChildUserIdAndIsDirectParent(id, true).
                stream().map(userRelationshipMapper::entityToBusinessObject).collect(Collectors.toList());
        logger.debug("GetImmediateParentUsersByUserId Output: " + Json.serialize(userRelationships));
        return userRelationships;
    }
    
    public List<UserRelationship> GetAllChildUsersByUserId(Long id) {
        logger.debug("GetAllChildUsersByUserId Input: " + id);
        List<UserRelationshipEntity> userRelationshipEntities = userRelationshipDao.findByUserId(id);
        List<UserRelationship> userRelationships = userRelationshipMapper.entityToBusinessObject(userRelationshipEntities);
        logger.debug("GetAllChildUsersByUserId Output: " + Json.serialize(userRelationships));
        return userRelationships;
    }
    
    @Transactional
    public List<UserRelationship> UpdateUserRelationship(List<UserRelationship> userRelationships) {
        logger.debug("UpdateUserRelationship Input: " + Json.serialize(userRelationships));
        List<UserRelationshipEntity> updatedUserRelationShipEntities = new ArrayList<>();
        
        for (UserRelationship userRelationship : userRelationships) {
            this.moveChildUsersToParent(userRelationship.childUserId);
            userRelationshipDao.deleteByChildUserId(userRelationship.childUserId);
            userRelationshipDao.deleteByUserId(userRelationship.childUserId);
            userRelationship = this.addUserRelationShip(userRelationship);
            UserRelationshipEntity userRelationshipEntity = userRelationshipMapper.businessToEntity(userRelationship);
            updatedUserRelationShipEntities.add(userRelationshipEntity);
        }
        userRelationships = userRelationshipMapper.entityToBusinessObject(updatedUserRelationShipEntities);
        logger.debug("UpdateUserRelationship Output: " + Json.serialize(userRelationships));
        return userRelationships;
    }

    public List<UserRelationship> updateUserManagerRelationship(List<UserRelationship> userRelationships) {
        logger.debug("UpdateUserRelationship Input: " + Json.serialize(userRelationships));
        List<UserRelationshipEntity> updatedUserRelationShipEntities = new ArrayList<>();
        
        for (UserRelationship userRelationship : userRelationships) {
            userRelationshipDao.deleteByChildUserId(userRelationship.childUserId);
            try {
                userRelationship = addUserRelationShip(userRelationship);
            }
            catch (DataIntegrityViolationException e) {
                logger.error("add userRelationship throws an error: " + Json.serialize(userRelationship), e);
            }
            UserRelationshipEntity userRelationshipEntity = userRelationshipMapper.businessToEntity(userRelationship);
            updatedUserRelationShipEntities.add(userRelationshipEntity);
        }
        userRelationships = userRelationshipMapper.entityToBusinessObject(updatedUserRelationShipEntities);
        logger.debug("UpdateUserRelationship Output: " + Json.serialize(userRelationships));
        return userRelationships;
    }
    
    private List<UserRelationshipEntity> moveChildUsersToParent(Long childUserId) {
        logger.debug("moveChildUsersToParent Input: " + childUserId);
        UserRelationship parentUserRelationship = this.GetImmediateParentUsersByUserId(childUserId).get(0);
        List<UserRelationship> childUserRelationships = this.GetImmediateChildUsersByUserId(childUserId);
        List<UserRelationshipEntity> updatedUserRelationship = new ArrayList<>();
        for (UserRelationship childUserRelationship : childUserRelationships) {
            
            UserRelationshipEntity userRelationshipEntity = UserRelationshipEntity.builder()
                    .userId(parentUserRelationship.userId)
                    .childUserId(childUserRelationship.childUserId)
                    .isDirectParent(true)
                    .build();
            
            updatedUserRelationship.add(userRelationshipEntity);
        }
        updatedUserRelationship = userRelationshipDao.save(updatedUserRelationship);
        logger.debug("moveChildUserstoParent Output: " + Json.serialize(updatedUserRelationship));
        return updatedUserRelationship;
    }

    @Transactional
    public UserRelationship addUserRelationShip(UserRelationship userRelationship) {
        logger.debug("addUserRelationShip Input: " + userRelationship);
        List<UserRelationshipEntity> parentUserRelationShipEntities = userRelationshipDao.findByChildUserId(userRelationship.userId);
        List<UserRelationshipEntity> updateUserRelationshipEntities = new ArrayList<>();

        UserRelationshipEntity userRelationshipEntity = UserRelationshipEntity.builder()
                .userId(userRelationship.userId)
                .childUserId(userRelationship.childUserId)
                .updatedBy(userRelationship.updatedBy)
                .isDirectParent(true)
                .build();
        
        updateUserRelationshipEntities.add(userRelationshipEntity);

        for (UserRelationshipEntity parentUserRelationshipEntity :
                parentUserRelationShipEntities) {
            
            UserRelationshipEntity userMapEntity = UserRelationshipEntity.builder()
                    .userId(parentUserRelationshipEntity.userId)
                    .childUserId(userRelationship.childUserId)
                    .isDirectParent(false)
                    .updatedBy(userRelationship.updatedBy)
                    .build();

            updateUserRelationshipEntities.add(userMapEntity);
        }
        updateUserRelationshipEntities = userRelationshipDao.save(updateUserRelationshipEntities);
        
        logger.debug("addUserRelationShip-UpdatedUserRelationshipEntities : " + updateUserRelationshipEntities);
        logger.debug("addUserRelationShip Output : " + userRelationship);
        return userRelationship;
    }
    
    public boolean isParent(Long parentId, Long childId) {
        List<UserRelationship> userRelationship = GetAllChildUsersByUserId(parentId);
        return userRelationship.stream().anyMatch(userRelationship1 -> userRelationship1.childUserId.equals(childId));
    }
    
}