package com.swiggy.restaurantone.service;

import com.swiggy.restaurantone.data.businessEntity.Batch;
import com.swiggy.restaurantone.response.PlacingResponse;
import com.swiggy.restaurantone.response.RestaurantListResponse;
import org.json.simple.JSONObject;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;
import java.util.Map;

/**
 * Created by alok.singhal on 9/27/17.
 */
public interface PlacingAPIService {
    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/api/v1/batches/unconfirmed")
    Call<PlacingResponse<Map<Long,List<Batch>>>> getUnconfirmedBatches(@Body RestaurantListResponse restaurantListResponse);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @PUT("/api/v1/batch/")
    Call<JSONObject> batchActions(@Body JSONObject jsonObject);
}
