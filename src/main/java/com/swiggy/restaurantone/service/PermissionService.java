package com.swiggy.restaurantone.service;

import com.swiggy.restaurantone.data.businessEntity.Permission;
import com.swiggy.restaurantone.data.coreEntity.PermissionEntity;
import com.swiggy.restaurantone.dataAccess.PermissionDao;
import com.swiggy.restaurantone.mapper.PermissionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rajnishc on 4/25/17.
 */
@Service
public class PermissionService {
    @Autowired
    private PermissionDao permissionDao;

    @Autowired
    private PermissionMapper permissionMapper;

    public PermissionEntity findPermissionEntity(Long id) {
        return permissionDao.findOne(id);
    }

    public List<Permission> findAll() {
        return permissionMapper.entityToBusinessObject(permissionDao.findAll());
    }
}
