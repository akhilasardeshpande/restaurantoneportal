package com.swiggy.restaurantone.service;

import com.swiggy.restaurantone.pojos.igccAttribution.UploadResponse;
import com.swiggy.restaurantone.response.PresignedUrlResponse;

public interface IgccAttributionService {

    UploadResponse syncIgccAttribution(final String key);

    PresignedUrlResponse getPresignedUrl();
}
