package com.swiggy.restaurantone.service;

public interface BranchIOService {
    String generateRestaurantSpocDeepLink(Long restaurantId);
}
