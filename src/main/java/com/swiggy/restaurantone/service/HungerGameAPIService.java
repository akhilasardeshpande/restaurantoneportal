package com.swiggy.restaurantone.service;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface HungerGameAPIService {
    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/upload")
    Call<String> fileuploadUpdate(@Body String s3Url, @Body String month, @Body String year, @Body String type);
}
