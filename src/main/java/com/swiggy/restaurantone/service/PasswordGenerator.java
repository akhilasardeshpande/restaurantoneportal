package com.swiggy.restaurantone.service;

import com.swiggy.restaurantone.data.ApplicationEntity.PasswordPolicy;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by rajnishc on 8/9/17.
 */
@Service
public class PasswordGenerator {

    protected String generatePassword(PasswordPolicy policy) {
        String upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String lowercase ="abcdefghijklmnopqrstuvwxyz";
        String digits = "0123456789";
        String symbols = "@$!%*?&#";

        StringBuilder password = new StringBuilder();

        password.append(generateRandomString(upperCase, policy.getUppercaseCount()))
        .append(generateRandomString(lowercase, policy.getLowercaseCount()))
        .append(generateRandomString(digits, policy.getNumbersCount()))
        .append(generateRandomString(symbols, policy.getSpecialCharactersCount()));

        int passwordLength = password.length();

        if (passwordLength < policy.getMinLength()) {
            password.append(generateRandomString(lowercase, policy.getMinLength() - passwordLength));
        }

        return shuffle(password.toString());
    }

    private StringBuilder generateRandomString (String category, long length) {
        Random generator = new Random();
        StringBuilder randomString = new StringBuilder();
        int randomPosition;
        for(int i = 1; i <= length; i++){
            randomPosition = generator.nextInt(category.length());
            randomString.append(category.charAt(randomPosition));
        }
        return randomString;
    }


    private String shuffle(String input){
        List<Character> characters = new ArrayList<>();
        for(char c:input.toCharArray()){
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(input.length());
        while(characters.size()!=0){
            int randPicker = (int)(Math.random()*characters.size());
            output.append(characters.remove(randPicker));
        }
        return output.toString();
    }
}
