package com.swiggy.restaurantone.service;

import com.google.gson.Gson;
import com.swiggy.commons.response.ErrorResponse;
import com.swiggy.commons.response.Response;
import com.swiggy.restaurantone.apiClients.LetterboxApiClient;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.pojos.requests.letterbox.UploadRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class LetterboxService {

    private final LetterboxApiClient letterboxApiClient;

    @Autowired
    private AuthorizationHelper authorizationHelper;

    private final Gson gson;

    @Autowired
    public LetterboxService(LetterboxApiClient letterboxApiClient){
        this.letterboxApiClient = letterboxApiClient;
        this.gson = new Gson();
    }

    public Response getTemplates() throws IOException {
        retrofit2.Response<Response> response = letterboxApiClient.getTemplates().execute();
        if (response.isSuccessful()) {
            return response.body();
        } else {
            return new ErrorResponse<>(gson.fromJson(response.errorBody().string(), Response.class).getStatusMessage());
        }
    }

    public Response upload(UploadRequest uploadRequest) throws IOException {
        if(uploadRequest.getEmail() == null) {
            uploadRequest.setEmail(getEmail());
        }

        retrofit2.Response<Response> response = letterboxApiClient.processUpload(uploadRequest).execute();
        if (response.isSuccessful()) {
            return response.body();
        } else {
            return new ErrorResponse<>(gson.fromJson(response.errorBody().string(), Response.class).getStatusMessage());
        }
    }

    public Response getPresignedUrl(String fileName, String uploadType) throws IOException {
        retrofit2.Response<Response> response = letterboxApiClient.presignedUrl(fileName, uploadType).execute();
        if (response.isSuccessful()) {
            return response.body();
        } else {
            return new ErrorResponse<>(gson.fromJson(response.errorBody().string(), Response.class).getStatusMessage());
        }
    }

    public Response getUserAuditLogs() throws IOException {
        String email = getEmail();
        retrofit2.Response<Response> response = letterboxApiClient.userAuditLogs(email).execute();
        if (response.isSuccessful()) {
            return response.body();
        } else {
            return new ErrorResponse<>(gson.fromJson(response.errorBody().string(), Response.class).getStatusMessage());
        }
    }

    public Response getUserUploadStatus(String uploadType, int page) throws IOException {
        String email = getEmail();
        retrofit2.Response<Response> response = letterboxApiClient.userUploadsStatus(email, uploadType, page).execute();

        if (response.isSuccessful()) {
            return response.body();
        } else {
            return new ErrorResponse<>(gson.fromJson(response.errorBody().string(), Response.class).getStatusMessage());
        }
    }

    public String getEmail() {
        User user = authorizationHelper.fetchUser();
        return user.getEmail();
    }
}
