package com.swiggy.restaurantone.service;

import com.swiggy.annotations.cache.annotations.CacheDefaults;
import com.swiggy.annotations.cache.annotations.CacheResult;
import com.swiggy.commons.Json;
import com.swiggy.rest.dao.CrudDao;
import com.swiggy.rest.service.AbstractMappingConverter;
import com.swiggy.rest.service.CachedCrudBoService;
import com.swiggy.restaurantone.data.businessEntity.RestaurantUserMap;
import com.swiggy.restaurantone.data.businessEntity.SMRestaurantMap;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.data.coreEntity.RestaurantUserMapEntity;
import com.swiggy.restaurantone.dataAccess.RestaurantUserMapDao;
import com.swiggy.restaurantone.mapper.UserRestaurantMapMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by balamuneeswaran.s on 1/15/17.
 */
@CacheDefaults(prefix = "restaurantUserMap", deserialize = RestaurantUserMap.class)
@Service
public class RestaurantUserMapService extends CachedCrudBoService<RestaurantUserMapEntity, RestaurantUserMap> {
    private final RestaurantUserMapDao restaurantUserMapDao;
    private final UserRestaurantMapMapper userRestaurantMapMapper;
    private final UserRelationshipService userRelationshipService;
    private final CacheService cacheService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserService userService;

    @Autowired
    public RestaurantUserMapService(RestaurantUserMapDao restaurantUserMapDao, UserRestaurantMapMapper userRestaurantMapMapper, UserRelationshipService userRelationshipService, CacheService cacheService) {
        this.restaurantUserMapDao = restaurantUserMapDao;
        this.userRestaurantMapMapper = userRestaurantMapMapper;
        this.userRelationshipService = userRelationshipService;
        this.cacheService = cacheService;
    }

    @CacheResult(key = "#p0")
    public List<RestaurantUserMap> getRestaurantsByUserId(Long userId) {
        logger.debug("getRestaurantsByUserId Input: " + userId);
        List<RestaurantUserMapEntity> restaurantUserMapEntities = restaurantUserMapDao.findByUserId(userId);
        List<RestaurantUserMap> restaurantUserMaps = userRestaurantMapMapper.entityToBusinessObject(restaurantUserMapEntities);
        logger.debug("getRestaurantsByUserId Output: " + Json.serialize(restaurantUserMaps));
        return restaurantUserMaps;
    }

    public Long getRestaurantForRmsUser(Long userId) {
        List<RestaurantUserMap> restaurantUserMaps = this.getRestaurantsByUserId(userId);
        Long result;
        if (!restaurantUserMaps.isEmpty()) {
            result = restaurantUserMaps.get(0).getRestaurantId();
        } else {
            Set<Long> restaurants = this.getRestaurantsByParentUserId(userId);
            result = restaurants.isEmpty() ? null : restaurants.iterator().next();
        }
        return result;
    }

    public Set<Long> getRestaurantsByParentUserId(Long userId) {
        logger.debug("getRestaurantsByParentUserId Input: " + userId);

        List<Long> userIds = userRelationshipService
                .GetAllChildUsersByUserId(userId)
                .stream()
                .map(userRelationship -> userRelationship.childUserId)
                .collect(Collectors.toList());

        userIds.add(userId);
        List<RestaurantUserMapEntity> restaurantUserMapEntities = restaurantUserMapDao.findByUserIdIn(userIds);
        List<RestaurantUserMap> restaurantUserMaps = userRestaurantMapMapper.entityToBusinessObject(restaurantUserMapEntities);
        logger.debug("getRestaurantsByParentUserId Output: " + Json.serialize(restaurantUserMaps));
        return restaurantUserMaps.stream().map(restaurantUserMap -> restaurantUserMap.restaurantId)
                .collect(Collectors.toSet());
    }

    public List<RestaurantUserMap> updateRestaurantUserMap(List<RestaurantUserMap> restaurantUserMaps) {
        logger.debug("updateRestaurantUserMap Input: " + Json.serialize(restaurantUserMaps));
        List<RestaurantUserMap> restaurantUserMapList = new ArrayList<>();
        for (RestaurantUserMap restaurantUserMap :
                restaurantUserMaps) {
            restaurantUserMapList.add(updateRestaurantUserMap(restaurantUserMap));
        }
        logger.debug("updateRestaurantUserMap Output: " + Json.serialize(restaurantUserMapList));
        return restaurantUserMapList;
    }

    public RestaurantUserMap updateRestaurantUserMap(RestaurantUserMap restaurantUserMap) {
        RestaurantUserMapEntity restaurantUserMapEntity = restaurantUserMapDao.findOne(restaurantUserMap.id);
        cacheService.delete("restaurantUserMap-" + restaurantUserMapEntity.getUserId());
        cacheService.delete("restaurantUserMap-" + restaurantUserMap.userId);
        cacheService.delete("getRestaurantsByParentUserId-" + restaurantUserMap.userId);
        cacheService.delete("getRestaurantsByParentUserId-" + restaurantUserMapEntity.getUserId());
        return userRestaurantMapMapper.convertToBO(
                restaurantUserMapDao.save(
                        userRestaurantMapMapper.businessToEntity(restaurantUserMap)));
    }

    public RestaurantUserMap getRestaurantById(RestaurantUserMap restaurantUserMap) {
        RestaurantUserMapEntity entity = restaurantUserMapDao.findById(restaurantUserMap.id);
        return entity == null ? null : userRestaurantMapMapper.convertToBO(entity);
    }

    public RestaurantUserMap getMappingByUserIdAndRestId(Long userId, Long restaurantId) {
        RestaurantUserMapEntity entity = restaurantUserMapDao.findByUserIdAndAndRestaurantId(userId, restaurantId);
        return entity == null ? null : userRestaurantMapMapper.convertToBO(entity);
    }

    public List<RestaurantUserMap> getMappingsByRestaurantId(Long restaurantId) {
        List<RestaurantUserMapEntity> entities = restaurantUserMapDao.findByRestaurantId(restaurantId);

        return entities == null ? null : userRestaurantMapMapper.entityToBusinessObject(entities);
    }

    public List<RestaurantUserMap> getUsersByRestaurantsId(Long restaurantId) {
        logger.debug("getUsersByRestaurantsId Input: " + restaurantId);
        List<RestaurantUserMap> restaurantUserMaps = restaurantUserMapDao.findByRestaurantId(restaurantId).stream().map(userRestaurantMapMapper::convertToBO).collect(Collectors.toList());
        logger.debug("getRestaurantsByUserId Output: " + Json.serialize(restaurantUserMaps));
        return restaurantUserMaps;
    }

    public void updateMapping(SMRestaurantMap map) {
        logger.info("Email = " + map.getEmail() + " id " + map.getRestaurantId());
        User user = userService.find(map.getEmail());
        Long restaurantId = map.getRestaurantId();
        if (user == null || restaurantId == null)
            return;
        restaurantUserMapDao.deleteByRestaurantId(restaurantId);
        RestaurantUserMapEntity mapEntity = RestaurantUserMapEntity.builder()
                .restaurantId(restaurantId)
                .userId(user.getId())
                .build();
        restaurantUserMapDao.save(mapEntity);
    }

    public RestaurantUserMap addUserRestaurantMap(RestaurantUserMap restaurantUserMap) {
        RestaurantUserMapEntity mapEntity = RestaurantUserMapEntity.builder()
                .restaurantId(restaurantUserMap.restaurantId)
                .userId(restaurantUserMap.userId)
                .build();
        mapEntity = restaurantUserMapDao.save(mapEntity);
        cacheService.delete("restaurantUserMap-" + restaurantUserMap.userId);
        return userRestaurantMapMapper.convertToBO(mapEntity);
    }

    @Override
    public AbstractMappingConverter getMapper() {
        return userRestaurantMapMapper;
    }

    @Override
    public CrudDao getDaoManager() {
        return restaurantUserMapDao;
    }
}
