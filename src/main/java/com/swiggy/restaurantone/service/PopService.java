package com.swiggy.restaurantone.service;

import com.swiggy.commons.Json;
import com.swiggy.restaurantone.data.businessEntity.Batch;
import com.swiggy.restaurantone.helper.ClientHelper;
import com.swiggy.restaurantone.response.RestaurantBatches;
import com.swiggy.restaurantone.response.RestaurantContactResponse;
import com.swiggy.restaurantone.response.RestaurantListResponse;
import com.swiggy.restaurantone.response.SRSResponse;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by alok.singhal on 9/20/17.
 */
@Service
@Slf4j
public class PopService {

    @Autowired
    Environment environment;

    @Value("${baseurl.srs}")
    private String srsBaseUrl;

    public JSONArray getAllAreas() {
        try {
            return ClientHelper.srsAPIService(srsBaseUrl).getAllAreas().execute().body().getData();
        } catch (IOException ex) {
            log.error("Could not get all areas for pop: " + ex.getMessage());
            return null;
        }
    }

    public List<RestaurantBatches> getUnconfirmedBatches(List<Long> areaIds) {
        List<RestaurantContactResponse> restaurantContactResponses;
        try {
            SRSResponse<List<RestaurantContactResponse>> srsResponse =  ClientHelper.srsAPIService(srsBaseUrl).getRestaurantsInAreas(areaIds).execute().body();
            restaurantContactResponses = srsResponse.getData();
            log.info("Restaurant contact responses : {}", Json.serialize(restaurantContactResponses));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        Map<Long, RestaurantContactResponse> queryableRContactResponse = restaurantContactResponses.stream()
                .collect(Collectors.toMap(RestaurantContactResponse::getId, Function.identity()));
        Map<Long,List<Batch>> mapOfBatches;
        List<Long> restaurants = new ArrayList<>(queryableRContactResponse.keySet());
        RestaurantListResponse restaurantListResponse = RestaurantListResponse.builder().restaurantIds(restaurants).build();
        try {
            mapOfBatches = ClientHelper.placingAPIService(srsBaseUrl).getUnconfirmedBatches(restaurantListResponse).execute().body().getData();
            log.info("map of batches : {}", mapOfBatches);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return mapOfBatches.keySet().stream().map(restId -> RestaurantBatches.builder()
                .restaurantId(restId)
                .name(queryableRContactResponse.get(restId).getName())
                .phoneNumbers(queryableRContactResponse.get(restId).getPhoneNumbers())
                .batches(mapOfBatches.get(restId))
                .build())
                .collect(Collectors.toList());
    }

    public Object batchActions(JSONObject jsonObject) {
        try {
            Response<JSONObject> response = ClientHelper.placingAPIService(srsBaseUrl).batchActions(jsonObject).execute();

            return response.body().get("data");
        } catch (Exception ex) {
            return null;
        }
    }
}
