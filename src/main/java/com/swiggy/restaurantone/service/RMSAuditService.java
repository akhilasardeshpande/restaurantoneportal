package com.swiggy.restaurantone.service;

import com.swiggy.restaurantone.data.businessEntity.RMSUser;
import com.swiggy.restaurantone.data.coreEntity.RMSUserEntity;
import com.swiggy.restaurantone.dataAccess.RMSUserAuditDao;
import com.swiggy.restaurantone.mapper.RMSUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Date;

/**
 * Created by rajnish.chandra on 20/06/18
 */
@Service
public class RMSAuditService {
    private final RMSUserAuditDao rmsUserAuditDao;
    private final RMSUserMapper rmsUserMapper;
    
    @Autowired
    public RMSAuditService(RMSUserAuditDao rmsUserAuditDao, RMSUserMapper rmsUserMapper) {
        this.rmsUserAuditDao = rmsUserAuditDao;
        this.rmsUserMapper = rmsUserMapper;
    }
    
    void addToAuditTable(RMSUser rmsUser, Long ropUserId) {
        if (!ObjectUtils.isEmpty(rmsUser)) {
            rmsUser.setUpdatedAt(new Date(System.currentTimeMillis()));
            
            if (ObjectUtils.isEmpty(rmsUser.getUpdatedBy())) {
                rmsUser.setUpdatedBy(String.valueOf(ropUserId));
            }
            RMSUserEntity rmsUserEntity = rmsUserMapper.businessToEntity(rmsUser);
            rmsUserAuditDao.save(rmsUserEntity);
        }
    }
}
