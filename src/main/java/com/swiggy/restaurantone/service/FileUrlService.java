package com.swiggy.restaurantone.service;

import com.amazonaws.HttpMethod;
import com.swiggy.restaurantone.pojos.communication_service.UploadUrlRequest;
import com.swiggy.restaurantone.pojos.communication_service.UploadUrlResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

@Service
@Slf4j
public class FileUrlService {

    @Value("${cdn.domainName}")
    public String cdnDomain;

    @Autowired
    private AmazonS3ClientService amazonS3ClientService;

    public UploadUrlResponse fetchUploadUrl(UploadUrlRequest uploadUrlRequest) {
        String preSignedUrl = this.amazonS3ClientService.getPreSignedUrl(
                uploadUrlRequest.getFileName(),
                uploadUrlRequest.getContentType(),
                HttpMethod.PUT
        );
        String cdnUrl = null;
        if (uploadUrlRequest.isCdn()) {
            cdnUrl = computeCdnUrl(preSignedUrl);
        }
        return UploadUrlResponse.builder().cdnUrl(cdnUrl).preSignedUrl(preSignedUrl).build();
    }

    public String computeCdnUrl(String rawUrl) {
        log.info(rawUrl);
        return UriComponentsBuilder.fromUriString(rawUrl).host(cdnDomain).query(null).build().toString();
    }

}
