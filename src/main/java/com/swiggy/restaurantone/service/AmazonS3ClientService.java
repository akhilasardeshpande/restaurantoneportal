package com.swiggy.restaurantone.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;

@Service
@Slf4j
public class AmazonS3ClientService {
    private AmazonS3 s3Client;

    @Value("${amazon.s3.bucket.appNotification}")
    private String bucketName;

    @Autowired
    private void initialize(AmazonS3 s3Client) {
        this.s3Client = s3Client;
    }

    public String getPreSignedUrl(String fileName, String contentType, HttpMethod httpMethod) {
        Date expiration = new Date();
        long expTimeMillis = expiration.getTime();
        expTimeMillis += 1000 * 7 * 60 * 60;
        expiration.setTime(expTimeMillis);
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, fileName)
                .withMethod(httpMethod)
                .withExpiration(expiration);
        if (StringUtils.isNotEmpty(contentType)) {
            generatePresignedUrlRequest.setContentType(contentType);
        }
        return s3Client.generatePresignedUrl(generatePresignedUrlRequest).toString();
    }

    public BufferedReader getFileFromS3(String s3Url) {
        BufferedReader bufferedReader = null;
        try {
            URI fileToBeDownloaded = new URI(s3Url);
            AmazonS3URI s3URI = new AmazonS3URI(fileToBeDownloaded);
            S3Object s3Object = s3Client.getObject(s3URI.getBucket(), s3URI.getKey());
            bufferedReader = new BufferedReader(new InputStreamReader(s3Object.getObjectContent()));
        } catch (URISyntaxException | AmazonServiceException e) {
            log.error(e.getMessage(), e.getCause(), e.getStackTrace());
        }
        return bufferedReader;
    }

    public void Upload(String fileName, File file, String contentType) throws IOException {
        try {
            PutObjectRequest request = new PutObjectRequest(bucketName, fileName, file);
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(contentType);
            request.setMetadata(metadata);
            s3Client.putObject(request);
        } catch (AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            log.error(e.getMessage(), e.getCause(), e.getStackTrace());
        } catch (SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            log.error(e.getMessage(), e.getCause(), e.getStackTrace());
        }
    }
}
