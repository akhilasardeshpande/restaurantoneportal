package com.swiggy.restaurantone.service;

import com.swiggy.restaurantone.data.coreEntity.Session;
import com.swiggy.restaurantone.dataAccess.SessionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * Created by balamuneeswaran.s on 1/5/17.
 */
@Service
public class SessionService {
    private final SessionDao sessionDao;

    @Value("${sessionKey}")
    private String sessionListKey;

    @Value("${sessionExpiration}")
    private Long sessionExpiration;

    @Autowired
    public SessionService(SessionDao sessionDao) {
        this.sessionDao = sessionDao;
    }

    private void saveSessionList(Long id, List<Session> sessions) {
        sessionDao.saveSessionList(sessionListKey + id, sessions);
    }

    private List<Session> getSessions(Long id) {
        return sessionDao.getSessionList(sessionListKey + id);
    }

    public void saveSession(Session session) {
        Long id = session.getUserId();
        List<Session> sessions = getSessions(id);

        Iterator<Session> iterator = sessions.iterator();
        while (iterator.hasNext()) {
            Session currentSession = iterator.next();
            if (currentSession.getSessionId().equals(session.getSessionId())) {
                iterator.remove();
                break;
            }
        }
        sessions.add(session);
        saveSessionList(id, sessions);
    }

    public void deleteSession(Session session) {
        Long id = session.getUserId();
        List<Session> sessions = getSessions(id);
        Iterator<Session> iterator = sessions.iterator();
        while (iterator.hasNext()) {
            Session currentSession = iterator.next();
            if (currentSession.getSessionId().equals(session.getSessionId())) {
                iterator.remove();
                break;
            }
        }
        saveSessionList(id, sessions);
    }

    public void clearSessions(Long id) {
        sessionDao.deleteSessionList(sessionListKey + id);
    }

    public Session getSession(Long id, UUID sessionId) {
        return getSessions(id)
                .stream()
                .filter(session -> session.getSessionId().equals(sessionId))
                .findFirst()
                .orElse(null);
    }

    private Session createSession(Long userId, UUID sessionId, String authProperties, Long expiryTime, String authType) {
        return Session.builder()
                .userId(userId)
                .sessionId(sessionId)
                .authDetails(authProperties)
                .tokenExpirationTime(expiryTime)
                .authType(authType)
                .sessionExpiration( System.currentTimeMillis() + sessionExpiration)
                .build();
    }


    public void createAndSaveSession(Long userId, UUID sessionId, String authProperties, Long expiryTime, String authType) {
        saveSession(createSession(userId, sessionId, authProperties, expiryTime, authType));
    }

}
