package com.swiggy.restaurantone.service;

import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.swiggy.restaurantone.helper.IgccAttributionHelper;
import com.swiggy.restaurantone.pojos.igccAttribution.UploadResponse;
import com.swiggy.restaurantone.response.PresignedUrlResponse;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class IgccAttributionServiceImpl implements IgccAttributionService {

    private static final Logger logger = LoggerFactory.getLogger(IgccAttributionServiceImpl.class);
    private final S3Service s3Service;
    private final IgccAttributionHelper igccAttributionHelper;
    private String message;

    @Value("${local.file.path}")
    private String localFilePath;

    @Value("${igcc.s3.bucket}")
    private String igccS3Bucket;

    @Autowired
    public IgccAttributionServiceImpl(IgccAttributionHelper igccAttributionHelper, S3Service s3Service) {
        this.s3Service = s3Service;
        this.igccAttributionHelper = igccAttributionHelper;
    }

    @Override
    public UploadResponse syncIgccAttribution(final String key) {
        return igccAttributionHelper.syncIgccAttributionData(key, igccS3Bucket);
    }

    @Override
    public PresignedUrlResponse getPresignedUrl() {
        logger.info("Request Received for generating presigned url");
        PresignedUrlResponse response = null;
        try {
            response = s3Service.getPresignedUrl();
            logger.info("Presigned Url Generated - {}", response);
            return response;
        } catch (AmazonS3Exception ex) {
            logger.error("Error reading input file. Error: {}, StackTrace: {}", ex.getMessage(), ex.getCause());
        }

        return response;
    }

}

