package com.swiggy.restaurantone.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * Created by rajnishc on 2/6/17.
 */
@Component
public class CacheService {

    private final RedisTemplate<Object, Object> redisTemplate;

    @Autowired
    public CacheService(RedisTemplate<Object, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void set(Object key, Object value) {
        redisTemplate.opsForValue().set(key, value);
    }

    public Object get(Object key) {
        return redisTemplate.opsForValue().get(key);
    }

    public void delete(Object key) {
        redisTemplate.delete(key);
    }
}
