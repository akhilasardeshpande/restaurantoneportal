package com.swiggy.restaurantone.service;

import com.swiggy.restaurantone.data.businessEntity.RestaurantSpoc;
import org.springframework.stereotype.Service;

import java.io.InputStream;

@Service
public interface RestaurantSpocService {
    RestaurantSpoc getRestaurantSpoc(Long restaurantId, String userType);
    String updateRestaurantsSpoc(InputStream file, String updatedBy);
    void updateSpoc(RestaurantSpoc restaurantSpoc);
}
