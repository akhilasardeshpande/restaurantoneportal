package com.swiggy.restaurantone.service;

import com.swiggy.annotations.cache.annotations.CacheResult;
import com.swiggy.restaurantone.data.businessEntity.Permission;
import com.swiggy.restaurantone.data.businessEntity.Role;
import com.swiggy.restaurantone.data.coreEntity.PermissionEntity;
import com.swiggy.restaurantone.data.coreEntity.RoleEntity;
import com.swiggy.restaurantone.data.coreEntity.RolePermissionEntity;
import com.swiggy.restaurantone.dataAccess.RoleDao;
import com.swiggy.restaurantone.dataAccess.RolePermissionDao;
import com.swiggy.restaurantone.mapper.PermissionMapper;
import com.swiggy.restaurantone.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by balamuneeswaran.s on 1/16/17.
 */
@Service
public class RoleService {

    private final RoleDao roleDao;

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private RolePermissionDao rolePermissionDao;

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    public RoleService(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @CacheResult(
            key = "Roles"
    )
    public List<Role> findAll() {
        List<RoleEntity> roleLists = roleDao.findAll();
        return roleLists.stream().map(roleEntity -> roleEntity != null
                ? roleMapper.entityToBusiness(roleEntity)
                : null).collect(Collectors.toList());
    }

    public List<PermissionEntity> findPermissions(Long roleId) {
        return  roleDao.findOne(roleId).permissions;
    }

    public Role findRoleById(Long id) {
        return roleMapper.entityToBusiness(roleDao.findOne(id));
    }

    public Role findRoleName(String name) {
        return roleMapper.entityToBusiness(roleDao.findByRoleIgnoreCase(name));
    }

    public Role addRole(Role role) {
        RoleEntity roleEntity = roleMapper.businessRequestToEntity(role);
        if(roleEntity != null) {
            roleEntity = roleDao.save(roleEntity);
        }

        List<PermissionEntity> mappingList = addMapping(role, roleEntity);
        if(mappingList != null)
        roleEntity.setPermissions(mappingList);

        return roleMapper.entityToBusiness(roleEntity);
    }

    public Role editRole(Role role) {
        RoleEntity roleEntity = roleMapper.businessToEntity(role);
        RoleEntity existing = roleDao.findOne(role.id);

        if(roleEntity != null) {
            roleEntity = roleDao.save(roleEntity);
        }
        rolePermissionDao.deleteByRoleId(role.getId());

        List<PermissionEntity> mappingList = addMapping(role, roleEntity);

        if(mappingList != null)
            roleEntity.setPermissions(mappingList);
        return roleMapper.entityToBusiness(roleEntity);
    }

    private List<PermissionEntity> addMapping(Role role, RoleEntity roleEntity) {
        Iterator<Permission> iterator = role.getPermissions().iterator();
        List<PermissionEntity> mappingList = new LinkedList<>();
        while (iterator.hasNext()) {
            Permission permission = iterator.next();
            RolePermissionEntity entity  = RolePermissionEntity
                    .builder()
                    .roleId(roleEntity.getId())
                    .permissionId(permission.getId())
                    .build();
            RolePermissionEntity rolePermissionEntity = rolePermissionDao.save(entity);
            mappingList.add(permissionService.findPermissionEntity(rolePermissionEntity.getPermissionId()));
        }
        return mappingList;
    }
}
