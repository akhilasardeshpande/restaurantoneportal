package com.swiggy.restaurantone.service;

import com.swiggy.restaurantone.data.businessEntity.City;
import com.swiggy.restaurantone.data.businessEntity.sellerTier.SellerTierConfig;
import com.swiggy.restaurantone.pojos.requests.srs.PartnerTypeSearchRequest;
import com.swiggy.restaurantone.pojos.requests.srs.RestaurantHolidaySlotRequest;
import com.swiggy.restaurantone.pojos.requests.srs.RestaurantHolidaySlotResult;
import com.swiggy.restaurantone.pojos.response.RestaurantHolidaySlotDisposition;
import com.swiggy.restaurantone.response.RestaurantContactResponse;
import com.swiggy.restaurantone.response.SRSResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

/**
 * Created by shahbaz.khalid on 6/16/17.
 */
public interface SRSAPIService {

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("seller-tier/v1/metadata")
    Call<SRSResponse<JSONObject>> getMetadata();

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/seller-tier/v1/benchmark")
    Call<SRSResponse<JSONObject>> uploadBenchmark(@Body List input);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/seller-tier/v1/tier/{configId}")
    Call<SRSResponse<JSONObject>> uploadRestaurantTier(@Body SellerTierConfig input, @Path("configId") Integer configId);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/seller-tier/v1/config")
    Call<SRSResponse<JSONObject>> saveSellerTierConfig(@Body SellerTierConfig input);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/seller-tier/v1/config/{cityId}")
    Call<SRSResponse<JSONArray>> getBenchmark(@Path("cityId") Long cityId);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @PUT("/seller-tier/v1/tier")
    Call<SRSResponse<JSONObject>> scheduleUpdateTierJob();

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/seller-tier/v1/tier")
    Call<SRSResponse<String>> getUploadedTier();

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("area/getAll")
    Call<SRSResponse<JSONArray>> getAllAreas();

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("restaurant/getByAreaIds")
    Call<SRSResponse<List<RestaurantContactResponse>>> getRestaurantsInAreas(@Query("area_ids") List<Long> areaIds);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("city/all")
    Call<SRSResponse<List<City>>> getAllCities();

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("city/{cityId}")
    Call<SRSResponse<City>> getCityById(@Path("cityId") Long cityId);

    @Headers("Content-Type: application/json")
    @POST(value = "/holiday-slots/")
    Call<SRSResponse<RestaurantHolidaySlotResult>>
    addHolidaySlot(@Body RestaurantHolidaySlotRequest restaurantHolidaySlotRequest,
                   @Header("user_meta") String userMeta);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/partnermap/findByPartnerType")
    Call<SRSResponse<List<Long>>> getRestaurantIdsFromPartnerId(@Body PartnerTypeSearchRequest partnerTypeSearchRequest);

    @Headers("Content-Type: application/json")
    @DELETE(value = "/holiday-slots/{id}")
    Call<SRSResponse> deleteHolidaySlot(@Path("id") Long slotId, @Header("user_meta") String userMeta);

    @Headers("Content-Type: application/json")
    @GET(value = "/holiday-slots/")
    Call<SRSResponse<List<RestaurantHolidaySlotResult>>>
    getFutureHolidaySlotsOnRestaurant(@Query("restaurant_id") Long restaurantId);

    @Headers("Content-Type: application/json")
    @GET(value = "/restaurant/holiday-slots/dispositions")
    Call<SRSResponse<List<RestaurantHolidaySlotDisposition>>>
    getRestaurantHolidaySlotDispositions();

    @Headers({"Content-Type: application/json"})
    @GET("/restaurant/v2/search")
    Call<SRSResponse<List<Long>>> findByCityIdsAndPartnerTypes(@Query("city_ids") List<Integer> cityIds,
                                                               @Query("partner_types") List<Integer> typeOfPartner,
                                                               @Query("is_enable") Boolean enabled);


    @Headers({"Content-Type: application/json"})
    @GET("/restaurant/daily")
    Call<SRSResponse<List<Long>>> getAllDailyRestaurants();

}
