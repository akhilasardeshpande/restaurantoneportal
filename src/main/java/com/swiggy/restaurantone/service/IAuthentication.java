package com.swiggy.restaurantone.service;

import com.swiggy.restaurantone.data.ApplicationEntity.LoginUserInfo;
import com.swiggy.restaurantone.data.ApplicationEntity.UserAuthInfo;

import java.io.IOException;

/**
 * Created by rajnishc on 1/18/17.
 */
public interface IAuthentication {
    UserAuthInfo authenticate(LoginUserInfo loginInfo) throws IOException;

    String reAuthenticate(UserAuthInfo userAuthInfo);
}
