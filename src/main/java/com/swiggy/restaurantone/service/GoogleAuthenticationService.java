package com.swiggy.restaurantone.service;

import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.swiggy.commons.Json;
import com.swiggy.restaurantone.data.ApplicationEntity.GoogleAuthInfo;
import com.swiggy.restaurantone.data.ApplicationEntity.GoogleLoginInfo;
import com.swiggy.restaurantone.data.ApplicationEntity.LoginUserInfo;
import com.swiggy.restaurantone.data.ApplicationEntity.UserAuthInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Created by rajnishc on 1/18/17.
 */

@Service
public class GoogleAuthenticationService implements IAuthentication {

    @Value("${google.clientId}")
    private String clientId;

    @Value("${google.secretId}")
    private String secretId;

    @Value("${google.redirectUri}")
    private String redirectUri;

    @Value("${google.apiUrl}")
    private String googleApiUrl;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public UserAuthInfo authenticate(LoginUserInfo loginUserInfo) throws IOException {
        String code = Json.deserialize(loginUserInfo.getProperties(), GoogleLoginInfo.class).getCode();
        GoogleTokenResponse tokenResponse = new GoogleAuthorizationCodeTokenRequest(
                new NetHttpTransport(),
                JacksonFactory.getDefaultInstance(),
                googleApiUrl,
                clientId,
                secretId,
                code,
                redirectUri)
                .execute();
        String accessToken = tokenResponse.getAccessToken();
        String refreshToken = tokenResponse.getRefreshToken();
        GoogleIdToken idToken = tokenResponse.parseIdToken();
        GoogleIdToken.Payload payload = idToken.getPayload();
        GoogleAuthInfo googleAuthInfo = GoogleAuthInfo
                .builder()
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .build();

        return UserAuthInfo
                .builder()
                .email(payload.getEmail())
                .authProperties(Json.serialize(googleAuthInfo))
                .build();

    }

    @Override
    public String reAuthenticate(UserAuthInfo userAuthInfo) {

        GoogleAuthInfo googleAuthInfo = Json.deserialize(userAuthInfo.getAuthProperties(), GoogleAuthInfo.class);
        String token = googleAuthInfo.getRefreshToken();
        GoogleCredential credential = createCredentialWithRefreshToken(
                new NetHttpTransport(), JacksonFactory.getDefaultInstance(), new TokenResponse().setRefreshToken(token));
        boolean result;
        try {
            result = credential.refreshToken();
        } catch (IOException e) {
            logger.error("Error in refreshing access for the user " + e.getMessage());
            e.printStackTrace();
            return null;
        }
        if (result) {
            googleAuthInfo.setRefreshToken(credential.getRefreshToken());
            userAuthInfo.setAuthProperties(Json.serialize(googleAuthInfo));
        }
        return result ? Json.serialize(userAuthInfo) : null;
    }

    private GoogleCredential createCredentialWithRefreshToken(HttpTransport transport,
                                                              JsonFactory jsonFactory, TokenResponse tokenResponse) {
        return new GoogleCredential.Builder().setTransport(transport)
                .setJsonFactory(jsonFactory)
                .setClientSecrets(clientId, secretId)
                .build()
                .setFromTokenResponse(tokenResponse);
    }
}
