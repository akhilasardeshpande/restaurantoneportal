package com.swiggy.restaurantone.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.commons.Json;
import com.swiggy.commons.response.ErrorResponse;
import com.swiggy.commons.response.Response;
import com.swiggy.commons.response.SuccessResponse;
import com.swiggy.restaurantone.data.ApplicationEntity.PasswordPolicyDetails;
import com.swiggy.restaurantone.data.ApplicationEntity.RmsFetchPolicyRequest;
import com.swiggy.restaurantone.data.businessEntity.RMSUser;
import com.swiggy.restaurantone.helper.ClientHelper;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by shahbaz.khalid on 3/22/17.
 */

@Component
public class RMSUserService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private PasswordGenerator passwordGenerator;
    
    @Autowired
    private RMSAuditService rmsAuditService;

    @Value("${baseurl.rmsint}")
    private String rmsBaseUrl;

    public Response getUsersDetails(long userId) {
        Response response;
        try {
             return ClientHelper.rmsAPIService(rmsBaseUrl).getUser(userId).execute().body();
        } catch (Exception ex) {
            response = null;
            logger.error("unknown error occurred");
        }
        return response;
    }

    public Response getAllUsers(Long userId, String email, String userName, String mobile,
                                Long restaurantId, Integer offset, Integer limit) {
        Response response;
        try {
            return ClientHelper.rmsAPIService(rmsBaseUrl)
                    .getAllUsers(userId, email, userName, mobile, restaurantId, offset, limit).execute().body();
        } catch (Exception ex) {
            response = new ErrorResponse(ex.getMessage());
            logger.error("unknown error occurred");
        }
        return response;
    }

    public Response addRMSUser(JSONObject userDetails) {
        Response response;
        try {
            response = ClientHelper.rmsAPIService(rmsBaseUrl).createUser(userDetails).execute().body();
            if (response.getStatusCode() == 1) {
                logger.info("New RMS user is created by userId: " + getUserId());
            }
        } catch (Exception ex) {
            response = new ErrorResponse(ex.getMessage());
            logger.error("unknown error occurred");
        }
        return response;
    }

    public Response<JSONObject> addRMSUserV2(JSONObject userDetails) {
        Response<JSONObject> response;
        try {
            response = ClientHelper.rmsAPIService(rmsBaseUrl).createUserV2(userDetails).execute().body();
            if(response.getStatusCode() == 1) {
                logger.info("New RMS user created or updated. UserId : ", getUserId());
            }
        } catch (IOException ex) {
            response = new ErrorResponse(ex.getMessage());
            logger.error("IoException error occured. UserId : ", getUserId());
        }
        return response;
    }

    public Response updateRMSUser(long userId, JSONObject userDetails) {
        Response response;
        ObjectMapper mapper = new ObjectMapper();
        try {
            response = ClientHelper.rmsAPIService(rmsBaseUrl).updateUser(userId, userDetails).execute().body();
            if (response.getStatusCode() == 1) {
                logger.info("RMS user details for user: " + userId + " is updated by userId: " + getUserId());
                try {
                    RMSUser rmsUser = mapper.readValue(userDetails.toJSONString(), RMSUser.class);
                    rmsAuditService.addToAuditTable(rmsUser, getUserId());
                } catch (IOException e) {
                    logger.error("Exception while adding audit. Exception {}, Exception class {}", e.getMessage(), e.getClass());
                }
            }
        } catch (Exception ex) {
            response = new ErrorResponse(ex.getMessage());
            logger.error("unknown error occurred");
        }
        return response;
    }

    public Response addRestaurantToUser(long userId, JSONObject restaurantId) {
        Response response;
        try {
            response = ClientHelper.rmsAPIService(rmsBaseUrl).addRestaurantToUser(userId, restaurantId).execute().body();
            if (response.getStatusCode() == 1) {
                logger.info("Restaurant user mapping for user: " + userId + " and restaurant: " + restaurantId.get("rest_id") + " is created by userId: " + getUserId());
            }
        } catch (Exception ex) {
            response = new ErrorResponse(ex.getMessage());
            logger.error("unknown error occurred");
        }
        return response;
    }

    public Response removeRestaurantFromUser(long userId, long restaurantId, String updatedBy) {
        Response response;
        try {
            response = ClientHelper.rmsAPIService(rmsBaseUrl).removeRestaurantFromUser(userId, restaurantId, updatedBy).execute().body();
            if (response.getStatusCode() == 1) {
                logger.info("Restaurant user mapping for user: " + userId + " and restaurant: " + restaurantId + " is deleted by userId: " + getUserId());
            }
        } catch (Exception ex) {
            response = new ErrorResponse(ex.getMessage());
            logger.error("unknown error occurred");
        }
        return response;
    }

    public Response getRestaurantsForUser(long userId) {
        Response response;
        try {
            return ClientHelper.rmsAPIService(rmsBaseUrl).getRestaurantsForUser(userId).execute().body();
        } catch (Exception ex) {
            response = new ErrorResponse(ex.getMessage());
            logger.error("unknown error occurred");
        }
        return response;
    }

    public Response changePassword(JSONObject requestObject) {
        Response response;
        try {
            response = ClientHelper.rmsAPIService(rmsBaseUrl).changePassword(requestObject).execute().body();
            if (response.getStatusCode() == 1) {
                logger.info("Password for user " + requestObject.get("userId") + "is updated by userId: " + getUserId());
            }
        } catch (Exception ex) {
            response = new ErrorResponse(ex.getMessage());
            logger.error("unknown error occurred");
        }
        return response;
    }

    public Response getPolicyAndGeneratePassword(JSONObject requestObject) {
        Response response;
        String password = null;
        try {
            String isOperational = requestObject.toString();
            RmsFetchPolicyRequest rmsFetchPolicyRequest = Json.deserialize(isOperational, RmsFetchPolicyRequest.class);
            response = ClientHelper.rmsAPIService(rmsBaseUrl).getPasswordPolicy(rmsFetchPolicyRequest.getIsOperational()).execute().body();
            if (response.getStatusCode() == 0) {
                logger.info("Password config is fetched by user : " + getUserId());
                PasswordPolicyDetails policyDetails = Json.deserialize(response.getData().toString(), PasswordPolicyDetails.class);
                password = passwordGenerator.generatePassword(policyDetails.getPasswordPolicy());
                response = new SuccessResponse(password);
            } else {
                response = new ErrorResponse();
            }
        } catch (Exception ex) {
            logger.error("Error occurred : ", ex.getMessage());
            response = new ErrorResponse(ex.getMessage());
        }
        return response;
    }

    private Long getUserId() {
        String userIdString;
        Long userId;
        try {
            userIdString = httpServletRequest.getHeader("userId");
            if (userIdString.contains(".")) {
                return null;
            }
            userId = Long.parseLong(userIdString);
        } catch (Exception e) {
            logger.error("Error getting session info from header");
            return null;
        }

        return userId;
    }

    public Response logoutUser(JSONObject requestObject) {
        Response response;
        try {
            response = ClientHelper.rmsAPIService(rmsBaseUrl).logoutUser(requestObject).execute().body();
            if (response.getStatusCode() == 1) {
                logger.info("User " + requestObject.get("userId") + "has been logged out by userId : " + getUserId());
            }
        } catch (Exception ex) {
            String exceptionMessage = ex.getMessage();
            response = new ErrorResponse(exceptionMessage);
            logger.error("Exception occurred : " +exceptionMessage);
        }
        return response;
    }

    public Response forcePasswordChange(MultipartFile file, String email, Short typeOfUser) {
        Response response,rmsResponse;
        byte[] bytes;
        try {
            bytes = file.getBytes();
            String completeData = new String(bytes);
            String[] values = completeData.split(",");
            int[] intValues = new int[values.length];
            for (int i = 0; i < values.length; i++) {
                try {
                    intValues[i] = Integer.parseInt(values[i]);
                }
                catch (NumberFormatException nfe) {
                    String exceptionMessage = nfe.getMessage();
                    logger.info("Exception occurred : " + exceptionMessage);
                    return new ErrorResponse(exceptionMessage);
                }
            }
            if (intValues.length == 0) {
                return new ErrorResponse("Invalid input");
            }
            JSONObject input = new JSONObject();
            input.put("restaurants", intValues);
            input.put("email", email);
            input.put("type_of_user", typeOfUser);
            rmsResponse = ClientHelper.rmsAPIService(rmsBaseUrl).enforcePolicy(input).execute().body();
            if (rmsResponse.getStatusCode() == 0) {
                response = new SuccessResponse(null);
                response.setStatusMessage("File upload successful");
            } else {
                response = new ErrorResponse();
                response.setStatusMessage("File upload failed");
            }


        } catch (Exception ex) {
            String exceptionMessage = ex.getMessage();
            response = new ErrorResponse(exceptionMessage);
            logger.error("Exception occurred in outer catch: " + exceptionMessage);
        }
        return response;
    }
}
