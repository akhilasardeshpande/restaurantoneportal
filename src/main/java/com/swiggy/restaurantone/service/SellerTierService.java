package com.swiggy.restaurantone.service;

import com.swiggy.restaurantone.Constant.Tier;
import com.swiggy.restaurantone.data.businessEntity.sellerTier.SellerTierConfig;
import com.swiggy.restaurantone.data.businessEntity.sellerTier.SellerTierData;
import com.swiggy.restaurantone.helper.ClientHelper;
import com.swiggy.restaurantone.response.SRSResponse;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by shahbaz.khalid on 5/25/17.
 */
@Service
@Slf4j
public class SellerTierService {

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    Environment environment;

    @Value("${baseurl.srs}")
    private String srsBaseUrl;

    @Value("${baseurl.hg}")
    private String hgBaseUrl;

    private final Integer HEADER = 1;

    public JSONObject getMetadata() {
        try {
            return ClientHelper.srsAPIService(srsBaseUrl).getMetadata().execute().body().getData();
        } catch (IOException ex) {
            log.error("Could not get metadata for Seller Tier: " + ex.getMessage());
            return null;
        }

    }

    public JSONObject uploadBenchmark(JSONObject request) {
        try {
            List req = (ArrayList) request.get("data");
            return ClientHelper.srsAPIService(srsBaseUrl).uploadBenchmark(req).execute().body().getData();
        } catch (IOException ex) {
            log.error("Could not upload benchmark for Seller Tier: " + ex.getMessage());
            return null;
        }

    }

    public JSONArray getBenchmark(Long cityId) {
        try {
            return ClientHelper.srsAPIService(srsBaseUrl).getBenchmark(cityId).execute().body().getData();
        } catch (IOException ex) {
            log.error("Could not get benchmark for Seller Tier:" +ex.getMessage());
            return null;
        }
    }

    public String saveRestaurantDetails(InputStream file, String startDate,
                                        String endDate, String goLiveDate, String refreshDate) {
        List<SellerTierData> restaurantVersionDetails;
        String message = "Successfully uploaded all the restaurants";

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(file));
            try {
                // skip the header of the csv
                restaurantVersionDetails = br.lines().skip(HEADER).map(mapToItem)
                        .filter(r -> r != null).collect(Collectors.toList());
            } finally {
                br.close();
            }
        } catch (IOException ex) {
            log.error("There was an error in reading file: " + ex.getMessage());
            return ex.getMessage();
        }
        int size = restaurantVersionDetails.size();
        int updateTierBatchSize = Integer.parseInt(environment.getProperty("sellerTierUploadBatchSize","2000"));
        SellerTierConfig sellerTierConfig = SellerTierConfig.builder()
                .startDate(startDate)
                .endDate(endDate)
                .goLiveDate(goLiveDate)
                .refreshDate(refreshDate)
                .uploadedBy(getUserId().toString())
                .build();
        SRSResponse<JSONObject> response;
        try {
            response = ClientHelper.srsAPIService(srsBaseUrl)
                    .saveSellerTierConfig(sellerTierConfig).execute().body();
        } catch (IOException ex) {
            log.error("Upload failed " + ex.getMessage());
            return "Upload failed. Please try again.";
        }
        Integer configId = (Integer) response.getData().get("id");

        for (int i = 0; i <= (size / updateTierBatchSize); i++) {
            int start = i * updateTierBatchSize;
            int end = ((start + updateTierBatchSize) < size) ? (start + updateTierBatchSize - 1) : size - 1;
            SellerTierConfig sellerTierConfig1 = SellerTierConfig.builder()
                    .data(restaurantVersionDetails.subList(start, end + 1))
                    .startDate(startDate)
                    .endDate(endDate)
                    .goLiveDate(goLiveDate)
                    .refreshDate(refreshDate)
                    .build();
            try {
                ClientHelper.srsAPIService(srsBaseUrl)
                        .uploadRestaurantTier(sellerTierConfig1, configId).execute();
            } catch (IOException ex) {
                log.error("There was some error in uploading file. " + ex.getMessage());
                message = "There was some error in uploading file. Some of the records might not have been uploaded.";
            }
        }
        try {
            ClientHelper.srsAPIService(srsBaseUrl).scheduleUpdateTierJob().execute().body();
        } catch (IOException ex) {
            log.error("Could not update restaurant's tier in CMS");
        }
        return message;
    }

    public String getSampleFile() {
        return environment.getProperty("sellerTierSampleFileUrl");
    }

    public String getUploadedTier() {
        try {
            return ClientHelper.srsAPIService(srsBaseUrl).getUploadedTier().execute().body().getData();
        } catch (IOException ex) {
            log.error("Could not update restaurant's tier in CMS");
            return null;
        }
    }

    public Response<String> fileUploadUpdate(String s3Url, String month, String year, String type) {
        try {
            return ClientHelper.hgApiClient(hgBaseUrl).fileuploadUpdate(s3Url, month, year, type).execute();
        } catch (IOException e) {
            log.error("Could not call hungerGame service for file upload notification");
            return null;
        }
    }

    private Function<String, SellerTierData> mapToItem  = (line) -> {
        String[] p = line.split(",");// a CSV has comma separated lines
        try {
            if (!contains(p[1])) {
                throw new RuntimeException("Invalid tier value: " + p[1]);
            }
            SellerTierData item = SellerTierData.builder()
                    .restId(Long.parseLong(p[0]))
                    .tier(p[1])
                    .build();
            return item;
        } catch (Exception ex) {
            log.error("Parse exception: " + ex.getMessage());
            throw new RuntimeException(ex);
        }
    };

    private boolean contains(String test) {

        for (Tier tier : Tier.values()) {
            if (tier.name().equals(test)) {
                return true;
            }
        }

        return false;
    }

    private Long getUserId() {
        String userIdString;
        Long userId;
        try {
            userIdString = httpServletRequest.getHeader("userId");
            if (userIdString.contains(".")) {
                return null;
            }
            userId = Long.parseLong(userIdString);
        } catch (Exception e) {
            log.error("Error getting session info from header");
            return null;
        }

        return userId;
    }
}
