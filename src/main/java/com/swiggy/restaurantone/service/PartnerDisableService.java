package com.swiggy.restaurantone.service;

import com.swiggy.restaurantone.Constant.PartnerDisableConstants;
import com.swiggy.restaurantone.data.businessEntity.PartnerOrRestaurantDisableRequest;
import com.swiggy.restaurantone.data.businessEntity.PartnerEnableRequest;
import com.swiggy.restaurantone.data.coreEntity.PartnerDisableRunInfoEntity;
import com.swiggy.restaurantone.dataAccess.PartnerDisableRunInfoDao;
import com.swiggy.restaurantone.helper.PartnerDisableEnableHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class PartnerDisableService {
    private final PartnerDisableRunInfoDao partnerDisableRunInfoDao;
    private final PartnerDisableEnableHelper partnerDisableEnableHelper;
    
    @Autowired
    public PartnerDisableService(PartnerDisableRunInfoDao partnerDisableRunInfoDao,
                                 PartnerDisableEnableHelper partnerDisableEnableHelper) {
        this.partnerDisableRunInfoDao = partnerDisableRunInfoDao;
        this.partnerDisableEnableHelper = partnerDisableEnableHelper;
    }
    
    public PartnerDisableRunInfoEntity disablePartner(PartnerOrRestaurantDisableRequest
                                                              partnerOrRestaurantDisableRequest) {
        PartnerDisableRunInfoEntity partnerDisableRunInfoEntity = PartnerDisableRunInfoEntity.builder()
                .fromTime(partnerOrRestaurantDisableRequest.getFromTime())
                .toTime(partnerOrRestaurantDisableRequest.getToTime())
                .partnerId(partnerOrRestaurantDisableRequest.getPartnerId())
                .dispositionId(partnerOrRestaurantDisableRequest.getDispositionId())
                .createdAt(LocalDateTime.now())
                .createdBy(partnerOrRestaurantDisableRequest.getUpdatedBy())
                .statusCode(PartnerDisableConstants.DISABLE_PENDING_CODE)
                .reason(partnerOrRestaurantDisableRequest.getReason())
                .statusMessage(PartnerDisableConstants.PARTNER_DISABLE_PENDING)
                .build();
        
        partnerDisableRunInfoEntity = partnerDisableRunInfoDao.save(partnerDisableRunInfoEntity);
        partnerDisableEnableHelper.closePartnerOrRestaurant
                (partnerOrRestaurantDisableRequest, partnerDisableRunInfoEntity.getId());
        return partnerDisableRunInfoEntity;
    }
    
    public PartnerDisableRunInfoEntity enablePartner(PartnerEnableRequest partnerEnableRequest) {
        PartnerDisableRunInfoEntity partnerDisableRunInfoEntity = partnerDisableRunInfoDao
                .findOne(partnerEnableRequest.getRunInfoId());
        
        partnerDisableRunInfoEntity.setUpdatedAt(LocalDateTime.now());
        partnerDisableRunInfoEntity.setUpdatedBy(partnerEnableRequest.getUpdatedBy());
        partnerDisableRunInfoEntity.setStatusCode(PartnerDisableConstants.ENABLE_PENDING_CODE);
        partnerDisableRunInfoEntity.setStatusMessage(PartnerDisableConstants.PARTNER_ENABLE_PENDING);
        partnerDisableEnableHelper.deleteHolidaySlotsOnRestaurant(partnerDisableRunInfoEntity.getToTime(),
                partnerEnableRequest);
        
        return partnerDisableRunInfoDao.save(partnerDisableRunInfoEntity);
    }
    
    public PartnerDisableRunInfoEntity getRunIfo(Long id) {
        PartnerDisableRunInfoEntity partnerDisableRunInfoEntity = partnerDisableRunInfoDao.findOne(id);
        
        if (ObjectUtils.isEmpty(partnerDisableRunInfoEntity)) {
            return null;
        }
        
        boolean isActive = !partnerDisableRunInfoEntity.getToTime().isBefore(LocalDateTime.now());
        
        //Changing the status of holiday slot is toTime is earlier than now
        if (partnerDisableRunInfoEntity.isActive() && isActive != partnerDisableRunInfoEntity.isActive()) {
            partnerDisableRunInfoEntity.setActive(isActive);
            partnerDisableRunInfoDao.save(partnerDisableRunInfoEntity);
        }
        
        return partnerDisableRunInfoEntity;
    }
    
    public List<PartnerDisableRunInfoEntity> getAllRunIfo() {
        Pageable limit = new PageRequest(0,20);
        List<PartnerDisableRunInfoEntity> partnerDisableRunInfoEntities = partnerDisableRunInfoDao
                .findAllByOrderByIdDesc(limit);
        
        if (ObjectUtils.isEmpty(partnerDisableRunInfoEntities)) {
            return null;
        }
        
        for (PartnerDisableRunInfoEntity partnerDisableRunInfoEntity : partnerDisableRunInfoEntities) {
            boolean isActive = !partnerDisableRunInfoEntity.getToTime().isBefore(LocalDateTime.now());
            
            //Changing the status of holiday slot is toTime is earlier than now
            if (partnerDisableRunInfoEntity.isActive() && isActive != partnerDisableRunInfoEntity.isActive()) {
                partnerDisableRunInfoEntity.setActive(isActive);
                partnerDisableRunInfoDao.save(partnerDisableRunInfoEntity);
            }
        }
        
        return partnerDisableRunInfoEntities;
    }


}