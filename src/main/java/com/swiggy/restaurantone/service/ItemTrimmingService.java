package com.swiggy.restaurantone.service;

import java.io.InputStream;

public interface ItemTrimmingService {
    String applyHolidaySlot(InputStream file);
}
