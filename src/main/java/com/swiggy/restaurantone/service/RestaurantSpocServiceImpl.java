package com.swiggy.restaurantone.service;

import com.swiggy.restaurantone.data.businessEntity.*;
import com.swiggy.restaurantone.helper.RestaurantSpocHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RestaurantSpocServiceImpl implements RestaurantSpocService {
    private static final Integer HEADER = 1;

    private static final Long SM_ROLE_ID = 16l;

    private static final Long ASM_ROLE_ID = 7l;

    private static final Long ASM_MANAGER_ID = 868l;

    private static final String ASM_MANAGER_NAME = "ASM Manager";

    private final RestaurantSpocHelper restaurantSpocHelper;

    private final RoleService roleService;

    @Value("${restaurant.spoc.support.email}")
    private String supportEmail;

    @Value("${restaurant.spoc.support.phone}")
    private String supportPhone;
    
    @Value("${restaurant.spoc.support.email-daily}")
    private String supportEmailDaily;
    
    @Value("${restaurant.spoc.support.phone-daily}")
    private String supportPhoneDaily;
    

    @Autowired
    public RestaurantSpocServiceImpl(RestaurantSpocHelper restaurantSpocHelper, RoleService roleService) {
        this.restaurantSpocHelper = restaurantSpocHelper;
        this.roleService = roleService;
    }

    /**
     * update restaurant SPOC(SM and ASM) detail
     *
     * @param restaurantSpoc
     */
    @Override
    public void updateSpoc(RestaurantSpoc restaurantSpoc) {
        // create ASM user
        User newAsmUser = User.builder()
                .name(restaurantSpoc.getAsmName())
                .email(restaurantSpoc.getAsmEmail())
                .contactNumber(restaurantSpoc.getAsmMobile())
                .role(roleService.findRoleById(ASM_ROLE_ID))
                .manager(Manager.builder().id(ASM_MANAGER_ID).name(ASM_MANAGER_NAME).build())
                .city(City.builder().id(1l).name("Bangalore").build())
                .team(Team.builder().id(1l).name("city team").build())
                .updatedBy(restaurantSpoc.getUpdatedBy())
                .build();
        //update ASM user and its restaurant mapping
        User updatedAsmUser = restaurantSpocHelper.updateSpoc(newAsmUser);
        updatedAsmUser.setUpdatedBy(restaurantSpoc.getUpdatedBy());
        RestaurantUserMap updatedRestaurantAsmMap = restaurantSpocHelper.updateRestaurantSpocMap(restaurantSpoc.getRestaurantId(), updatedAsmUser);

        //create SM user
        User newSmUser = User.builder()
                .name(restaurantSpoc.getSmName())
                .email(restaurantSpoc.getSmEmail())
                .contactNumber(restaurantSpoc.getSmMobile())
                .role(roleService.findRoleById(SM_ROLE_ID))
                .manager(Manager.builder().id(updatedAsmUser.getId()).name(updatedAsmUser.name).build())
                .city(newAsmUser.getCity())
                .updatedBy(restaurantSpoc.getUpdatedBy())
                .build();
        //update SM user and its restaurant mapping
        User updatedSmUser = restaurantSpocHelper.updateSpoc(newSmUser);
        updatedSmUser.setUpdatedBy(restaurantSpoc.getUpdatedBy());
        RestaurantUserMap updatedRestaurantSmMap = restaurantSpocHelper.
                updateRestaurantSpocMap(restaurantSpoc.getRestaurantId(), updatedSmUser);

        // on successful update trigger notification event
        restaurantSpocHelper.sendSpocNotification(restaurantSpoc);
    }

    /**
     * get restaurant spoc for given restaurant id
     *
     * @param restaurantId
     * @return
     */
    @Override
    public RestaurantSpoc getRestaurantSpoc(Long restaurantId, String userType) {
        Optional<User> smOptional = restaurantSpocHelper.getSpoc(restaurantId, SM_ROLE_ID);
        Optional<User> asmOptional = restaurantSpocHelper.getSpoc(restaurantId, ASM_ROLE_ID);
        boolean isDaily = !ObjectUtils.isEmpty(userType) && userType.equals("daily");
        
        RestaurantSpoc restaurantSpoc = RestaurantSpoc.builder()
                .restaurantId(restaurantId)
                .supportEmail(isDaily ? supportEmailDaily : supportEmail)
                .supportPhone(isDaily ? supportPhoneDaily : supportPhone)
                .build();
        if (smOptional.isPresent()) {
            User sm = smOptional.get();
            restaurantSpoc.setSmName(sm.getName());
            restaurantSpoc.setSmEmail(sm.getEmail());
            restaurantSpoc.setSmMobile(sm.getContactNumber());
        }
        if (asmOptional.isPresent()) {
            User asm = asmOptional.get();
            restaurantSpoc.setAsmName(asm.getName());
            restaurantSpoc.setAsmEmail(asm.getEmail());
            restaurantSpoc.setAsmMobile(asm.getContactNumber());
        }
        return restaurantSpoc;
    }

    /**
     * update restaurants spoc
     *
     * @param file
     * @return
     */
    @Override
    public String updateRestaurantsSpoc(InputStream file, String updatedBy) {
        String message = "success";
        List<RestaurantSpoc> restaurantSpocs = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(file));
            try {
                restaurantSpocs = br.lines().skip(HEADER).map(mapToSpoc)
                        .filter(r -> r != null).collect(Collectors.toList());
            } finally {
                br.close();
            }
        } catch (Exception ex) {
            message = "Error reading input file";
            log.error("Message: {}, Error: {}, StackTrace: {}",message, ex.getMessage(), ex.getCause());
        }
        restaurantSpocHelper.updateRestaurantsSpoc(restaurantSpocs, updatedBy);
        return message;
    }

    private Function<String, RestaurantSpoc> mapToSpoc = (line) -> {
        String[] spocDetails = line.split(",");// a CSV has comma separated lines
        try {
            RestaurantSpoc restaurantSpoc = RestaurantSpoc.builder()
                    .restaurantId(Long.parseLong(spocDetails[0]))
                    .smName(spocDetails[1])
                    .smEmail(spocDetails[2])
                    .smMobile(spocDetails[3])
                    .asmName(spocDetails[4])
                    .asmEmail(spocDetails[5])
                    .asmMobile(spocDetails[6])
                    .build();
            return restaurantSpoc;
        } catch (Exception ex) {
            log.error("Error while parsing spoc update file. Error: {}, StackTrace: {} " + ex.getMessage(), ex.getCause());
            throw new RuntimeException(ex);
        }
    };
}
