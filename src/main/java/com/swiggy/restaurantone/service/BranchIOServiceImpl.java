package com.swiggy.restaurantone.service;

import com.swiggy.restaurantone.helper.ClientHelper;
import com.swiggy.restaurantone.pojos.branchIO.Data;
import com.swiggy.restaurantone.pojos.branchIO.DeepLinkRequest;
import com.swiggy.restaurantone.pojos.branchIO.DeepLinkResponse;
import com.swiggy.restaurantone.pojos.branchIO.ExtraData;
import org.springframework.beans.factory.annotation.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.IOException;

@Service
@Slf4j
public class BranchIOServiceImpl implements BranchIOService {
    @Value("${restaurant.branch.key}")
    private String branchKey;

    @Value("${restaurant.spoc.branch.campaign}")
    private String spocCampaign;

    @Value("${restaurant.spoc.branch.channel}")
    private String spocChannel;

    @Value("${restaurant.branch.orgTitle}")
    private String orgTitle;

    @Value("${restaurant.branch.orgDescription}")
    private String orgDescription;

    @Value("${restaurant.branch.orgImage}")
    private String orgImageUrl;

    @Value("${restaurant.branch.desktopUrl}")
    private String desktopUrl;

    @Value("${restaurant.branch.androidUrl}")
    private String androidUrl;

    @Value("${restaurant.branch.iosUrl}")
    private String iosUrl;

    @Value("${restaurant.spoc.branch.screenName}")
    private String spocScreenName;

    @Value("${restaurant.spoc.branch.feature}")
    private String spocFeature;

    @Override
    public String generateRestaurantSpocDeepLink(Long restaurantId) {
        DeepLinkResponse deepLinkResponse;
        DeepLinkRequest request = DeepLinkRequest.builder()
                .branchKey(branchKey)
                .campaign(spocCampaign)
                .channel(spocChannel)
                .data(Data.builder()
                        .orgTitle(orgTitle)
                        .orgDescription(orgDescription)
                        .orgImageUrl(orgImageUrl)
                        .desktopUrl(desktopUrl+String.valueOf(restaurantId))
                        .androidUrl(androidUrl)
                        .iosUrl(iosUrl)
                        .extraData(ExtraData.builder()
                                .restaurantID(restaurantId)
                                .build())
                        .screenName(spocScreenName)
                        .build())
                .feature(spocFeature)
                .build();
        try{
            Response<DeepLinkResponse> response = ClientHelper
                    .branchIOClient("https://api.branch.io/")
                    .generateDeepLink(request).execute();
            deepLinkResponse = response.body();
        }catch (IOException e){
            throw new RuntimeException(e.getMessage(), e.getCause());
        }
        return deepLinkResponse.getUrl();
    }
}
