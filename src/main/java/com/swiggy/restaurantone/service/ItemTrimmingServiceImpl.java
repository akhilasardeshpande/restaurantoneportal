package com.swiggy.restaurantone.service;

import com.swiggy.restaurantone.Constant.ItemTrimmingConstants;
import com.swiggy.restaurantone.helper.ItemTrimmingHelper;
import com.swiggy.restaurantone.pojos.ItemTrimming.ItemData;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.swiggy.restaurantone.Constant.ItemTrimmingConstants.HEADER;
import static com.swiggy.restaurantone.Constant.ItemTrimmingConstants.SUCCESS;

@Service
@Slf4j
public class ItemTrimmingServiceImpl implements ItemTrimmingService {

    private final ItemTrimmingHelper itemTrimmingHelper;
    private String message;

    @Autowired
    public ItemTrimmingServiceImpl(ItemTrimmingHelper itemTrimmingHelper) {
        this.itemTrimmingHelper = itemTrimmingHelper;
    }

    @Override
    public String applyHolidaySlot(InputStream file) {
        message = SUCCESS;
        List<ItemData> itemDataList;
        List<List<ItemData>> itemDataListBatches;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(file));
            try {
                itemDataList = br.lines().skip(HEADER).map(mapToItemData)
                        .filter(r -> r != null).collect(Collectors.toList());
                itemDataListBatches = batchItemsData(itemDataList);
            } finally {
                br.close();
            }
            itemTrimmingHelper.applyHolidaySlotAndSendNotification(itemDataListBatches);
        } catch (IOException ex) {
            message = "Error reading input file";
            log.error("Error reading input file. Error: {}, StackTrace: {}", ex.getMessage(), ex.getCause());
        }

        return message;
    }

    private List<List<ItemData>> batchItemsData(List<ItemData> itemsData){
        return ListUtils.partition(itemsData, ItemTrimmingConstants.BATCH_SIZE);
    }

    private Function<String, ItemData> mapToItemData = (line) -> {
        String[] itemDetails = line.split(",");
        try {
            ItemData itemData = ItemData.builder()
                    .restaurantId(itemDetails[0])
                    .itemId(itemDetails[1])
                    .itemName(itemDetails[2])
                    .build();
            return itemData;
        } catch (Exception ex) {
            log.error("Parse exception: " + ex.getMessage());
            throw new RuntimeException(ex);
        }
    };
}
