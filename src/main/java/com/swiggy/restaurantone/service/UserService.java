package com.swiggy.restaurantone.service;

import com.swiggy.annotations.cache.annotations.CacheDefaults;
import com.swiggy.annotations.cache.annotations.CachePut;
import com.swiggy.annotations.cache.annotations.CacheResult;
import com.swiggy.commons.Json;
import com.swiggy.rest.dao.CrudDao;
import com.swiggy.rest.service.AbstractMappingConverter;
import com.swiggy.rest.service.CachedCrudBoService;
import com.swiggy.restaurantone.data.businessEntity.*;
import com.swiggy.restaurantone.data.coreEntity.UserEntity;
import com.swiggy.restaurantone.data.coreEntity.UserRelationshipEntity;
import com.swiggy.restaurantone.dataAccess.UserDao;
import com.swiggy.restaurantone.dataAccess.UserRelationshipDao;
import com.swiggy.restaurantone.mapper.ManagerMapper;
import com.swiggy.restaurantone.mapper.UserMapper;
import com.swiggy.restaurantone.mapper.UserRelationshipMapper;
import com.swiggy.restaurantone.validation.UserRelationshipValidation;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by balamuneeswaran.s on 1/3/17.
 */
@Service
@CacheDefaults(prefix = "user", deserialize = User.class)
public class UserService extends CachedCrudBoService<UserEntity, User> {

    private final UserMapper userMapper;
    private UserRelationshipService userRelationshipService;
    private UserRelationshipDao userRelationshipDao;
    private UserRelationshipMapper userRelationshipMapper;
    private final UserDao userDao;
    @Autowired
    private  HttpServletRequest httpServletRequest;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private UserRelationshipValidation userRelationshipValidation;

    @Autowired
    private ManagerMapper managerMapper;

    @Autowired
    public UserService(UserMapper userMapper, UserDao userDao,UserRelationshipService userRelationshipService,UserRelationshipDao relationshipDao,UserRelationshipMapper userRelationshipMapper) {
        this.userMapper = userMapper;
        this.userDao = userDao;
        this.userRelationshipService = userRelationshipService;
        this.userRelationshipDao = relationshipDao;
        this.userRelationshipMapper = userRelationshipMapper;
    }

    @CacheResult(
            key = "#p0"
    )
    public User find(Long id) {
        UserEntity userEntity = userDao.findOne(id);
        return userMapper.entityToBusinessObject(userEntity);
    }


    @Transactional
    public User find(String email) {
        UserEntity userEntity = userDao.findByEmail(email);
        return userMapper.entityToBusinessObject(userEntity);
    }

    public List<Manager> findNamesLike(String name) {
        return managerMapper.convertToBusinessEntity(userDao.findTop15ByNameStartingWithIgnoreCase(name));
    }

    public List<User> getUsersByCriteria(UserCriteria userCriteria) {
        logger.info("getUsersByCriteria Input : " + Json.serialize(userCriteria));
        userCriteria.idList = userCriteria.idList == null ? new ArrayList<>() : userCriteria.idList;
        userCriteria.emailList = userCriteria.emailList == null ? new ArrayList<>() : userCriteria.emailList;
        userCriteria.mobileNumberList = userCriteria.mobileNumberList == null ? new ArrayList<>() : userCriteria.mobileNumberList;
        Pagination pagination = userCriteria.pagination == null ? new Pagination() : userCriteria.pagination;
        PageRequest pageRequest = new PageRequest(pagination.pageNumber - 1, pagination.size, pagination.isAscending ? Sort.Direction.ASC : Sort.Direction.DESC, pagination.sortColumn);

        List<UserEntity> userEntities = userDao.findByIdInOrEmailInOrContactNumberIn(userCriteria.idList,
                userCriteria.emailList, userCriteria.mobileNumberList, pageRequest);

        List<User> users = userMapper.entityToBusinessObject(userEntities);
        logger.debug("getUsersByCriteria Output : " + Json.serialize(users));
        return users;
    }

    @CachePut(
            key = "#p0.id"
    )
    public User updateUser(User user) {
        logger.info("updateUser Input : " + Json.serialize(user));
        UserEntity userEntity = userMapper.businessRequestToEntity(user);
        UserEntity userEntity1 = userDao.findOne(user.getId());
        userEntity.setPermissions(userEntity1.getPermissions());
        logger.info("User entity" + userEntity);
        if(userEntity == null)
            return null;
        UserRelationship userRelationship = UserRelationship.builder()
                .childUserId(user.getId())
                .userId(user.getManager().id)
                .updatedBy(user.getUpdatedBy())
                .build();
        List<UserRelationship> list = new ArrayList<>();
        list.add(userRelationship);
        userRelationshipService.updateUserManagerRelationship(list);

        userEntity = userDao.save(userEntity);
        logger.debug("updateUser Output : " + Json.serialize(userEntity));

        user = userMapper.entityToBusinessObject(userEntity);
        logger.info("Updated user : " + user );
        return user;
    }

    @Transactional
    public User addUser(User user) throws ConstraintViolationException {
        String createdBy;
        if(StringUtils.isNotEmpty(user.getUpdatedBy())){
            createdBy = user.getUpdatedBy();
        }else{
            createdBy = httpServletRequest.getHeader("userId");
        }
        UserCriteria userCriteria = new UserCriteria();
        userCriteria.emailList = new ArrayList<>();
        userCriteria.emailList.add(user.email);
        List<User> existingUser = getUsersByCriteria(userCriteria);
        if (existingUser!= null && existingUser.size() > 0) {
            return  existingUser.get(0);
        }
        UserEntity userEntity = userMapper.businessRequestToEntity(user);
        if(userEntity == null)
            return null;
        UserEntity entity = userDao.save(userEntity);
        List<UserRelationshipEntity> userRelationshipEntities = new ArrayList<>();
        userRelationshipEntities.add(new UserRelationshipEntity(user.getManager().getId(), entity.getId(), true, new Date(), new Date(), createdBy, createdBy));
        List<UserRelationship> parentBusinessEntity = userRelationshipService.GetImmediateParentUsersByUserId(user.getManager().getId());
        parentBusinessEntity.forEach(businessEntity -> businessEntity.setUpdatedBy(createdBy));

        while (parentBusinessEntity.size() != 0) {
            List<UserRelationshipEntity> parentCoreEntities = userRelationshipMapper.businessToEntity(parentBusinessEntity);
            parentCoreEntities.forEach(parentCoreEntity -> userRelationshipEntities.add(new UserRelationshipEntity(parentCoreEntity.userId, entity.getId(), false, new Date(), new Date(), createdBy, createdBy)));

            List<UserRelationship> result = new ArrayList<>();
            parentCoreEntities.forEach(parentCoreEntity -> result.addAll(userRelationshipService.GetImmediateParentUsersByUserId(parentCoreEntity.userId)));
            result.forEach(resultEntity -> resultEntity.setUpdatedBy(createdBy));
            parentBusinessEntity = result;
        }
        userRelationshipDao.save(userRelationshipEntities);
        return userMapper.entityToBusinessObject(entity);

    }

    @Override
    public CrudDao getDaoManager() {
        return userDao;
    }

    @Override
    public AbstractMappingConverter getMapper() {
        return userMapper;
    }

}
