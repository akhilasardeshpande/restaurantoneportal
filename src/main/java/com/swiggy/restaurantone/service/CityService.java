package com.swiggy.restaurantone.service;

import com.swiggy.annotations.cache.annotations.CacheDefaults;
import com.swiggy.annotations.cache.annotations.CacheResult;
import com.swiggy.restaurantone.data.businessEntity.City;
import com.swiggy.restaurantone.dataAccess.CityDao;
import com.swiggy.restaurantone.mapper.CityMapper;
import com.swiggy.restaurantone.response.SRSResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by rajnishc on 5/6/17.
 */
@Service
@CacheDefaults(prefix = "city", deserialize = City.class)
public class CityService {
    private final CityDao cityDao;
    private final CityMapper cityMapper;

    @Autowired
    private SRSAPIService srsAPIService;

    @Autowired
    public CityService(CityDao cityDao, CityMapper cityMapper) {
        this.cityDao = cityDao;
        this.cityMapper = cityMapper;
    }

    @CacheResult(key = "#p0")
    public City findCity(Long id) {
        SRSResponse<City> response;
        try {
            response = srsAPIService.getCityById(id).execute().body();
        }
        catch (Exception e) {
            return null;
        }

        City city = response.getData();
        return city;
    }

    public List<City> findAllCities() {
        SRSResponse<List<City>> response;
        try {
            response = srsAPIService.getAllCities().execute().body();
        }
        catch (Exception e) {
            return null;
        }

        List<City> cities = response.getData();
        return cities;
    }
}
