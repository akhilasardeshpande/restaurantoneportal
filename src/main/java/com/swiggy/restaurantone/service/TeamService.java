package com.swiggy.restaurantone.service;

import com.swiggy.restaurantone.data.businessEntity.Team;
import com.swiggy.restaurantone.data.coreEntity.TeamEntity;
import com.swiggy.restaurantone.dataAccess.TeamDao;
import com.swiggy.restaurantone.mapper.TeamMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by rajnishc on 5/5/17.
 */
@Service
public class TeamService {
    @Autowired
    TeamDao teamDao;

    @Autowired
    TeamMapper teamMapper;
    public Team find(Long id) {
        TeamEntity teamEntity = teamDao.findOne(id);
        return teamEntity != null ? teamMapper.entityToBusiness(teamEntity) : null;
    }
    public List<Team> findAll() {
        List<TeamEntity> teamList = teamDao.findAll();
        return teamList.stream().map(teamEntity -> teamEntity != null
                ? teamMapper.entityToBusiness(teamEntity)
                : null).collect(Collectors.toList());
    }
}
