package com.swiggy.restaurantone.service;

import com.swiggy.commons.response.Response;;
import org.json.simple.JSONObject;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

/**
 * Created by shahbaz.khalid on 4/26/17.
 */
public interface RMSAPIService {

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("profile/v1/createUser/")
    Call<Response<String>> createUser(@Body JSONObject input);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("contactability/updateUser/")
    Call<Response<JSONObject>> createUserV2(@Body JSONObject input);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @PUT("profile/v1/updateUser/{userId}")
    Call<Response<String>> updateUser(@Path("userId") long userId, @Body JSONObject input);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("profile/v1/allUsers/")
    Call<Response<JSONObject>> getAllUsers(@Query("user_id") Long userId,
                                           @Query("email") String email,
                                           @Query("name") String userName,
                                           @Query("mobile_no") String mobile,
                                           @Query("rest_id") Long restaurantId,
                                           @Query("offset") Integer offset,
                                           @Query("limit") Integer limit
                                           );

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("profile/v1/user/{userId}")
    Call<Response<JSONObject>> getUser(@Path("userId") long userId);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("profile/v1/user/{userId}/restaurants")
    Call<Response<List<Long>>> getRestaurantsForUser(@Path("userId") long userId);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("profile/v1/user/{userId}/restaurants")
    Call<Response<JSONObject>> addRestaurantToUser(@Path("userId") long userId, @Body JSONObject input);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @DELETE("profile/v1/user/{userId}/restaurants/{restaurantId}")
    Call<Response<JSONObject>> removeRestaurantFromUser(@Path("userId") long userId, @Path("restaurantId")
            long restaurantId, @Query("updated_by") String updatedBy);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/authentication/v1/updatePassword/")
    Call<Response<JSONObject>> changePassword(@Body JSONObject input);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/authentication/v1/password-policy/")
    Call<Response<JSONObject>> getPasswordPolicy(@Query("is_operational") Short isOperational);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/authentication/v1/sessions/invalidate/")
    Call<Response<JSONObject>> logoutUser(@Body JSONObject input);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @POST("/authentication/v1/enforce-password/")
    Call<Response<JSONObject>> enforcePolicy(@Body JSONObject input);

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/v1/roles")
    Call<Response<JSONObject>> getRoles();

    @Headers({"Content-Type: application/json", "charset: utf-8"})
    @GET("/v1/permissions")
    Call<Response<JSONObject>> getPermissions();
}
