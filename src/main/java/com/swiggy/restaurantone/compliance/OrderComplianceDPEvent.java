package com.swiggy.restaurantone.compliance;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class OrderComplianceDPEvent implements Serializable {
    private Long requestId;
    private Long orderId;
    private Boolean uploadSuccess;
    private String occurredAt;
}
