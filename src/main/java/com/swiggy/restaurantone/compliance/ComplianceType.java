package com.swiggy.restaurantone.compliance;

public enum ComplianceType {
    OrderCompliance,
    RestaurantComplianceStatus
}
