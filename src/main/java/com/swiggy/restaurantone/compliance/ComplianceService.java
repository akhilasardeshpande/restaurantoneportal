package com.swiggy.restaurantone.compliance;

import com.swiggy.restaurantone.data.businessEntity.User;

import java.io.IOException;
import java.util.List;

public interface ComplianceService {
    OrderComplianceResponse createOrderCompliance(final OrderComplianceRequest request)
            throws IOException, ComplianceServiceException;

    RestaurantComplianceStatusResponse createRestaurantComplianceStatus(
            final RestaurantComplianceStatusRequest request) throws IOException, ComplianceServiceException;

    void reportProcessingFailure(User user, List<String[]> data, String message, ComplianceType type);

    void sendRestComplianceStatusEventToDP(Long eventId, RestaurantComplianceStatusRequest request, Boolean isSuccess);

    void sendOrderComplianceEventToDP(Long eventId, OrderComplianceRequest request, Boolean isSuccess);

    void sendComplianceUploadEventToDP(Long eventId, String fileURL, String userEmail, Integer noOfRecords, ComplianceType type);
}
