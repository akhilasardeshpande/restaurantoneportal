package com.swiggy.restaurantone.compliance;

public class ComplianceServiceException extends RuntimeException {
    public ComplianceServiceException(String message) {
        super(message);
    }

    public ComplianceServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
