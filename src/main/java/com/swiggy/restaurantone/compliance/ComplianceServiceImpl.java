package com.swiggy.restaurantone.compliance;

import com.amazonaws.HttpMethod;
import com.opencsv.CSVWriter;
import com.swiggy.backlit.batteries.prometheus.Instrumentation.InstrumentMetrics;
import com.swiggy.commons.Json;
import com.swiggy.dp.client.DPClient;
import com.swiggy.restaurantone.communication.notification.NotificationsService;
import com.swiggy.restaurantone.communication.notification.NotificationsServiceException;
import com.swiggy.restaurantone.data.businessEntity.User;
import com.swiggy.restaurantone.helper.ClientHelper;
import com.swiggy.restaurantone.service.AmazonS3ClientService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class ComplianceServiceImpl implements ComplianceService {

    private static final int STATUS_CODE_SUCCESS = 1;
    private static final String COMPLIANCE_PROCESSING_FAILURE_COMM_EVENT = "compliance_processing_failure";
    private static final String CONTENT_TYPE_TEXT_CSV = "text/csv";
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

    @Value("${igcc.baseurl}")
    private String igccBaseUrl;

    @Autowired
    private AmazonS3ClientService amazonS3ClientService;

    @Autowired
    private NotificationsService notificationsService;

    @Autowired
    private DPClient dpClient;

    @Retryable(
            value = {IOException.class},
            backoff = @Backoff(delay = 500, random = true, multiplier = 1.0)
    )
    @Override
    public OrderComplianceResponse createOrderCompliance(OrderComplianceRequest request) throws IOException,
            ComplianceServiceException {
        OrderComplianceResponse responseData;
        Response<OrderComplianceResponse> response = ClientHelper
                .igccApiService(igccBaseUrl).createOrderCompliance(request).execute();
        if (response == null || !response.isSuccessful() || response.body() == null) {
            log.error("Failed response from IGCC service for order compliance: {}, response: {}", request, response);
            throw new ComplianceServiceException("Failed response from IGCC service for order compliance");
        }
        if (response.body().getStatusCode() != STATUS_CODE_SUCCESS) {
            log.error("Error processing the order compliance request from IGCC service. request: {}, response: {}",
                    request, response.body().toString());
            throw new ComplianceServiceException(response.body().getStatusMessage());
        }
        responseData = response.body();
        return responseData;
    }

    @Recover
    private OrderComplianceResponse createOrderCompliance(final IOException e, OrderComplianceRequest request)
            throws ComplianceServiceException {
        log.error("Error calling IGCC service for order compliance, retries exhausted. Request: {}, error: {}",
                request, e);
        throw new ComplianceServiceException("Error calling IGCC service for order compliance, retries exhausted");
    }

    @Recover
    private OrderComplianceResponse createOrderCompliance(final ComplianceServiceException e, OrderComplianceRequest request)
            throws ComplianceServiceException {
        throw new ComplianceServiceException(e.getMessage());
    }

    @Retryable(
            value = {IOException.class},
            backoff = @Backoff(delay = 500, random = true, multiplier = 1.0)
    )
    @Override
    public RestaurantComplianceStatusResponse createRestaurantComplianceStatus(RestaurantComplianceStatusRequest
                                                                                       request) throws IOException {
        RestaurantComplianceStatusResponse responseData;
        Response<RestaurantComplianceStatusResponse> response = ClientHelper
                .igccApiService(igccBaseUrl).createRestaurantComplianceStatus(request).execute();
        if (response == null || !response.isSuccessful() || response.body() == null) {
            log.error("Failed response from IGCC service for restaurant compliance request: {}, response: {}", request, response);
            throw new ComplianceServiceException("Failed response from IGCC service for restaurant compliance");
        }
        if (response.body().getStatusCode() != STATUS_CODE_SUCCESS) {
            log.error("Error processing the restaurant compliance status request from IGCC service. request: {}, response: {}",
                    request, response.body().toString());
            throw new ComplianceServiceException(response.body().getStatusMessage());
        }
        responseData = response.body();
        return responseData;
    }

    @Recover
    private RestaurantComplianceStatusResponse createRestaurantComplianceStatus(
            final Throwable e, RestaurantComplianceStatusRequest request) throws ComplianceServiceException {
        log.error("Error calling IGCC service for restaurant compliance request, retries exhausted. Request: {}, " +
                "error: {}", request, e);
        throw new ComplianceServiceException("Error calling IGCC service, retries exhausted");
    }

    @Recover
    private RestaurantComplianceStatusResponse createRestaurantComplianceStatus(final ComplianceServiceException e,
                                                                                RestaurantComplianceStatusRequest request)
            throws ComplianceServiceException {
        throw new ComplianceServiceException(e.getMessage());
    }

    @Override
    @InstrumentMetrics
    public void reportProcessingFailure(User user, List<String[]> data, String message, ComplianceType complianceType) {
        String dir = "/tmp/";
        String filenamePrefix = "compliance-failure-";
        String filenameExt = ".csv";
        String time = sdf.format(new Timestamp(System.currentTimeMillis()));
        String filename = dir + filenamePrefix + time + filenameExt;
        String uploadFileName = filenamePrefix + time + filenameExt;
        String preSignedUrl = null;
        if (CollectionUtils.isNotEmpty(data)) {
            try {
                CSVWriter writer = new CSVWriter(new FileWriter(filename));
                writer.writeAll(data);
                writer.close();
                File file = new File(filename);
                amazonS3ClientService.Upload(uploadFileName, file, CONTENT_TYPE_TEXT_CSV);
                preSignedUrl = amazonS3ClientService.getPreSignedUrl(uploadFileName, null,
                        HttpMethod.GET);
            } catch (IOException ex) {
                log.error("Error writing compliance processing failure to csv file: {}", filename);
            }
        }
        try {
            Map<String, Object> ctx = new HashMap<>();
            ctx.put("subject", complianceType.name() + " : " + message);
            List<String> attachments = new ArrayList<>();
            if (preSignedUrl != null) {
                attachments.add(preSignedUrl);
            }
            notificationsService.sendEmail(COMPLIANCE_PROCESSING_FAILURE_COMM_EVENT, user.email, attachments, ctx);
        } catch (NotificationsServiceException e) {
            log.error("Error sending compliance processing failure report: {}", filename);
        }
    }

    public void sendRestComplianceStatusEventToDP(Long eventId, RestaurantComplianceStatusRequest request, Boolean isSuccess) {
        if (request != null) {
            RestaurantComplianceStatusDPEvent dpEvent = RestaurantComplianceStatusDPEvent.builder()
                    .requestId(eventId)
                    .restaurantId(request.getRestaurantId())
                    .instanceNumber(request.getInstanceNumber())
                    .uploadSuccess(isSuccess)
                    .occurredAt(String.valueOf(Instant.now().getEpochSecond()))
                    .build();
            try {
                dpClient.send(dpEvent);
                log.info("Success sending restaurant_compliance_status_dp_event: {}", Json.serialize(dpEvent));
            } catch (IOException | IllegalArgumentException e) {
                log.error("Error while sending DP event: {}, error: {}", dpEvent, e.getMessage());
            }
        }
    }

    public void sendOrderComplianceEventToDP(Long eventId, OrderComplianceRequest request, Boolean isSuccess) {
        if (request != null) {
            OrderComplianceDPEvent dpEvent = OrderComplianceDPEvent.builder()
                    .requestId(eventId)
                    .orderId(request.getOrderId())
                    .uploadSuccess(isSuccess)
                    .occurredAt(String.valueOf(Instant.now().getEpochSecond()))
                    .build();
            try {
                dpClient.send(dpEvent);
                log.info("Success sending order_compliance_dp_event: {}", Json.serialize(dpEvent));
            } catch (IOException | IllegalArgumentException e) {
                log.error("Error while sending DP event: {}, error: {}", dpEvent, e.getMessage());
            }
        }
    }

    @Override
    public void sendComplianceUploadEventToDP(Long eventId, String fileURL, String userEmail, Integer noOfRecords,
                                              ComplianceType type) {
        ComplianceUploadDPEvent dpEvent = ComplianceUploadDPEvent.builder()
                .requestId(eventId)
                .userEmail(userEmail)
                .fileURL(fileURL)
                .noOfRecords(noOfRecords)
                .uploadType(type.name())
                .occurredAt(String.valueOf(Instant.now().getEpochSecond()))
                .build();
        try {
            dpClient.send(dpEvent);
            log.info("Success sending compliance_upload_dp_event: {}", Json.serialize(dpEvent));
        } catch (IOException | IllegalArgumentException e) {
            log.error("Error while sending DP event: {}, error: {}", dpEvent, e.getMessage());
        }
    }
}
