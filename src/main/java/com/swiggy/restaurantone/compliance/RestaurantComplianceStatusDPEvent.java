package com.swiggy.restaurantone.compliance;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class RestaurantComplianceStatusDPEvent implements Serializable {
    private Long requestId;
    private Long restaurantId;
    private Integer instanceNumber;
    private Boolean uploadSuccess;
    private String occurredAt;
}
