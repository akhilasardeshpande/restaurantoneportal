package com.swiggy.restaurantone.compliance;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class ComplianceUploadDPEvent implements Serializable {
    private Long requestId;
    private String fileURL;
    private String userEmail;
    private Integer noOfRecords;
    private String uploadType;
    private String occurredAt;
}
