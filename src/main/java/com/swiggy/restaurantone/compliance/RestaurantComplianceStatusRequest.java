package com.swiggy.restaurantone.compliance;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
public class RestaurantComplianceStatusRequest {
    @NonNull
    private String type;
    @NonNull
    private Long restaurantId;
    @NonNull
    private Integer instanceNumber;
}
