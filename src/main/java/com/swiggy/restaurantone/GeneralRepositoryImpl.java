package com.swiggy.restaurantone;

import com.swiggy.annotations.audit.GeneralRepository;
import org.springframework.stereotype.Service;

/**
 * Created by balamuneeswaran.s on 2/1/17.
 */
@Service
public class GeneralRepositoryImpl implements GeneralRepository {
    @Override
    public <T> void save(T t) {

    }

    @Override
    public <T> T findById(Class<T> aClass, Object o) {
        return null;
    }
}
