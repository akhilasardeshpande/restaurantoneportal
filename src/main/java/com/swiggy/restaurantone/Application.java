package com.swiggy.restaurantone;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.swiggy.restaurantone.apiClients.LetterboxApiClient;
import com.swiggy.restaurantone.apiClients.RMSApiClient;
import com.swiggy.restaurantone.apiClients.SelfServeApiClient;
import com.swiggy.restaurantone.filter.SessionFilter;
import com.swiggy.restaurantone.helper.ClientHelper;
import com.swiggy.restaurantone.service.SRSAPIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;

/**
 * Created by balamuneeswaran.s on 1/3/17.
 */
@SpringBootApplication(scanBasePackages = {"com.swiggy"})
@EnableTransactionManagement
@EnableZuulProxy
@Import(value = {
        ApplicationConfig.class,
})
@EnableAspectJAutoProxy
@EnableRetry
public class Application {

    @Value("${baseurl.srs}")
    private String srsBaseUrl;

    @Value("${baseurl.letterbox}")
    private String letterboxBaseUrl;

    @Value("${amazon.aws.accesskey}")
    private String accessKey;

    @Value("${amazon.aws.secretkey}")
    private String secretKey;
    
    @Autowired
    Environment environment;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public SessionFilter sessionFilter() {
        return new SessionFilter();
    }

    @Bean
    public CorsFilter corsFilter() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.setExposedHeaders(Arrays.asList("sessionId", "userId"));
        config.addAllowedMethod("OPTIONS");
        config.addAllowedMethod("HEAD");
        config.addAllowedMethod("GET");
        config.addAllowedMethod("PUT");
        config.addAllowedMethod("POST");
        config.addAllowedMethod("DELETE");
        config.addAllowedMethod("PATCH");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }

    @Bean
    public SRSAPIService srsapiService() {
        return ClientHelper.srsAPIService(srsBaseUrl);
    }
    
    @Bean
    public SelfServeApiClient selfServeApiService() {
        return ClientHelper.selfServeApiService(environment.getProperty("baseurl.self-serve"));
    }
    
    @Bean
    public RMSApiClient rmsApiClient() {
        return ClientHelper.rmsApiClient(environment.getProperty("baseurl.rmsint"));
    }

    @Bean
    public AmazonS3 s3Client() {
        return AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(this.accessKey,
                        this.secretKey))).withRegion(Regions.AP_SOUTHEAST_1)
                .build();
    }

    @Bean
    public LetterboxApiClient letterboxApiClient() {
        return ClientHelper.letterboxApiClient(letterboxBaseUrl);
    }
}
