package com.swiggy.restaurantone.response;

import lombok.*;

import java.util.List;


/**
 * Created by alok.singhal on 11/3/17.
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RestaurantListResponse {
    protected List<Long> restaurantIds;
}
