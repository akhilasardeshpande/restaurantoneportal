package com.swiggy.restaurantone.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PresignedUrlResponse {

    @JsonProperty(value = "statusCode")
    String code;

    @JsonProperty(value = "statusMessage")
    String message;

    @JsonProperty(value = "url")
    private String url;

    @JsonProperty(value = "key")
    private String key;
}
