package com.swiggy.restaurantone.response;

import lombok.*;

import java.util.List;

/**
 * Created by alok.singhal on 9/27/17.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PollResponse {
    protected List<RestaurantBatches> restaurantBatchesList;
}
