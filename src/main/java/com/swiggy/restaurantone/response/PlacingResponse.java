package com.swiggy.restaurantone.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.restaurantone.data.businessEntity.Batch;
import lombok.*;

import java.util.List;
import java.util.Map;

/**
 * Created by alok.singhal on 9/27/17.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class PlacingResponse<T> {

    @JsonProperty(value = "statusCode")
    String code;

    @JsonProperty(value = "statusMessage")
    String message;

    @JsonProperty(value = "data")
    T data;
}
