package com.swiggy.restaurantone.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Created by shahbaz.khalid on 7/12/17.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class SRSResponse<T> {
    @JsonProperty(value = "status")
    Integer status;

    @JsonProperty(value = "code")
    String code;

    @JsonProperty(value = "message")
    String message;

    @JsonProperty(value = "data")
    T data;
}
