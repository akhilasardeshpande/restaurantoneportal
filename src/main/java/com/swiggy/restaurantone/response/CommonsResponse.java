package com.swiggy.restaurantone.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by rajnish.chandra on 2019-05-15
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CommonsResponse<T> {
    @JsonProperty(value = "statusCode")
    String code;
    
    @JsonProperty(value = "statusMessage")
    String message;
    
    @JsonProperty(value = "data")
    T data;
}
