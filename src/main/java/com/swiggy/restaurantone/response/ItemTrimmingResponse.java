package com.swiggy.restaurantone.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ItemTrimmingResponse<T> {

    @JsonProperty(value = "statusCode")
    String code;

    @JsonProperty(value = "statusMessage")
    String message;

    @JsonProperty(value = "data")
    @Valid
    private List<ItemOosData> data;


    @Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ItemOosData implements Serializable {
        private final static long serialVersionUID = 2405935277562085373L;

        @JsonProperty("id")
        public Long id;

        @JsonProperty("uniqueId")
        public Long uniqueId;

        @JsonProperty("item_id")
        public Long itemId;

        @JsonProperty("from_time")
        public String fromTime;

        @JsonProperty("to_time")
        public String toTime;

        @JsonProperty("updated_by")
        public UpdatedAndCreated updatedBy;

        @JsonProperty("created_by")
        public UpdatedAndCreated createdBy;

        @Data
        @JsonInclude(JsonInclude.Include.NON_NULL)
        @NoArgsConstructor
        @AllArgsConstructor
        public static class UpdatedAndCreated implements Serializable {
            private final static long serialVersionUID = 2405935277562085374L;

            @JsonProperty("source")
            public String source;

            @JsonProperty("meta")
            public Meta uniqueId;

            @Data
            @JsonInclude(JsonInclude.Include.NON_NULL)
            @NoArgsConstructor
            @AllArgsConstructor
            public static class Meta implements Serializable {
                private final static long serialVersionUID = 332171587881104737L;

                @JsonProperty("userid")
                public String userId;

                @JsonProperty("reason")
                public String reason;

            }
        }
    }
}
