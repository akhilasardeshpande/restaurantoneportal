package com.swiggy.restaurantone.response;

import com.swiggy.restaurantone.data.businessEntity.Batch;
import lombok.*;

import java.util.List;

/**
 * Created by alok.singhal on 9/27/17.
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RestaurantBatches {
    protected Long restaurantId;
    protected String name;
    protected String phoneNumbers;
    protected List<Batch> batches;
}
