package com.swiggy.restaurantone.response.selfserve;

/**
 * Created by rajnish.chandra on 2019-05-15
 */
public enum EntityType {
    RESTAURANT, ITEM, ADDON, VARIANT, MENU
}

