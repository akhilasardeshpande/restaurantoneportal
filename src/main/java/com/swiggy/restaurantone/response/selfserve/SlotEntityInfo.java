package com.swiggy.restaurantone.response.selfserve;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Created by rajnish.chandra on 2019-05-15
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SlotEntityInfo {
    
    @JsonProperty("entity_type")
    EntityType type;
    
    @JsonProperty("menu_type")
    MenuType menuType;
    
    @JsonProperty("slot_id")
    Long slotId;
    
}
