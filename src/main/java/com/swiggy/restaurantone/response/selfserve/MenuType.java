package com.swiggy.restaurantone.response.selfserve;

/**
 * Created by rajnish.chandra on 2019-05-15
 */
public enum MenuType {
    REGULAR_MENU, POP_MENU
}
