package com.swiggy.restaurantone.vendorconfig.controllers;

import com.swiggy.commons.response.Response;
import com.swiggy.rest.controller.AbstractController;
import com.swiggy.restaurantone.helper.AuthorizationHelper;
import com.swiggy.restaurantone.vendorconfig.pojo.request.fetch.FeatureConfigFetchRequest;
import com.swiggy.restaurantone.vendorconfig.pojo.request.fetch.IGCCConfigFetchRequest;
import com.swiggy.restaurantone.vendorconfig.pojo.request.fetch.RestaurantConfigFetchRequest;
import com.swiggy.restaurantone.vendorconfig.pojo.request.update.VendorConfigUpdateRequest;
import com.swiggy.restaurantone.vendorconfig.service.VendorConfigServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@Slf4j
@RequestMapping(value = "/api/v1/vendorconfig")
public class VendorConfigController extends AbstractController {

    @Autowired
    private AuthorizationHelper authorizationHelper;

    @Autowired
    private VendorConfigServiceImpl vendorConfigService;

    @PostMapping(value = "/rid", produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Response<?> fetchRestaurantConfig(@RequestBody RestaurantConfigFetchRequest restaurantConfigFetchRequest) {
        log.info("fetchRestaurantConfig Api Request {}", restaurantConfigFetchRequest);
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(() -> vendorConfigService.fetchRestaurantConfig(restaurantConfigFetchRequest));
        }
        return this.getFailureResponse("Unauthorized");
    }

    @PostMapping(value = "/fetch/igcc", produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Response<?> fetchIGCCConfiguration(@RequestBody IGCCConfigFetchRequest igccConfigFetchRequest) {
        log.info("fetchIGCCConfig Api Request {}", igccConfigFetchRequest);
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(() -> vendorConfigService.fetchIGCCConfig(igccConfigFetchRequest));
        }
        return this.getFailureResponse("Unauthorized");
    }

    @PostMapping(value = "/feature", produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Response<?> fetchFeatureConfiguration(@RequestBody FeatureConfigFetchRequest featureConfigFetchRequest) {
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(() -> vendorConfigService.fetchFeatureConfig(
                    Arrays.asList(featureConfigFetchRequest.getRestaurantIds())));
        }
        return this.getFailureResponse("Unauthorized");
    }

    @PostMapping(value = "/igcc", produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Response<?> updateIGCCConfiguration(@RequestBody List<VendorConfigUpdateRequest> igccConfigUpdateRequestList) {
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(() -> vendorConfigService.updateIGCCConfig(igccConfigUpdateRequestList));
        }
        return this.getFailureResponse("Unauthorized");
    }

    @PostMapping(value = "/global", produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Response<?> updateGlobalConfiguration(@RequestBody VendorConfigUpdateRequest vendorConfigUpdateRequest) {
        log.info("updateGlobalConfiguration Api Request {}", vendorConfigUpdateRequest);
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(() -> vendorConfigService.updateGlobalConfiguration(vendorConfigUpdateRequest));
        }
        return this.getFailureResponse("Unauthorized");
    }

    @GetMapping(value = "/global", produces = "application/json")
    @ResponseBody
    public Response<?> getAllGlobalConfiguration() {
        log.info("getAllGlobalConfiguration Api Request");
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(() -> vendorConfigService.getAllGlobalConfiguration());
        }
        return this.getFailureResponse("Unauthorized");
    }

    @DeleteMapping(value = "/global/{key}", produces = "application/json")
    @ResponseBody
    public Response<?> deleteGlobalConfiguration(@PathVariable(value = "key") String globalKey) {
        log.info("getAllGlobalConfiguration Api Request");
        if(authorizationHelper.authenticateUserSession()) {
            return this.executeTask(() -> vendorConfigService.deleteGlobalConfiguration(Collections.singletonList(globalKey)));
        }
        return this.getFailureResponse("Unauthorized");
    }

}
