package com.swiggy.restaurantone.vendorconfig;

import com.swiggy.restaurantone.apiClients.ApiClientBuilder;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class VendorConfigClientConfiguration {

    @Value("${hostname.vendorconfig}")
    private String vendorConfigServiceHostName;
    @Value("${timeout.vendorconfig}")
    private String vendorConfigServiceTimeout;

    @Bean
    public VendorConfigServiceClient vendorConfigServiceClient() {
        return ApiClientBuilder.builder().
                baseUrl(vendorConfigServiceHostName).
                loggingInterceptor(new HttpLoggingInterceptor()).
                connectTimeout(Long.parseLong(vendorConfigServiceTimeout), TimeUnit.SECONDS).
                readTimeout(Long.parseLong(vendorConfigServiceTimeout), TimeUnit.SECONDS).
                writeTimeout(Long.parseLong(vendorConfigServiceTimeout), TimeUnit.SECONDS).
                build(VendorConfigServiceClient.class);
    }

}
