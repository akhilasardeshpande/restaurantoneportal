package com.swiggy.restaurantone.vendorconfig.service;

import com.swiggy.restaurantone.vendorconfig.pojo.request.fetch.IGCCConfigFetchRequest;
import com.swiggy.restaurantone.vendorconfig.pojo.request.fetch.RestaurantConfigFetchRequest;
import com.swiggy.restaurantone.vendorconfig.pojo.request.update.VendorConfigUpdateRequest;
import com.swiggy.restaurantone.vendorconfig.pojo.response.fetch.VendorConfigFetchGlobalResponse;
import com.swiggy.restaurantone.vendorconfig.pojo.response.fetch.VendorConfigFetchResponseItem;
import com.swiggy.restaurantone.vendorconfig.pojo.response.update.VendorConfigUpdateResponse;
import com.swiggy.restaurantone.vendorconfig.pojo.response.upload.VendorConfigBulkUploadResponse;

import java.util.List;

public interface VendorConfigService {
    VendorConfigFetchResponseItem fetchRestaurantConfig(String rid);

    List<VendorConfigFetchResponseItem> fetchRestaurantConfig(RestaurantConfigFetchRequest restaurantConfigFetchRequest);

    VendorConfigFetchResponseItem fetchIGCCConfig(String rid);

    List<VendorConfigFetchResponseItem> fetchIGCCConfig(IGCCConfigFetchRequest igccConfigFetchRequest);

    VendorConfigFetchGlobalResponse getAllGlobalConfiguration();

    VendorConfigUpdateResponse updateGlobalConfiguration(VendorConfigUpdateRequest vendorConfigUpdateRequest);

    VendorConfigUpdateResponse deleteGlobalConfiguration(List<String> globalKeys);

    VendorConfigBulkUploadResponse uploadRestaurantConfig(List<VendorConfigUpdateRequest> list);

    List<VendorConfigFetchGlobalResponse> fetchFeatureConfig(List<String> rids);

    VendorConfigUpdateResponse updateConfig(String apiName, String type, List<VendorConfigUpdateRequest> updateList);
}
