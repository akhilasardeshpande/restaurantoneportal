package com.swiggy.restaurantone.vendorconfig.service;

import com.swiggy.commons.Json;
import com.swiggy.restaurantone.vendorconfig.VendorConfigServiceClient;
import com.swiggy.restaurantone.vendorconfig.pojo.request.fetch.IGCCConfigFetchRequest;
import com.swiggy.restaurantone.vendorconfig.pojo.request.fetch.RestaurantConfigFetchRequest;
import com.swiggy.restaurantone.vendorconfig.pojo.request.update.VendorConfigUpdateRequest;
import com.swiggy.restaurantone.vendorconfig.pojo.response.fetch.VendorConfigFetchGlobalResponse;
import com.swiggy.restaurantone.vendorconfig.pojo.response.fetch.VendorConfigFetchResponseItem;
import com.swiggy.restaurantone.vendorconfig.pojo.response.update.VendorConfigUpdateResponse;
import com.swiggy.restaurantone.vendorconfig.pojo.response.upload.VendorConfigBulkUploadResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.swiggy.restaurantone.Constant.VendorConfigConstants.*;

@Service
@Slf4j
public class VendorConfigServiceImpl implements VendorConfigService {

    @Autowired
    private VendorConfigServiceClient vendorConfigServiceClient;

    @Override
    public VendorConfigFetchResponseItem fetchRestaurantConfig(String rid) {
        return fetchConfig("fetchRestaurantConfig", RID, rid);
    }

    @Override
    public List<VendorConfigFetchResponseItem> fetchRestaurantConfig(
            RestaurantConfigFetchRequest restaurantConfigFetchRequest) {
        return Arrays.asList(restaurantConfigFetchRequest.getRestaurantIds()).stream()
                .map(this::fetchRestaurantConfig).collect(Collectors.toList());
    }

    @Override
    public VendorConfigFetchResponseItem fetchIGCCConfig(String rid) {
        return fetchConfig("fetchIGCCConfig", IGCC, rid);
    }

    @Override
    public List<VendorConfigFetchResponseItem> fetchIGCCConfig(
            IGCCConfigFetchRequest igccConfigRequest) {
        return Arrays.asList(igccConfigRequest.getRestaurantIds()).stream()
                .map(this::fetchIGCCConfig).collect(Collectors.toList());
    }


    public VendorConfigUpdateResponse updateIGCCConfig(List<VendorConfigUpdateRequest> igccConfigUpdateRequestList) {
        return updateConfig("updateIGCCConfig", IGCC, igccConfigUpdateRequestList);
    }

    @Override
    public VendorConfigFetchGlobalResponse getAllGlobalConfiguration() {
        return fetchAllConfigs("getAllGlobalConfiguration", GLOBAL, null);
    }

    @Override
    public VendorConfigUpdateResponse updateGlobalConfiguration(VendorConfigUpdateRequest vendorConfigUpdateRequest) {
        return updateConfig("updateGlobalConfiguration", GLOBAL, Collections.singletonList(
                VendorConfigUpdateRequest.builder()
                        .key(vendorConfigUpdateRequest.getKey())
                        .value(vendorConfigUpdateRequest.getValue())
                        .build()
        ));
    }

    public VendorConfigUpdateResponse deleteGlobalConfiguration(
            List<String> globalKeys) {
        try {
            log.info("deleteGlobalConfiguration API has been called");
            Response<VendorConfigUpdateResponse> response =
                    vendorConfigServiceClient.deleteConfigs(GLOBAL, globalKeys).execute();
            if (!response.isSuccessful()) {
                log.error("deleteGlobalConfiguration API has failed code {} body {}",
                        response.code(), response.errorBody().string());
                return Json.deserialize(response.errorBody().string(),
                        VendorConfigUpdateResponse.class);
            }
            return response.body();
        } catch (IOException ex) {
            log.error("deleteGlobalConfiguration API has failed exception {}", ex.getMessage());
            return null;
        }
    }

    @Override
    public VendorConfigBulkUploadResponse uploadRestaurantConfig(List<VendorConfigUpdateRequest> list) {
        VendorConfigBulkUploadResponse vendorConfigBulkUploadResponse =
                VendorConfigBulkUploadResponse.builder().message(SUCCESS).build();
        try {
            log.info("uploadRestaurantConfig API has been called");
            Response<VendorConfigUpdateResponse> response =
                    vendorConfigServiceClient.updateConfigs(RID, list).execute();
            if (!response.isSuccessful()) {
                log.error("fetchRestaurantConfig API has failed code {} body {}",
                        response.code(), response.errorBody().string());
                vendorConfigBulkUploadResponse.setMessage(response.errorBody().string());
                return vendorConfigBulkUploadResponse;
            }
            return vendorConfigBulkUploadResponse;
        } catch (IOException ex) {
            log.error("getAllGlobalConfiguration API has failed exception {}", ex.getMessage());
            vendorConfigBulkUploadResponse.setMessage(ex.getMessage());
            return vendorConfigBulkUploadResponse;
        }
    }

    private Map<String, List<String>> getAllFeatureFlags() {
        Map<String, List<String>> schemaMap = new HashMap<>();
        List<String> projectNames = getList(PROJECT_NAMES);
        for (String projectName : projectNames) {
            schemaMap.put(projectName, getList(projectName));
        }
        return schemaMap;
    }

    private List<String> getList(String key) {
        try {
            VendorConfigFetchResponseItem vendorConfigFetchResponseItem =
                    fetchConfig("fetchMetaFeatureConfig", META_FEATURE, key);
            if (Objects.isNull(vendorConfigFetchResponseItem)) {
                return Collections.emptyList();
            }
            return (List<String>) vendorConfigFetchResponseItem.getValue();
        } catch (Exception ex) {
            log.error("getList API has failed exception {}", ex.getMessage());
            return Collections.emptyList();
        }
    }

    private String computeQueryString(List<String> rids, List<String> features) {
        if (rids.size() == 0 || features.size() == 0) {
            return "";
        }
        StringBuilder queryBuilder = new StringBuilder();
        for (int ridIndex = 0; ridIndex < rids.size(); ridIndex++) {
            String rid = rids.get(ridIndex);
            for (String feature : features) {
                queryBuilder.append(String.format(",%s:%s", feature, rid));
            }
        }
        return queryBuilder.substring(1);
    }

    @Override
    public List<VendorConfigFetchGlobalResponse> fetchFeatureConfig(List<String> rids) {
        try {
            List<VendorConfigFetchGlobalResponse> result = new ArrayList<>();
            Map<String, List<String>> schemaMap = getAllFeatureFlags();
            for (String projectName : schemaMap.keySet()) {
                result.add(fetchAllConfigs("fetchFeatureConfig", projectName,
                        computeQueryString(rids, schemaMap.get(projectName))));
            }
            return result;
        } catch (Exception ex) {
            log.error("fetchFeatureConfig API has failed exception {} for rid {}", ex.getMessage(), rids);
            return null;
        }
    }

    @Override
    public VendorConfigUpdateResponse updateConfig(String apiName, String type, List<VendorConfigUpdateRequest> updateList) {
        try {
            log.info("{} API has been called", apiName);
            Response<VendorConfigUpdateResponse> response =
                    vendorConfigServiceClient.updateConfigs(type, updateList).execute();
            if (!response.isSuccessful()) {
                log.error("{} API has failed code {} body {}", apiName,
                        response.code(), response.errorBody().string());
                return Json.deserialize(response.errorBody().string(),
                        VendorConfigUpdateResponse.class);
            }
            return response.body();
        } catch (IOException ex) {
            log.error("{} API has failed exception {}", apiName, ex.getMessage());
            return null;
        }
    }

    private VendorConfigFetchResponseItem fetchConfig(String apiName, String type, String key) {
        try {
            log.info("{} API has been called", apiName);
            Response<VendorConfigFetchResponseItem> response =
                    vendorConfigServiceClient.getConfig(type, key).execute();
            if (!response.isSuccessful()) {
                log.error("{} API has failed code {} body {}", apiName,
                        response.code(), response.errorBody().string());
                return Json.deserialize(response.errorBody().string(),
                        VendorConfigFetchResponseItem.class);
            }
            return response.body();
        } catch (IOException ex) {
            log.error("{} API has failed exception {} for type {} key {}",
                    apiName, ex.getMessage(), type, key);
            return null;
        }
    }

    public VendorConfigFetchGlobalResponse fetchAllConfigs(String apiName, String type, String query) {
        try {
            log.info("{} API has been called", apiName);
            Response<VendorConfigFetchGlobalResponse> response =
                    vendorConfigServiceClient.getAllConfig(type, query).execute();
            if (!response.isSuccessful()) {
                log.error("{} API has failed code {} body {}", apiName,
                        response.code(), response.errorBody().string());
                return Json.deserialize(response.errorBody().string(),
                        VendorConfigFetchGlobalResponse.class);
            }
            return response.body();
        } catch (IOException ex) {
            log.error("{} API has failed exception {} for type {} query {}",
                    apiName, ex.getMessage(), type, query);
            return null;
        }
    }
}
