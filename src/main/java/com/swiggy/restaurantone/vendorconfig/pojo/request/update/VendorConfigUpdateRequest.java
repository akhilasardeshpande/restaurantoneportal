package com.swiggy.restaurantone.vendorconfig.pojo.request.update;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class VendorConfigUpdateRequest {
    private String key;
    private String config;
    private Object value;
}
