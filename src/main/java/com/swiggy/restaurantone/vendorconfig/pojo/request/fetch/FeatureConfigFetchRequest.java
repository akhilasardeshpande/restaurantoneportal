package com.swiggy.restaurantone.vendorconfig.pojo.request.fetch;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class FeatureConfigFetchRequest {
    @JsonProperty("rid")
    private String[] restaurantIds;
}
