package com.swiggy.restaurantone.vendorconfig.pojo.request.upload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class VendorConfigBulkUploadRequest {
    private String url;
}
