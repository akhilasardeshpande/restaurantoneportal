package com.swiggy.restaurantone.vendorconfig.pojo.response.upload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class VendorConfigBulkUploadResponse {

    private String message;
}
