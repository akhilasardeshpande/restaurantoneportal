package com.swiggy.restaurantone.vendorconfig.pojo.request.fetch;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class RestaurantConfigFetchRequest {

    @JsonProperty("rid")
    private String[] restaurantIds;
}
