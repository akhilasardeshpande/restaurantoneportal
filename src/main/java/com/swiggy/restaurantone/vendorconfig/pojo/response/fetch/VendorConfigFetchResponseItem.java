package com.swiggy.restaurantone.vendorconfig.pojo.response.fetch;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class VendorConfigFetchResponseItem {

    @JsonProperty("PK")
    private String primaryKey;

    @JsonProperty("SK")
    private String typeKey;

    @JsonProperty("value")
    private Object value;

    @JsonProperty("message")
    private String message;

}
