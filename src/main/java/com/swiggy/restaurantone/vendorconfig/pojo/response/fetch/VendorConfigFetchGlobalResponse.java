package com.swiggy.restaurantone.vendorconfig.pojo.response.fetch;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class VendorConfigFetchGlobalResponse {

    @JsonProperty("items")
    private List<VendorConfigFetchResponseItem> itemList;

    @JsonProperty("count")
    private int itemCount;

    @JsonProperty("scannedCount")
    private int scannedCount;

    @JsonProperty("message")
    private String message;

    @JsonProperty("lastEvaluatedKey")
    private String lastEvaluatedKey;

}
