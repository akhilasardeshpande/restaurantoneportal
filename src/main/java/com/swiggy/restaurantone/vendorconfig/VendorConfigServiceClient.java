package com.swiggy.restaurantone.vendorconfig;

import com.swiggy.restaurantone.vendorconfig.pojo.request.update.VendorConfigUpdateRequest;
import com.swiggy.restaurantone.vendorconfig.pojo.response.fetch.VendorConfigFetchGlobalResponse;
import com.swiggy.restaurantone.vendorconfig.pojo.response.fetch.VendorConfigFetchResponseItem;
import com.swiggy.restaurantone.vendorconfig.pojo.response.update.VendorConfigUpdateResponse;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface VendorConfigServiceClient {

    @GET("/config/{type}")
    Call<VendorConfigFetchResponseItem> getConfig(@Path("type") String type, @Query("i") String query);

    @GET("/config/{type}/all")
    Call<VendorConfigFetchGlobalResponse> getAllConfig(@Path("type") String type, @Query("i") String query);

    @POST("/config/{type}")
    Call<VendorConfigUpdateResponse> updateConfigs(@Path("type") String type,
                                                      @Body List<VendorConfigUpdateRequest> updateBody);

    @HTTP(method = "DELETE", path ="/config/{type}", hasBody = true)
    Call<VendorConfigUpdateResponse> deleteConfigs(@Path("type") String type,
                                                        @Body List<String> deleteList);
}
