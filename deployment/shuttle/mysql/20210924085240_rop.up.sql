USE rop;

SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `city_aud`;
CREATE TABLE `city_aud` (
  `id` bigint(20) NOT NULL,
  `rev` int(11) NOT NULL,
  `revtype` tinyint(4) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`rev`),
  KEY `fk_rev_city` (`rev`),
  CONSTRAINT `fk_rev_city` FOREIGN KEY (`rev`) REFERENCES `tbl_revinfo` (`id`)
);

DROP TABLE IF EXISTS `DATABASECHANGELOG`;
CREATE TABLE `DATABASECHANGELOG` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
);

DROP TABLE IF EXISTS `DATABASECHANGELOGLOCK`;
CREATE TABLE `DATABASECHANGELOGLOCK` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
);

DROP TABLE IF EXISTS `partner_disable_holiday_slot_info`;
CREATE TABLE `partner_disable_holiday_slot_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `error_code` int(11) DEFAULT NULL,
  `from_time` datetime DEFAULT NULL,
  `holiday_slot_id` bigint(20) DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `restaurant_id` bigint(20) DEFAULT NULL,
  `retry` int(11) DEFAULT NULL,
  `runinfo_id` bigint(20) DEFAULT NULL,
  `to_time` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_restaurant_id_run_id` (`restaurant_id`,`runinfo_id`),
  KEY `idx_updated_at` (`updated_at`)
) DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `partner_disable_runinfo`;
CREATE TABLE `partner_disable_runinfo` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `from_time` datetime DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `partner_id` int(11) DEFAULT NULL,
  `to_time` datetime DEFAULT NULL,
  `disposition_id` int(11) DEFAULT NULL,
  `total_restaurants` int(11) DEFAULT NULL,
  `active_restaurants` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `reason` varchar(1000) DEFAULT NULL,
  `status_code` tinyint(4) DEFAULT NULL,
  `status_message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_updated_at` (`updated_at`)
) DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `permission` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `permission_aud`;
CREATE TABLE `permission_aud` (
  `id` bigint(20) NOT NULL,
  `rev` int(11) NOT NULL,
  `revtype` tinyint(4) DEFAULT NULL,
  `permission` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`rev`),
  KEY `fk_rev_permission` (`rev`),
  CONSTRAINT `fk_rev_permission` FOREIGN KEY (`rev`) REFERENCES `tbl_revinfo` (`id`)
);

DROP TABLE IF EXISTS `restaurant_event`;
CREATE TABLE `restaurant_event` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL DEFAULT '',
  `description` text,
  `created_by` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT '',
  `undo_allowed` tinyint(1) DEFAULT NULL,
  `action` varchar(255) NOT NULL DEFAULT '',
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `meta` mediumtext,
  PRIMARY KEY (`id`),
  KEY `type_timestamp_index` (`type`,`created_at`)
) DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `restaurant_user_map`;
CREATE TABLE `restaurant_user_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `restaurant_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rest_x` (`restaurant_id`),
  KEY `user_x` (`user_id`)
);

DROP TABLE IF EXISTS `restaurant_user_map_aud`;
CREATE TABLE `restaurant_user_map_aud` (
  `id` bigint(20) NOT NULL,
  `rev` int(11) NOT NULL,
  `revtype` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `restaurant_id` bigint(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`rev`),
  KEY `fk_rev_restaurant_user` (`rev`),
  CONSTRAINT `fk_rev_restaurant_user` FOREIGN KEY (`rev`) REFERENCES `tbl_revinfo` (`id`)
);

DROP TABLE IF EXISTS `rms_users_audit`;
CREATE TABLE `rms_users_audit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activation_date` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `deactivation_date` datetime DEFAULT NULL,
  `is_active` smallint(6) DEFAULT NULL,
  `is_editable` smallint(6) DEFAULT NULL,
  `is_primary` smallint(6) DEFAULT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `registration_complete` smallint(6) DEFAULT NULL,
  `rms_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL,
  `level` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `role_aud`;
CREATE TABLE `role_aud` (
  `id` bigint(20) NOT NULL,
  `rev` int(11) NOT NULL,
  `revtype` tinyint(4) DEFAULT NULL,
  `level` bigint(20) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`rev`),
  KEY `fk_rev_role` (`rev`),
  CONSTRAINT `fk_rev_role` FOREIGN KEY (`rev`) REFERENCES `tbl_revinfo` (`id`)
);

DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission` (
  `role_id` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL,
  KEY `FKf8yllw1ecvwqy3ehyxawqa1qp` (`permission_id`),
  KEY `FKa6jx8n8xkesmjmv6jqug6bg68` (`role_id`),
  CONSTRAINT `FKa6jx8n8xkesmjmv6jqug6bg68` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FKf8yllw1ecvwqy3ehyxawqa1qp` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`)
);

DROP TABLE IF EXISTS `role_permission_aud`;
CREATE TABLE `role_permission_aud` (
  `rev` int(11) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL,
  `revtype` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`permission_id`,`role_id`,`rev`),
  KEY `fk_rev_role_permission` (`rev`),
  CONSTRAINT `fk_rev_role_permission` FOREIGN KEY (`rev`) REFERENCES `tbl_revinfo` (`id`)
);

DROP TABLE IF EXISTS `tbl_revinfo`;
CREATE TABLE `tbl_revinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `team_aud`;
CREATE TABLE `team_aud` (
  `id` bigint(20) NOT NULL,
  `rev` int(11) NOT NULL,
  `revtype` tinyint(4) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`rev`),
  KEY `fk_rev_team` (`rev`),
  CONSTRAINT `fk_rev_team` FOREIGN KEY (`rev`) REFERENCES `tbl_revinfo` (`id`)
);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `properties` varchar(255) DEFAULT NULL,
  `role` int(11) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `city` bigint(20) DEFAULT NULL,
  `doj` datetime DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `team_id` bigint(20) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
);

DROP TABLE IF EXISTS `user_aud`;
CREATE TABLE `user_aud` (
  `id` bigint(20) NOT NULL,
  `rev` int(11) NOT NULL,
  `revtype` tinyint(4) DEFAULT NULL,
  `city` bigint(20) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `doj` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `properties` varchar(255) DEFAULT NULL,
  `role` bigint(20) DEFAULT NULL,
  `team_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`rev`),
  KEY `fk_rev_user` (`rev`),
  CONSTRAINT `fk_rev_user` FOREIGN KEY (`rev`) REFERENCES `tbl_revinfo` (`id`)
);

DROP TABLE IF EXISTS `user_permission`;
CREATE TABLE `user_permission` (
  `user_id` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL,
  KEY `FKbklmo9kchans5u3e4va0ouo1s` (`permission_id`),
  KEY `FK7c2x74rinbtf33lhdcyob20sh` (`user_id`),
  CONSTRAINT `FK7c2x74rinbtf33lhdcyob20sh` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKbklmo9kchans5u3e4va0ouo1s` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`)
);

DROP TABLE IF EXISTS `user_permission_aud`;
CREATE TABLE `user_permission_aud` (
  `rev` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL,
  `revtype` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`rev`,`user_id`,`permission_id`),
  CONSTRAINT `fk_rev_user_perm` FOREIGN KEY (`rev`) REFERENCES `tbl_revinfo` (`id`)
);

DROP TABLE IF EXISTS `user_relationship`;
CREATE TABLE `user_relationship` (
  `child_user_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `is_direct_parent` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`child_user_id`,`user_id`)
);

DROP TABLE IF EXISTS `user_relationship_aud`;
CREATE TABLE `user_relationship_aud` (
  `child_user_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `rev` int(11) NOT NULL,
  `revtype` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `is_direct_parent` bit(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`child_user_id`,`user_id`,`rev`),
  KEY `fk_rev_user_relation` (`rev`),
  CONSTRAINT `fk_rev_user_relation` FOREIGN KEY (`rev`) REFERENCES `tbl_revinfo` (`id`)
);

SET FOREIGN_KEY_CHECKS=1;
